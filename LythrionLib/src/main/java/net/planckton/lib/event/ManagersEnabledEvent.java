package net.planckton.lib.event;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.planckton.lib.manager.Manager;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class ManagersEnabledEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	@Getter Collection<Manager> managers;
}
