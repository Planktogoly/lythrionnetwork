package net.planckton.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;

public class LythrionPlayerQuitEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	@Getter @NonNull private LythrionPlayer lythrionPlayer;
	
	public LythrionPlayerQuitEvent(LythrionPlayer lythrionPlayer) {
		this.lythrionPlayer = lythrionPlayer;
	}

}
