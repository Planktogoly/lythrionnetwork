package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.planckton.lib.playmode.PlayMode;
import net.planckton.lib.rank.Rank;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class DefaultPlayModeRequestEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	@Getter @Setter @NonNull private Class<? extends PlayMode> playMode;
	@Getter @NonNull private Player player; 
	@Getter @NonNull private Rank rank;
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
