package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayMode;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class PlayerPlayModeSwitchEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	@Getter private LythrionPlayer player;
	@Getter private PlayMode previousMode;
	@Getter private PlayMode newMode;
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
