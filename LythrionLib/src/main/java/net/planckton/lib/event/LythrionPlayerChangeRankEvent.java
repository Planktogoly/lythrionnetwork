package net.planckton.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.rank.Rank;

public class LythrionPlayerChangeRankEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	@Getter @NonNull private LythrionPlayer lythrionPlayer;
	@Getter @NonNull private Rank rank; 
	
	public LythrionPlayerChangeRankEvent(LythrionPlayer lythrionPlayer, Rank rank) {
		this.lythrionPlayer = lythrionPlayer;
		this.rank = rank;
	}

}
