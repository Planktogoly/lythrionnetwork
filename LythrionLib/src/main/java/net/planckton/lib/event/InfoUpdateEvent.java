package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class InfoUpdateEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	@Getter @NonNull private String server;
	@Getter @NonNull private String key;
	@Getter @NonNull private String value;
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
