package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.manager.Manager;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class ManagerEnableEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	@Getter @NonNull private Manager manager;
	
	public Class<? extends Manager> getType() {
		return manager.getClass();
	}

	public boolean isType(Class<? extends Manager> klass) {
		if(manager.getClass() == klass) return true;
		if(klass.isAssignableFrom(manager.getClass())) return true;
		if(manager.getClass().isAssignableFrom(klass)) return true;
		
		return false;
	}
}
