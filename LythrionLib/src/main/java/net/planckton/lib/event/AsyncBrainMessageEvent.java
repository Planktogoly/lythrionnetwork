package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

@AllArgsConstructor
public class AsyncBrainMessageEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	@Getter @NonNull private String from;
	@Getter @NonNull private String to;
	@Getter @NonNull private String channel;
	@Getter @NonNull private byte[] data;
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public ByteArrayDataInput getInput() {
		return ByteStreams.newDataInput(data);
	}
}
