package net.planckton.lib.event;

import lombok.Getter;
import lombok.Setter;
import net.planckton.lib.player.PlayerManager;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RequestPlayerManagerEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public static enum Action {
		ADD,REMOVE;
	}
	
	
	@Getter @Setter private PlayerManager<?> manager;
}
