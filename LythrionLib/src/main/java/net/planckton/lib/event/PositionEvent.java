package net.planckton.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.planckton.lib.position.Position;

public class PositionEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public static enum Action {
		ADD,REMOVE;
	}
	
	private Position position;
	private Action action;
	
	public PositionEvent(Position position, Action action) {
		this.position = position;
		this.action = action;
	}

	public Position getPosition() {
		return position;
	}
	
	public Action getAction() {
		return action;
	}
}
