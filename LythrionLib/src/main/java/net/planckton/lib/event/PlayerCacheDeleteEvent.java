package net.planckton.lib.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class PlayerCacheDeleteEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	@Getter @NonNull LythrionPlayer player;
}
