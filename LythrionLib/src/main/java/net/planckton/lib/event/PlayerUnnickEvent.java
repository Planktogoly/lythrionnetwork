package net.planckton.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.planckton.lib.player.LythrionPlayer;

public class PlayerUnnickEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	private LythrionPlayer player;
	
	public PlayerUnnickEvent(LythrionPlayer player) {
		this.player = player;
	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

	public LythrionPlayer getPlayer() {
		return player;
	}
}
