package net.planckton.lib.playmode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.google.common.collect.ImmutableSet;

import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.event.PlayerCacheDeleteEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.nick.NickNameManager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.utils.HashMapSet;

public class PlayModeManager extends Manager implements Listener {
	private static HashMap<String, PlayMode> modes;
	private static ArrayList<PlayMode> preAddedModes = new ArrayList<PlayMode>();
	private static HashMapSet<Class<? extends PlayMode>, LythrionPlayer> playersPerPlayMode;
	private static HashMapSet<Class<? extends PlayMode>, LythrionPlayer> playersPerPlayModeCached;
	private static Set<LythrionPlayer> EMPTY_SET = new HashSet<>();
	
	@Deprecated
	/**
	 * Use registerPlayMode(PlayMode mode) instead
	 */
	public static void preAddMode(PlayMode mode) {
		registerPlayMode(mode);
	}
	
	public static void registerPlayMode(PlayMode mode) {
		System.out.println("[PlayMode] Pre-added mode " + mode.getName());
		preAddedModes.add(mode);
	}
	
	public PlayModeManager() {
		super(PositionManager.class, NickNameManager.class);
	}
	
	@Override
	public void onEnable() {
		modes = new HashMap<>();
		playersPerPlayMode = new HashMapSet<>();
		playersPerPlayModeCached = new HashMapSet<>();
		
		ServiceLoader<PlayMode> loader = ServiceLoader.load(PlayMode.class, LythrionLib.getFullClassLoader());
		for(PlayMode mode : loader) {
			System.out.println("[LythrionLib] Found playmode " + mode.getName());
			modes.put(mode.getClass().getSimpleName(), mode);
			
			Bukkit.getPluginManager().registerEvents(mode, LythrionLib.getInstance());
		}
		
		for(PlayMode mode : preAddedModes) {
			if(modes.containsKey(mode.getClass().getSimpleName())) {
				System.out.println("[LythrionLib] Playmode " + mode.getName() + " was already added.");
				continue;
			}
			
			System.out.println("[LythrionLib] Found pre-added playmode " + mode.getName());
			modes.put(mode.getClass().getSimpleName(), mode);
			
			Bukkit.getPluginManager().registerEvents(mode, LythrionLib.getInstance());
		}
		
		System.out.println("[LythrionLib] Loaded " + modes.size() + " play modes.");
	}
	
	@Override
	public void onDisable() {
		modes = null;
		playersPerPlayMode = null;
		playersPerPlayModeCached = null;
	}
	
	public static Set<LythrionPlayer> getPlayersPerMode(Class<? extends PlayMode> mode) {
		Set<LythrionPlayer> players = playersPerPlayMode.get(mode);
		if(players == null) return EMPTY_SET;
		
		for (LythrionPlayer player : players) {
			if (player.getId() == null) {
				System.out.println("uuid == null of ");
				players.remove(player);
			}
		}
		
		return ImmutableSet.copyOf(players);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends LythrionPlayer> Set<T> getPlayersPerMode(Class<? extends PlayMode> mode, Class<T> player) {
		return (Set<T>) (Set<?>) getPlayersPerMode(mode);
	}
	
	public static Set<LythrionPlayer> getPlayersPerModeCached(Class<? extends PlayMode> mode) {
		Set<LythrionPlayer> players = playersPerPlayModeCached.get(mode);
		if(players == null) players = EMPTY_SET;
		
		
		 
		return ImmutableSet.copyOf(players);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends LythrionPlayer> Set<T> getPlayersPerModeCached(Class<? extends PlayMode> mode, Class<T> player) {
		return (Set<T>) (Set<?>) getPlayersPerModeCached(mode);
	}
	
	public static void setPlayerPlayMode(LythrionPlayer player, Class<? extends PlayMode> newPlayMode, Class<? extends PlayMode> oldPlayMode) {
		if(oldPlayMode != null) playersPerPlayMode.remove(oldPlayMode, player);
		playersPerPlayMode.add(newPlayMode, player);
		
		if(oldPlayMode != null) playersPerPlayModeCached.remove(oldPlayMode, player);
		playersPerPlayModeCached.add(newPlayMode, player);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onLythrionPlayerJoin(LythrionPlayerJoinEvent event) {
		playersPerPlayMode.add(event.getLythrionPlayer().getPlayMode().getClass(), event.getLythrionPlayer());
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onLythrionPlayerQuit(LythrionPlayerQuitEvent event) {
		playersPerPlayMode.remove(event.getLythrionPlayer().getPlayMode().getClass(), event.getLythrionPlayer());
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerCacheDelete(PlayerCacheDeleteEvent event) {
		playersPerPlayModeCached.remove(event.getPlayer().getPlayMode().getClass(), event.getPlayer());
	}
	 
	public static PlayMode fromString(String name) {
		for(PlayMode mode : modes.values()) {
			if(mode.getName().equalsIgnoreCase(name) || mode.getName().equalsIgnoreCase("mode." + name)) return mode;
		}
		
		return null;
	}
	
	public static PlayMode fromClass(Class<? extends PlayMode> modeClass) {
		return modes.get(modeClass.getSimpleName());
	}
}
