package net.planckton.lib.playmode;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import mkremins.fanciful.FancyMessage;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;
import net.planckton.lib.utils.Utils;

public class PlayModeAdmin extends PlayMode {
	public static enum AdminModes {
		ADMIN,SPEC;
	}
	
	@Override
	public void onSelect(LythrionPlayer player) {
		player.setGameMode(GameMode.CREATIVE);
		player.getInventory().clear();
		
		for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			if(p.hasRankPower(Rank.MOD)) continue;
			if(p == player) continue;
			
			p.hidePlayer(player);
		}
		player.setOp(true);
	}
	
	@Override
	public void onDeSelect(LythrionPlayer player) {		
		Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
					if(p.hasRankPower(Rank.MOD)) continue;
					if(p == player) continue;
					
					p.showPlayer(player);
				}
			}
		}, 10);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLythrionPlayerJoin(LythrionPlayerJoinEvent event) {
		LythrionPlayer player = event.getLythrionPlayer();
		
		if(player.getPlayMode() == this) {
			for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
				if(p.hasRankPower(Rank.MOD)) continue;
				if(p == player) continue;
				
				p.hidePlayer(player);
			}
		}
		else {
			if(player.hasRankPower(Rank.MOD)) return;
			
			for(LythrionPlayer p : PlayModeManager.getPlayersPerMode(getClass())) {
				player.hidePlayer(p);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onSignChangeEvent(SignChangeEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		String[] lines = event.getLines();
		for (int i = 0; i < lines.length; i++) {
			event.setLine(i, ChatColor.translateAlternateColorCodes('&', lines[i]));
		}
	}
	
	@Override
	public void onPickupItem(PlayerPickupItemEvent event) { }
	
	@Override
	public void onDropItem(PlayerDropItemEvent event) { }
	
	@Override
	public void onDamage(EntityDamageEvent event) {	}
	
	@Override
	public void onBlockBreak(BlockBreakEvent event) { }
	
	@Override
	public void onBlockPlace(BlockPlaceEvent event) { }
	
	@Override
	public void onPlayerDeath(PlayerDeathEvent event) {	}
	
	@Override
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) { 
		if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		Block block = event.getClickedBlock();
		Material blockType = block.getType();
		
		if(blockType != Material.SIGN_POST && blockType != Material.WALL_SIGN) return;
		Sign sign = (Sign) block.getState();
		
		FancyMessage message = ChatPrefix.getLythrionGoldArrows();
		message.then("Edit line: ")
		.color(ChatColor.BLUE);
		
		String line1 = Utils.stripChatColors('&', sign.getLine(0));
		message.then("1 ")
		.tooltip(ChatColor.GRAY + "Edit: ", line1)
		.suggest("/signedit 1 " + line1);
		
		String line2 = Utils.stripChatColors('&', sign.getLine(1));
		message.then("2 ")
		.tooltip(ChatColor.GRAY + "Edit: ",line2)
		.suggest("/signedit 2 " + line2);
		
		String line3 = Utils.stripChatColors('&', sign.getLine(2));
		message.then("3 ")
		.tooltip(ChatColor.GRAY + "Edit: ",line3)
		.suggest("/signedit 3 " + line3);
		
		String line4 = Utils.stripChatColors('&', sign.getLine(3));
		message.then("4 ")
		.tooltip(ChatColor.GRAY + "Edit: ",line4)
		.suggest("/signedit 4 " + line4);
		
		player.sendMessage(message);
	}
	
	@Override
	public void onArmorStandBreak(EntityDamageEvent event) { }
	
	@Override
	public void onArmorStand(PlayerInteractAtEntityEvent event) { }
	
	@Override
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) { }
}
