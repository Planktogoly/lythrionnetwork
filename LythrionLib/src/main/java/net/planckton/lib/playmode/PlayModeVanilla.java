package net.planckton.lib.playmode;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import net.planckton.lib.player.LythrionPlayer;

public class PlayModeVanilla extends PlayMode {
	
	@Override
	public void onSelect(LythrionPlayer player) {
		
	}
	
	@Override
	public void onArmorStand(PlayerInteractAtEntityEvent event) { }
	
	@Override
	public void onArmorStandBreak(EntityDamageEvent event) { }
	
	@Override
	public void onBlockBreak(BlockBreakEvent event) { }
	
	@Override
	public void onBlockPlace(BlockPlaceEvent event) { }
	
	@Override
	public void onCropTrample(PlayerInteractEvent event) { }
	
	@Override
	public void onDamage(EntityDamageEvent event) { }
	
	@Override
	public void onDeSelect(LythrionPlayer player) { }
	
	@Override
	public void onDropItem(PlayerDropItemEvent event) { }
	
	@Override
	public void onFoodLevelChange(FoodLevelChangeEvent event) { }
	
	@Override
	public void onPickupItem(PlayerPickupItemEvent event) { }
	
	@Override
	public void onPlayerDeath(PlayerDeathEvent event) { }
	
	@Override
	public void onPlayerInteract(PlayerInteractEvent event) { }
	
	@Override
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {}
}
