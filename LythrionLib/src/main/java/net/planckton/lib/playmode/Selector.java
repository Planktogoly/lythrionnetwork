package net.planckton.lib.playmode;

import net.planckton.lib.player.LythrionPlayer;

public interface Selector {
	public void onSelect(LythrionPlayer player);
}
