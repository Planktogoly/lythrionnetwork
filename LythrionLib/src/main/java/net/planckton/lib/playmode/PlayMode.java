package net.planckton.lib.playmode;

import lombok.Getter;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public abstract class PlayMode implements Listener { 
	@Getter private final String name;
	
	@Deprecated
	/**
	 * No constructor is required.
	 */
	protected PlayMode(String name) {
		this.name = "mode." + name;
	}
	
	public PlayMode() {
		this.name = "mode." + getClass().getSimpleName();
	}
	
	abstract public void onSelect(LythrionPlayer player);
	public void onDeSelect(LythrionPlayer player) {
		
	}
	
	//methods
	protected boolean isInMode(Player player) {
		return isInMode((LythrionPlayer) PlayerManager.getLythrionPlayer(player));
	}
	
	protected boolean isInMode(LythrionPlayer player) {
		if(player.getPlayMode() != this) return false;
		return true;
	}
	
	public boolean canEquipPet() {
		return false;
	}
	
	public boolean canBeSpectated() {
		return false;
	}
	
	public boolean canStartSpectating() {
		return false;
	}
	
	public boolean canGoAfk() {
		return false;
	}
	
	//Events
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPickupItem(PlayerPickupItemEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onDropItem(PlayerDropItemEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if(!(event.getEntity() instanceof Player)) return;
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDeath(PlayerDeathEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getEntity());
		if(player.getPlayMode() != this) return;
		
		event.setDroppedExp(0);
		event.setDeathMessage("");
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onDamage(EntityDamageEvent event) {
		if(!(event.getEntity() instanceof Player)) return;
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onArmorStandBreak(EntityDamageEvent event) {
		if(!(event.getEntity() instanceof ArmorStand) && !(event.getEntity() instanceof ItemFrame)) return;
		
		if(!(event instanceof EntityDamageByEntityEvent)) {
			event.setCancelled(true);
 			return;
		}
		
		EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) event;
		
		if(!(edbee.getDamager() instanceof Player)) {
			if(!(edbee.getDamager() instanceof Projectile)) return;
			
			event.setCancelled(true);
 			return;
		}
		
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer((Player) edbee.getDamager());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onArmorStand(PlayerInteractAtEntityEvent event) {
		if(!(event.getRightClicked() instanceof ArmorStand) && !(event.getRightClicked() instanceof ItemFrame)) return;
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCropTrample(PlayerInteractEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		if(event.getAction() == Action.PHYSICAL) {
			if(event.getClickedBlock().getType() != Material.SOIL) return;
			
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			
			if(block.getType() == Material.ANVIL) event.setCancelled(true);
			else if(block.getType() == Material.WORKBENCH) event.setCancelled(true);
			else if(block.getType() == Material.FURNACE) event.setCancelled(true);
			else if(block.getType() == Material.CHEST) event.setCancelled(true);
			else if(block.getType() == Material.ENDER_CHEST) event.setCancelled(true);
			else if(block.getType() == Material.TRAPPED_CHEST) event.setCancelled(true);
			else if(block.getType() == Material.TRAP_DOOR) event.setCancelled(true);
			else if(block.getType() == Material.BEACON) event.setCancelled(true);
			else if(block.getType().toString().endsWith("_DOOR")) event.setCancelled(true);
			else if(block.getType() == Material.FENCE_GATE) event.setCancelled(true);
			else if(block.getType() == Material.WOOD_BUTTON) event.setCancelled(true);
			else if(block.getType() == Material.BED_BLOCK) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		LythrionPlayer player = (LythrionPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if(player.getPlayMode() != this) return;
		
		if(event.getRightClicked().getType() == EntityType.ITEM_FRAME) event.setCancelled(true);
	}
}
