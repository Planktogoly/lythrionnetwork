package net.planckton.lib.rank;

import java.util.Collection;
import java.util.HashSet;

import lombok.Getter;

import org.bukkit.ChatColor;

public enum Rank {
	
	MEMBER(0, ChatColor.GRAY , ChatColor.GRAY + "", "" + ChatColor.GRAY + ChatColor.RESET, 1, 1),
	DION(1, ChatColor.AQUA, "" + ChatColor.AQUA + ChatColor.BOLD + "DION ","" + ChatColor.AQUA + ChatColor.BOLD + "DION " + ChatColor.RESET ,  2, 1.5),
	THRION(2, ChatColor.DARK_AQUA, "" + ChatColor.DARK_AQUA + ChatColor.BOLD + "THRION ","" + ChatColor.DARK_AQUA + ChatColor.BOLD + "THRION " + ChatColor.RESET,  3, 2),
	LYTHRION(3, ChatColor.RED, "" + ChatColor.RED + ChatColor.BOLD + "LYTHRION ","" + ChatColor.RED + ChatColor.BOLD + "LYTHRION " + ChatColor.RESET, 4, 3),
	VIP(4, ChatColor.DARK_PURPLE, "" + ChatColor.DARK_PURPLE + ChatColor.BOLD + "VIP ", "" + ChatColor.DARK_PURPLE + ChatColor.BOLD + "VIP " + ChatColor.RESET , 5, 3),
	BUILDER(5, ChatColor.BLUE, "" + ChatColor.BLUE + ChatColor.BOLD + "BUILDER ", "" + ChatColor.BLUE + ChatColor.BOLD + "BUILDER " + ChatColor.RESET, 4, 3),
	JRMOD(6,ChatColor.YELLOW, ChatColor.YELLOW + "Jr." + ChatColor.GOLD + ChatColor.BOLD + "MOD ", ChatColor.YELLOW + "Jr." + ChatColor.GOLD + ChatColor.BOLD + "MOD " + ChatColor.RESET , 4, 3),
	MOD(7, ChatColor.GOLD, "" + ChatColor.GOLD + ChatColor.BOLD + "MOD ", "" + ChatColor.GOLD + ChatColor.BOLD + "MOD " + ChatColor.RESET ,4, 3),
	SRMOD(8, ChatColor.GOLD, ChatColor.RED + "Sr." + ChatColor.GOLD + ChatColor.BOLD + "MOD ", ChatColor.RED + "Sr." + ChatColor.GOLD + ChatColor.BOLD + "MOD " + ChatColor.RESET , 4, 3),
	ADMIN(9, ChatColor.DARK_RED, ChatColor.DARK_RED.toString() + ChatColor.BOLD + "ADMIN ", ChatColor.DARK_RED.toString() + ChatColor.BOLD + "ADMIN " + ChatColor.RESET , 5, 4),
	OWNER(10, ChatColor.DARK_RED, "" + ChatColor.DARK_RED + ChatColor.BOLD + "OWNER ", "" + ChatColor.DARK_RED + ChatColor.BOLD + "OWNER " + ChatColor.RESET, 5, 4);
	
	private static HashSet<Rank> ranks;
	
	//Used to compare ranks to eachother
	@Getter private int rankId;
	@Getter private ChatColor color;
	@Getter private String prefix;
	@Getter private String chatPrefix;
	@Getter private int votes;
	@Getter private double generalCurrenyMultiplier;
	
	private Rank(int rankId, ChatColor color, String prefix, String chatPrefix, int votes, double generalCurrenyMultiplier) {
		this.rankId = rankId;
		this.color = color;
		this.prefix = prefix;
		this.chatPrefix = chatPrefix;
		this.votes = votes;
		this.generalCurrenyMultiplier = generalCurrenyMultiplier;
	}
	
	public boolean hasPower(Rank rank) {
		return rankId >= rank.rankId;
	}
	
	//static methods
	public static Rank byName(String name) {
		
		for(Rank rank : values()) {
			if(rank.toString().equalsIgnoreCase(name)) return rank;
		}
		
		return null;
	}

	public static Rank fromString(String rankString) {
		if(rankString == null) return null;
		
		for(Rank rank : values()) {
			if(rank.toString().toLowerCase().replace("_", "").equalsIgnoreCase(rankString)) return rank;
		}
		
		return null;
	}

	public static Rank fromInt(int rankInt) {
		for(Rank rank : values()) {
			if(rank.rankId == rankInt) return rank;
		}
		
		return MEMBER;
	}
	
	public boolean hasStaffPermissions() {
		return this.rankId >= JRMOD.rankId;
	}

	public boolean hasDonatorPermissions() {
		return this.rankId >= DION.rankId;
	}
	
	public boolean isDeveloper() {
		return this.rankId >= OWNER.rankId;
	}
	
	public static Collection<Rank> valuesSet() {
		if(ranks == null) {
			ranks = new HashSet<>();
			
			for(Rank rank : values()) {
				ranks.add(rank);
			}
		}
		
		return ranks;
	}
}
