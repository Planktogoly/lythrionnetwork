package net.planckton.lib.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ServiceLoader;

import lombok.Getter;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.ManagerDisableEvent;
import net.planckton.lib.event.ManagerEnableEvent;
import net.planckton.lib.event.ManagersEnabledEvent;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.collect.Lists;

public abstract class Manager {
	private static final HashMap<String, Manager> managers = new HashMap<String, Manager>();
	private static final ArrayList<Manager> loadAtStart = new ArrayList<Manager>();
	private static final ArrayList<Manager> preAddedManagers = new ArrayList<>();
	
	private static final HashMap<Class<? extends Manager>, Manager> overridenManagers = new HashMap<Class<? extends Manager>, Manager>();
	
	public static void findManagers() {
		System.out.println("[TosiLib] Started looking for managers.");
		
		ServiceLoader<Manager> loader = ServiceLoader.load(Manager.class, LythrionLib.getFullClassLoader());
		
		//find all managers
		for(Manager manager : loader) {
			loadManager(manager);
		}
		 
		for(Manager manager : preAddedManagers) {
			if(managers.containsKey(manager.getName())) {
				System.out.println("[LythrionLib] Pre-added-manager: " + manager.getName() + " was already found by the ServiceLoader");
				continue;
			}
			
			loadManager(manager);
		}
		
		//keep looping until a loop has made no changes, only then we can 
		//be sure every manager suits its requirement needs
		boolean done = false;
		while(!done) {
			done = true;
			
			//check if requirements for the before list are valid
			//and check for double dependings
			loadBeforeLoop:
			for (Iterator<Manager> iterator = loadAtStart.iterator(); iterator.hasNext();) {
				Manager manager = iterator.next();
				ArrayList<Class<? extends Manager>> required = manager.getRequired();
				if(required == null) continue;
				
				for(Class<? extends Manager> requir : required) {
					Manager requirManager = managers.get(requir.getSimpleName());
					if(requirManager == null) {
						System.out.println("[LythrionLib] Manager " + manager.getName() + " requires " + requir.getSimpleName() + ". But " + requir.getSimpleName() + " is not found.");
						continue loadBeforeLoop;
					}
					
					if(requirManager.dependsOn(manager)) {
						iterator.remove();
						managers.remove(manager.getClass().getSimpleName());
						done = false;
						
						System.out.println("[LythrionLib] Manager " + manager.getName() + " & " + requirManager.getName() + " both depend on eachother and won't be enabled");
						continue loadBeforeLoop;
					}
				}
			}
		}
		
		//Build edges
		HashMap<Manager, HashSet<Manager>> inEdges = new HashMap<>();
		HashMap<Manager, HashSet<Manager>> outEdges = new HashMap<>();
		
		for(Manager manager : managers.values()) {
			inEdges.put(manager, new HashSet<Manager>());
			outEdges.put(manager, new HashSet<Manager>());
		}
		
		//build edges
		for(Manager manager : loadAtStart) {
			if(manager.getRequired() == null) continue;
			System.out.println("BUILDING EDGES FROM MANAGER: " + manager);
			
			for(Class<? extends Manager> reqManagerClass : manager.getRequired()) {
				Manager reqManager = managers.get(reqManagerClass.getSimpleName());
				
				outEdges.get(manager).add(reqManager);
				inEdges.get(reqManager).add(manager);
			}
		}
		
		for(Manager manager : loadAtStart) {
			for(Class<? extends Manager> reqManagerClass : manager.getLoadBefore()) {
				Manager reqManager = managers.get(reqManagerClass.getSimpleName());
				
				outEdges.get(reqManager).add(manager);
				inEdges.get(manager).add(reqManager);
			}
		}
		
		HashSet<Manager> S = new HashSet<Manager>();
		for (Manager n : loadAtStart) {
			if (inEdges.get(n).size() == 0) {
				S.add(n);
			}
		}
		
		ArrayList<Manager> loadOnStart = new ArrayList<>();
		
		// order
		while (!S.isEmpty()) {
			// remove a node n from S
			Manager n = S.iterator().next();
			S.remove(n);

			// insert n into L
			loadOnStart.add(n);

			// for each node m with an edge e from n to m do
			for (Iterator<Manager> it = outEdges.get(n).iterator(); it
					.hasNext();) {
				// remove edge e from the graph
				Manager m = it.next();
				it.remove();// Remove edge from n
				inEdges.get(m).remove(n);// Remove edge from m

				// if m has no other incoming edges then insert m into S
				if (inEdges.get(m).isEmpty()) {
					S.add(m);
				}
			}
		}
		
		// Check to see if all edges are removed
		boolean cycle = false;
		for (Manager n : loadOnStart) {
			if (!inEdges.get(n).isEmpty()) {
				cycle = true;
				break;
			}
		}

		if(cycle) {
			System.out.println("[LythrionLib] Error while ordering managers, Cycle present, topological sort not possible.");
			Bukkit.shutdown();
			return;
		}
		
		//set new list
		loadAtStart.clear();
		loadAtStart.addAll(loadOnStart);
		Collections.reverse(loadAtStart);
		
		
		System.out.println("[LythrionLib] Found " + managers.size() + " managers.");
		System.out.println("[LythrionLib] Going to load " + loadAtStart.size() + " managers" + loadAtStart.toString() + " at start.");
	}
	
	public static void loadManager(Manager manager) {
		System.out.println("[LythrionLib] Found pre-added-manager: " + manager.getName());
		
		Manager overridenManager = overridenManagers.get(manager.getClass());
		if(overridenManager != null) {
			System.out.println("[Managers] Manager " + manager.getName() + " was overriden by " + overridenManager.getName());
			managers.put(manager.getName(), overridenManager);
			return;
		}
		
		managers.put(manager.getName(), manager);
		
		if(manager.getClass().isAnnotationPresent(OverrideManager.class)) {
			@SuppressWarnings("unchecked")
			Class<? extends Manager> parentClass = (Class<? extends Manager>) manager.getClass().getSuperclass();
			overridenManagers.put(parentClass, manager);
			System.out.println("[Managers] Manager " + manager.getName() + " overriding " + parentClass.getSimpleName());
			
			for(Manager checkManager : managers.values()) {
				if(!checkManager.getName().equals(parentClass.getSimpleName())) continue;
				System.out.println("[Managers] Manager " + checkManager.getName() + " was overriden by " + manager.getName());
				
				managers.put(checkManager.getName(), manager);
				loadAtStart.remove(checkManager);
			}
			
			managers.put(parentClass.getSimpleName(), manager);
		}
			
		if(!manager.isLoadOnStart()) return;
		loadAtStart.add(manager);
	}
	
	/**
	 * Please use {@code registerManager(Manager manager)} instead
	 */
	@Deprecated
	public static void preAddManager(Manager manager) {
		registerManager(manager);
	}
	
	public static void registerManager(Manager manager) {
		System.out.println("[Managers] Pre-added manager " + manager.getName());
		preAddedManagers.add(manager);
	}
	
	public static void enableManagers() {
		System.out.println("[LythrionLib] Enabling managers");
		
		for(Manager manager : loadAtStart) manager.enable();
		
		Bukkit.getPluginManager().callEvent(new ManagersEnabledEvent(Collections.unmodifiableCollection(managers.values())));
		
		System.out.println("[LythrionLib] Done enabling managers");
	}
	
	public static void disableManagers() {
		Collections.reverse(loadAtStart);
		
		for(Manager manager : loadAtStart) {
			if(!manager.isEnabled()) continue;
			
			manager.disable();
		}
		
		for(Manager manager : managers.values()) {
			if(!manager.isEnabled()) continue;
			
			manager.disable();
		}
	}
	
	public static <T extends Manager>  boolean isManagerEnabled(Class<T> klass) {
		return managers.get(klass.getSimpleName()).isEnabled();
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Gets the manager of the given class type.
	 * 
	 * @param klass Class of the requested manager.
	 * @return The given manager. Null if not found or disabled.
	 */
	public static <T extends Manager> T GetManager(Class<T> klass) {
		return (T) managers.get(klass.getSimpleName());
	}
	
	//member manager stuff
	@Getter private boolean enabled;
	@Getter private final ArrayList<Class<? extends Manager>> required;
	@Getter private final ArrayList<Class<? extends Manager>> loadBefore = new ArrayList<Class<? extends Manager>>();
	@Getter private final boolean loadOnStart;
	
	protected Manager() {
		this(true);
	}
	
	protected Manager(boolean loadOnStart) {
		this(new ArrayList<Class<? extends Manager>>(), loadOnStart);
	}
	
	protected Manager(final Class<? extends Manager> required) {
		this(required, true);
	}

	@SafeVarargs 
	protected Manager(Class<? extends Manager>... required) {
		this(Lists.newArrayList(required));
	}
	
	@SafeVarargs
	protected Manager(boolean loadOnStart, final Class<? extends Manager>... required) {
		this(Lists.newArrayList(required), loadOnStart);
	}
	
	protected Manager(final Class<? extends Manager> required, boolean loadOnStart) {
		this(new ArrayList<Class<? extends Manager>>() {
			private static final long serialVersionUID = -2815123008626830211L; {add(required);}});
	}
	
	protected Manager(ArrayList<Class<? extends Manager>> required) {
		this(required, true);
	}
	
	protected Manager(ArrayList<Class<? extends Manager>> required, boolean loadOnStart) {
		this.required = required;
		this.loadOnStart = loadOnStart;
	}
	
	public final void enable() {
		if(enabled) return;
		
		if(this.required != null) for(Class<? extends Manager> req : this.required) {
			Manager manager = GetManager(req);
			
			if(manager == null) {
				System.out.println("[LythrionLib] Manager " + getName() + " cannot be enabled since " + req.getSimpleName() + " is not available.");
				return;
			}
			
			if(manager.isEnabled()) continue;
			System.out.println("[LythrionLib] Manager " + getName() + " cannot be enabled since " + req.getSimpleName() + " is not enabled but required.");
			return;
		}
		
		//System.out.println("[TostiLib] Enabling manager " + getName());
		
		if(this instanceof Listener) Bukkit.getPluginManager().registerEvents((Listener) this, LythrionLib.getInstance());
		if(this instanceof PluginMessageListener) Bukkit.getServer().getMessenger().registerIncomingPluginChannel(LythrionLib.getInstance(), "BungeeCord", (PluginMessageListener) this);
		
		enabled = true;
		
		try {
			onEnable();
		}
		catch(Exception e) {
			System.out.println("[LythrionLib] An error occured while enabling manager " + getName());
			e.printStackTrace();
			enabled = false;
			return;
		}
		
		Bukkit.getPluginManager().callEvent(new ManagerEnableEvent(this));
		
		System.out.println("[LythrionLib] Enabled manager " + getName());
	}
	
	public final void disable() {
		if(!enabled) return;
		
		System.out.println("[LythrionLib] Disabling manager " + getName());
		
		if(this instanceof Listener) HandlerList.unregisterAll((Listener) this);
		if(this instanceof PluginMessageListener) Bukkit.getServer().getMessenger().unregisterIncomingPluginChannel(LythrionLib.getInstance(), "BungeeCord", (PluginMessageListener) this);
		
		try {
			onDisable();
		}
		catch(Exception e) {
			System.out.println("[LythrionLib] An error occured while disabling manager " + getName());
			e.printStackTrace();
			enabled = false;
			return;
		}
		
		enabled = false;
		
		Bukkit.getPluginManager().callEvent(new ManagerDisableEvent(this));
		
		System.out.println("[LythrionLib] Disabled manager " + getName());
	}
	
	/**
	 * Called when the manager is enabled.
	 */
	public abstract void onEnable();
	
	/**
	 * Called when the manager is shutdown.
	 * Make sure to cleanup memory.
	 */
	public abstract void onDisable();
	
	//private checks
	private boolean dependsOn(Manager manager) {
		if(required == null) return false;
		
		for(Class<? extends Manager> req : required) {
			//boolean depend = req.getSimpleName().equals(manager.getClass().getSimpleName());
			//System.out.println("Depends " + getClass().getSimpleName() + " on " + manager.getClass().getSimpleName() + "? check " + req.getSimpleName() + " outcome: " + depend);
			
			if(!req.getSimpleName().equals(manager.getClass().getSimpleName())) continue;
			return true;
		}
		
		return false;
	}
	
	//getters & setters
	
	public String getName() {
		return getClass().getSimpleName();
	}
	
	public void addLoadBefore(Class<? extends Manager> klass) {
		loadBefore.add(klass);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
