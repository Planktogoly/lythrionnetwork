package net.planckton.lib.holo;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

public class Holo {
	private Location location;
	private String[] lines;
	@Getter private ArrayList<ArmorStand> stands;
	@Setter boolean pointer;
	
	public Holo(Location location, String... lines) {
		this.location = location;
		this.lines = lines;
		
		stands = new ArrayList<ArmorStand>();
	}
	
	protected void spawn() {
		Location curLoc = location;
		
		for(int i = 0; i < lines.length; i++) {
			spawnArmorStandAt(curLoc, lines[i]);
			
			curLoc.add(0, -0.25f, 0);
		}
	}
	
	protected void despawn() {
		for(ArmorStand stand : stands) {
			stand.remove();
		}
	}
	
	private void spawnArmorStandAt(Location loc, String title) {
		ArmorStand stand = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
		
		stand.setCustomName(title);
		stand.setCustomNameVisible(!pointer);
		
		stand.setNoDamageTicks(Integer.MAX_VALUE);
		stand.setGravity(false);
		stand.setMarker(true);
		stand.setGravity(false);
		stand.setVisible(false);
		stand.setBasePlate(false);
		if(pointer) stand.setSmall(true);
		
		stands.add(stand);
	}
	
	public void updateLines(String... lines) {
		this.lines = lines;
		
		for (int i = 0; i < stands.size(); i++) {
			ArmorStand stand = stands.get(i);
			
			stand.setCustomName(lines[i]);
		}
	}
	
	//getters
	public String[] getLines() {
		return lines;
	}
	
	public int[] getEntityIds() {
		int[] ids = new int[stands.size()];
		
		for (int i = 0; i < stands.size(); i++) {
			ArmorStand stand = stands.get(i);
			ids[i] = stand.getEntityId();
		}
		
		return ids;
	}
}
