package net.planckton.lib.holo;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import net.planckton.lib.event.PositionEvent;
import net.planckton.lib.event.PositionEvent.Action;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.position.Position;
import net.planckton.lib.position.PositionManager;

public class HoloManager extends Manager implements Listener {
	private static HashMap<Integer, Holo> holos;
	private static HashMap<Position, Holo> holosByPos;
	
	public static Holo fromPosition(Position position) {
		return fromPosition(position, position.getPosition());
	}
	
	public static Holo fromPosition(Position position, Location location) {
		String[] name;
		
		if(position.name.startsWith("holo.")) {
			name = ChatColor.translateAlternateColorCodes('&', position.name.replaceFirst("holo\\.", "").replace('#', ' ')).split("\\.");
		}
		else {
			name = new String[]{position.name.replaceFirst("pointer\\.", "")};
		}
		
		Holo holo = new Holo(location, name);
		if(position.name.startsWith("pointer")) holo.setPointer(true);
		
		holosByPos.put(position, holo);
		return holo;
	}
	
	public HoloManager() {
		super(PositionManager.class);
	}
	
	@Override
	public void onEnable() {
		holos = new HashMap<Integer, Holo>();
		holosByPos = new HashMap<Position, Holo>();
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/kill @e[type=ArmorStand,marker=1]");
		
		for(World world : Bukkit.getWorlds()) {
			for(ArmorStand stand : world.getEntitiesByClass(ArmorStand.class)) {
				if(stand.isVisible()) continue;
				
				stand.remove();
			}
		}
		
		for(Position position : PositionManager.getPositions()) {
			if(!position.name.startsWith("holo.") && !position.name.startsWith("pointer.")) continue;
			
			for(Location loc : position.getLocations()) {
				spawnHolo(fromPosition(position, loc));
			}
		}
		
		System.out.println("Loaded " + holosByPos.size() + " holos and " + holos.size() + " holo entities");
	}

	@Override
	public void onDisable() {
		for(Holo holo : holos.values()) {
			holo.despawn();
		}
		
		while(holos.size() > 0) {
			despawnHolo(holos.entrySet().iterator().next().getValue());
		}
		
		holos = null;
		holosByPos = null;
	}
	
	public void spawnHolo(Holo holo) {
		holo.spawn();
		
		for(int id : holo.getEntityIds()) {
			holos.put(id, holo);
		}
	}
	
	public void despawnHolo(Holo holo) {
		for(int id : holo.getEntityIds()) {
			holos.remove(id);
		}
		
		holo.despawn();
	}
	
	public static Holo getHolo(int entityId) {
		return holos.get(entityId);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPositionEvent(PositionEvent event) {
		Position pos = event.getPosition();
		if(!pos.name.startsWith("holo.") && !pos.name.startsWith("pointer.")) return;
		
		if(event.getAction() == Action.ADD) {
			spawnHolo(fromPosition(pos));
		}
		else {
			despawnHolo(holosByPos.get(pos));
			holosByPos.remove(pos);
		}
	}

	public static Collection<Holo> getHolos() {
		return holos.values();
	}
}
