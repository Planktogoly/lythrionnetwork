package net.planckton.lib.nick;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerNamedEntitySpawn;
import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardScore;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardTeam;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardTeam.Mode;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import com.google.common.collect.EvictingQueue;
import com.mojang.authlib.GameProfile;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.Cleanup;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.LythrionPlayerLoadEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.event.ManagerEnableEvent;
import net.planckton.lib.event.PlayerNickEvent;
import net.planckton.lib.event.PlayerUnnickEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;
import net.planckton.lib.utils.Utils;

public class NickNameManager extends Manager implements Listener {
	private static final String[] namePrefixes = new String[]{"The","xX","i","De","Het","le","_","We","Some","X","x","I","That","iT","My","a","s"};
	
	private static final String[] nameFirstpart = new String[]{"Unkown","Super","Extra","Man","Yolo","Uber","Mega","Builder","Miner","Fake","Spoof",
																"Scary","Black","Blue","Wanted","MLG_","Hammer","Great","Dude","Opaque","Only","Yellow",
																"Name","Is","Pug","Jake","Fin","Top","Ball","360","Strik","Mum","Craft","Snake","Hell",
																"Heaven","Shake","Jokey","Juicy","Shear","Byte","Binary","Ship","Slick","Smooth","Strider",
																"Infinity","Destroy","Down","Baking","Star","Drop","Double","Deal", "Something", "Every", 
																"All", "Tosti", "Choco", "Butter", "Better", "Swirl", "Every", "Frickin", "Owner"};
	
	private static final String[] nameSecondpart = new String[]{"Computer","MC","Bob","Jack","Killer","Assasin","God","Fire","Worker","Blocks",
																"Light","Water","Hot","CoCo","Strafe","Break","PvP","Guard","Dunk","123","Work",
																"Funky","Swag","Lover","TheDog","TheHuman","Spot","Sailor","Rain","Thunder","Sword",
																"Bite","Wolf","Walk","Jump","Smoothie","Milk","Jay","Funky","Bit","Illusion","Wreck",
																"Week","Sleep","Brew","Infinit","Sharp","Point","Spiral","Soda","Yoda","Shot","Once",
																"Bite", "Bawler", "Chicken", "Cow", "Jam", "Soup", "Mustard", "Strafe", "Marine", "Fearless",
																"Thunder", "Duck", "Squid", "Tiger", "Left", "Right", "Day", "Captcha", "Sick"};
	
	private static final String[] namePostfixes = new String[]{"HD","Xx","i","03","02","01","00","99","98","97","96","95","94","93","92","91","90",
																"47","42","10","NL","YT","Plays","_","TV", "2000", "2001", "2002", "2003", "2004"};
	
	private static Map<UUID, NickName> hiddenUUIDs;
	private static Map<String, String> actualNames;
	private static Map<String, String> hiddenNames;
	private static Map<String,Collection<WrappedSignedProperty>> textures;
	
	private static Adapter adapter;
	
	@Override
	public void onEnable() {
		hiddenUUIDs = new HashMap<>();
		actualNames = new HashMap<>();
		hiddenNames = new HashMap<>();
		textures = new HashMap<>();
		
		adapter = new Adapter();
		ProtocolManager manager = ProtocolLibrary.getProtocolManager();
		manager.addPacketListener(adapter);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManagerEnable(ManagerEnableEvent event) {
		if(!event.isType(PlayerManager.class)) return;
	}
	
	@Override
	public void onDisable() {
		ProtocolManager manager = ProtocolLibrary.getProtocolManager();
		manager.removePacketListener(adapter);
		
		hiddenUUIDs = null;
		actualNames = null;
		adapter = null;
	}
	
	private class Adapter extends PacketAdapter {
		public Adapter() {
			super(LythrionLib.getInstance(), ListenerPriority.NORMAL,
					PacketType.Play.Server.NAMED_ENTITY_SPAWN,
					PacketType.Play.Server.PLAYER_INFO,
					PacketType.Play.Server.SCOREBOARD_SCORE);
		}
		
		@Override
		public void onPacketSending(PacketEvent event) {
			PacketType type = event.getPacketType();
			
			if(type == PacketType.Play.Server.NAMED_ENTITY_SPAWN) onNamedEntitySpawn(event);
			else if(type == PacketType.Play.Server.PLAYER_INFO) onPlayerInfo(event);
			else if(type == PacketType.Play.Server.SCOREBOARD_SCORE) onScoreboardScore(event);
		}

		private EvictingQueue<Integer> PROCESSED_GLOBAL = EvictingQueue.create(200);
		private EvictingQueue<Integer> PROCESSED_OWN = EvictingQueue.create(200);
		
		private void onPlayerInfo(PacketEvent event) {
			PacketContainer packet = event.getPacket();
			
			WrapperPlayServerPlayerInfo wrapper = new WrapperPlayServerPlayerInfo(packet);
			if(wrapper.getAction() != PlayerInfoAction.ADD_PLAYER) return;
			
			String toPlayerName = event.getPlayer().getName();
			List<PlayerInfoData> original = wrapper.getData();
			
			Integer identityCode = System.identityHashCode(packet.getHandle());
			if(!PROCESSED_GLOBAL.contains(identityCode)) {
				PROCESSED_GLOBAL.add(identityCode);
				
				boolean modified = false;
				ListIterator<PlayerInfoData> it = original.listIterator();
				while(it.hasNext()) {
					PlayerInfoData data = it.next();
					
					WrappedGameProfile profile = data.getProfile();
					UUID currentId = profile.getUUID();
					NickName nickname = getNickNameFor(currentId);
					
					if(nickname == null) continue;
					modified = true;
					data = new PlayerInfoData(nickname.getProfile(), data.getPing(), data.getGameMode(), data.getDisplayName());
					it.set(data);
					modified = true;
				}
				
				if(modified) wrapper.setData(original);
			}
			
			if(!PROCESSED_OWN.contains(identityCode)) {
				PROCESSED_OWN.add(identityCode);
				
				boolean containsOwn = false;
				for(PlayerInfoData data : original) {
					WrappedGameProfile profile = data.getProfile();
					String currentName = profile.getName();
					String actualName = actualNames.get(currentName);
					if(actualName == null) continue;
					if(!toPlayerName.equals(actualName)) continue;
					
					containsOwn = true;
					break;
				}
				
				if(containsOwn) {
					wrapper = new WrapperPlayServerPlayerInfo(packet.deepClone());
					
					List<PlayerInfoData> datas = wrapper.getData();
					ListIterator<PlayerInfoData> it = datas.listIterator();
					while(it.hasNext()) {
						PlayerInfoData data = it.next();
						WrappedGameProfile profile = data.getProfile();
						String currentName = profile.getName();
						String actualName = actualNames.get(currentName);
						if(actualName == null) continue;
						if(!actualName.equals(toPlayerName)) continue;
						
						data = new PlayerInfoData(data.getProfile(), data.getPing(), data.getGameMode(), WrappedChatComponent.fromText(ChatColor.DARK_AQUA + "" + ChatColor.UNDERLINE + currentName));
						it.set(data);
						break;
					}
					
					wrapper.setData(datas);
					event.setPacket(wrapper.getHandle());
				}
			}
		}

		private void onNamedEntitySpawn(PacketEvent event) {
			PacketContainer packet = event.getPacket();
			WrapperPlayServerNamedEntitySpawn wrapper = new WrapperPlayServerNamedEntitySpawn(packet);
			
			//debug send metadata
			//Bukkit.broadcastMessage("SPAWNm8");
			//for(WrappedWatchableObject data : wrapper.getMetadata()) {
			//	Bukkit.broadcastMessage(data.getType() + " at " + data.getIndex() + ": " + data.getValue());
			//}
			
			UUID currentId = wrapper.getPlayerUUID();
			NickName hiddenUUID = getNickNameFor(currentId);
			if(hiddenUUID == null) return;
			
			wrapper.setPlayerUUID(hiddenUUID.getId());
		}
		
		private void onScoreboardScore(PacketEvent event) {
			PacketContainer packet = event.getPacket();
			WrapperPlayServerScoreboardScore wrapper = new WrapperPlayServerScoreboardScore(packet);
			
			String objectiveName = wrapper.getScoreName();
			String hiddenObjectiveName = hiddenNames.get(objectiveName);
			if(hiddenObjectiveName == null) return;
			wrapper.setScoreName(hiddenObjectiveName);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLythrionPlayerLoad(LythrionPlayerLoadEvent event) {
		LythrionLib.getExecutorPool().submit(() -> handleNickname(event.getLythrionPlayer()));
	}
	
	public void handleNickname(LythrionPlayer player) {
		NickName nickname = getNickName(player.getId());
	
		if(nickname == null) return;
		
		WrapperPlayServerScoreboardTeam realTeamWrapper = wrapperScoreboardTeam(player.getRank() + "_rank", Mode.PLAYERS_ADDED, player.getName());
		WrapperPlayServerScoreboardTeam nickTeamWrapper = wrapperScoreboardTeam(Rank.MEMBER + "_rank", Mode.PLAYERS_ADDED, nickname.getName());
		//create packet to remove old name from tab
		WrapperPlayServerPlayerInfo addWrapper = wrapperPlayerInfo(PlayerInfoAction.ADD_PLAYER, WrappedGameProfile.fromHandle(player.getProfile()));
		
		try {
			player.sendPacket(realTeamWrapper.getHandle());
			player.sendPacket(addWrapper.getHandle());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			try {
				p.sendPacket(nickTeamWrapper.getHandle());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
		addNickName(player, nickname);
		
		player.setNickName(nickname);
		player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + "Currently nicked as: " + ChatColor.UNDERLINE + ChatColor.DARK_AQUA + nickname.getName());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(LythrionPlayerQuitEvent event) {
		LythrionPlayer player = event.getLythrionPlayer();
		
		NickName hiddenUUID = hiddenUUIDs.get(player.getId());
		if(hiddenUUID == null) return;
		
		//remove uuids
		removeNickName(player);
		player.setNickName(null);
		
		WrapperPlayServerPlayerInfo removeWrapper = wrapperPlayerInfo(PlayerInfoAction.REMOVE_PLAYER, hiddenUUID.getProfile());
		for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			try {
				p.sendPacket(removeWrapper.getHandle());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static boolean nickPlayer(LythrionPlayer player, String name, String skinName) {
		UUID newUUID = Bukkit.getOfflinePlayer(name).getUniqueId();
		if(newUUID == null) return false;
		
		if(!DatabaseLib.setNickName(player.getId(), newUUID, name, skinName)) return false;
		
		Future<WrappedGameProfile> future = createGameProfile(name, newUUID, skinName);
		future.addListener(new GenericFutureListener<Future<WrappedGameProfile>>() {

			@Override
			public void operationComplete(Future<WrappedGameProfile> arg0) throws Exception {
				NickName nickName = new NickName(name, newUUID, future.get(), skinName);
				player.setNickName(nickName);
				
				//Create packets
				//Add to teams
				WrapperPlayServerScoreboardTeam teamWrapper = wrapperScoreboardTeam(Rank.MEMBER + "_rank", Mode.PLAYERS_ADDED, name);
				//create packet to remove old name from tab
				WrapperPlayServerPlayerInfo removeWrapper = wrapperPlayerInfo(PlayerInfoAction.REMOVE_PLAYER, WrappedGameProfile.fromHandle(player.getProfile()));
				//create despawn packet
				WrapperPlayServerEntityDestroy destroyWrapper = wrapperPlayerDespawn(player.getEntityId());
				//create packet to add new name to tab
				WrapperPlayServerPlayerInfo addWrapper = wrapperPlayerInfo(PlayerInfoAction.ADD_PLAYER, nickName.getProfile());
				//create spawn packet
				WrapperPlayServerNamedEntitySpawn spawnWrapper = wrapperPlayerSpawn(player, newUUID);
				
				//send remove packets
				for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
					try {
						if(p == player) continue;
						
						p.sendPacket(destroyWrapper.getHandle());
						p.sendPacket(removeWrapper.getHandle());
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
				
				addNickName(player, nickName);
				
				//send add packets
				for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
					try {
						p.sendPacket(teamWrapper.getHandle());
						
						if(p == player) {
							p.sendPacket(addWrapper.getHandle());
						}
						else {
							p.sendPacket(addWrapper.getHandle());
						}
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
				
				//Send spawn packet later as they won't work within the same tick
				Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
							try {
								if(p == player) continue;
								
								p.sendPacket(spawnWrapper.getHandle());
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}
					}
				}, 10);
				
				Bukkit.getPluginManager().callEvent(new PlayerNickEvent(player));
				
				player.sendMessage(ChatColor.RED + "This is a serious feature. You should not mess with it and troll players etc. Failing to do so will result in punishments.");
				player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + "Nicknamed " + ChatColor.UNDERLINE + ChatColor.DARK_AQUA + name + ChatColor.WHITE + "! " + (skinName != name && skinName != null ? "skin of: " + ChatColor.GREEN + skinName + ChatColor.WHITE + "." : ""));
				

			}
		});
		return true;
	}
	
	public static boolean isPlayerNicked(LythrionPlayer player) {
		return hiddenUUIDs.containsKey(player.getId());
	}
	
	public static void unnickPlayer(LythrionPlayer player) {
		NickName hiddenId = hiddenUUIDs.get(player.getId());
		
		removeNickName(player);
		player.setNickName(null);
		
		player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + "Nickname " + ChatColor.RED + "off" + ChatColor.WHITE + "!");
		
		//create remove nickname from tab packet
		WrapperPlayServerPlayerInfo removeWrapper = wrapperPlayerInfo(PlayerInfoAction.REMOVE_PLAYER, hiddenId.getProfile());
		//create add normal name to tab packet
		WrapperPlayServerPlayerInfo addWrapper = wrapperPlayerInfo(PlayerInfoAction.ADD_PLAYER, WrappedGameProfile.fromHandle(player.getProfile()));
		//despawn nicked player packet
		WrapperPlayServerEntityDestroy despawnWrapper = wrapperPlayerDespawn(player.getEntityId());
		//spawn normal player packet
		WrapperPlayServerNamedEntitySpawn spawnWrapper = wrapperPlayerSpawn(player, player.getId());
		//Add player back to correct team
		WrapperPlayServerScoreboardTeam teamWrapper = wrapperScoreboardTeam(player.getRank() + "_rank", Mode.PLAYERS_ADDED, player.getCurrentName());
		
		//send all packets
		for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			try {
				if(p == player) {
					p.sendPacket(removeWrapper.getHandle());
					p.sendPacket(addWrapper.getHandle());
				}
				else {
					p.sendPacket(despawnWrapper.getHandle());
					p.sendPacket(removeWrapper.getHandle());
					p.sendPacket(addWrapper.getHandle());
					p.sendPacket(spawnWrapper.getHandle());
				}
				
				p.sendPacket(teamWrapper.getHandle());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
		//Send spawn packet later as they won't work within the same tick
		Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				for(LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
					try {
						if(p == player) continue;
						
						p.sendPacket(spawnWrapper.getHandle());
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}, 10);
		
		DatabaseLib.disableNickName(player.getId());
		Bukkit.getPluginManager().callEvent(new PlayerUnnickEvent(player));
	}
	
	public static NickName getNickNameFor(UUID id) {
		return hiddenUUIDs.get(id);
	}

	public static String getActualName(String hiddenName) {
		return actualNames.get(hiddenName);
	}
	
	private static void removeNickName(LythrionPlayer player) {
		NickName nickName = hiddenUUIDs.remove(player.getId());
		actualNames.remove(nickName.getName());
		hiddenNames.remove(player.getName());
	}
	
	private static void addNickName(LythrionPlayer player, NickName nickName) {
		hiddenUUIDs.put(player.getId(), nickName);
		actualNames.put(nickName.getName(), player.getName());
		hiddenNames.put(player.getName(), nickName.getName());
	}
	
	public static String generateName() {
		String name = "";
		
		//first find a first and second name part
		name += nameFirstpart[Utils.random(nameFirstpart.length)];
		String secondPart = nameSecondpart[Utils.random(nameSecondpart.length)];
		
		if(secondPart.length() + name.length() < 16 && Utils.random(20) == 0) name += "_" + secondPart;
		else name += secondPart;
		
		if(name.length() <= 16) {
			String postfix = namePostfixes[Utils.random(namePostfixes.length)];
			if(postfix.length() + name.length() <= 16 && Utils.random(2) == 0) name += postfix;
			
			String prefix = namePrefixes[Utils.random(namePrefixes.length)];
			if(prefix.length() + name.length() <= 16 && Utils.random(2) == 0) name = prefix + name;
		}
		else name = name.substring(0, 15);
		
		return name;
	}
	
	//packet creators
	public static WrapperPlayServerScoreboardTeam wrapperScoreboardTeam(Rank rank, int mode, String... names) {
		return wrapperScoreboardTeam(rank.toString(), mode, names);
	}
	
	public static WrapperPlayServerScoreboardTeam wrapperScoreboardTeam(String teamName, int mode, String... names) {
		WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
		wrapper.setName(teamName);
		wrapper.setMode(mode);
		wrapper.setPlayers(Arrays.asList(names));
		
		return wrapper;
	}
	
	public static WrapperPlayServerPlayerInfo wrapperPlayerInfo(PlayerInfoAction action, WrappedGameProfile profile) {
		WrapperPlayServerPlayerInfo wrapper = new WrapperPlayServerPlayerInfo();
		wrapper.setAction(action);
		
		//WrappedGameProfile profile  = new WrappedGameProfile(uuid, name);
		//if(action == PlayerInfoAction.ADD_PLAYER) {
		//	profile.getProperties().removeAll("textures");
		//	profile.getProperties().putAll("textures", createTextures("Jeroenimoo0"));
		//}
		
		PlayerInfoData data = new PlayerInfoData(profile, 0, NativeGameMode.SURVIVAL, null);
		
		wrapper.setData(Arrays.asList(data));
		
		return wrapper;
	}
	
	public static WrapperPlayServerEntityDestroy wrapperPlayerDespawn(int... ids) {
		WrapperPlayServerEntityDestroy wrapper = new WrapperPlayServerEntityDestroy();
		wrapper.setEntityIds(ids);
		
		return wrapper;
	}
	
	private static final int METADATA_SKIN_SETTINGS = 10;
	public static WrapperPlayServerNamedEntitySpawn wrapperPlayerSpawn(LythrionPlayer player, UUID newUUID) {
		WrapperPlayServerNamedEntitySpawn wrapper = new WrapperPlayServerNamedEntitySpawn();
		wrapper.setEntityID(player.getEntityId());
		wrapper.setPlayerUUID(newUUID);
		
		Location loc = player.getLocation();
		wrapper.setX(loc.getBlockX());
		wrapper.setY(loc.getBlockY());
		wrapper.setZ(loc.getBlockZ());
		wrapper.setYaw((byte) (loc.getYaw() * 256.0f / 360.0f));
		wrapper.setPitch((byte) (loc.getPitch() * 256.0f / 360.0f));
		
		List<WrappedWatchableObject> values = new ArrayList<>();
		values.add(new WrappedWatchableObject(METADATA_SKIN_SETTINGS, (byte)127));
		wrapper.setMetadata(WrappedDataWatcher.getEntityWatcher(player.getBukkitPlayer()));
		
		return wrapper;
	}
	
	@SuppressWarnings("deprecation")
	public static Collection<WrappedSignedProperty> createTextures(String name) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(name);
		
		GameProfile handle = new GameProfile(player.getUniqueId(), name);
		Object sessionService = getSessionService();
		try {
			Method method = getFillMethod(sessionService);
			method.invoke(sessionService, handle, true);
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		} catch (InvocationTargetException ex) {
			ex.printStackTrace();
			return null;
		}
		
		return WrappedGameProfile.fromHandle(handle).getProperties().get("textures");
	}
	
	private static Future<WrappedGameProfile> createGameProfile(String name, UUID uuid, String skinName) {
		return LythrionLib.getExecutorPool().submit(new Callable<WrappedGameProfile>() {

			@Override
			public WrappedGameProfile call() throws Exception {
				WrappedGameProfile profile = new WrappedGameProfile(uuid, name);
				if(skinName == null) return profile;
				
				Collection<WrappedSignedProperty> data = textures.get(skinName.toLowerCase());
				if(data == null) data = createTextures(skinName);
				if(data == null) return profile;
				
				textures.put(skinName.toLowerCase(), data);
				
				profile.getProperties().removeAll("textures");
				profile.getProperties().putAll("textures", data);
				
				return profile;
			}
		});
	}
	
	private static Object getSessionService() {
		Server server = Bukkit.getServer();
		try {
			Object mcServer = server.getClass().getDeclaredMethod("getServer")
					.invoke(server);
			for (Method m : mcServer.getClass().getMethods()) {
				if (m.getReturnType().getSimpleName()
						.equalsIgnoreCase("MinecraftSessionService")) {
					return m.invoke(mcServer);
				}
			}
		} catch (Exception ex) {
			throw new IllegalStateException(
					"An error occurred while trying to get the session service",
					ex);
		}
		throw new IllegalStateException("No session service found :o");
	}

	private static Method getFillMethod(Object sessionService) {
		for (Method m : sessionService.getClass().getDeclaredMethods()) {
			if (m.getName().equals("fillProfileProperties")) {
				return m;
			}
		}
		throw new IllegalStateException(
				"No fillProfileProperties method found in the session service :o");
	}
	
	public static NickName getNickName(final UUID id) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT uuid,name,skin FROM player_nick WHERE id_player='" + id + "' AND active=1");
			if(!rs.next()) return null;
			
			String name = rs.getString("name");
			UUID uuid = UUID.fromString(rs.getString("uuid"));
			String skin = rs.getString("skin");
			
			//TODO FIX MEH
			return new NickName(name, uuid, createGameProfile(name, uuid, skin).get(), skin);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
