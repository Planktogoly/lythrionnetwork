package net.planckton.lib.nick;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import com.comphenix.protocol.wrappers.WrappedGameProfile;

@AllArgsConstructor
public class NickName {
	@Getter @NonNull private String name;
	@Getter @NonNull private UUID id;
	@Getter @NonNull private WrappedGameProfile profile;
	@Getter private String skinName;
}
