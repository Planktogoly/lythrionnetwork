package net.planckton.lib.kommand;

import java.util.Set;

import org.bukkit.ChatColor;

import com.google.common.base.Joiner;

import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Used to list a {@link Set<String>}.
 * 
 * @author Jeroenimoo0
 *
 */
public class SubKommandList extends StaticSubKommand implements OnKommand {
	private String message;
	private Set<String> values;
	private String joinOn;
	
	/**
	 * 
	 * @param message The message to prefix the values with.
	 * @param values The values to be displayed.
	 */
	public SubKommandList(String message, Set<String> values) {
		this(message, values, ChatColor.GRAY + ", " + ChatColor.GOLD);
	}
	
	/**
	 * 
	 * @param message The message to prefix the values with.
	 * @param values The values to be displayed.
	 * @param joinOn The value used to join the values with.
	 */
	public SubKommandList(String message, Set<String> values, String joinOn) {
		super("list");
		
		this.message = message;
		this.values = values;
		this.joinOn = joinOn;
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		player.sendMessage(message + Joiner.on(joinOn).join(values));
	}
}
