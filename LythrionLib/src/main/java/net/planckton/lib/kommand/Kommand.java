package net.planckton.lib.kommand;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import com.google.common.collect.Iterables;

/**
 * 
 * This class is used as the link between {@link TabExecutor} and the
 * {@link SubKommand} System.
 * 
 * @author Jeroenimoo0
 *
 */
public abstract class Kommand extends StaticSubKommand implements TabExecutor {
	@Getter private String[] aliases;
	@Getter private LythrionPlayer sender;
	
	/**
	 * @param command The base command for this kommand
	 */
	public Kommand(String command) {
		super(command);
	}
	
	public Kommand(String... aliases) {
		super(aliases[0]);
		
		this.aliases = aliases;
	}
	
	/**
	 * Handles the Bukkit {@link TabExecutor} and converts it into a {@link SubKommand}
	 */
	@Override
	public final boolean onCommand(CommandSender cs, Command cmd, String alias, String[] arg) {
		if(!(cs instanceof Player)) {
			cs.sendMessage("Command \'" + alias + "\' is player only");
			return true;
		}
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) cs);
		if(!hasPermission(player)) {
			player.sendMessage(ChatColor.RED + "No permission for this command.");
			return true;
		}
		
		sender = player;
		
		for(Precondition precondition : getPreconditions()) {
			if(precondition.canUse(player)) continue;
			
			player.sendMessage(precondition.getReason());
			return true;
		}
		
		if(arg.length == 0 || getSubCommands() == null) {
			if(this instanceof OnKommand) ((OnKommand)this).onCommand(player, arg);
			else onUnkownSubCommand(player);
			return true;
		}
		
		for(SubKommand command : getSubCommands()) {
			if(!command.isSubCommand(player, arg[0])) continue;
			
			command.continueCommand(player, arg, 0);
			return true;
		}
		
		onUnkownSubCommand(player);
		return true;
	}
	
	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		return null;
	}
	
	/**
	 * Empty list used for empty responses.
	 */
	private static final LinkedList<String> EMPTY_LIST = new LinkedList<>();
	
	/**
	 * Handles the Bukkit {@link TabExecutor} tab complete and converts it into 
	 * a {@link SubKommand} tab complete.
	 */
	@Override
	public final List<String> onTabComplete(CommandSender cs, Command cmd, String alias, String[] arg) {
		if(!(cs instanceof Player)) {
			cs.sendMessage("Command \'" + alias + "\' is player only");
			return EMPTY_LIST;
		}
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) cs);

		if(arg.length == 0 || !hasPermission(player)) {
			return EMPTY_LIST;
		}
		
		Set<SubKommand> subCommands = getSubCommands();
		if(subCommands == null) return EMPTY_LIST;
		
		Set<Set<String>> tabLists = new HashSet<>();
		for(SubKommand command : subCommands) {
			if(!command.couldSubCommand(player, arg[0])) continue;
			
			Set<Set<String>> lists = command.continueTab(player, arg, 0);
			if(lists == null) continue;
			tabLists.addAll(lists);
		}
		
		
		if(tabLists.size() == 0) return EMPTY_LIST;
	
		LinkedList<String> finalList = new LinkedList<>();
		if(tabLists.size() == 1) {
			finalList.addAll(Iterables.get(tabLists, 0));
			
			return finalList;
		}
		
		for(Set<String> list : tabLists) {
			finalList.addAll(list);
		}
		
		return finalList;
	}
	
	@Override
	public Kommand addPreCondition(@NonNull Precondition precondition) {
		super.addPreCondition(precondition);
		
		return this;
	}
}
