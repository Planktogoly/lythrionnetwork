package net.planckton.lib.kommand;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import net.planckton.lib.player.LythrionPlayer;

public class AliasSubKommand extends VarSubKommand {
	private String name;
	private Set<String> aliases;
	
	public AliasSubKommand(String... names) {
		name = names[0];
		
		aliases = new LinkedHashSet<>(Arrays.asList(names));
	}
	
	@Override
	protected boolean matches(LythrionPlayer player, String subCommand) {
		for(String alias : aliases) if(alias.equalsIgnoreCase(subCommand)) return true;
		return false;
	}
	
	@Override
	protected boolean startsWith(LythrionPlayer player, String subCommand) {
		for(String alias : aliases) if(alias.toLowerCase().startsWith(subCommand.toLowerCase())) return true;
		return false;
	}
	
	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		Set<String> values = new HashSet<>();
		
		for(String alias : aliases) {
			if(alias.toLowerCase().startsWith(getArgument(arg))) {
				values.add(alias);
				break;
			}
		}
		
		return values;
	}
	
	@Override
	protected String onUsage(LythrionPlayer player, boolean optional) {
		if(optional) return "[" + name + "]";
		
		return "<" + name + ">";
	}
}
