package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * A {@link VarSubKommand} is a {@link SubKommand} which has more
 * than one option and is used to be a leaf kommand or used to 
 * forward this kommand.
 * <p>
 * Together with {@link StaticSubKommand} these are the two types of
 * Usable {@link SubKommand}'s.
 * 
 * @author Jeroenimoo0
 * 
 */
public abstract class VarSubKommand extends SubKommand {
	/**
	 * Tests whether the given subCommand matches this {@link VarSubKommand}.
	 * 
	 * @param subCommand The command to test for.
	 * @return {@code true} if the subCommand matches, else {@code false}.
	 */
	protected abstract boolean matches(LythrionPlayer player, String subCommand);
	
	/**
	 * Tests whether the given subCommand could be this {@link VarSubKommand}.
	 * 
	 * @param subCommand The command to test for.
	 * @return {@code true} if the subCommand could be this {@link VarSubKommand}, else {@code false}.
	 */
	protected abstract boolean startsWith(LythrionPlayer player, String subCommand);
	
	@Override
	public boolean isSubCommand(LythrionPlayer player, String command) {
		return matches(player, command);
	}
	
	@Override
	public boolean couldSubCommand(LythrionPlayer player, String command) {
		return startsWith(player, command);
	}
}
