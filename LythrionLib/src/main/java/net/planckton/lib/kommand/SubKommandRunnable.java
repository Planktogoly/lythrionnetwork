package net.planckton.lib.kommand;

import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;


public class SubKommandRunnable extends StaticSubKommand implements OnKommand {
	private Runnable runnable;
	
	public SubKommandRunnable(@NonNull String command, @NonNull Runnable runnable) {
		super(command);
		
		this.runnable = runnable;
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		runnable.run();
	}
}
