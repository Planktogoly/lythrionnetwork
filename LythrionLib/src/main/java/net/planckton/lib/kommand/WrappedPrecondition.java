package net.planckton.lib.kommand;

import lombok.AllArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;

@AllArgsConstructor
public class WrappedPrecondition implements Precondition {
	private String reason;
	private Precondition precondition;

	@Override
	public boolean canUse(LythrionPlayer player) {
		return precondition.canUse(player);
	}
	
	@Override
	public String getReason() {
		return reason;
	}
}
