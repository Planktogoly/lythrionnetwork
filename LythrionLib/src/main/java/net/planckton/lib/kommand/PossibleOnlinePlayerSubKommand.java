package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;


/**
 * 
 * Used for a {@link PlayerListSubKommand} with the comparison methods
 * already implemented. And using the online players as {@link Set<LythrionPlayer>}.
 * 
 * @author Jeroenimoo0
 * 
 */
public class PossibleOnlinePlayerSubKommand extends OnlinePlayerSubKommand {
	
	public PossibleOnlinePlayerSubKommand() {
		
	}
	
	public PossibleOnlinePlayerSubKommand(SubKommand... kommands) {
		for(SubKommand kommand : kommands) addSubCommand(kommand);
	}
	
	@Override
	protected boolean matches(LythrionPlayer player, String subCommand) {
		return true;
	}
	
	@Override
	protected boolean startsWith(LythrionPlayer player, String subCommand) {
		return true;
	}
}
