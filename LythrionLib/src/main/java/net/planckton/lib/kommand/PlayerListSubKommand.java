package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Used for a {@link SetSubKommand<LythrionPlayer>} with the comparison methods
 * already implemented.
 * 
 * @author Jeroenimoo0
 * 
 */
public abstract class PlayerListSubKommand extends SetSubKommand<LythrionPlayer> {

	public PlayerListSubKommand() {
		super("player");
	}

	@Override
	protected boolean matches(LythrionPlayer player, LythrionPlayer value, String command) {
		return value.getCurrentName().equalsIgnoreCase(command);
	}

	@Override
	protected boolean startsWith(LythrionPlayer player, LythrionPlayer value, String command) {
		return value.getCurrentName().toLowerCase().startsWith(command.toLowerCase());
	}
	
	@Override
	protected String valueToString(LythrionPlayer value) {
		return value.getCurrentName();
	}
}
