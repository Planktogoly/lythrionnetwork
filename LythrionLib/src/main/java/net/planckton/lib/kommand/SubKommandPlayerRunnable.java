package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.PlayerRunnable;


public class SubKommandPlayerRunnable extends StaticSubKommand implements OnKommand {
	private PlayerRunnable runnable;
	
	public SubKommandPlayerRunnable(String command, PlayerRunnable runnable) {
		super(command);
		
		this.runnable = runnable;
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		runnable.run(player);
	}
}
