package net.planckton.lib.kommand;

import org.bukkit.ChatColor;

import net.planckton.lib.player.LythrionPlayer;

public interface Precondition {
	public boolean canUse(LythrionPlayer player);
	
	default public String getReason() {
		return ChatColor.RED + "You currently can't use this command.";
	}
}
