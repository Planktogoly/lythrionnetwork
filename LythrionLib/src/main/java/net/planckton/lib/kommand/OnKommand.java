package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Used to make a {@link SubKommand} executable. Leaf commands must implement this interface.
 * 
 * @author Planckton
 *
 */
public interface OnKommand {
	/**
	 * Called when a {@link SubKommand} is called.
	 * 
	 * @param player The {@link LythrionPlayer} who issues the command.
	 * @param arg The arguments used. The last argument belonging to this {@link SubKommand}
	 */
	void onCommand(LythrionPlayer player, String[] arg);
}
