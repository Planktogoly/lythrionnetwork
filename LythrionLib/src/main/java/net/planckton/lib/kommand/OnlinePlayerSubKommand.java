package net.planckton.lib.kommand;

import java.util.Collection;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

/**
 * 
 * Used for a {@link PlayerListSubKommand} with the comparison methods
 * already implemented. And using the online players as {@link Set<LythrionPlayer>}.
 * 
 * @author Jeroenimoo0
 * 
 */
public class OnlinePlayerSubKommand extends PlayerListSubKommand {
	protected boolean allowSelf;
	
	public OnlinePlayerSubKommand() {
		this(false);
	}
	
	public OnlinePlayerSubKommand(SubKommand... kommands) {
		for(SubKommand kommand : kommands) addSubCommand(kommand);
	}
	
	/**
	 * @param allowSelf Whether or not the player can have itself as target.
	 */
	public OnlinePlayerSubKommand(boolean allowSelf) {
		this.allowSelf = allowSelf;
	}
	
	@Override
	protected Collection<LythrionPlayer> getValues(LythrionPlayer player) {
		return PlayerManager.getOnlineLythrionPlayers();
	}
	
	protected LythrionPlayer getPlayer(LythrionPlayer player, String[] arg) {
		LythrionPlayer target = PlayerManager.getLythrionPlayer(arg[getArgumentIndex()]);
		if(target == null) return null;
		if(!allowSelf && player == target) return null;
		
		return target;
	}
	
	@Override
	public boolean shouldAddToTab(LythrionPlayer player, LythrionPlayer value) {
		return player != value;
	}
}
