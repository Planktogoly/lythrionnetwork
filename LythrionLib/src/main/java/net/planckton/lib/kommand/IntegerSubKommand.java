package net.planckton.lib.kommand;

import java.util.Set;

import lombok.AllArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;

@AllArgsConstructor
public class IntegerSubKommand extends VarSubKommand {
	private String name;
	
	@Override
	protected boolean matches(LythrionPlayer player, String subCommand) {
		return subCommand.matches("[0-9]{1,}");
	}

	@Override
	protected boolean startsWith(LythrionPlayer player, String subCommand) {
		return subCommand.matches("[0-9]{1,}");
	}

	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		return null;
	}

	@Override
	protected String onUsage(LythrionPlayer player, boolean optional) {
		return "<" + name + ">";	
	}

}
