package net.planckton.lib.kommand;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * A StaticSubKommand is a {@link SubKommand} which only has
 * one option and is used to be a leaf kommand or used to 
 * forward this kommand.
 * <p>
 * Together with {@link StaticSubKommand} these are the two types of
 * Usable {@link SubKommand}'s.
 * 
 * @author Jeroenimoo0
 * 
 */
public class StaticSubKommand extends SubKommand {
	@Getter @NonNull private String command;
	private Set<String> set = new HashSet<>();
	
	/**
	 * @param command the static name of this {@link SubKommand}
	 */
	public StaticSubKommand(String command) {
		this.command = command;
		set.add(command);
	}
	
	/**
	 * Creates the usage information for this {@link SubKommand}
	 */
	@Override
	protected String onUsage(LythrionPlayer player, boolean optional) {
		if(optional) return "[" + command + "]";
		
		return command;
	}
	
	@Override
	public boolean isSubCommand(LythrionPlayer player, String command) {
		return this.command.equalsIgnoreCase(command);
	}
	
	@Override
	public boolean couldSubCommand(LythrionPlayer player, String command) {
		return this.command.startsWith(command);
	}
	
	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		return set;
	}
}
