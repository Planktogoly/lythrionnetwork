package net.planckton.lib.kommand;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Getter;
import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Used to allow a variable amount of arguments.
 * 
 * @author Jeroenimoo0
 *
 * @param <T> The type elements stores in the set.
 */
public abstract class SetSubKommand<T> extends VarSubKommand {
	@Getter(AccessLevel.PROTECTED) private String name;
	
	/**
	 * @param name The name used for usage information.
	 */
	public SetSubKommand(String name) {
		this.name = name;
	}
	
	/**
	 * @return The values used to compare.
	 */
	protected abstract Collection<T> getValues(LythrionPlayer player);
	
	/**
	 * Test if the value of type T matches the command.
	 * 
	 * @param value The value of type T to test for.
	 * @param command The command to test with.
	 * @return {@code true} if the command matches the value of type T, else {@code false}
	 */
	protected abstract boolean matches(LythrionPlayer player, T value, String command);
	
	/**
	 * Test if the value of type T starts with the command.
	 * 
	 * @param value The value of type T to test for.
	 * @param command The command to test with.
	 * @return {@code true} if the command matches the value of type T, else {@code false}
	 */
	protected abstract boolean startsWith(LythrionPlayer player, T value, String command);
	
	@Override
	protected boolean matches(LythrionPlayer player, String subCommand) {
		Collection<T> values = getValues(player);
		if(values == null) return false;
		
		for(T value : values) {
			if(matches(player, value, subCommand)) return true;
		}
		
		return false;
	}
	
	@Override
	protected boolean startsWith(LythrionPlayer player, String subCommand) {
		Collection<T> values = getValues(player);
		if(values == null) return false;
		
		for(T value : values) {
			if(startsWith(player, value, subCommand)) return true;
		}
		
		return false;
	}
	
	/**
	 * Transform the value into a {@link String}.
	 * Default behavior is using the toString() method.
	 * 
	 * @param value The value to be transformed
	 * @return The value transformed into a string
	 */
	protected String valueToString(T value) {
		return value.toString();
	}

	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		Collection<T> values = getValues(player);
		if(values == null) return null;

		Set<String> set = new LinkedHashSet<>();
		
		for(T value : values) {
			if(!startsWith(player, value, arg[arg.length - 1])) continue;
			if(!shouldAddToTab(player, value)) continue;
			
			set.add(valueToString(value));
		}
		
		return set;
	}
	
	@Override
	protected String onUsage(LythrionPlayer player, boolean optional) {
		if(optional) return "[" + name + "]";
		
		return "<" + name + ">";
	}
	
	public boolean shouldAddToTab(LythrionPlayer player, T value) {
		return true;
	}
}
