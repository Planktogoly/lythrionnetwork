package net.planckton.lib.kommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Main class in the Kommand API. This command is the base class  
 * of many classes used in this API.
 * 
 * @author Jeroenimoo0
 *
 */
public abstract class SubKommand {
	@Getter(AccessLevel.PROTECTED) 
	private Set<SubKommand> subCommands;
	
	@Getter(AccessLevel.PROTECTED) 
	@Setter(AccessLevel.PRIVATE) private SubKommand parent;
	
	@Getter(AccessLevel.PROTECTED) 
	/**
	 * @param requiredRank The {@link Rank} required for this {@link SubKommand}.
	 */
	@Setter(AccessLevel.PROTECTED) 
	private net.planckton.lib.rank.Rank requiredRank;
	
	@Getter(AccessLevel.PROTECTED) 
	/**
	 * @param requiredPermisson The permissions required for this {@link SubKommand}.
	 */
	@Setter(AccessLevel.PROTECTED) 
	private String requiredPermissions;
	
	/**
	 * Indicates at what index this {@link SubKommand} is. The base {@link Kommand} is 0
	 */
	@Getter
	private int index;
	
	/**
	 * An list of {@link Precondition} interfaces which have to be met in order
	 * To use this {@link SubKommand} or continue though this {@link SubKommand}.
	 */
	private List<Precondition> preconditions = new ArrayList<>();
	
	public SubKommand addSubCommand(@NonNull SubKommand command) {
		if(subCommands == null) subCommands = new HashSet<>();
		
		subCommands.add(command);
		command.setParent(this);
		command.setIndex(index + 1);
		if(requiredPermissions != null && command.getRequiredPermissions() == null) command.setRequiredPermissions(requiredPermissions);
		if(requiredRank != null && (command.getRequiredRank() == null || !command.getRequiredRank().hasPower(requiredRank))) command.setRequiredRank(requiredRank);
		
		return this;
	}
	
	/**
	 * Checks whether the given command matches this sub {@link SubKommand}.
	 * 
	 * @param command the command to test for.
	 * @return {@code true} if the command matches this {@link SubKommand}, else {@code false}.
	 */
	protected abstract boolean isSubCommand(LythrionPlayer player, String command);
	
	/**
	 * Checks whether the given command could be this {@link SubKommand}.
	 * 
	 * @param command the command to test for.
	 * @return {@code true} if the command could be this {@link SubKommand}, else {@code false}.
	 */
	protected abstract boolean couldSubCommand(LythrionPlayer player, String command);
	
	/**
	 * Gets all the possible {@link SubKommand} options for this {@link SubKommand}.
	 * 
	 * @param player The {@link LythrionPlayer} requesting all options.
	 * @param arg the arguments supplied so far. The last one being part of this {@link SubKommand}.
	 * @return A {@link Set} containing all possible options based on the supplied arguments.
	 */
	protected abstract Set<String> onTab(LythrionPlayer player, String[] arg);
	
	/**
	 * Return how this command will be displayed within the usage information.
	 * 
	 * @param optional whether or not this command is optional.
	 * @return the {@link String} how this {@link SubKommand} will be displayed in the usage options.
	 */
	protected abstract String onUsage(LythrionPlayer player, boolean optional);
	
	/**
	 * Messages this player that this command is unknown and supplies usage information.
	 * 
	 * @param player The {@link LythrionPlayer} who issues the command
	 */
	protected void onUnkownSubCommand(@NonNull LythrionPlayer player) {
		for(String usage : generateUsage(player)) {
			player.sendMessage(ChatColor.RED + "Usage: /" + usage);
		}
	}
	
	/**
	 * Generates the usage history so far.
	 * 
	 * @return The usage history so far.
	 */
	protected String generateHistoryUsage(LythrionPlayer player) {
		SubKommand parent = getParent();
		
		String message;
		if(parent != null) message = parent.generateHistoryUsage(player) + " ";
		else message = onUsage(player, false);
		
		return message;
	}
	
	/**
	 * Generates all future usages. As there may be multiple {@link SubKommand}'s
	 * A list of usage options will be generated.
	 * 
	 * @return All usage options from this {@link SubKommand}.
	 */
	protected List<String> generateUsage(LythrionPlayer player) {
		if(parent == null) {
			List<String> usages = new ArrayList<String>();
			if(this instanceof OnKommand) usages.add(onUsage(player, false));
			return generateUsage(player, "", usages, false);
		}
		
		return generateUsage(player, generateHistoryUsage(player), new ArrayList<String>(), false);
	}
	
	/**
	 * Generates all future usages. As there may be multiple {@link SubKommand}'s
	 * A list of usage options will be generated. This command is recursive and 
	 * calls all  {@link SubKommand} children until it reaches a Leaf kommand 
	 * and will then create the usage string for this path.
	 * 
	 * @param start The usage so far.
	 * @param list The list in which all usage options will be supplied.
	 * @param optional Whether this {@link SubKommand} is optional.
	 * @return All usage options from this {@link SubKommand}.
	 */
	protected List<String> generateUsage(@NonNull LythrionPlayer player, @NonNull String start, @NonNull List<String> list, boolean optional) {
		String newStart = start + onUsage(player, optional);
		
		if(isLeafCommand()) {
			list.add(newStart);
		}
		else {
			if(this instanceof OnKommand && parent != null) optional = true;
			
			for(SubKommand command : getSubCommands()) {
				command.generateUsage(player, newStart + " ", list, optional);
			}
		}
		
		return list;
	}
	
	/**
	 * Continue the command forward into the kommand tree.
	 * 
	 * @param player The {@link LythrionPlayer} who issued the kommand.
	 * @param arg The current kommand arguments.
	 * @param index The current index. Also the last argument.
	 */
	protected void continueCommand(@NonNull LythrionPlayer player, @NonNull String[] arg, int index) {
		if(!hasPermission(player)) {
			player.sendMessage(ChatColor.RED + "No permission for this command.");
			return;
		}
		
		for(Precondition precondition : preconditions) {
			if(precondition.canUse(player)) continue;
			
			player.sendMessage(precondition.getReason());
			return;
		}
		
		if(index + 1 == arg.length || getSubCommands() == null) {
			if(this instanceof OnKommand) ((OnKommand)this).onCommand(player, arg);
			else onUnkownSubCommand(player);
			
			return;
		}
		
		index++;
		for(SubKommand command : getSubCommands()) {
			if(!command.isSubCommand(player, arg[index])) continue;
			command.continueCommand(player, arg, index);
			return;
		}
		
		player.sendMessage(ChatColor.RED + "Wrong usage for " + onUsage(player, false) + " with '" + arg[index] + "'");
		onUnkownSubCommand(player);
	}
	
	/**
	 * Continue the tab request forward into the kommand tree.
	 * 
	 * @param player The {@link LythrionPlayer} who issued the kommand.
	 * @param arg The current kommand arguments.
	 * @param index The current index. Also the last argument.
	 * @return A {@link Set<String>} containing all {@link SubKommand} options.
	 */
	protected Set<Set<String>> continueTab(@NonNull LythrionPlayer player, @NonNull String[] arg, int index) {
		if(!hasPermission(player)) return null;
		
		for(Precondition precondition : preconditions) {
			if(precondition.canUse(player)) continue;
			
			//player.sendMessage(precondition.getReason());
			return null;
		}
		
		Set<Set<String>> set = new HashSet<>();
		
		if(index + 1 == arg.length) {
			Set<String> tab = onTab(player, arg);
			if(tab == null) return null;
			
			set.add(tab);
			if(set.size() == 0) return null;
			
			return set;
		}
		
		if(getSubCommands() == null) return null;
		
		index++;
		for(SubKommand command : getSubCommands()) {
			if(!command.couldSubCommand(player, arg[index])) continue;
			
			set.addAll(command.continueTab(player, arg, index));
		}
		
		if(set.size() == 0) return null;
		
		return set;
	}
	
	/**
	 * @return {@code true} if this command is a leaf kommand, else {@code false}.
	 */
	protected boolean isLeafCommand() {
		return subCommands == null || subCommands.size() == 0;
	}
	
	protected boolean hasPermission(@NonNull LythrionPlayer player) {
		return (requiredRank == null || player.hasRankPower(requiredRank)) && (requiredPermissions == null || player.hasPermission(requiredPermissions));
	}
	
	/**
	 * Adds the {@link Precondition} to the precondition list.
	 * 
	 * @param precondition The {@link Precondition} to be added.
	 */
	public SubKommand addPreCondition(@NonNull Precondition precondition) {
		preconditions.add(precondition);
		
		return this;
	}
	
	/**
	 * Returns the index which corresponds to the position in of
	 * the argument meant for this {@link SubKommand} in the
	 * arguments array.
	 * 
	 * @return Index for the arguments array
	 */
	public int getArgumentIndex() {
		return index - 1;
	}
	
	/**
	 * Returns the argument which belongs to the supplied
	 * arguments array.
	 * 
	 * @param args Arguments supplies by the {@link OnKommand} executor.
	 */
	public String getArgument(@NonNull String[] args) {
		return args[getArgumentIndex()];
	}
	
	/**
	 * Sets the index in the current {@link SubKommand} tree.
	 * 
	 * @param index Index in the current {@link SubKommand} tree.
	 */
	protected void setIndex(int index) {
		this.index = index;
		
		if(subCommands == null) return;
		for(SubKommand kommand : subCommands) {
			kommand.setIndex(index + 1);
		}
	}
	
	/**
	 * @return The preconditions required for this {@link SubKommand}.
	 */
	public List<Precondition> getPreconditions() {
		return Collections.unmodifiableList(preconditions);
	}
}
