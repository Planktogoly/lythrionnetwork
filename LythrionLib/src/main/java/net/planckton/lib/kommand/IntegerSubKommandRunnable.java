package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.RunnableParam;

public class IntegerSubKommandRunnable extends IntegerSubKommand implements OnKommand {
	private RunnableParam<Integer> runnable;
	
	public IntegerSubKommandRunnable(String name, RunnableParam<Integer> runnable) {
		super(name);
		
		this.runnable = runnable;
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		runnable.run(Integer.parseInt(getArgument(arg)));
	}
}
