package net.planckton.lib.kommand;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.RunnableParam;

public class StringSubKommandRunnable extends StringSubKommand implements OnKommand {
	private RunnableParam<String> runnable;
	
	public StringSubKommandRunnable(String name, RunnableParam<String> runnable) {
		super(name);
		
		this.runnable = runnable;
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		runnable.run(getArgument(arg));
	}
}
