package net.planckton.lib.kommand;

public abstract class StaticSubKommandExecutor extends StaticSubKommand implements OnKommand {

	public StaticSubKommandExecutor(String command) {
		super(command);
	}
}
