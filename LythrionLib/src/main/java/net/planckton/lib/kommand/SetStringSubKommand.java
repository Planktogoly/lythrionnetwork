package net.planckton.lib.kommand;

import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;

import lombok.Setter;
import net.planckton.lib.player.LythrionPlayer;

/**
 * 
 * Used for a {@link SetSubKommand<String>} with the comparison methods
 * already implemented.
 * 
 * @author Jeroenimoo0
 * 
 */
public abstract class SetStringSubKommand extends SetSubKommand<String> {
	@Setter private boolean ignoreCase;
	@Setter private int showAllOptionsThreshold = 3;
	
	/**
	 * @param name The name used for usage information.
	 */
	public SetStringSubKommand(String name) {
		this(name, false);
	}
	
	public SetStringSubKommand(String name, boolean ignoreCase) {
		super(name);
		
		this.ignoreCase = ignoreCase;
	}
	
	@Override
	protected boolean matches(LythrionPlayer player, String value, String subCommand) {
		if(ignoreCase) return subCommand.equalsIgnoreCase(value);
		
		return subCommand.equals(value);
	}

	@Override
	protected boolean startsWith(LythrionPlayer player, String value, String subCommand) {
		if(ignoreCase) return value.toLowerCase().startsWith(subCommand.toLowerCase());
		
		return value.startsWith(subCommand);
	}
	
	protected abstract Set<String> getValues(LythrionPlayer player);
	
	@Override
	protected Set<String> onTab(LythrionPlayer player, String[] arg) {
		ImmutableSet.Builder<String> builder = ImmutableSet.builder();
		
		for(String string : getValues(player)) {
			if(!string.toLowerCase().startsWith(getArgument(arg).toLowerCase())) continue;
		
			builder.add(string);
		}
		
		return builder.build();
	}
	
	@Override
	protected String onUsage(LythrionPlayer player, boolean optional) {
		Set<String> values = getValues(player);
		if(values == null) return "<" + getName() + ">";
		
		boolean showoptions = values.size() <= showAllOptionsThreshold;
		
		if(optional) {
			if(showoptions) {
				return "[" + Joiner.on("|").join(getValues(player)) + "]";
			}
			
			return "[" + getName() + "]";
		}
		
		if(showoptions) {
			return "<" + Joiner.on("|").join(getValues(player)) + ">";
		}
		
		return "<" + getName() + ">";
	}
}
