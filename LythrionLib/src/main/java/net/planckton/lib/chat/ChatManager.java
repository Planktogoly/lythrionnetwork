package net.planckton.lib.chat;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import lombok.Getter;
import lombok.Setter;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;
import net.planckton.lib.utils.UniChars;

public class ChatManager extends Manager implements Listener {
	public static HashMap<String, ChatChannel> chats;
	public static ChatChannel CHANNEL_GLOBAL;
	public static ChatChannel CHANNEL_ADMIN;
	
	@Getter @Setter public static boolean globalMuteEnabled;
	
	@Override
	public void onEnable() {
		chats = new HashMap<String, ChatChannel>();
		
		CHANNEL_ADMIN = create("Admin", "Admin", ChatColor.RED + UniChars.DOUBLE_LINE_UP + " ");
		CHANNEL_ADMIN.global = true;
		CHANNEL_ADMIN.chatCharacter = '!';
		CHANNEL_ADMIN.ignoresGlobalmute = true;
		
		CHANNEL_GLOBAL = create("Global", "Global");
	}

	@Override
	public void onDisable() {
		chats = null;
		CHANNEL_ADMIN = null;
	}
	
	//handle events
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if(event.isCancelled()) return;
		event.setCancelled(true);
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		
		String message = event.getMessage();
		
		ChatChannel chat = null;
		
		//First check if the message was a charCharacter message
		for(ChatChannel availableChat : player.chat.getAvailableChats()) {
			if(availableChat.chatCharacter == '\u0000') continue;
			if(availableChat.chatCharacter != message.charAt(0)) continue;
			
			message = message.substring(1, message.length());
			chat = availableChat;
			break;
		}
		
		if(chat == null) chat = player.chat.getChat();
		if(chat == null) {
			player.chat.setChatChannel(CHANNEL_GLOBAL);
			chat = player.chat.getChat();
		}
		
		if(globalMuteEnabled && !chat.ignoresGlobalmute && !player.hasRankPower(Rank.MOD) && !player.hasPermission("globalmute.ignore")) {
			event.getPlayer().sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.RED + "GlobalMute is currently enabled.");
			event.getPlayer().sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.RED + "You can still \'/msg\' players.");
			return;
		}
		
		chat.broadcast(player, message);
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.setJoinMessage("");
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event) {
		event.setQuitMessage("");
	}
	
	public static ChatChannel create(String name, String displayName) {
		return create(name, displayName, null);
	}
	
	public static ChatChannel create(String name, String displayName, String prefix) {
		return create(name, displayName, prefix, null);
	}
	
	public static ChatChannel create(String name, String displayName, String prefix, String playerPostfix) {
		return create(name, displayName, prefix, playerPostfix, null);
	}
	
	//modifiers
	public static ChatChannel create(String name, String displayName, String prefix, String playerPostfix, ChatColor playerMessageColor) {
		System.out.println("[LythrionLib] Creating new chat channel: " + name);
		ChatChannel chatChannel = new ChatChannel(name, displayName);
		
		if(prefix != null) chatChannel.prefix = prefix;
		if(playerPostfix != null) chatChannel.playerPostfix = playerPostfix;
		if(playerMessageColor != null) chatChannel.playerMessageColor = playerMessageColor;
		
		chats.put(name.toLowerCase(), chatChannel);
		
		return chatChannel;
	}
	
	//getters and setters
	public static ChatChannel get(String name) {
		return chats.get(name.toLowerCase());
	}
	
	public static boolean has(String name) {
		return chats.containsKey(name.toLowerCase());
	}

	public static void deleteChat(ChatChannel chat) {
		chats.remove(chat.chatName.toLowerCase());
	}
}
