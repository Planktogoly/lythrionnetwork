package net.planckton.lib.chat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import org.bukkit.ChatColor;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import mkremins.fanciful.FancyMessage;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.prefix.PlayerPrefix;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.UniChars;

public class ChatChannel {
	private static final String urlRegex = "(https?:\\/\\/)?[a-zA-Z0-9\\.]{1,}\\.[a-zA-Z0-9]{2,6}";
	private static final String splitRegex = "([ ](?=(" + urlRegex + ")))";
	
	public String chatName;
	public String displayName;
	public List<LythrionPlayer> listeners;
	
	public String prefix = "";
	public String playerPostfix = ChatColor.GRAY + " " + UniChars.ARROWS_RIGHT + " ";
	public ChatColor playerMessageColor = ChatColor.GRAY;
	
	public boolean global;
	public char chatCharacter;
	
	public boolean ignoresGlobalmute = false;
	
	protected ChatChannel(String chatName, String displayName) {
		this.chatName = chatName;
		this.displayName = displayName;
		listeners = Collections.synchronizedList(new ArrayList<LythrionPlayer>());
	}
	
	//special methods
	
	public void broadcast(final LythrionPlayer player, final String message) {
		final ChatChannel thisChannel = this;
		
		LythrionLib.getExecutorPool().submit(new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {
				return DatabaseLib.logChatMessage(player, message, thisChannel);
			}
		}).addListener(new GenericFutureListener<Future<Boolean>>() {

			@Override
			public void operationComplete(Future<Boolean> future)
					throws Exception {
				if(!future.isSuccess()) {
					player.sendMessage(ChatColor.RED + "Something went wrong. Try again.");
					return;
				}
				
				Boolean succes = future.get();
				if(succes == null || !succes) {
					player.sendMessage(ChatColor.RED + "Something went wrong. Try again.");
					return;
				}
				

				synchronized (listeners) {
					FancyMessage fm = new FancyMessage(prefix);
					
					Set<PlayerPrefix> prefixes = player.prefixes.getPrefixes();
					if(prefixes != null) {
						for(PlayerPrefix prefix : prefixes) {
							if(prefix.isHideWhileNicked() && player.isNicked()) continue;
							
							fm = fm.then(prefix.getPrefix() + " ");
							if(prefix.getInfo() == null) continue;
							fm.tooltip(prefix.getInfo());
						}
					}
					
					fm = fm.then(player.getDisplayNameWithRank() + ChatColor.DARK_GRAY + ": ");
						
					String[] split = message.split(splitRegex);
					for (int j = 0; j < split.length; j++) {
						if(split.length > 1 && j < split.length - 1) split[j] += " ";
						
						String[] sub = split[j].split(" ", 2);
						if(sub.length > 1) sub[0] += " ";
						
						int start = 0;
						boolean subZeroMatches = sub[0].matches(urlRegex + ".*");
						//if (((j == split.length - 1 || j == 0) && subZeroMatches) || (sub.length > 1 && subZeroMatches)) {
						if((sub.length > 1 && j != 0 && j < split.length) || ((j == split.length - 1 || j == 0) && subZeroMatches)) {
							if(sub[0].endsWith(" ")) sub[0] = sub[0].substring(0, sub[0].length() - 1);
							if(sub.length > 1) sub[1] = " " + sub[1];
							String linkString = sub[0].matches("(https?://).*") ? sub[0] : ("http://" + sub[0]);
												
							fm = fm.then(sub[0]).link(linkString).tooltip(ChatColor.RED + "Click to open!", linkString).color(playerMessageColor);
							
							if(player.hasRankPower(Rank.VIP)) {
								fm = fm.color(ChatColor.GRAY);
							}
							
							start = 1;
						}
						
						ChatColor chatColor = playerMessageColor;
						if (player.hasRankPower(Rank.JRMOD)) chatColor = ChatColor.WHITE;
						
						for(int k = start; k < sub.length; k++) {						
							fm = fm.then(sub[k]).color(chatColor);
						}
					}
			
					
					Iterator<LythrionPlayer> i = listeners.iterator();
					while (i.hasNext()) {
						LythrionPlayer p = i.next();
						if(!p.isValid()) {
							System.out.println("[ERROR] Player " + p + " was leaked in chat " + chatName);
							i.remove();
							continue;
						}
						
						p.sendMessage(fm);
					}
				}
			}
		});
		
		/*if(global) {
			String prefixString = "";
			
			Set<PlayerPrefix> prefixes = player.prefixes.getPrefixes();
			if(prefixes != null) {
				for(PlayerPrefix prefix : prefixes) {
					if(prefix.isHideWhileNicked() && player.isNicked()) continue;
					
					prefixString += prefix.getPrefix() + " ";
				}
			}
			
			
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("chat");
			out.writeUTF(chatName);
			out.writeUTF(prefixString + player.getDisplayName() + playerPostfix + playerMessageColor + message);
			
			TostiLib.sendHeartMessage("allserver", "chat", out.toByteArray());
		}*/
		
		System.out.println("[" + chatName + "] " + player.getName() + ": " + message);
	}
	
	/*public void broadcast(String message, boolean wPrefix) {
		broadcast(message, wPrefix, false);
	}
	
	public void broadcast(String message, boolean wPrefix, boolean global) {
		if(wPrefix) {
			message = prefix + message;
		}
		
		for(LythrionPlayer player : listeners) {
			player.sendMessage(message);
		}
		
		if(global && this.global) {
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("chat");
			out.writeUTF(chatName);
			out.writeUTF(message);
			
			TostiLib.sendHeartMessage("allserver", "chat", out.toByteArray());
		}
		
		System.out.println("[" + chatName + "] " + message);
	}
	
	public void broadcast(String message) {
		broadcast(message, false);
	}*/
	
	//modifiers
	public void join(LythrionPlayer player) {
		if(listeners.contains(player)) return;
		
		listeners.add(player);
	}
	
	public void leave(LythrionPlayer player) {
		listeners.remove(player);
	}

	public void broadcast(FancyMessage message) {
		broadcast(message, false);
	}
	
	public void broadcast(FancyMessage message, boolean global) {
		for(LythrionPlayer player : listeners) {
			player.sendMessage(message);
		}
		
		/*if(global && this.global) {
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("chat");
			out.writeUTF(chatName);
			out.writeUTF(((char)2) + message.toJSONString());
			
			TostiLib.sendHeartMessage("allserver", "chat", out.toByteArray());
		}*/
		
		System.out.println("[" + chatName + "] " + message.toOldMessageFormat());
	}
}
