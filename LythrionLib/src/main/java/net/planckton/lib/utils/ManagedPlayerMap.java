package net.planckton.lib.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;

import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;

public class ManagedPlayerMap<V> implements Map<UUID, V>, Listener {
	
	public static <T> Map<UUID, T> wrap(Map<UUID, T> map) {
		return new ManagedPlayerMap<T>(map, 0);
	}
	
	public static <T> Map<UUID, T> wrap(Map<UUID, T> map, int removeTime) {
		return new ManagedPlayerMap<T>(map, removeTime);
	}
	
	private Map<UUID, V> map;
	private int removeTime;
	
	private HashMap<UUID, BukkitTask> removeTasks = new HashMap<UUID, BukkitTask>();
	
	public ManagedPlayerMap(int removeTime) {
		this(new HashMap<UUID, V>(),removeTime);
	}
	
	public ManagedPlayerMap(Map<UUID,V> map, int removeTime) {
		this.map = map;
		this.removeTime = removeTime;
		
		Bukkit.getPluginManager().registerEvents(this, LythrionLib.getInstance());
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerJoin(LythrionPlayerJoinEvent event) {
		BukkitTask task = removeTasks.remove(event.getLythrionPlayer().getId());
		if(task == null) return;
		
		task.cancel();
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerQuit(LythrionPlayerQuitEvent event) {
		final UUID playerId = event.getLythrionPlayer().getId();
		
		if(removeTime == 0) {
			remove(playerId);
			return;
		}
		
		BukkitTask task = Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				if(Bukkit.getPlayer(playerId) != null) return;
				
				remove(playerId);
			}
		}, removeTime);
		
		removeTasks.put(playerId, task);
	}
	
	public void unregister() {
		HandlerList.unregisterAll(this);
	}
	
	//super methods
	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<UUID, V>> entrySet() {
		return map.entrySet();
	}

	@Override
	public V get(Object key) {
		return map.get(key);
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public Set<UUID> keySet() {
		return map.keySet();
	}

	@Override
	public V put(UUID key, V value) {
		return map.put(key, value);
	}

	@Override
	public void putAll(Map<? extends UUID, ? extends V> m) {
		map.putAll(m);
	}

	@Override
	public V remove(Object key) {
		removeTasks.remove(key);
		
		return map.remove(key);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Collection<V> values() {
		return map.values();
	}
}
