package net.planckton.lib.utils;

public interface RunnableParam<T> {
	public void run(T value);
}
