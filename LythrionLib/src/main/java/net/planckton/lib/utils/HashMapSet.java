package net.planckton.lib.utils;

import java.util.HashMap;
import java.util.HashSet;

import lombok.NonNull;

public class HashMapSet<K,V> implements MapSet<K, V> {
	private HashMap<K, HashSet<V>> map = new HashMap<K, HashSet<V>>();
	
	public void add(@NonNull K key, @NonNull V value) {
		HashSet<V> list = map.get(key);
		if(list == null) list = new HashSet<V>();
		
		list.add(value);
		map.put(key, list);
	}
	
	public HashSet<V> get(@NonNull Object key) {
		return map.get(key);
	}
	
	public void remove(@NonNull K key, @NonNull V value) {
		HashSet<V> list = map.get(key);
		if(list == null) return;
		
		list.remove(value);
		map.put(key, list);
	}
}
