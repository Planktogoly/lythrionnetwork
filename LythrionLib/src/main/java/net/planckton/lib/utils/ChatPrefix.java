package net.planckton.lib.utils;

import org.bukkit.ChatColor;

import mkremins.fanciful.FancyMessage;

public class ChatPrefix {

	public static final String LYHTRION = "Lythrion" + ChatColor.RESET;
	public static final String LYHRION_RED = ChatColor.RED.toString() + ChatColor.BOLD + "Lythrion" + ChatColor.RESET;
	public static final String LYTHRION_RED_ARROW = ChatColor.RED.toString() + ChatColor.BOLD + "Lythrion" + ChatColor.GRAY + " " + UniChars.ARROWS_RIGHT + " " + ChatColor.RESET;

	public static FancyMessage getLythrionGoldArrows() {
		return new FancyMessage("Lythrion")
					.color(ChatColor.RED)
					.then(" " + UniChars.ARROWS_RIGHT + " ")
					.color(ChatColor.GRAY);
	}
	
}
