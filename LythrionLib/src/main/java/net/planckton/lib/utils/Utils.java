package net.planckton.lib.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Random;
import java.util.regex.Pattern;

public class Utils {
	
	private static Random random = new Random();
	
	public static int random(int range) {
		return random.nextInt(range);
	}
	
	public static float random() {
		return random.nextFloat();
	}

	public static void nullify(Object object) {
		for(Field field : object.getClass().getDeclaredFields()) {
			if(field.getType().isPrimitive()) continue;
			if(Modifier.isFinal(field.getModifiers())) continue;
			
			boolean isAccessable = field.isAccessible();
			if(!isAccessable) field.setAccessible(true);
			
			try {
				field.set(object, null);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
			
			if(!isAccessable) field.setAccessible(false);
		}
	}	
	
	public static final char COLOR_CHAR = '\u00A7';
	private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)" + String.valueOf(COLOR_CHAR));

	public static String stripChatColors(char chr, String input) {
		return STRIP_COLOR_PATTERN.matcher(input).replaceAll(String.valueOf(chr));
	}
	
}
