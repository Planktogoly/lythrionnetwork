package net.planckton.lib.utils;

import net.planckton.lib.player.LythrionPlayer;

public interface PlayerRunnable {
	public void run(LythrionPlayer player);
}
