package net.planckton.lib.utils;

import java.util.Set;

public interface MapSet<K,V> {
	public void add(K key, V value);
	public Set<V> get(Object key);
	public void remove(K key, V value);
}
