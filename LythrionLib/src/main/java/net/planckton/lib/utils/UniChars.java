package net.planckton.lib.utils;

import org.bukkit.ChatColor;

public class UniChars {
	public static final String ADMIN = "\u0398";
	public static final String ARROWS_RIGHT = "\u27A4";
	public static final String ARROWS_LEFT = "\u2B9C";
	public static final String CROSS_THICK = "\u2716";
	public static final String CHECKMARK = "\u2714"; 
	public static final String WRITING_HAND = "\u270D";
	public static final String LINE_UP = "\u2503";
	public static final String LINE_UP_THICK = "\u2502";
	public static final String DOUBLE_LINE_UP = "\u2551";
	public static final String INFORMATION_CIRCLE = "\uD83D\uDEC8";
	public static final String DIAMOND = ChatColor.BOLD + " \u26AB " + ChatColor.RESET;
	public static final String CROSS_DIRTY = "\u2717";
	public static final String HEART = "\u2764";
	public static final String ENVELOPE = "\u2709";
	public static final String FULL_BLOCK = "\u2588";
	public static final String WARNING_SIGN = "\u26A0";
}
