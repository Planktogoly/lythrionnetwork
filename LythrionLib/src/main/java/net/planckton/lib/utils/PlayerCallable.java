package net.planckton.lib.utils;

import net.planckton.lib.player.LythrionPlayer;

public interface PlayerCallable<T> {
	public T call(LythrionPlayer player);
}
