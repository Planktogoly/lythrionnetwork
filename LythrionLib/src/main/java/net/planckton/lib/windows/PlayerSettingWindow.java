package net.planckton.lib.windows;

import net.planckton.lib.window.Window;

public class PlayerSettingWindow extends Window {

	@Override
	protected void build() {
				
	}

	@Override
	protected String getName() {
		return "Player Settings";
	}

	@Override
	protected int getRows() {
		return 4;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
