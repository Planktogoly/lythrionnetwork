package net.planckton.lib.player.data;

import net.planckton.lib.lvariable.AsyncSafeVariable;
import net.planckton.lib.lvariable.Callback;
import net.planckton.lib.player.LythrionPlayer;

public class AsyncSafePlayerData<T> extends PlayerData {
	private AsyncSafeVariable<T> asyncSafeVariable = new AsyncSafeVariable<>();
	
	public AsyncSafePlayerData(LythrionPlayer player) {
		super(player);
	}
	
	public void getSafe(Callback<T> callback) {
		asyncSafeVariable.getSafe(callback);
	}
	
	/**
	 * Make sure to test if loaded first
	 * 
	 * @return
	 */
	public T get() {
		if(!asyncSafeVariable.isSet()) throw new RuntimeException("variable was not loaded yet");
		
		return asyncSafeVariable.getValue();
	}
	
	public boolean isSet() {
		return asyncSafeVariable.isSet();
	}
	
	protected void set(T value) {
		asyncSafeVariable.set(value);
	}
	
	protected T getValue() {
		return asyncSafeVariable.getValue();
	}
}
