package net.planckton.lib.player.data;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.chat.ChatChannel;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.ChatPrefix;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;

public class ChatData extends PlayerData {
	private LinkedHashSet<ChatChannel> availableChats = new LinkedHashSet<ChatChannel>();
	private LinkedHashSet<ChatChannel> listeningChats = new LinkedHashSet<ChatChannel>();
	@Getter private ChatChannel chatChannel;
	
	@Getter private ChatChannel modeChannel;
	
	public ChatData(LythrionPlayer player) {
		super(player);
	}
	
	//unloading
	@Override
	public void unload() {
		for(ChatChannel chat : getAvailableChats()) {
			chat.leave(player);
		}
		
		for(ChatChannel chat : getListeningChats()) {
			chat.leave(player);
		}
	}
	
	//modifiers chat channels
	public void setChatChannel(ChatChannel chatChannel, boolean informPlayer) {
		if(!availableChats.contains(chatChannel)) {
			availableChats.add(chatChannel);
			listeningChats.add(chatChannel);
		}
		
		this.chatChannel = chatChannel;
		chatChannel.join(player);
		
		if(informPlayer) player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + "You are now talking in \"" + ChatColor.GOLD + WordUtils.capitalizeFully(chatChannel.chatName) + ChatColor.RESET + "\".");
	}
	
	public void setChatChannel(ChatChannel chatChannel) {
		setChatChannel(chatChannel, false);
	}
	
	public void listenToChat(ChatChannel channel) {
		if(!availableChats.contains(channel)) {
			availableChats.add(channel);
		}
		
		if(listeningChats.contains(channel)) return;
		
		listeningChats.add(channel);
		channel.join(player);
	}
	
	public void muteChat(ChatChannel chat) {
		//we can't mute the chat we are talking in
		if(chatChannel == chat) 
			return;
		
		listeningChats.remove(chat);
		chat.leave(player);
	}
	
	public ChatChannel getChat() {
		return chatChannel;
	}
	
	public Set<ChatChannel> getAvailableChats() {
		return Collections.unmodifiableSet(availableChats);
	}
	
	public ChatChannel hasAvailableChat(String chatName) {
		for(ChatChannel chat : availableChats) {
			if(chat.displayName.equalsIgnoreCase(chatName)) return chat;
		}
		
		return null;
	}
	
	public void removeAvailableChat(ChatChannel channel) {
		availableChats.remove(channel);
		channel.leave(player);
	}
	
	public Set<ChatChannel> getListeningChats() {
		return Collections.unmodifiableSet(listeningChats);
	}
	
	public void removeListeningChats(ChatChannel channel) {
		listeningChats.remove(channel);
		channel.leave(player);
	}
	
	public void addAvailableChats(ChatChannel channel) {
		availableChats.add(channel);
	}
	
	public void addListeningChats(ChatChannel channel) {
		listeningChats.add(channel);
		channel.join(player);
	}
	
	
	public void setModeChat(ChatChannel chatChannel) {
		setModeChat(chatChannel, modeChannel == getChatChannel() || getChatChannel() == null);
	}
	
	public void setModeChat(@NonNull ChatChannel chatChannel, boolean talkChat) {
		if(this.modeChannel != null) {
			removeAvailableChat(modeChannel);
			removeListeningChats(modeChannel);
			this.modeChannel.leave(player);
		}
		
		addAvailableChats(chatChannel);
		addListeningChats(chatChannel);
		chatChannel.join(player);
		
		this.modeChannel = chatChannel;
		if(talkChat) setChatChannel(chatChannel);
	}
}
