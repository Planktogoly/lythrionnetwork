package net.planckton.lib.player.data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.player.LythrionPlayer;

public class GeneralData extends PlayerData {

	@Getter private Timestamp firstJoin;
	
	public GeneralData(LythrionPlayer player) {
		super(player);
	}
	
	@Override
	protected void onLoad() {
		firstJoin = DatabaseLib.getFirstJoin(player.getId());
	}
	
	
	public String getFullTime() {
		Date date = new Date(firstJoin.getTime());


		return new SimpleDateFormat("E, dd-MM-yyyy HH:mm:ss").format(date);
	}
}
