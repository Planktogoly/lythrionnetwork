package net.planckton.lib.player;

import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Player.Spigot;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mojang.authlib.GameProfile;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import mkremins.fanciful.FancyMessage;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.planckton.blood.packet.PacketPlayerConnect;
import net.planckton.blood.packet.PacketPlayerJoinMinigame;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.PlayerPlayModeSwitchEvent;
import net.planckton.lib.nick.NickName;
import net.planckton.lib.player.data.BalanceData;
import net.planckton.lib.player.data.ChatData;
import net.planckton.lib.player.data.CrateData;
import net.planckton.lib.player.data.GeneralData;
import net.planckton.lib.player.data.PermissionsData;
import net.planckton.lib.player.data.PlayerData;
import net.planckton.lib.player.data.RankData;
import net.planckton.lib.player.data.TabColorData;
import net.planckton.lib.player.prefix.PrefixesData;
import net.planckton.lib.playmode.PlayMode;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.position.Position;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.UniChars;
import net.planckton.lib.utils.Utils;

public class LythrionPlayer {
	
	@Getter @Setter(AccessLevel.PROTECTED) @NonNull private Player bukkitPlayer;
	@Getter @Setter(AccessLevel.PROTECTED) @NonNull private UUID id;
	
	@Getter private PlayMode playMode;
	@Getter private NickName nickname;
	
	private List<PlayerData> playerDatas = new ArrayList<PlayerData>();
	
	//Uses onLoad() method
	public final GeneralData general = registerPlayerData(new GeneralData(this));
	public final RankData rank = registerPlayerData(new RankData(this));
	public final BalanceData balance = registerPlayerData(new BalanceData(this));
	
	//Uses onLoadAdditional() method
	public final PermissionsData permissions = registerPlayerData(new PermissionsData(this));
	public final PrefixesData prefixes = registerPlayerData(new PrefixesData(this)); 
	public final CrateData crate = registerPlayerData(new CrateData(this));
	
	//Uses non of the methods
	public final TabColorData tab = registerPlayerData(new TabColorData(this));
	public final ChatData chat = registerPlayerData(new ChatData(this));
	
	private HashMap<MetaData<?>, Object> metaData = new HashMap<>();
	//@Getter private Map<PlayerSetting, PlayerSettingInstance> settings = new HashMap<>();
	
	@Getter @Setter private long lastDamageTime;
	@Getter @Setter private long lastPlayerDamageTime;
	
	protected <T extends PlayerData> T registerPlayerData(T data) {
		playerDatas.add(data);
		
		return data;
	}
	
	/**
	 * Load important data on other threads then the main thread
	 * Watch out as this method may still be called sync.
	 * In a playerLoginEvent
	 */
	protected boolean load() {
		for(PlayerData data : playerDatas) {
			try {
				data.load();
			} catch (Exception e) {
				System.out.println("Error while loading playerdata: " + data.getClass().getSimpleName() + " for " + getName());
				e.printStackTrace();
				
				if(data.shouldCancelLoadOnFail()) {
					return false;
				}
			}
		}
		
		/*for(Entry<PlayerSetting, PlayerSettingInstance> setting : settings.entrySet()) {
			if(!setting.getKey().isLoadSync()) continue;
		
			setting.getValue().loadSetting();
		}*/
		
		for(MetaData<?> metaData : PlayerManager.getMetaDatas()) metaData.load(this);
		
		return true;
	}
	
	/**
	 * 
	 * Less important data to be loaded async.
	 * In a playerJoinEvent
	 */
	protected boolean loadAdditional() {
		for(PlayerData data : playerDatas) {
			try {
				data.loadAdditional();
			} catch (Exception e) {
				System.out.println("Error while loading additional playerdata: " + data.getClass().getSimpleName() + " for " + getName());
				e.printStackTrace();
				
				if(data.shouldCancelLoadOnFail()) {
					return false;
				}
			}
		}
		
		/*for(Entry<PlayerSetting, PlayerSettingInstance> setting : settings.entrySet()) {
			if(setting.getKey().isLoadSync()) continue;
		
			setting.getValue().loadSetting();
		}*/
		
		for(MetaData<?> metaData : PlayerManager.getMetaDatas()) metaData.loadAdditional(this);
		
		return true;
	}
	
	protected boolean loadAll() {
		if(!load()) return false;
		return loadAdditional();
	}
	
	protected void unload() {
		chat.unload();
		
		Utils.nullify(this);
	}
	
	public boolean save() {
		for(PlayerData data : playerDatas) {
			try {
				data.save();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		Iterator<Entry<MetaData<?>, Object>> it = metaData.entrySet().iterator();
		while(it.hasNext()) {
			Entry<MetaData<?>, Object> pair = it.next();
			if(pair.getValue() == null) continue;
			
			pair.getKey().save(pair.getValue(), this);
		}
		
		return true;
	}
	
	public void setPlayMode(PlayMode mode) {
		PlayMode oldPlayMode = null;
		
		if(this.playMode != null) {
			if(this.playMode == mode) return;
			
			try {
				this.playMode.onDeSelect(this);
			} catch(Exception e) {
				e.printStackTrace();
				System.out.println("Something went wrong during deselect mode");
			}
			oldPlayMode = playMode;
		}
		
		closeInventory();
		
		Bukkit.getPluginManager().callEvent(new PlayerPlayModeSwitchEvent(this, oldPlayMode, mode));
		
		this.playMode = mode;
		PlayModeManager.setPlayerPlayMode(this, mode.getClass(), oldPlayMode != null ? oldPlayMode.getClass() : null);

		mode.onSelect(this);
	}
	
	public void setPlayMode(Class<? extends PlayMode> mode) {
		setPlayMode(PlayModeManager.fromClass(mode));
	}
	
	public String getCurrentName() {
		return nickname == null ? bukkitPlayer.getName() : nickname.getName();
 	}
	
	public String getDisplayName() {		
		return (nickname != null ? tab.getExactTabColor() + nickname.getName() : tab.getExactTabColor() + bukkitPlayer.getName());
	}
	
	public String getDisplayNameWithRank() {		
		return (nickname != null ? Rank.MEMBER.getChatPrefix()+ tab.getExactTabColor() + nickname.getName() : rank.getChatPrefix() + tab.getExactTabColor() + bukkitPlayer.getName());
	}
	
	public boolean hasPermission(String permission) {
		return permissions.hasPermission(permission);
	}
	
	public void sendPacket(PacketContainer packet) throws InvocationTargetException {
		ProtocolLibrary.getProtocolManager().sendServerPacket(bukkitPlayer, packet);
	}
	
	public void sendPluginMessage(Plugin plugin, String string, byte[] byteArray) {
		bukkitPlayer.sendPluginMessage(plugin, string, byteArray);
	}
	
	public Rank getDisplayRank() {
		if(isNicked()) return Rank.MEMBER;		
		return rank.getRank();
	}
	
	public void teleport(Position position) {
		position.teleport(bukkitPlayer);
	}	
	
	public void setNickName(NickName nickname) {
		this.nickname = nickname;
	}
	
	public void sendMessage(FancyMessage message) {
		message.send(bukkitPlayer);
	}
	
	public boolean isInPlayMode(PlayMode mode) {
		return isInPlayMode(mode.getClass());
	}
	
	public boolean isInPlayMode(Class<? extends PlayMode> mode) {
		if(playMode == null) return false;
		
		return playMode.getClass() == mode;
	}
	
	public void sendTitle(String title, String subtitle) {
		sendTitle(title, subtitle, 10, 60, 10);
	}
	
	public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {	
	    PlayerConnection connection = ((CraftPlayer)bukkitPlayer).getHandle().playerConnection;
	    
	    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn, stay, fadeOut);
	    connection.sendPacket(packetPlayOutTimes);
	    if (subtitle != null) {
	      subtitle = subtitle.replaceAll("%player%", bukkitPlayer.getDisplayName());
	      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
	      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
	      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
	      connection.sendPacket(packetPlayOutSubTitle);
	    }
	    if (title != null) {
	      title = title.replaceAll("%player%", bukkitPlayer.getDisplayName());
	      title = ChatColor.translateAlternateColorCodes('&', title);
	      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
	      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
	      connection.sendPacket(packetPlayOutTitle);
	    }
	}
	
	public void cleanTitle() {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "title " + bukkitPlayer.getName() + " clear");
	}
	
	public void sendActionBarMessage(String message) {
		CraftPlayer p = (CraftPlayer)bukkitPlayer;
		IChatBaseComponent cbc = ChatSerializer.a(WrappedChatComponent.fromText(message).getJson());
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
		p.getHandle().playerConnection.sendPacket(ppoc);
	}
	
	public void respawn() {
		bukkitPlayer.spigot().respawn();
	}
	
	public void connectToHub() {
		LythrionLib.sendPacket(new PacketPlayerConnect(id, "Lobby", true));
	}
	
	public void connectToMinigame(String minigame) {
		LythrionLib.sendPacket(new PacketPlayerJoinMinigame(id, minigame, true));
	}
	
	/**
	 * Connects the player to the supplied server. If the supplied
	 * server cannot be found. The player will stay in the server.
	 * 
	 * @param server Server to connect to
	 */
	public void connect(String server) {
		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {		
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
		    out.writeUTF("sendTo");
		    out.writeUTF(server);
		    out.writeUTF(this.getName());
		    sendPluginMessage(LythrionLib.getInstance(), "BungeeCord", out.toByteArray());
		});
	}
	
	/**
	 * Connects the player to the supplied server. The player will
	 * instantly disconnect from the server. If the supplied server
	 * cannot be found the player will be disconnected from the network.
	 * 
	 * @param server Server to connect to
	 */
	public void connectKick(String server) {
		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {
			Bukkit.getScheduler().runTask(LythrionLib.getInstance(), () -> bukkitPlayer.kickPlayer("S:" + server.toUpperCase()));
		});
	}
	
	/**
	 * Connects the player to the supplied server type. If the supplied
	 * server cannot be found. The player will stay in the server.
	 * 
	 * If multiple players have to be connected to the same server see {@link #connectToServerType(String, String) connectToServerType(serverType, groupIdentifier)}
	 * 
	 * @param serverType Server type to connect to
	 * @see {@link #connectToServerType(String, String) connectToServerType(serverType, groupIdentifier)}
	 */
	public void connectToServerType(String serverType) {
		if(serverType.matches("[a-zA-Z]{1,}[0-1]{1,}")) throw new RuntimeException("Server types cannot end with numbers");

		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {
			
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
		    out.writeUTF("BalanceConnect");
		    out.writeUTF("C:" + serverType.toUpperCase());
		    sendPluginMessage(LythrionLib.getInstance(), "BungeeCord", out.toByteArray());
		});
	}
	
	/**
	 * Connects the player to the supplied server type. If the supplied
	 * server cannot be found. The player will stay in the server.
	 * 
	 * If multiple players have to be connected to a server of the defined 
	 * server type and should all be directed to the same server defining
	 * the same group identifier will assure they join the same server.
	 * 
	 * @param serverType Server type to connect to
	 * @param groupIdentifier The group identifier
	 */
	public void connectToServerType(String serverType, int groupIdentifier) {
		connectToServerType(serverType, Integer.toString(groupIdentifier));
	}
	
	/**
	 * Connects the player to the supplied server type. If the supplied
	 * server cannot be found. The player will stay in the server.
	 * 
	 * If multiple players have to be connected to a server of the defined 
	 * server type and should all be directed to the same server defining
	 * the same group identifier will assure they join the same server.
	 * 
	 * @param serverType Server type to connect to
	 * @param groupIdentifier The group identifier
	 */
	public void connectToServerType(String serverType, String groupIdentifier) {
		if(serverType.matches("[a-zA-Z]{1,}[0-1]{1,}")) throw new RuntimeException("Server types cannot end with numbers");
		
		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("BalanceConnect");
			out.writeUTF("C:" + serverType.toUpperCase() + ":" + groupIdentifier);
			sendPluginMessage(LythrionLib.getInstance(), "BungeeCord", out.toByteArray());
		});
	}
	
	/**
	 * Connects the player to the supplied server type. The player will
	 * instantly disconnect from the server. If a server of the supplied 
	 * server type cannot be found the player will be disconnected from 
	 * the network.
	 * 
	 * If multiple players have to be connected to the same server see {@link #connectToServerTypeKick(String, String) connectToServerTypeKick(serverType, groupIdentifier)}
	 * 
	 * @param server Server type to connect to
	 * @see {@link #connectToServerTypeKick(String, String) connectToServerTypeKick(serverType, groupIdentifier)}
	 */
	public void connectToServerTypeKick(String serverType) {
		if(serverType.matches("[a-zA-Z]{1,}[0-1]{1,}")) throw new RuntimeException("Server types cannot end with numbers");
		
		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {
			Bukkit.getScheduler().runTask(LythrionLib.getInstance(), () -> bukkitPlayer.kickPlayer("C:" + serverType.toUpperCase()));
		});
	}
	
	/**
	 * Connects the player to the supplied server type. The player will
	 * instantly disconnect from the server. If a server of the supplied 
	 * server type cannot be found the player will be disconnected from 
	 * the network.
	 * 
	 * If multiple players have to be connected to a server of the defined 
	 * server type and should all be directed to the same server defining
	 * the same group identifier will assure they join the same server.
	 * 
	 * @param server Server type to connect to
	 * @param groupIdentifier The group identifier
	 */
	public void connectToServerTypeKick(String serverType, int groupIdentifier) {
		connectToServerTypeKick(serverType, Integer.toString(groupIdentifier));
	}
	
	/**
	 * Connects the player to the supplied server type. The player will
	 * instantly disconnect from the server. If a server of the supplied 
	 * server type cannot be found the player will be disconnected from 
	 * the network.
	 * 
	 * If multiple players have to be connected to a server of the defined 
	 * server type and should all be directed to the same server defining
	 * the same group identifier will assure they join the same server.
	 * 
	 * @param serverType Server type to connect to
	 * @param groupIdentifier The group identifier
	 */
	public void connectToServerTypeKick(String serverType, String groupIdentifier) {
		if(serverType.matches("[a-zA-Z]{1,}[0-1]{1,}")) throw new RuntimeException("Server types cannot end with numbers");
		
		LythrionLib.getExecutorPool().submit(this::save).addListener((arg) -> {
			Bukkit.getScheduler().runTask(LythrionLib.getInstance(), () -> bukkitPlayer.kickPlayer("C:" + serverType.toUpperCase() + ":" + groupIdentifier));
		});
	}
	
	public void setHeaderAndFooter(FancyMessage header, FancyMessage footer) {
		LythrionLib.getExecutorPool().submit(() -> {
			PacketContainer packet = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);
			
			packet.getChatComponents().write(0, WrappedChatComponent.fromJson(header.toJSONString()));
			packet.getChatComponents().write(1, WrappedChatComponent.fromJson(footer.toJSONString()));
			
			try {
				sendPacket(packet);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void teleport(LythrionPlayer player) {
		teleport(player.getBukkitPlayer());
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getMetaData(MetaData<T> meta) {
		return (T) metaData.get(meta);
	}
	
	public <T> void setMetaData(MetaData<T> meta, T value) {
		metaData.put(meta, value);
	}
	
	public <T> void removeMetaData(MetaData<T> meta) {
		metaData.remove(meta);
	}

	public boolean isBukkitPlayer(Player player) {
		return bukkitPlayer.equals(player);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this) return true;
		if(obj instanceof UUID) return id.equals(obj);
		if(obj instanceof LythrionPlayer) return id.equals(((LythrionPlayer) obj).getId());
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	public void clearInventory() {
		bukkitPlayer.getInventory().clear();
		
		bukkitPlayer.getInventory().setArmorContents(null);
	}
	
	public InventoryView getOpenInventory() {
		return bukkitPlayer.getOpenInventory();
	}
	
	public void heal() {
		setHealth(getMaxHealth());
	}

	public void feed() {
		setSaturation(20.0F);
		setFoodLevel(20);
	}
	
	public Rank getRank() {
		return rank.getRank();
	}

	//forwards from bukkit player
	public String getName() {
		return bukkitPlayer.getName();
	}
	
	public int getEntityId() {
		return bukkitPlayer.getEntityId();
	}

	public void sendMessage(String message) {
		bukkitPlayer.sendMessage(message);
	}
	
	public void sendAdminMessage(String message) {
		bukkitPlayer.sendMessage(ChatColor.RED + UniChars.DOUBLE_LINE_UP + ChatColor.WHITE + " " + message);
	}

	public void openInventory(Inventory inventory) {
		bukkitPlayer.openInventory(inventory);
	}

	public void closeInventory() {
		bukkitPlayer.closeInventory();
	}

	public void teleport(Entity entity) {
		bukkitPlayer.teleport(entity);
	}
	
	public void teleport(Location location) {
		bukkitPlayer.teleport(location);
	}

	public World getWorld() {
		return bukkitPlayer.getWorld();
	}
	
	public Location getLocation() {
		return bukkitPlayer.getLocation();
	}

	public Spigot spigot() {
		return bukkitPlayer.spigot();
	}

	public boolean hasRankPower(Rank required) {
		return rank.hasPower(required);
	}

	public Block getTargetBlock(Set<Material> set, int i) {
		return bukkitPlayer.getTargetBlock(set, i);
	}
	
	public void setGameMode(GameMode mode) {
		bukkitPlayer.setGameMode(mode);
	}

	public PlayerInventory getInventory() {
		return bukkitPlayer.getInventory();
	}
	
	public void setScoreBoard(Scoreboard scoreboard) {
		bukkitPlayer.setScoreboard(scoreboard);
	}

	public boolean hasPlayedBefore() {
		return bukkitPlayer.hasPlayedBefore();
	}

	public void setLevel(int level) {
		bukkitPlayer.setLevel(level);
	}
	
	public int getLevel() {
		return bukkitPlayer.getLevel();
	}
	
	public void setHealth(double health) {
		bukkitPlayer.setHealth(health);
	}
	
	public double getHealth() {
		return bukkitPlayer.getHealth();
	}
	
	public void setHealthScale(double scale) {
		bukkitPlayer.setHealthScale(scale);
	}
	
	public void setHealthScale(boolean scaled) {
		bukkitPlayer.setHealthScaled(scaled);
	}
	
	public void setFoodLevel(int level) {
		bukkitPlayer.setFoodLevel(level);
	}
	
	public void setSaturation(float saturation) {
		bukkitPlayer.setSaturation(saturation);
	}
	
	public void setScoreboard(Scoreboard scoreboard) {
		bukkitPlayer.setScoreboard(scoreboard);
	}
	
	public Scoreboard getScoreboard() {
		return bukkitPlayer.getScoreboard();
	}
	
	public void setFireTicks(int ticks) {
		bukkitPlayer.setFireTicks(ticks);
	}
	
	public void addPotionEffect(PotionEffect effect) {
		bukkitPlayer.addPotionEffect(effect);
	}
	
	public boolean isOnline() {
		return bukkitPlayer != null && bukkitPlayer.isOnline();
	}
	
	public boolean isValid() {
		return bukkitPlayer != null;
	}
	
	public boolean hasPotionEffect(PotionEffectType type) {
		return bukkitPlayer.hasPotionEffect(type);
	}
	
	public void removePotionEffect(PotionEffectType type) {
		bukkitPlayer.removePotionEffect(type);
	}
	
	public boolean isDead() {
		return bukkitPlayer.isDead();
	}
	
	public ItemStack getItemInHand() {
		return bukkitPlayer.getItemInHand();
	}
	
	public void setItemInHand(ItemStack stack) {
		bukkitPlayer.setItemInHand(stack);
	}
	
	public int getFoodLevel() {
		return bukkitPlayer.getFoodLevel();
	}
	
	public void openWorkbench() {
		bukkitPlayer.openWorkbench(null, true);
	}
	
	public float getExp() {
		return bukkitPlayer.getExp();
	}
	
	public void setExp(float exp) {
		bukkitPlayer.setExp(exp); 
	}
	
	public Collection<PotionEffect> getActivePotionEffects() {
		return bukkitPlayer.getActivePotionEffects();
	}
	
	public void cleaPotionEffects() {
		for (PotionEffect pf : getActivePotionEffects()) {
			removePotionEffect(pf.getType());
		}
	}
	
	public Entity getPassenger() {
		return bukkitPlayer.getPassenger();
	}

	public boolean isSneaking() {
		return bukkitPlayer.isSneaking();
	}
	
	public Player getKiller() {
		return bukkitPlayer.getKiller();
	}

	public void setFallDistance(int falldistance) {
		bukkitPlayer.setFallDistance(falldistance);
	}

	public void playSound(Location location, Sound sound, int f1, int f2) {
		bukkitPlayer.playSound(location, sound, f1, f2);
	}
	
	public void setMaxHealth(double max) {
		bukkitPlayer.setMaxHealth(max);
	}
	
	public void sendRawMessage(String message) {
		bukkitPlayer.sendRawMessage(message);
	}
	
	public double getMaxHealth() {
		return bukkitPlayer.getMaxHealth();
	}
	
	public void setOp(boolean op) {
		bukkitPlayer.setOp(op);
	}
	
	public void kickPlayer(String reason) {
		bukkitPlayer.kickPlayer(reason);
	}

	public void setSpectatorTarget(Entity entity) {
		bukkitPlayer.setSpectatorTarget(entity);
	}
	
	public void openEnchanting() {
		bukkitPlayer.openEnchanting(null, true);
	}
	
	public void setFlySpeed(float speed) {
		bukkitPlayer.setFlySpeed(speed);
	}

	public void setWalkSpeed(float speed) {
		bukkitPlayer.setWalkSpeed(speed);
	}

	public void hidePlayer(LythrionPlayer player) {
		bukkitPlayer.hidePlayer(player.bukkitPlayer);
	}
	
	public void showPlayer(LythrionPlayer player) {
		bukkitPlayer.showPlayer(player.bukkitPlayer);
	}
	
	public boolean canSee(LythrionPlayer player) {
		return bukkitPlayer.canSee(player.bukkitPlayer);
	}
	
	public GameMode getGameMode() {
		return bukkitPlayer.getGameMode();
	}
	
	public void chat(String message) {
		bukkitPlayer.chat(message);
	}
	
	@SuppressWarnings("deprecation")
	public void sendBlockChange(Location location, Material material, byte data) {
		bukkitPlayer.sendBlockChange(location, material, data);
	}
	
	public Vector getVelocity() {
		return bukkitPlayer.getVelocity();
	}

	public String getIp() {
		return bukkitPlayer.getAddress().getAddress().getHostAddress();
	}

	public GameProfile getProfile() {
		return ((CraftPlayer)bukkitPlayer).getProfile();
	}

	public boolean isNicked() {
		return nickname != null;
	}

	public int getMinecraftPing() {
		return ((CraftPlayer) bukkitPlayer).getHandle().ping;
	}

	public void updateInventory() {
		bukkitPlayer.updateInventory();
	}

	public InetSocketAddress getAddress() {
		return bukkitPlayer.getAddress();
	}

	public EntityDamageEvent getLastDamageCause() {
		return bukkitPlayer.getLastDamageCause();
	}

	public void setVelocity(Vector velocity) {
		bukkitPlayer.setVelocity(velocity);
	}

	public void playSound(Sound click) {
		playSound(getLocation(), click, 1, 1);
	}

	public void updateTab() {
		WrapperPlayServerPlayerInfo wrapper = new WrapperPlayServerPlayerInfo();
		
		wrapper.setAction(PlayerInfoAction.ADD_PLAYER);
		wrapper.setData(Arrays.asList(new PlayerInfoData(WrappedGameProfile.fromHandle(getProfile()), getMinecraftPing(), NativeGameMode.fromBukkit( getGameMode()), null)));
		
		for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
			wrapper.sendPacket(player.getBukkitPlayer());
		}
	}

	public float getSaturation() {
		return bukkitPlayer.getSaturation();
	}	
}
