package net.planckton.lib.player.data;

import org.bukkit.Bukkit;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.event.LythrionPlayerChangeRankEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.rank.Rank;

public class RankData extends PlayerData {
	@Getter @NonNull private Rank rank;
	
	public RankData(LythrionPlayer player) {
		super(player);
	}
	
	@Override
	protected void onLoad() {
		setRank(DatabaseLib.getRank(player.getId()));
	}
	
	public void updateRank(Rank rank) {
		setRank(rank);
		
		DatabaseLib.setRank(player.getId(), rank);
	}
	
	private void setRank(Rank rank) {
		this.rank = rank;
		
		player.getBukkitPlayer().setOp(hasPower(Rank.MOD));
		player.tab.updateTabColor();
		Bukkit.getPluginManager().callEvent(new LythrionPlayerChangeRankEvent(player, rank));
	}

	//getter
	public String getPrefix() {
		return rank.getPrefix();
	}
	
	public String getChatPrefix() {
		return rank.getChatPrefix();
	}

	public boolean hasPower(Rank required) {
		return rank.hasPower(required);
	}
}
