package net.planckton.lib.player.data;

import lombok.Getter;
import lombok.Setter;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.player.LythrionPlayer;

public class CrateData extends PlayerData {
	
	@Getter @Setter private int commonCrate;
	@Getter @Setter private int uncommonCrate;
	@Getter @Setter private int legendaryCrate;
	@Getter @Setter private int townycrate;

	public CrateData(LythrionPlayer player) {
		super(player);
	}
	
	@Override
	protected void onLoadAdditional() {
		DatabaseLib.getCrateData(player);		
	}
	
	@Override
	public void save() {
		DatabaseLib.setCrateData(this);
	}
	
	public void addCommonCrate() {
		commonCrate++;
	}
	
	public void addUnCommonCrate() {
		uncommonCrate++;
	}
	
	public void addLegendaryCrate() {
		legendaryCrate++;
	}
	
	public void addTownyCrate() {
		townycrate++;
	}
	
	
	
	
	public void useCommonCrate() {
		commonCrate--;
	}
	
	public void useUncommonCrate() {
		uncommonCrate--;
	}
	
	public void useLegendaryCrate() {
		legendaryCrate--;
	}
	
	public void useTownyCrate() {
		townycrate--;
	}
	

}
