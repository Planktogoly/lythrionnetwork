package net.planckton.lib.player.data;

import net.planckton.lib.DatabaseLib;
import net.planckton.lib.player.LythrionPlayer;

public class BalanceData extends AsyncSafePlayerData<Balance> {

	public BalanceData(LythrionPlayer player) {
		super(player);
	}
	
	@Override
	protected void onLoad() {
		set(DatabaseLib.getBalance(player));
	}
	
	@Override
	public void save() {
		getValue().save();
	}
}
