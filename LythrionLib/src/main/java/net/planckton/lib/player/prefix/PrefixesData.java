package net.planckton.lib.player.prefix;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import lombok.Cleanup;
import lombok.Getter;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.data.PlayerData;

public class PrefixesData extends PlayerData {
	@Getter private Set<PlayerPrefix> prefixes = new HashSet<>();
	
	public PrefixesData(LythrionPlayer player) {
		super(player);
	}
	
	public void add(PlayerPrefix prefix) {
		prefixes.add(prefix);
	}

	public void addAll(Set<PlayerPrefix> prefixes) {
		prefixes.addAll(prefixes);
	}
	
	public void addPrefixLocal(PlayerPrefix prefix) {
		prefixes.add(prefix);
	}
	
	public void removePrefix(PlayerPrefix prefix) {
		prefixes.remove(prefix);
	}
	
	//loading
	@Override
	public void onLoadAdditional() {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT prefix,tooltip FROM player_prefix WHERE id_player=?");
			statement.setString(1, player.getId().toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			HashSet<PlayerPrefix> prefixes = new HashSet<>();
			
			while(rs.next()) {
				prefixes.add(new PlayerPrefix(rs.getString("prefix"), rs.getString("tooltip")));
			}
			
			@Cleanup PreparedStatement statementGroups = connection.prepareStatement("SELECT prefix,description FROM `group` WHERE group_id IN (SELECT group_id FROM player_group WHERE player_id=?)");
			statementGroups.setString(1, player.getId().toString());
			
			@Cleanup ResultSet rsGroups = statementGroups.executeQuery();
			while(rsGroups.next()) {
				String prefix = rsGroups.getString("prefix");
				if(prefix == null) continue;
				
				String description = rsGroups.getString("description");
				if(description == null) description = "";
				
				PlayerPrefix pf = new PlayerPrefix(prefix, description);
				pf.setHideWhileNicked(true);
				
				prefixes.add(pf);
			}
			
			if(prefixes.size() == 0) return;
			
			this.prefixes.addAll(prefixes);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
