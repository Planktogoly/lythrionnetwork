package net.planckton.lib.player;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitTask;
import org.spigotmc.CustomTimingsHandler;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardTeam;
import com.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.planckton.blood.packet.PacketPlayerServerJoin;
import net.planckton.blood.packet.PacketPlayerServerQuit;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.event.DefaultPlayModeRequestEvent;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.event.LythrionPlayerLoadEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.event.PlayerCacheDeleteEvent;
import net.planckton.lib.event.PlayerCacheEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.playmode.PlayModeVanilla;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;
import net.planckton.lib.utils.Utils;

public class PlayerManager<T extends LythrionPlayer> extends Manager implements Listener {
	private static final CustomTimingsHandler timerLoadPlayer = new CustomTimingsHandler("Custom: PlayerManager::loadPlayer");
	private static final CustomTimingsHandler timerLoadPlayerNew = new CustomTimingsHandler("Custom: PlayerManager::loadPlayerObject - new");
	private static final CustomTimingsHandler timerLoadPlayerCached = new CustomTimingsHandler("Custom: PlayerManager::loadPlayerObject - cached");
	private static final CustomTimingsHandler timerLoadPlayerData = new CustomTimingsHandler("Custom: PlayerManager::loadPlayerObject - data");
	private static final CustomTimingsHandler timerLoadPlayerDataLoad = new CustomTimingsHandler("Custom: PlayerManager::loadPlayerObject - data Load");
	private static final CustomTimingsHandler timerLoadPlayerDataLoadAdditional = new CustomTimingsHandler("Custom: PlayerManager::loadPlayerObject - data LoadAddittional");
	
	private static final Map<Rank, WrapperPlayServerScoreboardTeam> teamPackets = new HashMap<>();
	private static final Map<String, WrapperPlayServerScoreboardTeam> rankColorPackets = new HashMap<>();
	private static final Map<ChatColor, WrapperPlayServerScoreboardTeam> colorPackets = new HashMap<>();
	
	private static final Map<String, MetaData<?>> metaDatas = new HashMap<>();

	@Getter @Setter public static boolean shouldAddAdminChat = true;
	
	private static PlayerManager<?> instance;
	
	@Getter private Map<UUID, T> players;
	@Getter private Map<String, T> playersByName;
	@Getter private Map<Integer, T> playersByEntityId;
	
	@Getter private Class<T> playerClass;
	
	@Getter private int playerCacheTime = 20 * 60 * 10;
	private Map<UUID, T> cachedPlayers = new HashMap<>();
	private Map<UUID, BukkitTask> cachedTasks = new HashMap<>();
	
	
	public PlayerManager(Class<T> playerClass) {
		super(PlayModeManager.class, ChatManager.class);
		instance = this;
		this.playerClass = playerClass;
	}
	
	public static MetaData<?> registerMetaData(MetaData<?> metaData) {
		metaDatas.put(metaData.getKey().toLowerCase(), metaData);
		
		return metaData;
	}
	
	public static Collection<LythrionPlayer> getOnlineLythrionPlayers() {
		return ImmutableList.copyOf(instance.players.values());
	}
	
	public static Collection<LythrionPlayer> getCachedLythrionPlayers() {
		return ImmutableList.copyOf(instance.cachedPlayers.values());
	}
	
	public static LythrionPlayer getLythrionPlayer(Player player) {
		return instance.getPlayer(player.getUniqueId());
	}
	
	public static LythrionPlayer getLythrionPlayer(String name) {
		return instance.getPlayer(name);
	}
	
	public static LythrionPlayer getLythrionPlayer(int entityId) {
		return instance.getPlayer(entityId);
	}
	
	public static LythrionPlayer getLythrionPlayer(UUID id) {
		return instance.getPlayer(id);
	}
	
	public static int getOnlinePlayerCount() {
		return instance.players.size();
	}
	
	String member = Rank.MEMBER.getPrefix();
	String dion = Rank.DION.getPrefix(); 
	String thrion = Rank.THRION.getPrefix();
	String lythrion = Rank.LYTHRION.getPrefix();
	String builder = Rank.BUILDER.getPrefix();
	String jrmod = Rank.JRMOD.getPrefix();
	String mod = Rank.MOD.getPrefix();
	String srmod = Rank.SRMOD.getPrefix();
	String vip = Rank.VIP.getPrefix();
	String admin = Rank.ADMIN.getPrefix();
	String owner = Rank.OWNER.getPrefix();
	
	String red = ChatColor.RED.name();
	
	@Override
	public void onEnable() {
		players = new ConcurrentHashMap<>(8, 0.9f, 1); 
		playersByName = new ConcurrentHashMap<>(8, 0.9f, 1);
		playersByEntityId = new ConcurrentHashMap<>(8, 0.9f, 1);
		
		cachedPlayers = new ConcurrentHashMap<>(8, 0.9f, 1);
		cachedTasks = new ConcurrentHashMap<>(8, 0.9f, 1);
		
		for(Player player : Bukkit.getOnlinePlayers()) {
			loadPlayerObject(player);
		}
		
		for(Rank rank : Rank.values()) {
			WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
			wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.TEAM_CREATED); 
			wrapper.setName(rank.toString() + "_rank");
			wrapper.setDisplayName(rank.toString());
			switch (rank.name().toLowerCase()) {
			case "member":
				wrapper.setPrefix(member + ChatColor.RESET); 
				break;
			case "dion":
				wrapper.setPrefix(dion + ChatColor.RESET); 
				break;
			case "thrion":
				wrapper.setPrefix(thrion + ChatColor.RESET); 
				break;
			case "lythrion":
				wrapper.setPrefix(lythrion + ChatColor.RESET); 
				break;
		    case "jrmod":
				wrapper.setPrefix(jrmod + ChatColor.RESET); 
				break;
			case "builder":
				wrapper.setPrefix(builder + ChatColor.RESET); 
				break;
			case "mod":
				wrapper.setPrefix(mod + ChatColor.RESET); 
				break;
			case "srmod":
				wrapper.setPrefix(srmod + ChatColor.RESET); 
				break;
			case "vip":
				wrapper.setPrefix(vip + ChatColor.RESET); 
				break;
			case "admin":
				wrapper.setPrefix(admin + ChatColor.RESET);
				break;
			case "owner":
				wrapper.setPrefix(owner + ChatColor.RESET); 
				break;
			default:
				wrapper.setPrefix(member + ChatColor.RESET);
				break;
			}
			wrapper.setNameTagVisibility("always");
			
			teamPackets.put(rank, wrapper);
		}
		
		for(ChatColor color : ChatColor.values()) {
			for (Rank rank : Rank.values()) {
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.TEAM_CREATED); 
				wrapper.setName(color.name()+rank.name().substring(0, 2));
				wrapper.setDisplayName(color.name()+rank.name().substring(0, 2));
				switch (rank.name().toLowerCase()) {
				case "member":
					wrapper.setPrefix(member + color); 
					break;
				case "dion":
					wrapper.setPrefix(dion + color); 
					break;
				case "thrion":
					wrapper.setPrefix(thrion + color); 
					break;
				case "lythrion":
					wrapper.setPrefix(lythrion + color); 
					break;
			    case "jrmod":
					wrapper.setPrefix(jrmod + color);
					break;
				case "builder":
					wrapper.setPrefix(builder + color); 
					break;
				case "mod":
					wrapper.setPrefix(mod + color); 
					break;
				case "srmod":
					wrapper.setPrefix(srmod + color);  
					break;
				case "vip":
					wrapper.setPrefix(vip + color); 
					break;
				case "admin":
					wrapper.setPrefix(admin + color); 
					break;
				case "owner":
					wrapper.setPrefix(owner + color); 
					break;
				default:
					wrapper.setPrefix(member  + color);
					break;
				}
				wrapper.setNameTagVisibility("always");
				
				rankColorPackets.put(color.name()+rank.name().substring(0, 2), wrapper);
			}
		}
		
		for(ChatColor color : ChatColor.values()) {
			WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
			wrapper.setName(color.name());
			wrapper.setDisplayName(color.name());
			wrapper.setPrefix(color + "");
			wrapper.setNameTagVisibility("always");
			
			colorPackets.put(color, wrapper);
		}
	}

	@Override
	public void onDisable() {
		Utils.nullify(this);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onPlayerDamage(EntityDamageEvent event) {
		if(event.isCancelled()) return;
		if(event.getEntityType() != EntityType.PLAYER) return;
		
		LythrionPlayer player = getPlayer(event.getEntity().getEntityId());
		player.setLastDamageTime(System.currentTimeMillis());
		
		if(event instanceof EntityDamageByEntityEvent) {
			Entity damager = ((EntityDamageByEntityEvent) event).getDamager();
			if(damager == null) return;
			
			if(damager.getType() == EntityType.PLAYER) {
				player.setLastPlayerDamageTime(System.currentTimeMillis());
			}
			else if(damager instanceof Projectile) {
				ProjectileSource shooter = ((Projectile) damager).getShooter();
				if(shooter == null) return;
				if(shooter instanceof Player) player.setLastPlayerDamageTime(System.currentTimeMillis());
			}
		}
	}
	
	private static final String KICK_MESSAGE_ERROR = ChatPrefix.LYHRION_RED + ChatColor.WHITE + "\nSomething went wrong! Please try again later.";
	//private static final String KICK_MESSAGE_PENDING_RESTART = ChatPrefix.LYHRION_RED + ChatColor.GRAY + "\nThis server is pending for a restart.\nPlease contact staff.";
	//private static final String KICK_MESSAGE_MAINTENANCE = ChatPrefix.LYHRION_RED + ChatColor.GRAY + "\nServer is undergoing maintenance.\nCheck @tostinetwork on twitter for more info.";
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerPreJoin(AsyncPlayerPreLoginEvent event) {		
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent event) {	
		LythrionLib.getExecutorPool().submit(() -> {
			long totalTime = System.nanoTime();
			long loadPlayerTime;
			try {
				loadPlayerTime = System.nanoTime();
				T lythrionPlayer = loadPlayer(event.getPlayer());
				loadPlayerTime = System.nanoTime() - loadPlayerTime;
				
				if(lythrionPlayer == null) {
					event.getPlayer().kickPlayer(KICK_MESSAGE_ERROR);
					return;
				}
				
				Bukkit.getScheduler().runTaskAsynchronously(LythrionLib.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						//Add other players to name colors
						for(WrapperPlayServerScoreboardTeam packet : teamPackets.values()) {
							packet = new WrapperPlayServerScoreboardTeam(packet.getHandle().deepClone());
							Rank thisRank = Rank.fromString(packet.getName().replace("_rank", ""));
							
							List<String> players = new LinkedList<String>();
							
							for(LythrionPlayer p : getOnlinePlayers()) {
								if(p.getDisplayRank() != thisRank) continue;
								if(p.tab.hasCustomColor()) continue;
								if(p.tab.hasCustomRankColor()) continue;
								
								players.add(p.getCurrentName());
							}
							
							packet.setPlayers(players);
							packet.sendPacket(lythrionPlayer.getBukkitPlayer());
						}
						
						//Add colors to colors
						for(Entry<String, WrapperPlayServerScoreboardTeam> entry : rankColorPackets.entrySet()) {
							WrapperPlayServerScoreboardTeam packet = new WrapperPlayServerScoreboardTeam(entry.getValue().getHandle().deepClone());
							String thisRank = packet.getName();
							
							List<String> players = new LinkedList<String>();
							
							for(LythrionPlayer p : getOnlinePlayers()) {
								if(!p.tab.hasCustomRankColor()) continue;
                                if(!(p.tab.getCustomRankColor().name() + p.getDisplayRank().name().substring(0, 2)).equalsIgnoreCase(thisRank)) continue;

								players.add(p.getCurrentName());
							}
							
							packet.setPlayers(players);
							packet.sendPacket(lythrionPlayer.getBukkitPlayer());
						}
						
						for(Entry<ChatColor, WrapperPlayServerScoreboardTeam> entry : colorPackets.entrySet()) {
							WrapperPlayServerScoreboardTeam packet = new WrapperPlayServerScoreboardTeam(entry.getValue().getHandle().deepClone());
							ChatColor thisRank = entry.getKey();
							
							List<String> players = new LinkedList<String>();
							
							for(LythrionPlayer p : getOnlinePlayers()) {
								if(!p.tab.hasCustomColor()) continue;
								if(p.tab.getCustomColor() != thisRank) continue;
								
								players.add(p.getCurrentName());
							}
							
							packet.setPlayers(players);
							packet.sendPacket(lythrionPlayer.getBukkitPlayer());
						}
					}
				});
				
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				
				if(lythrionPlayer.tab.hasCustomColor()) wrapper.setName(lythrionPlayer.tab.getCustomColor().name()+lythrionPlayer.getDisplayRank().name().substring(0, 2));
				else wrapper.setName(lythrionPlayer.getDisplayRank().toString() + "_rank");
				
				wrapper.setMode(3);
				
				List<String> players = new ArrayList<String>(1);
				players.add(lythrionPlayer.getName());
				wrapper.setPlayers(players);
				
				for(LythrionPlayer p : getOnlinePlayers()) {
					if(p == lythrionPlayer) continue;
					
					wrapper.sendPacket(p.getBukkitPlayer());
				}				
				
				LythrionLib.getBootstrapper()
				.getChannel()
				.writeAndFlush(new PacketPlayerServerJoin(event.getPlayer()
					.getUniqueId()));
				
				lythrionPlayer.cleanTitle();				
			} catch(Exception e) {
				System.out.println("[LythrionLib] Error while player logging in: " + event.getPlayer().getName());
				e.printStackTrace();
				event.getPlayer().kickPlayer(KICK_MESSAGE_ERROR);
				return;
			}
			
			totalTime = System.nanoTime() - totalTime;
			System.out.println("[LythrionLib] Succesfully loaded player " + event.getPlayer().getName() + " (" + totalTime + "ns, " + loadPlayerTime + "ns)");			
		});
	}
	
	protected final T loadPlayer(Player bukkitPlayer) {
		T player = null;
		
		try {
			player = loadPlayerObject(bukkitPlayer);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		timerLoadPlayer.startTiming(); 
		
		addPlayer(player); 
		
		Bukkit.getPluginManager().callEvent(new LythrionPlayerLoadEvent(player));
		
		if(shouldAddAdminChat && (player.hasRankPower(Rank.BUILDER) || player.hasPermission("helper.adminchat"))) {
			player.chat.addAvailableChats(ChatManager.CHANNEL_ADMIN);
			player.chat.addListeningChats(ChatManager.CHANNEL_ADMIN);
		}
		
		final LythrionPlayer finalPlayer = player;
		Bukkit.getScheduler().runTask(LythrionLib.getInstance(), () -> {
			if(finalPlayer.hasRankPower(Rank.MOD)) {
				DefaultPlayModeRequestEvent requestEvent = new DefaultPlayModeRequestEvent(PlayModeVanilla.class, bukkitPlayer, finalPlayer.getRank());
				Bukkit.getPluginManager().callEvent(requestEvent);
					
				finalPlayer.setPlayMode(requestEvent.getPlayMode());				
			}
			else {
				DefaultPlayModeRequestEvent requestEvent = new DefaultPlayModeRequestEvent(PlayModeVanilla.class, bukkitPlayer, finalPlayer.getRank());
				Bukkit.getPluginManager().callEvent(requestEvent);
				
				finalPlayer.setPlayMode(requestEvent.getPlayMode());
			}
			
			Bukkit.getPluginManager().callEvent(new LythrionPlayerJoinEvent(finalPlayer));
		});
		
		System.out.println("[LythrionLib] Loaded LythrionPlayer: " + player.getName());
		timerLoadPlayer.stopTiming();
		return player;
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent event) {
		T player = getPlayer(event.getPlayer());
		
		Bukkit.getPluginManager().callEvent(new LythrionPlayerQuitEvent(player));
		
		unloadPlayer(player);
		
		Bukkit.getScheduler().runTaskAsynchronously(LythrionLib.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				LythrionLib.getBootstrapper()
						.getChannel()
						.writeAndFlush(new PacketPlayerServerQuit(event.getPlayer()
								.getUniqueId()));
			}
		});
	}	
	
	public Collection<T> getOnlinePlayers() {
		return players.values();
	}
	
	public void saveAll() {
		for(LythrionPlayer player : players.values()) {
			try {
				player.save();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected final void unloadPlayer(T player) {
		LythrionLib.getExecutorPool().submit(() -> player.save());
		
		unloadPlayerObject(player);
	}
	
	
	protected T loadPlayerObject(Player bukkitPlayer) {
		T cachedPlayer = cachedPlayers.remove(bukkitPlayer.getUniqueId());
		if(cachedPlayer != null) {
			timerLoadPlayerCached.startTiming();
			BukkitTask task = cachedTasks.remove(bukkitPlayer.getUniqueId());
			if(task != null) task.cancel();
			
			System.out.println("[PlayerManager] Retrieved cached player object for " + bukkitPlayer.getName() + ".");
			cachedPlayer.setBukkitPlayer(bukkitPlayer);
			cachedPlayer.setId(bukkitPlayer.getUniqueId());
			
			System.out.println("[PlayerManager] Doing an Async load for " + bukkitPlayer.getName() + ".");
			LythrionLib.getExecutorPool().submit(() -> {
				if(!cachedPlayer.loadAll()) cachedPlayer.kickPlayer(KICK_MESSAGE_ERROR);
			});
			
			timerLoadPlayerCached.stopTiming();
			return cachedPlayer;
		}
		try {
			timerLoadPlayerNew.startTiming();
			System.out.println("[PlayerManager] Creating new player object for " + bukkitPlayer.getName() + ".");
			T player = playerClass.newInstance();
			timerLoadPlayerNew.stopTiming();
			
			player.setBukkitPlayer(bukkitPlayer);
			player.setId(bukkitPlayer.getUniqueId());
			 
			timerLoadPlayerData.startTiming();
			timerLoadPlayerDataLoad.startTiming();
			if(!player.load()) {
				timerLoadPlayerDataLoad.stopTiming();
				timerLoadPlayerData.stopTiming();
				return null;
			}
			timerLoadPlayerDataLoad.stopTiming();
			
			timerLoadPlayerDataLoadAdditional.startTiming();			
			if(!player.loadAdditional()) player.kickPlayer(KICK_MESSAGE_ERROR);
			timerLoadPlayerDataLoadAdditional.stopTiming();
			timerLoadPlayerData.stopTiming();
			
			System.out.println("[PlayerManager] Created new player object for " + bukkitPlayer.getName() + ".");
			return player;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	protected void unloadPlayerObject(T player) {
		removePlayer(player);
		
		int playerCacheTime = getPerPlayerCacheTime(player);
		if(playerCacheTime != 0) {
			cachedPlayers.put(player.getId(), player);
			Bukkit.getPluginManager().callEvent(new PlayerCacheEvent(player));
			
			if(playerCacheTime > 0) {
				cachedTasks.put(player.getId(), Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), () -> unloadCachedPlayer(player), playerCacheTime));
			}
			
			System.out.println("[PlayerManager] Cached player object for " + player.getName() + ".");  
			return;
		}
		else {
			System.out.println("[PlayerManager] Unloaded player object for " + player.getName() + ".");
			player.unload();
			Bukkit.getPluginManager().callEvent(new PlayerCacheDeleteEvent(player));
		}
	}
	
	public boolean unloadCachedPlayer(@NonNull LythrionPlayer player) {
		if(player.isOnline()) throw new RuntimeException("Player is still online");
		
		T p = cachedPlayers.remove(player.getId());
		
		cachedTasks.remove(player.getId());
		
		if(p != null) {
			Bukkit.getPluginManager().callEvent(new PlayerCacheDeleteEvent(player));
			System.out.println("[PlayerManager] Unloaded cached player " + player.getName() + ".");
			p.unload();
		}
		
		return p != null;
	}
	
	public static MetaData<?> getMetaData(String key) {
		return metaDatas.get(key.toLowerCase());
	}
	
	protected int getPerPlayerCacheTime(T player) {
		return playerCacheTime;
	}
	
	/**
	 * Enabled the player cache.
	 * 
	 * @param playerCacheTime The time to keep player cached.
	 */
	public void enablePlayerCacheTime(int playerCacheTime) {
		this.playerCacheTime = playerCacheTime;
	}
	
	/**
	 * Enabled player cache with a default of 3 minutes cache time.
	 */
	public void enablePlayerCacheTime() {
		enablePlayerCacheTime(20 * 60 * 3);
	}
	
	/**
	 * Resets the player cache time to 0.
	 */
	public void disablePlayerCacheTime() {
		playerCacheTime = 0;
	}

	private void addPlayer(T player) {
		players.put(player.getId(), player);
		playersByName.put(player.getName().toLowerCase(), player);
		playersByEntityId.put(player.getEntityId(), player);
	}
	
	private void removePlayer(T player) {
		players.remove(player.getId());
		playersByName.remove(player.getName().toLowerCase());
		playersByEntityId.remove(player.getEntityId());
	}

	public T getPlayer(Player player) {
		return players.get(player.getUniqueId());
	}

	public T getPlayer(String name) {
		return playersByName.get(name.toLowerCase());
	}

	public T getPlayer(int entityId) {
		return playersByEntityId.get(entityId);
	}

	public T getPlayer(UUID id) {
		return players.get(id);
	}
	
	public T getCachedPlayer(Player player) {
		return cachedPlayers.get(player.getUniqueId());
	}
	
	public T getCachedPlayer(UUID id) {
		return cachedPlayers.get(id);
	}

	public boolean isPlayerCached(UUID playerId) {
		return cachedPlayers.containsKey(playerId);
	}
	
	public static Collection<MetaData<?>> getMetaDatas() {
		return metaDatas.values();
	}
}
