package net.planckton.lib.player.prefix;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;

public class PlayerPrefix {
	@Getter String prefix;
	@Getter String[] info;
	@Getter @Setter boolean hideWhileNicked;
	
	public PlayerPrefix(String prefix, String... info) {
		this.prefix = ChatColor.translateAlternateColorCodes('&', prefix);
		
		this.info = new String[info.length];
		for(int i = 0; i < info.length; i++) this.info[i] = ChatColor.translateAlternateColorCodes('&', info[i]);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this) return true;
		if(obj instanceof PlayerPrefix) {
			PlayerPrefix otherPrefix = ((PlayerPrefix) obj);

			return prefix.equals(otherPrefix.prefix) && Arrays.equals(info, otherPrefix.info);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return prefix.hashCode();
	}
}
