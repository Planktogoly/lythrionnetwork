package net.planckton.lib.player;

import lombok.Getter;

public class MetaData<T> {
	@Getter private String key;
	
	public MetaData(String key) {
		this.key = key;
	}
	
	public final T get(LythrionPlayer player) {
		try {
			return (T) player.getMetaData(this);
		} catch(ClassCastException e) {
			throw new RuntimeException("MetaData recieved a wrong type for key: " + key);
		}
	}
	
	public final void set(LythrionPlayer player, T value) {
		onSet(player, value);
		
		player.setMetaData(this, value);
	}
	
	public final void remove(LythrionPlayer player) {
		player.removeMetaData(this);
	}
	
	protected final void load(LythrionPlayer player) {
		T value = onLoad(player);
		if(value == null) return;
		
		player.setMetaData(this, value);
	}
	
	protected final void loadAdditional(LythrionPlayer player) {
		T value = onLoadAdditional(player);
		if(value == null) return;
		
		player.setMetaData(this, value);
	}
	
	protected T onLoad(LythrionPlayer player) {
		return null;
	}
	
	protected T onLoadAdditional(LythrionPlayer player) {
		return null;
	}
	
	protected void onSet(LythrionPlayer player, T newValue) {
		
	}
	
	@SuppressWarnings("unchecked")
	protected final void save(Object data, LythrionPlayer player) {
		save(player, (T)data);
	}
	
	protected void save(LythrionPlayer player, T data) {
		
	}

	public void resetForAll() {
		for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
			remove(player);
		}
		
		for(LythrionPlayer player : PlayerManager.getCachedLythrionPlayers()) {
			remove(player);
		}
	}
}
