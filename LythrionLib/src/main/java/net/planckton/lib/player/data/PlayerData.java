package net.planckton.lib.player.data;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;

@RequiredArgsConstructor
public class PlayerData {
	@Getter protected final @NonNull LythrionPlayer player;
	private boolean loaded;
	private boolean loadedAdditional;
	
	public final void load() {
		onLoad();
		
		loaded = true;
	}
	
	public final void loadAdditional() {
		onLoadAdditional();
		
		loadedAdditional = true;
	}
	
	public final boolean isFullyLoaded() {
		return loaded && loadedAdditional;
	}
	
	//Overrideable methods
	protected void onLoad() {
		
	}
	
	protected void onLoadAdditional() {
		
	}
	
	public void save() {
		
	}
	
	public void unload() {
		
	}
	
	public boolean shouldCancelLoadOnFail() {
		return false;
	}
}
