package net.planckton.lib.player.data;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.lvariable.AsyncSafeInteger;
import net.planckton.lib.lvariable.Listener;
import net.planckton.lib.player.LythrionPlayer;

@RequiredArgsConstructor
public class Balance {
	@NonNull @Getter private LythrionPlayer player;
	@NonNull private AsyncSafeInteger currencyGeneral;
	@NonNull private AsyncSafeInteger generalTotal;
	
	public Balance(LythrionPlayer player, int currencyGeneral, int generalTotal) {
		this.player = player;
		this.currencyGeneral = new AsyncSafeInteger(currencyGeneral);
		this.generalTotal = new AsyncSafeInteger(generalTotal);
	}
	
	public void save() {
		LythrionLib.getExecutorPool().submit(() -> DatabaseLib.setBalance(this));
	}
	
	public int getCurrencyGeneral() {
		return currencyGeneral.getValue();
	}
	
	public int getCurrencyGeneralTotal() {
		return generalTotal.getValue();
	}
	
	public void registerListenerGeneral(Listener<Integer> listener) {
		currencyGeneral.registerListener(listener);
	}
	
	public int rewardCurrencyGeneral(int amount) {
		amount *= player.getRank().getGeneralCurrenyMultiplier();
		
		currencyGeneral.add(amount);
		generalTotal.add(amount);
		
		Bukkit.getPluginManager().callEvent(new PlayerBalanceChangeEvent(player, this));
		
		save();
		return amount;
	}
	
	public void subtractCurrencyGeneral(int amount) {
		currencyGeneral.decrement(amount);
		
		Bukkit.getPluginManager().callEvent(new PlayerBalanceChangeEvent(player, this));
		
		save();
	}
	
	/**
	 * Only use setCurrencyGeneral() to reset the balance
	 */
	public void setCurrencyGeneral(int amount) {
		currencyGeneral.set(amount);
		
		Bukkit.getPluginManager().callEvent(new PlayerBalanceChangeEvent(player, this));
		
		save();
	}
	
	public int rewardCrystals(int amount) {
		int reward = rewardCurrencyGeneral(amount);
		player.sendMessage(ChatColor.GRAY + "You gained " + ChatColor.AQUA + reward + " crystals.");
		return reward;
	}
	
	@RequiredArgsConstructor
	public static class PlayerBalanceChangeEvent extends Event {
		private static final HandlerList handlers = new HandlerList();
		
		@Getter private final LythrionPlayer player;
		@Getter private final Balance balance;
		
		public HandlerList getHandlers() {
		    return handlers;
		}
		 
		public static HandlerList getHandlerList() {
		    return handlers;
		}
	}
}
