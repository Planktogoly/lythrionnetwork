package net.planckton.lib.player.data;

import java.util.Arrays;

import lombok.Getter;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

import org.bukkit.ChatColor;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardTeam;

public class TabColorData extends PlayerData {
	@Getter private ChatColor customColor;
	@Getter private ChatColor customRankColor;
	
	public TabColorData(LythrionPlayer player) {
		super(player);
	}
	
	/**
	 * Sets the players color in tab.
	 * 
	 * @param nameColor The color to use, or null to reset to default.
	 */
	public void setRankTabColor(ChatColor customRankColor) {
		this.customColor = null;
		this.customRankColor =customRankColor;
		
		updateTabColor();
	}
	
	public void setTabColor(ChatColor nameColor) {
		this.customColor = nameColor;
		this.customRankColor = null;
		
		updateTabColor();
	}
	
	/**
	 * Sets the tab color back to the original color.
	 */
	public void resetTabColor() {
		setTabColor(null);
		setRankTabColor(null);
	}
	
	public ChatColor getExactTabColor() {
		if(customColor != null) return customColor;
		if (customRankColor != null) return customRankColor;
		else return ChatColor.WHITE;
	}
	
	public String getTabColor() {
		if(customColor != null) return customColor.name();
		if (customRankColor != null) return customRankColor.name();
		else return player.getDisplayRank().getPrefix().toString();
	}
	
	public void updateTabColor() {
		if (customColor != null) {
			try {
				ChatColor color = ChatColor.valueOf(getTabColor());
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.PLAYERS_ADDED);
				wrapper.setName(color.name());
				
				wrapper.setPlayers(Arrays.asList(player.getCurrentName()));
				
				for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
					wrapper.sendPacket(player.getBukkitPlayer());
				}
			} catch (IllegalArgumentException e) {
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.PLAYERS_ADDED);
				wrapper.setName(player.getDisplayRank().toString()+ "_rank");
				
				wrapper.setPlayers(Arrays.asList(player.getCurrentName()));
				
				for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
					wrapper.sendPacket(player.getBukkitPlayer());
				}
			}
		} else {
			try {
				ChatColor color = ChatColor.valueOf(getTabColor());
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.PLAYERS_ADDED);
				wrapper.setName(color.name()+player.getDisplayRank().name().substring(0, 2));
				
				wrapper.setPlayers(Arrays.asList(player.getCurrentName()));
				
				for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
					wrapper.sendPacket(player.getBukkitPlayer());
				}
			} catch (IllegalArgumentException ee) {
				WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam();
				wrapper.setMode(WrapperPlayServerScoreboardTeam.Mode.PLAYERS_ADDED);
				wrapper.setName(player.getDisplayRank().toString()+ "_rank");
				
				wrapper.setPlayers(Arrays.asList(player.getCurrentName()));
				
				for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
					wrapper.sendPacket(player.getBukkitPlayer());
				}				
			}
		}
	}

	public boolean hasCustomColor() {
		return customColor != null;
	}
	
	public boolean hasCustomRankColor() {
		return customRankColor != null;
	}
}
