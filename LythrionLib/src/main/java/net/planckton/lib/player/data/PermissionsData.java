package net.planckton.lib.player.data;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import lombok.NonNull;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.rank.Rank;

public class PermissionsData extends PlayerData {
	@NonNull private Set<String> permissions = new HashSet<>();
//	@Getter private static Map<Class<? extends PlayMode>, List<String>> blockedPerms = new HashMap<>();
	
	public PermissionsData(LythrionPlayer player) {
		super(player);
	}
	
	@Override
	protected void onLoadAdditional() {
		List<String> loadedPermissions = DatabaseLib.getPermissions(player.getId());
		if(loadedPermissions != null) permissions.addAll(loadedPermissions);
	}
	
	/*public static void addBlockedPermission(String permission, Class<? extends PlayMode> playMode) {
		if (blockedPerms.containsKey(playMode)) {
			blockedPerms.get(playMode).add(permission);
		}		
		
		ArrayList<String> perms = new ArrayList<>(); 
		perms.add(permission);
		blockedPerms.put(playMode, perms);
		System.out.println("[TostiPerms] Added blockedPerm to playMode:" + playMode.getSimpleName() + " Permission: " + perms);
	}*/
	
	//modifiers
	public void addPermission(String permission, UUID granter) {
		permissions.add(permission);
		LythrionLib.getExecutorPool().submit(() -> DatabaseLib.addPermission(player.getId(), permission, granter));
	}
	
	public void removePermission(String permission) {
		permissions.remove(permission);
		LythrionLib.getExecutorPool().submit(() -> DatabaseLib.removePermission(player.getId(), permission));
	}
	
	public void addPermissionLocal(String permission) {
		permissions.add(permission);
	}
	
	//getters
	public Set<String> getPermissions() {
		return Collections.unmodifiableSet(permissions);
	}
	
	public boolean hasPermission(String permission) {
		if (player.getRank() == Rank.OWNER) return true;		
				
     /*	if (!(player.getPlayMode() == null)) {
			 if (getBlockedPerms().containsKey(player.getPlayMode().getClass())) {
					for (String blockedPerms : getBlockedPerms().get(player.getPlayMode().getClass())) {
						if (permission.matches(blockedPerms)) return false;
					}
				}
		}*/
     	
		for (String perm : permissions) {			
			if (permission.matches(perm)) {
				return true;
			}
		}
		
		return false;
	}
}
