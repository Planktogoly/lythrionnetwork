package net.planckton.lib.sidebar;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardScore;
import com.comphenix.protocol.wrappers.EnumWrappers.ScoreboardAction;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.Utils;

@RequiredArgsConstructor(access=AccessLevel.PACKAGE)
abstract class Score<T extends Sidebar<?,?,?>> {
	private @NonNull T board;
	@Getter @NonNull String key;
	
	public T getBoard() {
		return board;
	}
	
	public void remove() {
		board.removeScore(this);
		
		Utils.nullify(this);
	}

	public abstract void update(LythrionPlayer player);
	public abstract void updateAll();
	
	WrapperPlayServerScoreboardScore constructPacketRemove(@NonNull String key) {
		WrapperPlayServerScoreboardScore wrapper = new WrapperPlayServerScoreboardScore();
		
		wrapper.setObjectiveName(getBoard().getName());
		wrapper.setScoreboardAction(ScoreboardAction.REMOVE);
		wrapper.setScoreName(key);
		
		return wrapper;
	}
	
	WrapperPlayServerScoreboardScore constructPacket(@NonNull String key, int score) {
		WrapperPlayServerScoreboardScore wrapper = new WrapperPlayServerScoreboardScore();
		
		wrapper.setObjectiveName(getBoard().getName());
		wrapper.setScoreboardAction(ScoreboardAction.CHANGE);
		wrapper.setScoreName(key);
		wrapper.setValue(score);
		
		return wrapper;
	}
}
