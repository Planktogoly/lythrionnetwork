package net.planckton.lib.sidebar;

public class AdvancedScoreWhitespace extends AdvancedScore {
	private static int WHITESPACE_COUNTER;
	
	AdvancedScoreWhitespace(AdvancedSidebar board) {
		super(board, new String(new char[WHITESPACE_COUNTER++]).replace("\0", " "));
	}
}
