package net.planckton.lib.sidebar;

import lombok.AllArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayMode;
import net.planckton.lib.utils.PlayerCallable;

@AllArgsConstructor
public class PlayerCallableRequiredMode implements PlayerCallable<String> {
	private Class<? extends PlayMode> requiredMode;
	private PlayerCallable<String> callable;
	
	@Override
	public String call(LythrionPlayer player) {
		if(!player.isInPlayMode(requiredMode)) return null;
		
		return callable.call(player);
	}

}
