package net.planckton.lib.sidebar;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardDisplayObjective;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardObjective;
import com.google.common.collect.ImmutableSet;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.ManagedPlayerMap;
import net.planckton.lib.utils.PlayerCallable;

abstract class Sidebar<S extends Score<?>, P extends PersonalScore<?, V>, V> {
	private final WrapperPlayServerScoreboardObjective wrapperCreate = new WrapperPlayServerScoreboardObjective();
	private final WrapperPlayServerScoreboardDisplayObjective wrapperDisplay = new WrapperPlayServerScoreboardDisplayObjective();
	
	@Getter private final String name;
	@Getter private String displayName;

	private final Map<UUID, LythrionPlayer> listeningPlayers = ManagedPlayerMap.wrap(new HashMap<UUID, LythrionPlayer>());
	
	private final Set<Score<?>> scores = new LinkedHashSet<>();
	
	Sidebar(String name, String displayName) {
		this.name = name;
		this.displayName = displayName;
		
		wrapperCreate.setMode(WrapperPlayServerScoreboardObjective.Mode.ADD_OBJECTIVE);
		wrapperCreate.setName(name);
		wrapperCreate.setDisplayName(displayName);
		wrapperCreate.setHealthDisplay("integer");
		
		wrapperDisplay.setPosition(WrapperPlayServerScoreboardDisplayObjective.Position.SIDEBAR);
		wrapperDisplay.setScoreName(name);
	}
	
	public S createScore(String key) {
		S score = onCreateScore(key);
		
		scores.add(score);
		
		return score;
	}
	
	public P createPersonalScore(@NonNull String key, @NonNull PlayerCallable<V> callable) {
		P score = onCreatePersonalScore(key, callable);
		
		scores.add(score);
		
		score.updateAll();
		
		return score;
	}
	
	abstract S onCreateScore(String key);
	abstract P onCreatePersonalScore(String key, PlayerCallable<V> callable);
	
	protected void removeScore(Score<?> score) {
		scores.remove(score.getKey());
	}
	
	/**
	 * The player will join the sideboard and the board will
	 * also be displayed.
	 * 
	 * If The player just has to join the board use {@link #joinBoard(LythrionPlayer) joinBoard(player)}
	 * 
	 * @param player Player to join the board.
	 */
	@Deprecated
	public void setBoard(LythrionPlayer player) {
		joinBoard(player);
		display(player);
	}
	
	public void joinBoard(LythrionPlayer player) {
		if(listeningPlayers.containsKey(player.getId())) return;	
		listeningPlayers.put(player.getId(), player);
		
		wrapperCreate.sendPacket(player.getBukkitPlayer());
		
		for(Score<?> score : scores) {
			score.update(player);
		}
	}
	
	/**
	 * The player will join the sideboard and the board will
	 * also be displayed.
	 * 
	 * If The player just has to join the board use {@link #joinBoard(LythrionPlayer) joinBoard(player)}
	 * 
	 * @param player Player to join the board.
	 */
	public void display(LythrionPlayer player) {
		joinBoard(player);

		wrapperDisplay.sendPacket(player.getBukkitPlayer());
		
		for(Score<?> score : scores) {
			try {
				score.update(player);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void leaveBoard(LythrionPlayer player) {
		listeningPlayers.remove(player);		
	}
	
	public void setDisplayName(String displayName) {
		WrapperPlayServerScoreboardObjective wrapper = new WrapperPlayServerScoreboardObjective();
		wrapper.setMode(WrapperPlayServerScoreboardObjective.Mode.UPDATE_VALUE);
		
		wrapper.setDisplayName(displayName);
		wrapper.setHealthDisplay("integer");
		
		for(LythrionPlayer player : listeningPlayers.values()) {
			if(!player.isOnline()) continue;
			
			wrapper.sendPacket(player.getBukkitPlayer());
		}
	}
	
	//getters
	public Set<LythrionPlayer> getListeningPlayers() {
		return ImmutableSet.copyOf(listeningPlayers.values());
	}
}
