package net.planckton.lib.sidebar;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lombok.NonNull;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.PlayerCallable;

public class AdvancedPersonalScore extends PersonalScore<AdvancedSidebar, String> {
	private Map<UUID, String> lastValues = new HashMap<UUID, String>();
	
	AdvancedPersonalScore(AdvancedSidebar board, String key, PlayerCallable<String> callable) {
		super(board, key, callable);
	}

	@Override
	public void update(@NonNull LythrionPlayer player) {
		String value = getCallable().call(player);
		
		String lastValue = lastValues.put(player.getId(), value);
		if(lastValue != null && !lastValue.equals(value)) {
			constructPacketRemove(lastValue).sendPacket(player.getBukkitPlayer());
		}
		
		if(value != null) constructPacket(value, getBoard().getScoreIndex(this)).sendPacket(player.getBukkitPlayer());
	}
	
	public void set(@NonNull LythrionPlayer player, String value) {
		String lastValue = lastValues.put(player.getId(), value);
		if(lastValue != null && !lastValue.equals(value)) {
			constructPacketRemove(lastValue).sendPacket(player.getBukkitPlayer());
		}
		
		if(value != null) constructPacket(value, getBoard().getScoreIndex(this)).sendPacket(player.getBukkitPlayer());
	}

	@Override
	public void updateAll() {
		for(LythrionPlayer player : getBoard().getListeningPlayers()) {
			try {
				update(player);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
