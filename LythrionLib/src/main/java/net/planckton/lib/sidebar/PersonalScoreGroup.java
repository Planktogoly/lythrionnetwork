package net.planckton.lib.sidebar;

import net.planckton.lib.player.LythrionPlayer;

public class PersonalScoreGroup {
	private PersonalScore<?,?>[] scores;
	
	public PersonalScoreGroup(PersonalScore<?,?>... scores) {
		this.scores = scores;
	}
	
	public void updateAll() {
		for(PersonalScore<?,?> score : scores) {
			score.updateAll();
		}
	}
	
	public void update(LythrionPlayer player) {
		for(PersonalScore<?,?> score : scores) {
			score.update(player);
		}
	}
}
