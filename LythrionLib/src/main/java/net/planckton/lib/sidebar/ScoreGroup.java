package net.planckton.lib.sidebar;

public class ScoreGroup {
	private Score<?>[] scores;
	
	public ScoreGroup(Score<?>... scores) {
		this.scores = scores;
	}
	
	public void updateAll() {
		for(Score<?> score : scores) {
			score.updateAll();
		}
	}
}
