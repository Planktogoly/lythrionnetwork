package net.planckton.lib.sidebar;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardScore;

import lombok.Getter;
import net.planckton.lib.player.LythrionPlayer;

public final class SimpleScore extends Score<SimpleSidebar> {
	@Getter private int score;
	
	SimpleScore(SimpleSidebar board, String key) {
		super(board, key);
	}
	
	@Override
	public void update(LythrionPlayer player) {
		constructPacket(key, score).sendPacket(player.getBukkitPlayer());
	}
	
	public void setScore(int score) {
		this.score = score;
		
		updateAll();
	}
	
	@Override
	public void updateAll() {
		WrapperPlayServerScoreboardScore wrapper = constructPacket(key, score);
		
		for(LythrionPlayer player : getBoard().getListeningPlayers()) {
			try {
				wrapper.sendPacket(player.getBukkitPlayer());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
