package net.planckton.lib.sidebar;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardScore;

import net.planckton.lib.player.LythrionPlayer;

public class AdvancedScore extends Score<AdvancedSidebar> {
	
	AdvancedScore(AdvancedSidebar board, String key) {
		super(board, key);
	}

	@Override
	public void update(LythrionPlayer player) {
		constructPacket(key, getBoard().getScoreIndex(this)).sendPacket(player.getBukkitPlayer());
	}
	
	public void setValue(String value) {
		if(!key.equals(value)) removeAll();
		
		this.key = value;
		
		updateAll();
	}

	@Override
	public void updateAll() {
		WrapperPlayServerScoreboardScore wrapper = constructPacket(key, getBoard().getScoreIndex(this));
		
		for(LythrionPlayer player : getBoard().getListeningPlayers()) {
			try {
				wrapper.sendPacket(player.getBukkitPlayer());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void removeAll() {
		WrapperPlayServerScoreboardScore wrapperRemove = constructPacketRemove(key);
		
		for(LythrionPlayer player : getBoard().getListeningPlayers()) {
			try {
				wrapperRemove.sendPacket(player.getBukkitPlayer());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
