package net.planckton.lib.sidebar;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.utils.PlayerCallable;

public final class SimplePersonalScore extends PersonalScore<SimpleSidebar, Integer> {
	private Set<UUID> hasScore = new HashSet<>();
	
	SimplePersonalScore(SimpleSidebar board, String key, PlayerCallable<Integer> callable) {
		super(board, key, callable);
	}

	@Override
	public void update(LythrionPlayer player) {
		Integer value = getCallable().call(player);
		
		if(value == null) {
			if(hasScore.remove(player.getId())) {
				constructPacketRemove(key).sendPacket(player.getBukkitPlayer());
			}
		}
		else {
			hasScore.add(player.getId());
			
			constructPacket(key, value).sendPacket(player.getBukkitPlayer());
		}
	}

	@Override
	public void updateAll() {
		for(LythrionPlayer player : getBoard().getListeningPlayers()) {
			try {
				update(player);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
