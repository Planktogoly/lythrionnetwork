package net.planckton.lib.sidebar;

import java.util.ArrayList;

import net.planckton.lib.utils.PlayerCallable;

public class AdvancedSidebar extends Sidebar<AdvancedScore, AdvancedPersonalScore, String> {
	private ArrayList<Score<?>> scores = new ArrayList<>();
	
	public AdvancedSidebar(String name, String displayName) {
		super(name, displayName);
	}

	int getScoreIndex(Score<?> score) {
		return scores.indexOf(score);
	}
	
	public AdvancedScore createWhiteSpace() {
		return createScore("whitespace");
	}
	
	@Override
	AdvancedScore onCreateScore(String key) {
		AdvancedScore score;
		if(key.equals("whitespace")) {
			score = new AdvancedScoreWhitespace(this);
		}
		else score = new AdvancedScore(this, key);
		
		scores.add(0, score);
		for(Score<?> sc : scores) {
			sc.updateAll();
		}
		return score;
	}

	@Override
	AdvancedPersonalScore onCreatePersonalScore(String key, PlayerCallable<String> callable) {
		AdvancedPersonalScore score = new AdvancedPersonalScore(this, key, callable);
		
		scores.add(0, score);
		for(Score<?> sc : scores) {
			sc.updateAll();
		}
		return score;
	}
}
