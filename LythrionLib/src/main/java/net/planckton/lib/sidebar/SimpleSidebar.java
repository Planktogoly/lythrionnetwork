package net.planckton.lib.sidebar;

import net.planckton.lib.utils.PlayerCallable;

public final class SimpleSidebar extends Sidebar<SimpleScore, SimplePersonalScore, Integer> {

	public SimpleSidebar(String name, String displayName) {
		super(name, displayName);
	}
	
	SimpleScore onCreateScore(String key) {
		return new SimpleScore(this, key);
	}

	@Override
	SimplePersonalScore onCreatePersonalScore(String key, PlayerCallable<Integer> callable) {
		return new SimplePersonalScore(this, key, callable);
	}
}
