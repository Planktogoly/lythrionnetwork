package net.planckton.lib.sidebar;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.planckton.lib.utils.PlayerCallable;

public abstract class PersonalScore<T extends Sidebar<?, ?, ?>, V> extends Score<T> {
	@Setter @Getter(value=AccessLevel.PACKAGE) private PlayerCallable<V> callable;
	
	PersonalScore(T board, String key, @NonNull PlayerCallable<V> callable) {
		super(board, key);
		
		this.callable = callable;
	}
}
