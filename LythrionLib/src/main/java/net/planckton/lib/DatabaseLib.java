package net.planckton.lib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.zaxxer.hikari.HikariDataSource;

import lombok.Cleanup;
import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.chat.ChatChannel;
import net.planckton.lib.nick.NickNameManager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.player.data.Balance;
import net.planckton.lib.player.data.CrateData;
import net.planckton.lib.playmode.PlayModeAdmin;
import net.planckton.lib.playmode.PlayModeAdmin.AdminModes;
import net.planckton.lib.position.Position;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.rank.Rank;

public class DatabaseLib {
	
	@Getter private static ConcurrentLinkedQueue<LythrionConnection> connections = new ConcurrentLinkedQueue<>();
	
	static void onConnectionClose(LythrionConnection connection) {
		connections.remove(connection);
	}
	
	@Getter private static HikariDataSource datasource;
	
	public static void open() {
		if (datasource == null) {			
			datasource = new HikariDataSource();

			datasource.setDriverClassName("com.mysql.jdbc.Driver");
			datasource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/tostimc?autoReconnect=true&useSSL=false");
			datasource.setUsername("root");
			datasource.setPassword("8r6rDabZ");
			
			datasource.setMaximumPoolSize(10);		
		}
		System.out.println("Opened the database connections.");
	}
	
	public static Connection getConnection() {
		try {
			return datasource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void close() {
		datasource.close();
		System.out.println("Closed the database connections.");
	}	
	
	public static Rank getRank(@NonNull final UUID id) {
		try {
			@Cleanup Connection connection = getConnection();			
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT rank FROM player_serverrank WHERE id_player=? ORDER BY id_serverrank DESC LIMIT 1");
			statement.setString(1, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if (!rs.next()) return Rank.MEMBER;
			
			return Rank.fromString(rs.getString("rank"));
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean setRank(@NonNull final UUID id, @NonNull final Rank rank) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_serverrank(id_player,rank) VALUES(?,?)");
			statement.setString(1, id.toString());
			statement.setString(2, rank.toString());
			
			return statement.execute();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Balance getBalance(@NonNull final LythrionPlayer player) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT currency_general,general_total FROM player_balance WHERE id_player=?");
			statement.setString(1, player.getId().toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if (!rs.next()) return new Balance(player, 0, 0);
			
			return new Balance(player, rs.getInt("currency_general"), rs.getInt("general_total"));
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static final Pattern SERVER_PATTERN = Pattern.compile("server\\.([a-zA-Z]{0,})\\.(.{0,})");
	public static List<String> getPermissions(@NonNull final UUID id) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT permission FROM player_permission WHERE id_player =? UNION SELECT permission FROM group_permission WHERE group_id IN ( SELECT group_id FROM player_group WHERE player_id =? UNION SELECT group_child FROM group_child WHERE group_id = ( SELECT group_id FROM player_group WHERE player_id =? LIMIT 1 ))");
			statement.setString(1, id.toString());
			statement.setString(2, id.toString());
			statement.setString(3, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			ArrayList<String> permissions = new ArrayList<String>();
			
			while(rs.next()) {
				String perm = rs.getString("permission");
				
				Matcher matcher = SERVER_PATTERN.matcher(perm);
				if(matcher.matches()) {
					String serverName = matcher.group(1);
					if(!serverName.equalsIgnoreCase(LythrionLib.getServerName())) continue;
					
					String actualPermission = matcher.group(2);
					permissions.add(actualPermission);
					continue;
				}
				
				permissions.add(perm);
			}
			
			return permissions;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean addPermission(@NonNull final UUID id, @NonNull final String permission, final UUID granter) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_permission(id_player, permission, id_granter) VALUES(?,?,?)");
			statement.setString(1, id.toString());
			statement.setString(2, permission);
			
			if(granter != null) statement.setString(3, granter.toString());
			else statement.setNull(3, Types.VARCHAR);
			
			return statement.execute();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean removePermission(@NonNull final UUID id, @NonNull final String permission) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("DELETE FROM player_permission WHERE id_player=? AND permission=?");
			statement.setString(1, id.toString());
			statement.setString(2, permission);
			
			return statement.execute();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static ItemStack getHat(LythrionPlayer player) {
		if(NickNameManager.isPlayerNicked(player)) return null;
		
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT material, durability, name, data, description, required_permission, required_rank FROM hat WHERE id_hat IN (SELECT id_hat FROM player_hat WHERE id_player='" + player.getId() + "')");
			
			if(!rs.next()) return null;
			
			ItemStack hat = hatFromResultSet(player, rs);
			if(hat == null) return null;
			
			return hat;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ItemStack hatFromResultSet(LythrionPlayer player, ResultSet rs) {
		try {
			HashSet<String> columns = new HashSet<>();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();

			for (int i = 1; i <= numberOfColumns; i++) {
			    columns.add(rsMetaData.getColumnName(i));
			}
			
			if(columns.contains("required_permission")) {
				String permission = rs.getString("required_permission");
				if(permission != null && !permission.equals("")) {
					if(!player.hasPermission(permission)) return null;
				}
			}
			
			Material material = Material.getMaterial(rs.getString("material"));
			
			if(material == null || material == Material.AIR) return null;
			
			short durability = rs.getShort("durability");
			String name = rs.getString("name");
			String description = rs.getString("description");
			Rank rank = Rank.fromInt(rs.getInt("required_rank"));
			
			if(name == null || description == null || rank == null) return null;
			
			ItemStack hat = new ItemStack(material, 1, durability);
			
			if(!name.equals("")) {
				ItemMeta meta = hat.getItemMeta();
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
				
				ArrayList<String> lores = new ArrayList<>();
				if(!description.equals("")) {
					String[] descriptions = description.split("&z");
					
					for(int i = 0; i < descriptions.length; i++) {
						lores.add(ChatColor.translateAlternateColorCodes('&', descriptions[i]));
					}
				}
				
				if(rank.hasPower(Rank.DION)) {
					if(lores.size() > 0) lores.add("");
					lores.add(ChatColor.GRAY + "Ranked: " + rank.getColor() + rank.toString().toLowerCase());
				}
				
				if(columns.contains("price_general")) {
					if(lores.size() > 0) lores.add("");
					lores.add(ChatColor.GRAY + "Price: " + rs.getInt("price_general"));
				}
				
				meta.setLore(lores);
				
				hat.setItemMeta(meta);
			}
			
			if(isThere(rs, "data")) {
				String data = rs.getString("data");
				if(data != null) {
					if(material == Material.SKULL_ITEM && durability == 3) {
						SkullMeta meta = (SkullMeta) hat.getItemMeta();
						meta.setOwner(data);
						hat.setItemMeta(meta);
					}
				}
			}
			else if(material == Material.SKULL_ITEM && durability == 3) {
				SkullMeta meta = (SkullMeta) hat.getItemMeta();
				meta.setOwner(player.getName());
				hat.setItemMeta(meta);
			}
			
			return hat;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static boolean isThere(ResultSet rs, String column) {
		try {
			rs.findColumn(column);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean loadPositions() {
		try {
			
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			
			String statementString = "SELECT name,world,x,y,z,pitch,yaw,rank FROM position WHERE ";
			
			int curWorld = 0;
			for(World world : Bukkit.getWorlds()) {
				statementString += "world='" + world.getName() + "'";
				
				if(curWorld < Bukkit.getWorlds().size() - 1) {
					statementString += " OR ";
				}
				
				curWorld++;
			}
			
			@Cleanup ResultSet rs = statement.executeQuery(statementString);
			
			while(rs.next()) {
				String locationName = rs.getString("name");
				String worldName = rs.getString("world");

				float x = rs.getFloat("x");
				float y = rs.getFloat("Y");
				float z = rs.getFloat("Z");
				
				float pitch = rs.getFloat("pitch");
				float yaw = rs.getFloat("yaw");
				
				Rank rank = Rank.fromString(rs.getString("rank"));
				if(rank == null) rank = Rank.MEMBER;
				
				PositionManager.load(locationName, new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch), rank);
			}
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static List<Position> loadPositions(final String worldName, final World forWorld, final String prefix) {
		try {
			ArrayList<Position> positions = new ArrayList<Position>();
			
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			
			String statementString = "SELECT name,x,y,z,pitch,yaw,rank FROM position WHERE world='" + worldName + "'";
			
			@Cleanup ResultSet rs = statement.executeQuery(statementString);
			
			while(rs.next()) {
				String locationName = prefix + rs.getString("name");
				
				float x = rs.getFloat("x");
				float y = rs.getFloat("Y");
				float z = rs.getFloat("Z");
				
				float pitch = rs.getFloat("pitch");
				float yaw = rs.getFloat("yaw");
				
				Rank rank = Rank.fromString(rs.getString("rank"));
				
				positions.add(PositionManager.load(locationName, new Location(forWorld, x, y, z, yaw, pitch), rank));
			}
			
			return positions;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Position> loadPositions(World world) {
		return loadPositions(world.getName(), world, "");
	}
	
	public static boolean addPosition(Position position) {
		for(int i = 0; i < position.locations.length; i++) {
			addPosition(position, i);
		}
		
		return true;
	}
	
	public static boolean addPosition(final Position position, final int index) {
		try {
			Location loc = position.locations[index];
			
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO position (name,world,x,y,z,pitch,yaw,rank) VALUES(?,?,?,?,?,?,?,?)");
			
			statement.setString(1, position.name + "." + index);
			statement.setString(2, loc.getWorld().getName());
			
			statement.setFloat(3, (float)loc.getX());
			statement.setFloat(4, (float)loc.getY());
			statement.setFloat(5, (float)loc.getZ());
			
			statement.setFloat(6, (float)loc.getPitch());
			statement.setFloat(7, (float)loc.getYaw());
			
			if(position.getRank() == null || position.getRank() == Rank.MEMBER) statement.setNull(8, Types.CHAR);
			else statement.setString(8, position.getRank().toString());
			
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delPosition(final Position position) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			
			@Cleanup PreparedStatement statement = connection.prepareStatement("DELETE FROM position WHERE name LIKE ? AND world=?");
			statement.setString(1, position.name+ ".%");
			statement.setString(2, position.getWorld().getName());
			
			statement.execute();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delPosition(final Position position, final int index) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("DELETE FROM position WHERE name? AND world=?");
			statement.setString(1, position.name + "." + index);
			statement.setString(2, position.getWorld().toString());
			
			return statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean updatePosition(final Position position, final int oldIndex, final int newIndex) {
		try {
			Location loc = position.locations[newIndex];
			
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("UPDATE position SET " +
					"name='" + position.name + "." + newIndex + "', " +
					"world='" + loc.getWorld().getName() + "', " +
					"x=" + loc.getX() + "," +
					"y=" + loc.getY() + "," +
					"z=" + loc.getZ() + "," +
					"pitch=" + loc.getPitch() + ", " +
					"yaw=" + loc.getYaw() + "," +
					"rank='" + position.getRank() + "'" +
							
					" WHERE name='" + position.name + "." + oldIndex + "'");
			
			return statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean setBalance(final Balance balance) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup Statement statement = connection.createStatement();
			statement.execute("UPDATE player_balance SET "
					+ "currency_general=" + balance.getCurrencyGeneral()
					+ ",general_total=" + balance.getCurrencyGeneralTotal()
					
					+  " WHERE id_player='" + balance.getPlayer().getId() + "'");
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean startAdminMode(LythrionPlayer player, PlayModeAdmin.AdminModes mode) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO adminmode(id_player,type) VALUES (?,?)");
			statement.setString(1, player.getId().toString());
			statement.setString(2, mode.toString());
			
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	

	public static boolean stopAdminMode(LythrionPlayer player) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			String statementString = "DELETE FROM adminmode WHERE id_player='" + player.getId() + "'";
			
			@Cleanup Statement statement = connection.createStatement();
			statement.execute(statementString);
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static AdminModes isAdminMode(LythrionPlayer player) {
		return isAdminMode(player.getId());
	}
	
	public static AdminModes isAdminMode(UUID id) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup Statement statement = connection.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT type FROM adminmode WHERE id_player='" + id + "'");
			
			if(!rs.next()) return null;
			
			return AdminModes.valueOf(rs.getString("type"));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static UUID getUUIDByName(final String playerName) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(playerName);
		if(player != null) return player.getId();
		
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT id_player FROM player_name WHERE name=? ORDER BY id_player_name DESC LIMIT 1");
			statement.setString(1, playerName);
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return null;
			return UUID.fromString(rs.getString("id_player"));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getNameByUUID(final UUID id) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(id);
		if(player != null) return player.getName();
		
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT name FROM player_name WHERE id_player=? ORDER BY id_player_name DESC LIMIT 1");
			statement.setString(1, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return null;
			
			return rs.getString("name");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean logChatMessage(final LythrionPlayer player, final String message, final ChatChannel chatChannel) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO log_chat(chat,server,id_player,message) VALUES(?,?,?,?)");
			
			statement.setString(1, chatChannel.chatName);
			statement.setString(2, LythrionLib.getServerName());
			statement.setString(3, player.getId().toString());
			statement.setString(4, message);
			
			statement.execute();
			
		    return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String getSettingValue(String key) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT value FROM setting WHERE `key`=? AND (end IS NULL OR end>NOW()) ORDER BY start DESC, end ASC, id_setting DESC LIMIT 0, 1");
			statement.setString(1, key);
			
			@Cleanup ResultSet rs = statement.executeQuery();
			if(!rs.next()) return null;
			
			return rs.getString("value");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ItemStack[] getAvailableHats(LythrionPlayer player) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT material, durability, name, data, description, required_permission, required_rank FROM hat WHERE ((price_general=0 AND price_premium=0) OR id_hat IN (SELECT id_hat FROM purchase_hat WHERE id_player='" + player.getId() + "')) AND (((rank_only=0 AND required_rank<=" + player.getRank().getRankId() + ") OR required_rank=" + player.getRank().getRankId() + ") OR id_hat IN (SELECT id_hat FROM purchase_hat WHERE id_player='" + player.getId() + "'))");
			
			ArrayList<ItemStack> stacks = new ArrayList<>();
			
			while(rs.next()) {
				ItemStack hat = DatabaseLib.hatFromResultSet(player, rs);
				if(hat == null) continue;
				
				stacks.add(hat);
			}
			
			return stacks.toArray(new ItemStack[stacks.size()]);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean setHat(LythrionPlayer player, ItemStack stack) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			if(stack == null || stack.getType() == Material.AIR) {
				@Cleanup PreparedStatement statement = connection.prepareStatement("DELETE FROM player_hat WHERE id_player=?");
				statement.setString(1, player.getId().toString());
				
				statement.execute();
				return true;
			}
			
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT id_hat FROM hat WHERE material=? AND durability=?");
			statement.setString(1, stack.getType().toString());
			statement.setShort(2, stack.getDurability());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return false;
			
			
			int hatId = rs.getInt("id_hat");
			@Cleanup PreparedStatement finalStatement = connection.prepareStatement(""
					+ "INSERT INTO player_hat(id_player,id_hat) "
					+ "VALUES(?,?) "
					+ "ON DUPLICATE KEY UPDATE "
					+ "id_hat=VALUES(id_hat)");
			finalStatement.setString(1, player.getId().toString());
			finalStatement.setInt(2, hatId);
			
			finalStatement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void setCrateData(CrateData data) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("UPDATE player_crate SET common_crate = ?, uncommon_crate = ?, legendary_crate = ?, towny_crate = ? WHERE id_player=?");
			statement.setInt(1, data.getCommonCrate());
			statement.setInt(2, data.getUncommonCrate());
			statement.setInt(3, data.getLegendaryCrate());
			statement.setInt(4, data.getTownycrate());
			statement.setString(5, data.getPlayer().getId().toString());
			
			statement.execute();	
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	public static void getCrateData(LythrionPlayer player) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT common_crate, uncommon_crate, legendary_crate, towny_crate FROM player_crate WHERE id_player=?");
			statement.setString(1, player.getId().toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if (!rs.next()) {
				player.crate.setCommonCrate(0);
				player.crate.setUncommonCrate(0);
				player.crate.setLegendaryCrate(0);
				player.crate.setTownycrate(0);
				return;
			}
			
			player.crate.setCommonCrate(rs.getInt("common_crate"));
			player.crate.setUncommonCrate(rs.getInt("uncommon_crate"));
			player.crate.setLegendaryCrate(rs.getInt("legendary_crate"));
			player.crate.setTownycrate(rs.getInt("towny_crate"));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	public static HashMap<String, Object[]> getAvailableBuyHats(LythrionPlayer player) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT material, durability, name, data, description, required_permission, required_rank, price_general,price_premium, id_hat FROM hat WHERE (price_general>0 OR price_premium>0) AND id_hat NOT IN (SELECT id_hat FROM purchase_hat WHERE id_player=?) AND ((rank_only=0 AND required_rank<=?) OR (rank_only=1 AND required_rank=?))");
			
			statement.setString(1, player.getId().toString());
			statement.setInt(2, player.getRank().getRankId());
			statement.setInt(3, player.getRank().getRankId());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			HashMap<String, Object[]> hats = new HashMap<>();
			
			while(rs.next()) {
				ItemStack hat = DatabaseLib.hatFromResultSet(player, rs);
				if(hat == null) continue;
				
				hats.put(hat.getItemMeta().getDisplayName(), new Object[]{hat, rs.getInt("id_hat"), rs.getInt("price_general")});
			}
			
			return hats;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean buyHat(LythrionPlayer player, int hatId) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			statement.execute("INSERT INTO purchase_hat(id_player,id_hat) VALUES ('" + player.getId() + "'," + hatId + ")");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setNickName(UUID id, UUID newId, String newName, String skinName) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_nick(id_player,uuid,name,skin) VALUES(?,?,?,?)");
			statement.setString(1, id.toString());
			statement.setString(2, newId.toString());
			statement.setString(3, newName);
			
			if(skinName != null) statement.setString(4, skinName);
			else statement.setNull(4, Types.VARCHAR);
			
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean disableNickName(UUID id) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			statement.execute("UPDATE player_nick SET active=0 WHERE id_player='" + id + "'");
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Byte getBlockColor(UUID id) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT color FROM player_blockcolor WHERE id_player=?");
			statement.setString(1, id.toString());
			
			
			ResultSet rs = statement.executeQuery();
			if(!rs.next()) return null;
			
			return rs.getByte("color");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean updateBlockColor(UUID playerId, Byte blockColor) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_blockcolor(id_player,color) VALUES(?,?) ON DUPLICATE KEY UPDATE color=VALUES(color)");
			
			statement.setString(1, playerId.toString());
			statement.setInt(2, blockColor);
		
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/*public static boolean loadMotds() {
		try {
			@Cleanup Connection connection = Database.getConnection();
			@Cleanup Statement statement = connection.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT message FROM motd WHERE active=1");
			
			while(rs.next()) {
				MOTDManager.motds.add(ChatColor.translateAlternateColorCodes('&', rs.getString("message")));
			}
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean loadInventory(ItemInventory itemInventory) {
		try {
			@Cleanup Connection connection = Database.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT item,amount FROM player_inventory WHERE id_player=?");
			statement.setString(1, itemInventory.getPlayer().getId().toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			while(rs.next()) {
				String typeName = rs.getString("item");
				ItemType type = ItemType.getItem(typeName);
				if(type == null) {
					System.out.println("[TostiLib] Player " + itemInventory.getPlayer().getName() + " has an item of name " + typeName + " which does not exist.");
					continue;
				}
				
				itemInventory.addItem(type, rs.getInt("amount"));
			}
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean saveInventory(ItemInventory itemInventory) {
		try {			
			Iterator<Entry<ItemType, Integer>> it = itemInventory.getItems().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<ItemType, Integer> pair = (Map.Entry<ItemType, Integer>)it.next();
				
				@Cleanup Connection connection = TostiDB.getConnection();
				@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_inventory(id_player,item,amount) VALUES(?,?,?) ON DUPLICATE KEY UPDATE amount=VALUES(amount)");
				
				statement.setString(1, itemInventory.getPlayer().getId().toString());
				statement.setString(2, pair.getKey().getTypeName());
				statement.setInt(3, pair.getValue());
				
				statement.executeUpdate();
			}
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean removeInventoryItem(LythrionPlayer player, ItemType type) {
		try {
			@Cleanup Connection connection = TostiDB.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("DELETE FROM player_inventory WHERE id_player=? AND item=?");
			statement.setString(1, player.getId().toString());
			statement.setString(2, type.getTypeName());
			statement.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static int getGroupIDPlayer(UUID uuid) {
		try {
			@Cleanup Connection connection = Database.getConnection();
			@Cleanup PreparedStatement statementGroups = connection.prepareStatement("SELECT group_id FROM player_group WHERE player_id=?");
			statementGroups.setString(1, uuid.toString());
			
			@Cleanup ResultSet rsGroupID = statementGroups.executeQuery();

			while(rsGroupID.next()) {
				return rsGroupID.getInt("group_id");
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;	
		
	}*/
	
	public static Timestamp getFirstJoin(UUID id) {
		try {
			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT joindate FROM player WHERE id_player=?");
			statement.setString(1, id.toString());
			
			
			ResultSet rs = statement.executeQuery();
			if(!rs.next()) return null;
			
			return rs.getTimestamp("joindate");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
