package net.planckton.lib;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import io.netty.util.concurrent.DefaultEventExecutorGroup;
import lombok.Getter;
import net.planckton.blood.GameStatus;
import net.planckton.blood.ServerStatus;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketGameStatusUpdate;
import net.planckton.blood.packet.PacketMessage;
import net.planckton.blood.packet.PacketServerUpdate;
import net.planckton.item.CustomItemManager;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.commands.BuildKommand;
import net.planckton.lib.commands.ChatCommand;
import net.planckton.lib.commands.CommandManager;
import net.planckton.lib.commands.LobbyKommand;
import net.planckton.lib.commands.MJoinKommand;
import net.planckton.lib.commands.PositionCommand;
import net.planckton.lib.commands.RankCommand;
import net.planckton.lib.commands.SignEditCommand;
import net.planckton.lib.commands.WorldCommand;
import net.planckton.lib.event.RequestPlayerManagerEvent;
import net.planckton.lib.holo.HoloManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.net.Bootstrapper;
import net.planckton.lib.net.BungeeListener;
import net.planckton.lib.nick.NickNameManager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeAdmin;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.playmode.PlayModeVanilla;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.utils.JoinURLClassLoader;
import net.planckton.lib.window.WindowManager;

public class LythrionLib extends JavaPlugin {
	
	private static LythrionLib instance;
	@Getter private static String serverName;
	@Getter private static String serverType;
	
	@Getter private static boolean isMinigameServer;
	
	@Getter private static JsonObject serverConfig;
	@Getter private static JoinURLClassLoader fullClassLoader;
	@Getter private static Bootstrapper bootstrapper;
	
	@Getter private static JsonObject LythrionConfig;
	
    @Getter private static ServerStatus serverStatus = ServerStatus.RESTARTING;
    @Getter private static GameStatus gameStatus = GameStatus.NO_MINIGAME;
	
	public static LythrionLib getInstance() {
		return instance;
	}
	
	@Getter private static DefaultEventExecutorGroup executorPool;

	public void onEnable() {		
		instance = this;
		
	    try {
         	File config = new File(new File(".").getAbsolutePath() + "/serverConfig.json");
	    	if (!config.exists()) {
	    		System.out.println("[LythrionLib] WARNING ADD A SERVERCONFIG.YML TO YOUR SERVER FOLDER");
	    		Bukkit.getPluginManager().disablePlugin(this);
	    		return;
	    	}
	    
	    	serverConfig = new JsonParser().parse(new JsonReader(new FileReader(config))).getAsJsonObject();
	    	
	    	JsonObject server = serverConfig.getAsJsonObject("server");
	    	serverName = server.get("name").getAsString();
	    	serverType = server.get("type").getAsString();
	    	
	    	JsonObject minigame = serverConfig.getAsJsonObject("minigame");
	    	isMinigameServer = minigame.get("isMinigame").getAsBoolean();
	    } catch (Exception e) {
    		System.out.println("[LythrionLib] SOMETHING WENT WRONG WITH LOADING THE SERVER PROPERTIES");
	    	e.printStackTrace();
    		Bukkit.getPluginManager().disablePlugin(this);
    		return;
	    }
		
		try {
			File configFile = new File("../config.json");
			LythrionConfig = new JsonParser().parse(new JsonReader(new FileReader(configFile))).getAsJsonObject();
		} catch (Exception e) {
			System.out.println("Could not load config.");
			e.printStackTrace();
			//Bukkit.shutdown();
			//return;
		}
		
		executorPool = new DefaultEventExecutorGroup(30);		
		createClassLoader();
		
		DatabaseLib.open();
		
		JsonObject configHeart;
		if(isDevServer()) configHeart = getLythrionConfig().getAsJsonObject("heartdev");
		else configHeart = getLythrionConfig().getAsJsonObject("heart");
		
		bootstrapper = new Bootstrapper(configHeart.get("host").getAsString(), 25333);
		bootstrapper.connect();
		 
		 Manager.registerManager(new ChatManager());
		 Manager.registerManager(new PositionManager());
		 Manager.registerManager(new PlayModeManager());
		 Manager.registerManager(new NickNameManager());
		 Manager.registerManager(new WindowManager());
		 Manager.registerManager(new CommandManager());
		 Manager.registerManager(new CustomItemManager());
		 Manager.registerManager(new HoloManager());
		 
		 PlayModeManager.registerPlayMode(new PlayModeVanilla());
		 PlayModeManager.registerPlayMode(new PlayModeAdmin());
		 
		 CommandManager.registerCommand(this, new RankCommand());
		 CommandManager.registerCommand(this, new BuildKommand());
		 CommandManager.registerCommand(this, new MJoinKommand());
		 CommandManager.registerCommand(this, new LobbyKommand(), "hub");
		 
		 getCommand("signedit").setExecutor(new SignEditCommand());		 
		 getCommand("world").setExecutor(new WorldCommand());
		 getCommand("position").setExecutor(new PositionCommand());
		 getCommand("chat").setExecutor(new ChatCommand());
		 
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
				
				@Override
				public void run() {
					RequestPlayerManagerEvent event = new RequestPlayerManagerEvent();
					Bukkit.getPluginManager().callEvent(event);
					
					PlayerManager<? extends LythrionPlayer> manager = event.getManager();
					if(manager == null) manager = new PlayerManager<LythrionPlayer>(LythrionPlayer.class);
					else System.out.println("[LythrionLib] Found custom PlayManager: " + manager.getName());
					
					Manager.registerManager(manager);
					Manager.findManagers();
					Manager.enableManagers();
				}
			}, 0);
	
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "lib", new BungeeListener());
        
        setStatus(ServerStatus.ONLINE, true);
        
        for (World world : Bukkit.getWorlds()) {
        	world.setGameRuleValue("sendCommandFeedback", "false");
        }
	}
	
	public void onDisable() {
		setStatus(ServerStatus.RESTARTING);
		try {
			Manager.disableManagers();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			DatabaseLib.close();
		}catch (Exception e) {
			e.printStackTrace();			
		}
		
		if(fullClassLoader != null) {
			fullClassLoader.close();
		}
	}
	
	private void createClassLoader() {
		ArrayList<URL> urls = new ArrayList<>();
		try {
			
			File file = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile();
			System.out.println(file.getPath());
			for(File jar : file.listFiles()) {
				if(!jar.isFile()) continue;
				
				System.out.println("[LythrionLib] added jar " + jar.getName());
				urls.add(file.toURI().toURL());
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		}
		
		fullClassLoader = new JoinURLClassLoader(getClassLoader());
		
		try {
			Method getClassLoader = JavaPlugin.class.getDeclaredMethod("getClassLoader");
			getClassLoader.setAccessible(true);
			
			URL[] urlsArray = urls.toArray(new URL[urls.size()]);
			
			for(Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
				ClassLoader loader = (ClassLoader) getClassLoader.invoke(plugin);
				fullClassLoader.add(new URLClassLoader(urlsArray, loader));
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isDevServer() {
		return serverName.startsWith("DEV");
	}
	
	public static boolean isServerType(String type) {
		return serverType.equals(type);
	}
	
	public static void sendHeartMessage(String to, String channel, byte[] data) {
		bootstrapper.getChannel().writeAndFlush(new PacketMessage(getServerName(), to, channel, data));
	}
	
	public static void sendPacket(Packet packet) {
		bootstrapper.getChannel().writeAndFlush(packet);
	}
	
	public static void setStatus(ServerStatus status) {
		setStatus(status, true);
	}
	
	public static void setStatus(ServerStatus status, boolean update) {
		serverStatus = status;
		
		switch (status) {
		case PENDING_RESTART:
		case UNKOWN:
		case MAINTENANCE:
		case OFFLINE:
		case ONLINE:
		case RESTARTING:
			break;
		}
		
		if(update) updateServerStatus();
	}
	
	public static void startMaintenanceMode() {
		serverStatus = ServerStatus.MAINTENANCE;
		updateServerStatus();
	}
	
	public static void updateGameStatus(GameStatus status) {
		sendPacket(new PacketGameStatusUpdate(serverName, status));
	}
	
	public static void updateServerStatus() {
		try {
			bootstrapper.getChannel().writeAndFlush(new PacketServerUpdate(serverName, serverStatus));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
