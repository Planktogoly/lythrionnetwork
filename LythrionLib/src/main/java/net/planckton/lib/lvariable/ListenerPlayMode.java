package net.planckton.lib.lvariable;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.event.PlayerPlayModeSwitchEvent;
import net.planckton.lib.player.LythrionPlayer;

/**
 * Used to listener to changed to this variable.
 * This listener will be unregistered on a playmode switch.
 */
public abstract class ListenerPlayMode<T> extends Listener<T> implements org.bukkit.event.Listener {
	private LythrionPlayer player;
	
	public ListenerPlayMode(LythrionPlayer player) {
		this.player = player;
		
		Bukkit.getPluginManager().registerEvents(this, LythrionLib.getInstance());
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerPlayModeSwitch(PlayerPlayModeSwitchEvent event) {
		if(event.getPlayer() == player) unregister();
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onLythrionPlayerQuit(LythrionPlayerQuitEvent event) {
		if(event.getLythrionPlayer() == player) unregister();
	}
}