package net.planckton.lib.lvariable;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class AsyncSafeVariable<T> {
	protected List<Listener<T>> listeners = new ArrayList<>();
	protected List<Callback<T>> safeGetCallbacks = new ArrayList<>();
	
	@Getter private T value;
	private boolean setToNull;
	
	public AsyncSafeVariable() {
	}
	
	public AsyncSafeVariable(T value) {
		this.value = value;
	}
	
	public void set(T value) {
		setToNull = true;
		
		this.value = value;

		if(safeGetCallbacks.size() > 0) {
			for(Callback<T> callback : new ArrayList<>(safeGetCallbacks)) callback.call(value);
			safeGetCallbacks.clear();
		}
	}
	
	/**
	 * Makes sure the callback is called when this variable is first set.
	 * Can return null if the variable is null.
	 * 
	 * @param callback The callback to be called.
	 */
	public void getSafe(Callback<T> callback) {
		if(value != null || setToNull) callback.call(value);
		
		safeGetCallbacks.add(callback);
	}
	
	public void registerListener(Listener<T> listener) {
		listeners.add(listener);
		listener.setVariable(this);
	}
	
	public boolean isSet() {
		return setToNull || value != null;
	}
}
