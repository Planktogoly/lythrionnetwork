package net.planckton.lib.lvariable;

public class AsyncSafeInteger extends AsyncSafeVariable<Integer> {
	public AsyncSafeInteger() {
	}
	
	public AsyncSafeInteger(int value) {
		super(value);
	}
	
	public void add(int value) {
		set(getValue() + value);
	}
	
	public void decrement(int value) {
		set(getValue() - value);
	}
}
