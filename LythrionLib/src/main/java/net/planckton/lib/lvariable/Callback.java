package net.planckton.lib.lvariable;

public interface Callback<T> {
	public void call(T value);
}
