package net.planckton.lib.lvariable;

import lombok.AccessLevel;
import lombok.Setter;

public abstract class Listener<T> {
	@Setter(value=AccessLevel.PACKAGE) private AsyncSafeVariable<T> variable;
	
	public abstract void onUpdate(T value);
	
	public final void unregister() {
		variable.listeners.remove(this);
	}
}