package net.planckton.lib.net;

import org.bukkit.Bukkit;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;
import net.planckton.blood.packet.PacketDecoder;
import net.planckton.blood.packet.PacketEncoder;
import net.planckton.blood.packet.PacketHandshake;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.BrainConnectEvent;

public class Bootstrapper implements Runnable {
	private String host;
	private int port;
	
	@Getter private Bootstrap bootstrap;

	@Getter private EventLoopGroup workerGroup;
	@Getter private Channel channel;
	
	private HeartHandler serverHandler;

	public Bootstrapper(String host, int port) {
		this.host = host;
		this.port = port;
		
		workerGroup = new NioEventLoopGroup();

		bootstrap = configureBootstrap(new Bootstrap(), workerGroup);
		
		serverHandler = new HeartHandler(this);
	}
	
	public void connect() {
		System.out.println("[Bootstrapper] Attempting first connect.");
		
		ChannelFuture f;
		try {
			f = bootstrap.connect().sync();
		} catch (Exception e) {
			if(e.getMessage().startsWith("Connection refused")) {
				System.out.println("[Bootstrapper] First Connect failed: Connection refused. ");
			}
			else {
				System.out.println("[Bootstrapper] First Connect failed.");
				e.printStackTrace();
			}
			
			Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), this, 20 * 5);
			return;
		}
		
		channel = f.channel();
		
		sendInitPacket();
		
		System.out.println("[Bootstrapper] Connect was succesfull");
	}
	
	private void sendInitPacket() {
		channel.writeAndFlush(
				new PacketHandshake(PacketHandshake.InstanceType.SERVER,
						LythrionLib.getServerName(), LythrionLib.getServerType(), LythrionLib.isMinigameServer(), Bukkit.getPort()));
		
		Bukkit.getPluginManager().callEvent(new BrainConnectEvent());
	}

	@Override
	public void run() {
		connect();
	}

	public Bootstrap configureBootstrap(Bootstrap b, EventLoopGroup g) {
		b.group(g);
		b.channel(NioSocketChannel.class);
		b.option(ChannelOption.SO_KEEPALIVE, true);
		b.remoteAddress(host, port);
		b.handler(new ChannelInitializer<SocketChannel>() {
			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ch.pipeline().addLast(new PacketDecoder(), new PacketEncoder(),serverHandler);
			}
		});

		return b;
	}
	
	public void reconnect() {
		bootstrap = new Bootstrap();
		configureBootstrap(bootstrap, workerGroup);
		
		System.out.println("[Bootstrapper] Re-connecting...");
		bootstrap.connect().addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future)
					throws Exception {
				if (future.cause() != null) {
					if(future.cause().getMessage().startsWith("Connection refused")) System.out.println("[Bootstrapper] Failed to re-connect: Connection Refused");
					else System.out.println("[Bootstrapper] Failed to re-connect: " + future.cause());
					return;
				}
				
				channel = future.channel();
				
				sendInitPacket();
			}
		});
    }

	@SuppressWarnings("deprecation")
	public void stop() {
		try {
			if(channel != null) channel.close().sync();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			if (workerGroup != null) workerGroup.shutdownNow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return channel.isOpen();
	}
}
