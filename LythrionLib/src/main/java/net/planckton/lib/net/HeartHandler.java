 package net.planckton.lib.net;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoop;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketInfoRequest;
import net.planckton.blood.packet.PacketInfoUpdate;
import net.planckton.blood.packet.PacketMessage;
import net.planckton.blood.packet.PacketPlayerCommand;
import net.planckton.blood.packet.PacketPlayerServerJoin;
import net.planckton.blood.packet.PacketServerUpdate;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.event.AsyncBrainMessageEvent;
import net.planckton.lib.event.BrainMessageEvent;
import net.planckton.lib.event.InfoRequestEvent;
import net.planckton.lib.event.InfoUpdateEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

@Sharable
public class HeartHandler extends ChannelInboundHandlerAdapter {
	private Bootstrapper bootstrapper;
	
	public HeartHandler(Bootstrapper bootstrapper) {
		this.bootstrapper = bootstrapper;
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("[TostiServer] Connected succesfully!");
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Packet packet = (Packet) msg;
		
		LythrionPlayer player;
		 
		switch (packet.getPacketType()) {
		case HANDSHAKE:
			System.out.println("[TostiServer] Succesfully identified ourself.");
			
			for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				ctx.writeAndFlush(new PacketPlayerServerJoin(onlinePlayer.getUniqueId()));
			}
			
			LythrionLib.updateServerStatus();
			return;
		case INFO_UPDATE: 
			PacketInfoUpdate infoUpdate = (PacketInfoUpdate) packet;
			
			Bukkit.getPluginManager().callEvent(new InfoUpdateEvent(infoUpdate.getServer(),infoUpdate.getKey(),infoUpdate.getInfo()));
			return;
		case INFO_REQUEST:
			PacketInfoRequest infoRequest = (PacketInfoRequest) packet;
			
			InfoRequestEvent event = new InfoRequestEvent(infoRequest.getServer(),infoRequest.getKey(),null);
			Bukkit.getPluginManager().callEvent(event);
			
			if(event.getValue() == null) return;
			ctx.channel().writeAndFlush(new PacketInfoUpdate(event.getServer(), event.getKey(), event.getValue()));
			return;
		case MESSAGE:
			PacketMessage message = (PacketMessage) packet;
			
			AsyncBrainMessageEvent messageEvent = new AsyncBrainMessageEvent(message.getFrom(), message.getTo(), message.getChannel(), message.getData());
			Bukkit.getPluginManager().callEvent(messageEvent);
			
			//run on main thread
			Bukkit.getScheduler().runTaskLater(LythrionLib.getInstance(), new Runnable() {
				
				@Override
				public void run() {
					BrainMessageEvent messageEvent = new BrainMessageEvent(message.getFrom(), message.getTo(), message.getChannel(), message.getData());
					Bukkit.getPluginManager().callEvent(messageEvent);
				}
			}, 0);
			return;
		case PLAYER_COMMAND:
			PacketPlayerCommand command = (PacketPlayerCommand) packet;
			
			player = PlayerManager.getLythrionPlayer(command.getUuid());
			if(player == null) {
				System.out.println("Heart sent command for: " + command.getUuid() + ", but player was not online. (Command: " + command.getCommand() + ")");
				return;
			}
			
			Bukkit.getScheduler().runTask(LythrionLib.getInstance(), () -> player.chat(command.getCommand()));
			System.out.println("Executed player command for " + command.getUuid() + " (Command: " + command.getCommand() + ")");
			return;
		case SERVER_UPDATE:
			PacketServerUpdate update = (PacketServerUpdate) packet;
			LythrionLib.setStatus(update.getStatus(), false);
			return;
		}
	}
	
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
    
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
    	System.out.println("[TostiServer] Disconnected :(");
    	
    	final EventLoop loop = ctx.channel().eventLoop();
	    	loop.schedule(new Runnable() {
		    	@Override
		    	public void run() {
		    	if(bootstrapper.isConnected()) return;
		    	bootstrapper.reconnect();
	    	}
    	}, 5, TimeUnit.SECONDS);
    }
}
