package net.planckton.lib.window;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;

import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;

public class WindowManager extends Manager implements Listener {
	private static HashMap<Inventory, Window> windows = new HashMap<>();
	
	private HashMap<String, WindowRegister<? extends Window>> registerdWindows;
	
	public static void set(Inventory inventory, Window window) {
		windows.put(inventory, window);
	}
	
	@Override
	public void onEnable() {
		registerdWindows = new HashMap<>();
	}

	@Override
	public void onDisable() {
		registerdWindows = null;
	}
	
	public void registerWindow(String key, WindowRegister<?> register) {
		registerdWindows.put(key, register);
	}
	
	public void openRegisterdWindow(String key, LythrionPlayer player) {
		registerdWindows.get(key).open(player).open(player);
	}
	
	public boolean isWindowRegistered(String key) {
		return registerdWindows.containsKey(key);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryMoveItem(InventoryMoveItemEvent event) {
		Bukkit.broadcastMessage(event.getDestination().getType().name());
		Bukkit.broadcastMessage(event.getSource().getType().name());
		Bukkit.broadcastMessage(event.getInitiator().getType().name());
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryDrag(InventoryDragEvent event) {
		Window window = windows.get(event.getInventory());
		if(window == null) return;
		
		if (window.isNoButtons()) {
			event.setCancelled(false);
			return;
		}
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClick(InventoryClickEvent event) {
		Window window = windows.get(event.getInventory());
		if(window == null) return;
		
		if (window.isNoButtons()) {
			event.setCancelled(false);
			return;
		}
		event.setCancelled(true);
		window.onInventoryClick(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClose(InventoryCloseEvent event) {
		Window window = windows.get(event.getInventory());
		if(window == null) return;
		
		event.getPlayer().setItemOnCursor(null);
	}
}
