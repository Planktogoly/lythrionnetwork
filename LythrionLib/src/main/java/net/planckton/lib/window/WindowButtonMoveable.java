package net.planckton.lib.window;

import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;

public class WindowButtonMoveable extends WindowButton {

	public WindowButtonMoveable(FancyItem item) {
		super(item);
	}

	@Override
	public void onClick(LythrionPlayer player, InventoryClickEvent event) { }
}
