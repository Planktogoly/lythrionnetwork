package net.planckton.lib.window;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import lombok.AccessLevel;
import lombok.Getter;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

public abstract class Window implements Screen {
	@Getter(AccessLevel.PROTECTED) private Inventory inventory;
	@Getter private WindowButton[] buttons;
		
	protected abstract void build();
	
	protected abstract String getName();
	protected abstract int getRows();
	
	protected abstract boolean isNoButtons();
	
	public Inventory createInventory() {
		Inventory inventory = Bukkit.createInventory(null, 9 * getRows(), getName());
		WindowManager.set(inventory, this);
		
		buttons = new WindowButton[inventory.getSize()];
		
		return inventory;
	}
	
	public void open(LythrionPlayer player) {
		if(inventory == null) {
			inventory = createInventory();
			build();
		}
		
		player.openInventory(inventory);
	}
	
	public void rebuild() {
		List<HumanEntity> viewers = inventory.getViewers();
		
		inventory = createInventory();
		build();
		
		for(HumanEntity entity : viewers) entity.openInventory(inventory);
	}
	
	protected void addButton(int slot, WindowButton button) {
		if(slot < 0 || slot >= buttons.length) throw new RuntimeException("slot should range between 0 and inventory length");
	
		buttons[slot] = button;
		inventory.setItem(slot, button.getItem().stack());
	}
	
	protected void setItem(int slot, FancyItem item) {
		setItem(slot, item.stack());
	}
	
	protected void setItem(int slot, ItemStack stack) {
		if(slot < 0 || slot >= buttons.length) throw new RuntimeException("slot should range between 0 and inventory length");
	
		buttons[slot] = null;
		inventory.setItem(slot, stack);
	}
	
	protected void onInventoryClick(InventoryClickEvent event) {
		if(!(event.getWhoClicked() instanceof Player)) return;
		if(event.getRawSlot() < 0 || event.getRawSlot() >= inventory.getSize()) return;
		
		WindowButton button = buttons[event.getSlot()];
		if(button == null) return;
		
		if(button instanceof WindowButtonMoveable && (event.getAction() == InventoryAction.PICKUP_ALL || event.getAction() == InventoryAction.PLACE_ALL || event.getAction() == InventoryAction.SWAP_WITH_CURSOR)) {
			event.setCancelled(false);
		}
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		if(!event.getAction().toString().startsWith("DROP_")) button.onClick(player, event);
		else button.onDrop(player, event);
	}
}
