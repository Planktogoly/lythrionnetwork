package net.planckton.lib.window;

import net.planckton.lib.player.LythrionPlayer;

public interface WindowRegister<T extends Window> {
	public T open(LythrionPlayer player);

}
