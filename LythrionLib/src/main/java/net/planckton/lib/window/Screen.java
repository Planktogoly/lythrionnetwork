package net.planckton.lib.window;

import net.planckton.lib.player.LythrionPlayer;

public interface Screen {
	public abstract void open(LythrionPlayer player);
}
