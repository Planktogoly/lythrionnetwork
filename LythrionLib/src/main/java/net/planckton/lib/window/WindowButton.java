package net.planckton.lib.window;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;

import org.bukkit.event.inventory.InventoryClickEvent;

@AllArgsConstructor
public abstract class WindowButton {
	@Getter @NonNull private FancyItem item;
	
	public abstract void onClick(LythrionPlayer player, InventoryClickEvent event);
	
	public void onDrop(LythrionPlayer player, InventoryClickEvent event) { }
}
