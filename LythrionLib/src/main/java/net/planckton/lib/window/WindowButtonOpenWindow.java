package net.planckton.lib.window;

import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;

public class WindowButtonOpenWindow extends WindowButton {
	private Window window;
	
	public WindowButtonOpenWindow(FancyItem item, Window window) {
		super(item);
		
		this.window = window;
	}

	@Override
	public void onClick(LythrionPlayer player, InventoryClickEvent event) {
		window.open(player); 
	}
}
