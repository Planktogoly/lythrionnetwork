package net.planckton.lib.commands;

import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.player.LythrionPlayer;

public class MJoinKommand extends Kommand implements OnKommand {

	public MJoinKommand() {
		super("mjoin");
	}
	
	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		if (arg.length == 0) {
			return;
		}		
		
		player.connectToMinigame(arg[0]);
	}

}
