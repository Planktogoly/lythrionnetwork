package net.planckton.lib.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import net.planckton.lib.chat.ChatChannel;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;

public class ChatCommand implements TabExecutor {
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] arg) {
		if(!(cs instanceof Player)) return true;
		LythrionPlayer player = PlayerManager.getLythrionPlayer(Bukkit.getPlayer(cs.getName()));
		
		if(arg.length == 0) return onCommand(player, arg);
		else if(arg[0].equals("mute")) return onCommandMute(player, arg);
		else if(arg[0].equals("unmute")) return onCommandLuister(player, arg);
		else return onCommandChat(player, arg);
	}
	
	private boolean onCommand(LythrionPlayer player, String[] arg) {
		String chatsString = "";
		
		Set<ChatChannel> chats = player.chat.getAvailableChats();
		Iterator<ChatChannel> it = chats.iterator();
		while(it.hasNext()) {
			ChatChannel chat = it.next();
			chatsString += (chats.contains(chat) ? ChatColor.WHITE : ChatColor.RED) + chat.displayName;
			
			if(it.hasNext()) chatsString += ChatColor.GRAY + ", ";
		}
		
		player.sendMessage(ChatColor.RED + "Chats: " + chatsString);
		
		if(player.chat.getChat() != null) player.sendMessage(ChatColor.RED + "Talking in " + player.chat.getChat().displayName + ".");
		else player.sendMessage(ChatColor.RED + "You are not talking in a chat");
		
		return true;
	}
	
	private boolean onCommandMute(LythrionPlayer player, String[] arg) {
		if(arg.length <= 1) {
			player.sendMessage(ChatColor.RED + "Usage: /chat mute <chat>");
			return true;
		}
		
		ChatChannel chat = player.chat.hasAvailableChat(arg[1]);
		
		if(chat == null) {
			player.sendMessage(ChatColor.RED + "You have no available chat with this name.");
			return true;
		}
		
		if(player.chat.getChat() == chat) {
			player.sendMessage(ChatColor.RED + "You can't mute the chat you are talking in.");
			return true;
		}
		
		player.chat.muteChat(chat);
		player.sendMessage(ChatColor.RED + "\"" + chat.chatName + "\" muted.");
		
		return true;
	}
	
	private boolean onCommandLuister(LythrionPlayer player, String[] arg) {
		if(arg.length <= 1) {
			player.sendMessage(ChatColor.RED + "Usage: /chat unmute <chat>");
			return true;
		}
		
		ChatChannel chat = player.chat.hasAvailableChat(arg[1]);
		
		if(chat == null) {
			player.sendMessage(ChatColor.RED + "No chat available with this name.");
			return true;
		}
		
		player.chat.listenToChat(chat);
		player.sendMessage(ChatColor.RED + "You have unmuted \"" + chat.displayName + "\".");
		
		return true;
	}
	
	private boolean onCommandChat(LythrionPlayer player, String[] arg) {		
		ChatChannel chat = player.chat.hasAvailableChat(arg[0]);
		
		if(chat == null) {
			player.sendMessage(ChatColor.RED + "You have no chat with this name.");
			return true;
		}
		
		if(ChatManager.isGlobalMuteEnabled() && !chat.ignoresGlobalmute && !player.hasRankPower(Rank.MOD) && !player.hasPermission("globalmute.ignore")) {
			player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.RED + "GlobalMute is currently enabled.");
			player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.RED + "You can still \'/msg\' players.");
			return true;
		}
		
		if(arg.length > 1) {
			String message = "";
			
			for(int i = 1; i < arg.length; i++) {
				message += arg[i];
				if(i < arg.length - 1) message += " ";
			}
			
			chat.broadcast(player, message);
			return true;
		}
		
		if(chat == player.chat.getChat() && chat != ChatManager.CHANNEL_GLOBAL) {
			chat = ChatManager.CHANNEL_GLOBAL;
		}
		
		player.chat.setChatChannel(chat);
		player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.WHITE + "You are now talking in \"" + chat.displayName + "\".");
		
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] arg) {
		return new ArrayList<>();
	}
}
