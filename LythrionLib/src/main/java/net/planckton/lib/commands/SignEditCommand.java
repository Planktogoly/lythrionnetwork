package net.planckton.lib.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class SignEditCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] arg) {
		if(!(cs instanceof Player)) return true;
		LythrionPlayer player = PlayerManager.getLythrionPlayer(cs.getName());
		
		if(!player.hasRankPower(Rank.OWNER)) {
			System.out.println("Player " + player.getName() + " has no permission for command " + cmd);
			return true;
		}
		
		if(arg.length < 2) {
			player.sendMessage(ChatColor.RED + "Usage: /signedit <line> <text>");
			return true;
		}
		
		int line;
		try {
			line = Integer.parseInt(arg[0]);
		} catch(Exception e) {
			player.sendMessage(ChatColor.RED + arg[0] + " is not a line number");
			return true;
		}
		
		if(line < 1 || line > 4) {
			player.sendMessage(ChatColor.RED + "Please supply a line number between 1-4");
			return true;
		}
		
		Block block = player.getBukkitPlayer().getTargetBlock((Set<Material>)null, 5);
		if(block == null) {
			player.sendMessage(ChatColor.RED + "No sign in sight");
			return true;
		}
		
		Material blockType = block.getType();
		if(blockType != Material.SIGN_POST && blockType != Material.WALL_SIGN) {
			player.sendMessage(ChatColor.RED + "No sign in sight");
			return true;
		}
		
		StringBuilder message = new StringBuilder();
		for(int i = 1; i < arg.length; i++) {
			message.append(arg[i]);
			
			if(i < arg.length - 1) message.append(" ");
		}
		
		Sign sign = (Sign) block.getState();
		sign.setLine(line - 1, ChatColor.translateAlternateColorCodes('&', message.toString()));
		sign.update();
		
		player.sendMessage(ChatColor.GREEN + "Done.");	
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] arg) {
		if(arg.length == 1) return null;
		else if(arg.length == 2) {
			LinkedList<String> ranks = new LinkedList<String>();
			
			for(Rank rank : Rank.values()) {
				if(!rank.toString().equalsIgnoreCase(arg[1])) continue;
				ranks.add(rank.toString().toLowerCase());
			}
			
			return ranks;
		}
		else return new LinkedList<String>();
	}
}
