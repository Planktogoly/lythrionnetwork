package net.planckton.lib.commands;

import org.bukkit.Bukkit;

import net.md_5.bungee.api.ChatColor;
import net.planckton.lib.event.DefaultPlayModeRequestEvent;
import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeAdmin;
import net.planckton.lib.playmode.PlayModeVanilla;

public class BuildKommand extends Kommand implements OnKommand {
	
	public BuildKommand() {
		super("build");
	}

	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission("helper.buildmode")) return;
		
		if (player.isInPlayMode(PlayModeAdmin.class)) {
			DefaultPlayModeRequestEvent event = new DefaultPlayModeRequestEvent(PlayModeVanilla.class, player.getBukkitPlayer(), player.getRank());
			Bukkit.getPluginManager().callEvent(event);
			
			player.setPlayMode(event.getPlayMode());
			
			player.sendMessage(ChatColor.RED + "Stopped build mode!");
		} else {
			player.setPlayMode(PlayModeAdmin.class);
			player.sendMessage(ChatColor.RED + "Enabled build mode!");
		}		
	}

}
