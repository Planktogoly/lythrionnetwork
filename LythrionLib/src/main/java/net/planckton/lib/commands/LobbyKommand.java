package net.planckton.lib.commands;

import net.planckton.lib.LythrionLib;
import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.position.PositionManager;

public class LobbyKommand extends Kommand implements OnKommand {
	
	public LobbyKommand() {
		super("hub");
	}
	
	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		if (LythrionLib.getServerType().equalsIgnoreCase("lobby")) {
			player.teleport(PositionManager.getPosition("hub.spawn"));
			return;
		}
		
		player.connectToHub();
		return;
	}

}
