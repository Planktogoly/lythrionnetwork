package net.planckton.lib.commands;

import java.util.Collection;
import java.util.UUID;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;

import net.planckton.lib.DatabaseLib;
import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.kommand.PossibleOnlinePlayerSubKommand;
import net.planckton.lib.kommand.SetSubKommand;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;

public class RankCommand extends Kommand {

	public RankCommand() {
		super("setrank");
		setRequiredRank(Rank.OWNER);
		
		addSubCommand(new PossibleOnlinePlayerSubKommand(new RankSubKommand()));
	}
	
	private static class RankSubKommand extends SetSubKommand<Rank> implements OnKommand {
		public RankSubKommand() {
			super("rank");
		}

		@Override
		protected Collection<Rank> getValues(LythrionPlayer player) {
			return Rank.valuesSet();
		}

		@Override
		protected boolean matches(LythrionPlayer player, Rank value, String command) {
			return value.toString().toLowerCase().startsWith(command.toLowerCase());
		}

		@Override
		protected boolean startsWith(LythrionPlayer player, Rank value, String command) {
			return value.toString().toLowerCase().startsWith(command.toLowerCase());
		}

		@Override
		public void onCommand(LythrionPlayer player, String[] arg) {
			UUID editUUID;
			
			LythrionPlayer editPlayer = PlayerManager.getLythrionPlayer(arg[0]);
			if(editPlayer != null) editUUID = editPlayer.getId();
			else editUUID = DatabaseLib.getUUIDByName(arg[0]);
			
			if(editUUID == null) {
				player.sendMessage(ChatColor.RED + "Player \"" + arg[0] + "\" not found.");
				return;
			}
			
			Rank rank = Rank.fromString(arg[1]);
			
			if(rank == null) {
				player.sendMessage(ChatColor.RED + "No rank \"" + arg[1] + "\".");
				return;
			}
			
			if(editPlayer != null) {
				editPlayer.rank.updateRank(rank);
				player.sendMessage(ChatColor.GREEN + "Changed \"" + editPlayer.getDisplayNameWithRank() + ChatColor.GREEN + "\" to rank " + WordUtils.capitalizeFully(rank.toString()) + ".");
			}
			else {
				DatabaseLib.setRank(editUUID, rank);
				player.sendMessage(ChatColor.GREEN + "Changed \"" + arg[0] + ChatColor.GREEN + "\" to rank " + WordUtils.capitalizeFully(rank.toString()) + ".");
			}
			
		}
	}
}
