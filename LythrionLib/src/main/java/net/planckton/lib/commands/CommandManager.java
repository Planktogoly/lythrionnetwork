package net.planckton.lib.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.command.TabExecutor;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;

public class CommandManager extends Manager implements Listener {
	private static final List<String> EMPTY_LIST = new ArrayList<>();
	private static ArrayList<String> stopCommands;
	
	@SuppressWarnings("serial")
	@Override
	public void onEnable() {
		stopCommands = new ArrayList<String>(){{
			add("whitelist");
			add("gamemode");
			add("op");
			add("deop");
			add("pl");
			add("plugins");
			add("kill");
			add("me");
			add("restart");
			add("stop");
			}};
	}

	@Override
	public void onDisable() {
		stopCommands = null;
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		
		String message = event.getMessage();
		String splitCommand = message.split(" ")[0].replaceFirst("/([A-Za-z0-9]{1,}:)?", "").toLowerCase();
		
		if(stopCommands.contains(splitCommand.toLowerCase())) {
			if(player.hasPermission("command." + splitCommand)) return;
			
			event.setCancelled(true);
			return;
		}
			
		if(event.getMessage().startsWith("/reload") && event.getPlayer().isOp()) {
			Bukkit.broadcastMessage(ChatColor.RED + "");
			Bukkit.broadcastMessage(ChatColor.RED + "[!!!] Watch out! Server reload! There will be lagg! [!!!]");
			Bukkit.broadcastMessage(ChatColor.RED + "");
		}
	}
	
	public static void registerCommand(JavaPlugin pl, Kommand kommand) {
		if(kommand.getAliases() != null) registerCommand(pl, kommand, kommand.getAliases());
		else registerCommand(pl, kommand.getCommand(), kommand);
	}
	
	public static void registerCommand(JavaPlugin pl, String command, CommandExecutor commandExecutor) {
		PluginCommand pluginCommand =  pl.getCommand(command);
		if(pluginCommand != null) {
			pl.getCommand(command).setExecutor(commandExecutor);
			return;
		}
		
		((CraftServer) Bukkit.getServer()).getCommandMap().register(pl.getName(), new LythrionCommand(command, commandExecutor));
	}
	
	public static void registerCommand(JavaPlugin pl, CommandExecutor commandExecutor, String... commands) {
		PluginCommand pluginCommand =  pl.getCommand(commands[0]);
		if(pluginCommand != null) {
			pl.getCommand(commands[0]).setExecutor(commandExecutor);
			return;
		}
		
		((CraftServer) Bukkit.getServer()).getCommandMap().register(pl.getName(), new LythrionCommand(commands[0], Arrays.asList(commands), commandExecutor));
	}
	
	private static final class LythrionCommand extends BukkitCommand {
		private CommandExecutor executor;
		
		protected LythrionCommand(String name, CommandExecutor executor) {
			super(name);
			
			this.executor = executor;
		}
		
		protected LythrionCommand(String name, List<String> aliases, CommandExecutor executor) {
			super(name, "", "", aliases);
			
			this.executor = executor;
		}

		@Override
		public boolean execute(CommandSender cs, String alias, String[] args) {
			return executor.onCommand(cs, this, alias, args);
		}

		@Override
		public List<String> tabComplete(CommandSender cs, String alias, String[] args) throws IllegalArgumentException {
			if(executor instanceof TabExecutor) return ((TabCompleter) executor).onTabComplete(cs, this, alias, args);
			else return EMPTY_LIST;
		}
	}

	
	public static interface Function {
		public void call();
	}
	
	public static class FunctionKommand extends Kommand implements OnKommand {
		private Function function;
		
		public FunctionKommand(String command, Function function) {
			super(command);
			
			this.function = function;
		}
		
		@Override
		public void onCommand(LythrionPlayer player, String[] arg) {
			function.call();
		}	
	}
	
	public static void registerFunctionCommand(JavaPlugin pl, String command, Rank rank, Function function) {
		registerCommand(pl, new FunctionKommand(command, function).addPreCondition(player -> player.hasRankPower(rank)));
	}
}
