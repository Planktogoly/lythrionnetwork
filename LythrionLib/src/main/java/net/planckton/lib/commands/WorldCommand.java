package net.planckton.lib.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import mkremins.fanciful.FancyMessage;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.ChatPrefix;

public class WorldCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] arg) {
		if(!(cs instanceof Player)) return true;
		LythrionPlayer player = PlayerManager.getLythrionPlayer(cs.getName());
		
		if(!player.hasRankPower(Rank.OWNER)) return true;
		
		if(arg.length == 0) {
			player.sendMessage(ChatColor.RED + "Usage: /world list");
			player.sendMessage(ChatColor.RED + "Usage: /world load <name> [tp]");
			player.sendMessage(ChatColor.RED + "Usage: /world unload <name> [save]");
			player.sendMessage(ChatColor.RED + "Usage: /world tp <name>");
		}
		else if(arg[0].equalsIgnoreCase("list")) {
			//player.sendMessage(ChatPrefix.getHeader("Worlds"));
			
			for(World world : Bukkit.getWorlds()) {
				player.sendMessage(ChatColor.GRAY + "World: " + ChatColor.GOLD + world.getName() + ChatColor.GRAY + ". Players: " + ChatColor.GOLD + world.getPlayers().size() + ChatColor.GRAY + ".");
			}
		}
		else if(arg[0].equalsIgnoreCase("load")) {
			if(arg.length < 2) {
				player.sendMessage(ChatColor.RED + "Usage: /world load <name> [tp]");
				return true;
			}
			
			boolean tp = false;
			if(arg.length >= 3) {
				if(arg[2].equalsIgnoreCase("tp")) tp = true;
				else {
					try {
						tp = Boolean.parseBoolean(arg[2]);
					}
					catch(Exception e) {
						player.sendMessage(ChatColor.RED + "Use true of false instead of \"" + arg[2] + "\"");
						return true;
					}
				}
			}
			
			WorldCreator worldCreator = new WorldCreator(arg[1]).type(WorldType.FLAT);
			worldCreator = worldCreator.generateStructures(false);
			
			World world = Bukkit.getServer().createWorld(worldCreator);
			if(world == null) {
				player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW + ChatColor.RED + "Error while loaded world");
				return true;
			}
			
			player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW  + ChatColor.GREEN + "Succesfully loaded world");
			
			if(tp) player.teleport(world.getSpawnLocation());
			else {
				
				FancyMessage message = ChatPrefix.getLythrionGoldArrows()
									.then("Click ")
										.color(ChatColor.GRAY)
									.then("here")
										.color(ChatColor.GOLD)
										.style(ChatColor.UNDERLINE)
										.command("/world tp " + arg[1])
										.tooltip(ChatColor.GRAY + "Click to teleport")
									.then(" to TP to world.")
										.color(ChatColor.GRAY);
				
				player.sendMessage(message);
			}
		}
		else if(arg[0].equalsIgnoreCase("unload")) {
			if(arg.length < 2) {
				player.sendMessage(ChatColor.RED + "Usage: /world unload <name> [save]");
				return true;
			}
			
			World world = Bukkit.getServer().getWorld(arg[1]);
			if(world == null) {
				player.sendMessage(ChatColor.RED + "World was not found");
			}
			
			boolean save = true;
			if(arg.length > 2) {
				if(arg[2].equalsIgnoreCase("false")) save = false;
				else if(arg[2].equalsIgnoreCase("true")) save = true;
				else {
					player.sendMessage(ChatColor.RED + "save can only be true or false.");
					return true;
				}
			}
			
			Bukkit.unloadWorld(world, save);
			player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW  + ChatColor.GREEN + "Succesfully unloaded world");
		}
		else if(arg[0].equalsIgnoreCase("tp")) {
			if(arg.length < 2) {
				player.sendMessage(ChatColor.RED + "Usage: /world tp <name>");
				return true;
			}
			
			World world = Bukkit.getServer().getWorld(arg[1]);
			if(world == null) {
				player.sendMessage(ChatColor.RED + "World was not found");
				return true;
			}
			
			player.teleport(world.getSpawnLocation());
			player.sendMessage(ChatPrefix.LYTHRION_RED_ARROW  + "Teleported to: " + ChatColor.GOLD + arg[1]);
		}
		else player.sendMessage(ChatColor.RED + "Unknown sub command");
		
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] arg) {
		return new ArrayList<>();
	}

}
