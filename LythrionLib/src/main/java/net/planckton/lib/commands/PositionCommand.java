package net.planckton.lib.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import mkremins.fanciful.FancyMessage;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.position.Position;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.rank.Rank;

public class PositionCommand implements TabExecutor {
	//static stuffs
	private static final String editPermission = "helper.adminmode.warp";
	private static final String[] FORBIDDEN_WARP_NAMES = {"add", "list", "del", "delete", "addspawn", "listspawn", "delspawn", "rank", "setrank"};
	
	private static boolean isForbiddenName(String name) {
		for(String forbiddenName : FORBIDDEN_WARP_NAMES) {
			if(forbiddenName.equalsIgnoreCase(name)) return true;
		}
		
		return false;
	}
	
	@Override
	public boolean onCommand(CommandSender cm, Command cmd, String label, String[] arg) {
		if(!(cm instanceof Player)) return true;
		Player player = Bukkit.getPlayer(cm.getName());
		LythrionPlayer tostiPlayer = PlayerManager.getLythrionPlayer(player);
		
		if(arg.length == 0) {
			player.sendMessage(ChatColor.RED + "Gebruik: /warp <naam>");
			return true;
		}
		
		if(arg[0].equalsIgnoreCase("add")) return onCommandAdd(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("list")) return onCommandList(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("del") || arg[0].equalsIgnoreCase("delete")) return onCommandDelete(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("addspawn")) return onCommandAddSpawnPoint(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("listspawn")) return onCommandListSpawnPoints(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("delspawn")) return onCommandListDeleteSpawnPoints(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("rank")) return onCommandRank(tostiPlayer, arg);
		else if(arg[0].equalsIgnoreCase("setrank")) return onCommandSetRank(tostiPlayer, arg);
		else return onCommandWarp(tostiPlayer, arg);
	}
	
	private boolean onCommandAdd(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length == 1) {
			player.sendMessage(ChatColor.RED + "Usage: /warp add <name> [rank]");
			return true;
		}
		
		String warpName = arg[1];
		
		if(isForbiddenName(warpName)) {
			player.sendMessage(ChatColor.RED + "\"" + warpName + "\" is a forbidden name.");
			return true;
		}
		
		if(PositionManager.hasPosition(warpName)) {
			player.sendMessage(ChatColor.RED + "Warp already exists.");
			return true;
		}
		
		Rank rank = Rank.MEMBER;
		
		if(arg.length == 3) {
			rank = Rank.byName(arg[2]);
			
			if(rank == null) {
				player.sendMessage(ChatColor.RED + "No rannk \"" + arg[2] + "\" found.");
				return true;
			}
		}
		
		PositionManager.create(warpName, rank, player.getLocation());
		player.sendMessage(ChatColor.GREEN + "Succesfully created warp " + arg[1] + ".");
		
		return true;
	}
	
	public static final int PAGE_ITEMS = 5;
	
	private boolean onCommandList(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;

		int page = 0;
		
		if(arg.length >= 2) {
			try {
				page = Integer.parseInt(arg[1]) - 1;
			}
			catch (Exception exception) {
				player.sendMessage(ChatColor.RED + "\"" + arg[1] + "\" is geen getal.");
				return true;
			}
		}
		
		int pages = (int)((float)PositionManager.length() / (float)PAGE_ITEMS);
		
		if(page > pages) {
			player.sendMessage(ChatColor.RED + "There " + (pages > 0 ? "are" : "is") + " only " + (pages + 1) + " pages.");
			return true;
		}
		else if(page < 0) {
			player.sendMessage(ChatColor.RED + "Really....");
			return true;
		}
		
		if(PositionManager.getPositionsByName().size() <= 0) {
			player.sendMessage(ChatColor.RED + "No warps yet D:");
			return true;
		}
		
		Position[] warps = PositionManager.getPositions(page * PAGE_ITEMS, PAGE_ITEMS);
		
		player.sendMessage(ChatColor.GRAY + "=============== " + ChatColor.GOLD + "Warps" + ChatColor.GRAY + " ===============");
		
		for(int i = 0; i < warps.length; i++) {
			FancyMessage message = new 
					FancyMessage("Name: ").color(ChatColor.GRAY)
						.then(warps[i].name).color(ChatColor.GOLD).suggest(warps[i].name).tooltip(ChatColor.GRAY + "Click to copy")
					.then(". Rank: ").color(ChatColor.GRAY)
						.then(WordUtils.capitalizeFully(warps[i].getRank().toString())).color(ChatColor.GOLD)
					.then(". Spawns: ").color(ChatColor.GRAY)
						.then("" + warps[i].locations.length).color(ChatColor.GOLD)
					.then(".").color(ChatColor.GRAY);
			
			player.sendMessage(message);
		}
		player.sendMessage(ChatColor.YELLOW + "=============== " + ChatColor.GRAY + "(" + ChatColor.GREEN +  (page + 1) + ChatColor.GRAY + "/" + ChatColor.GREEN + (pages + 1) + ChatColor.GRAY + ")" + ChatColor.YELLOW +  " ===============");
		
		return true;
	}
	
	private boolean onCommandDelete(final LythrionPlayer player, final String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length == 1) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp del <name>");
			return true;
		}
		
		if(!PositionManager.hasPosition(arg[1])) {
			player.sendMessage(ChatColor.RED + "No warp \"" + arg[1] + "\" found.");
			return true;
		}
		
		if(PositionManager.delete(arg[1])) {
			player.sendMessage(ChatColor.GREEN + "Succesfully removed \"" + arg[1] + "\".");
		}
		else {
			player.sendMessage(ChatColor.GREEN + "Something went wrong while removing \"" + arg[1] + "\".");
		}
		
		return true;
	}
	
	private boolean onCommandWarp(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission("helper.position")) return true;
		
		Position warp = PositionManager.getPosition(arg[0]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "Warp \"" + arg[0] + "\" niet gevonden.");
			return true;
		}
		
		if(!player.hasRankPower(warp.getRank())) {
			player.sendMessage(ChatColor.RED + "Je hebt geen toestemming tot deze warp");
			return true;
		}
		
		player.sendMessage(ChatColor.GREEN + "Teleporteren...");
		warp.teleport(player);
		
		return true;
	}
	
	private boolean onCommandAddSpawnPoint(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length == 1) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp addspawn <name>");
			return true;
		}
		
		Position warp = PositionManager.getPosition(arg[1]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "No warp named \"" + arg[1] + "\".");
			return true;
		}
		
		warp.createSpawn(player.getLocation());
		player.sendMessage(ChatColor.GREEN + "Succesfully added spawnpoint to \"" + WordUtils.capitalizeFully(warp.name) + "\".");
			
		return true;
	}
	
	private boolean onCommandListSpawnPoints(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length == 1) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp listspawn <name>");
			return true;
		}
		
		Position warp = PositionManager.getPosition(arg[1]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "No warp names \"" + arg[1] + "\".");
			return true;
		}
		
		if(warp.locations.length <= 0) {
			player.sendMessage(ChatColor.RED + "This warp has no spawns D:");
			return true;
		}
		
		int page = 0;
		
		if(arg.length > 2) {
			try {
				page = Integer.parseInt(arg[2]) - 1;
			}
			catch (Exception exception) {
				player.sendMessage(ChatColor.RED + "\"" + arg[2] + "\" is no number.");
				return true;
			}
		}
		
		int pages = warp.locations.length / PAGE_ITEMS;
		
		if(page > pages) {
			player.sendMessage(ChatColor.RED + "There " + (pages > 0 ? "are" : "is") + " only " + (pages + 1) + " pages.");
			return true;
		}
		else if(page < 0) {
			player.sendMessage(ChatColor.RED + "Really...");
			return true;
		}
		
		Location[] locations = warp.getSpawns(page, PAGE_ITEMS);
		
		player.sendMessage(ChatColor.YELLOW + "========== " + ChatColor.GREEN + "Spawns - " + WordUtils.capitalizeFully(warp.name) + ChatColor.YELLOW + " ==========");
		
		for(int i = 0; i < locations.length; i++) {
			Location location = warp.locations[page * PAGE_ITEMS + i];
			player.sendMessage(ChatColor.GRAY + " #" + (pages * PAGE_ITEMS + i + 1) + " X: " + (int)location.getX() + " Y: " + (int)location.getY() + " Z: " + (int)location.getZ() + ". ");
		}
		
		player.sendMessage(ChatColor.YELLOW + "=============== " + ChatColor.GRAY + "(" + ChatColor.GREEN +  (page + 1) + ChatColor.GRAY + "/" + ChatColor.GREEN + (pages + 1) + ChatColor.GRAY + ")" + ChatColor.YELLOW +  " ===============");
		
		return true;
	}
	
	private boolean onCommandListDeleteSpawnPoints(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length < 3) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp delspawn <name> <index>");
			return true;
		}
		
		Position warp = PositionManager.getPosition(arg[1]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "No warp named \"" + arg[1] + "\".");
			return true;
		}
		
		if(warp.locations.length <= 0) {
			player.sendMessage(ChatColor.RED + "This warp has no spawns D:");
			return true;
		}
		
		int index = 0;
		try {
			index = Integer.parseInt(arg[2]);
		}
		catch (Exception exception) {
			player.sendMessage(ChatColor.RED + "\"" +arg[2] + "\" is no number.");
			return true;
		}
		
		if(index >= warp.locations.length) {
			player.sendMessage(ChatColor.RED + "There " + (warp.locations.length > 1 ? "are" : "is") + " only " + warp.locations.length + " spawn" + (warp.locations.length > 1 ? "s." : "."));
			return true;
		}
		
		warp.delSpawn(index);
		player.sendMessage(ChatColor.GREEN + "Succesfully deleted spawn from " + WordUtils.capitalizeFully(warp.name) + " at index " + (index + 1));
		
		return true;
	}
	
	private boolean onCommandRank(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length < 2) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp setrank <name>");
			return true;
		}
		
		Position warp = PositionManager.getPosition(arg[1]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "No warp named \"" + arg[1] + "\".");
			return true;
		}
		
		player.sendMessage(ChatColor.GREEN + "Rank for \"" + WordUtils.capitalizeFully(warp.name) + "\" is " + WordUtils.capitalizeFully(warp.getRank().toString()) + ".");
			
		return true;
	}
	
	private boolean onCommandSetRank(LythrionPlayer player, String[] arg) {
		if(!player.hasPermission(editPermission)) return true;
		
		if(arg.length < 3) {
			player.sendMessage(ChatColor.RED + "Usage: /Warp setrank <name> <rank>");
			return true;
		}
		
		Position warp = PositionManager.getPosition(arg[1]);
		
		if(warp == null) {
			player.sendMessage(ChatColor.RED + "No warp named \"" + arg[1] + "\".");
			return true;
		}
		
		Rank rank = Rank.byName(arg[2]);
		if(rank == null) {
			player.sendMessage(ChatColor.RED + "No rank named \"" + arg[2] + "\"");
			return true;
		}
		
		warp.setRank(rank);
		player.sendMessage(ChatColor.GREEN + "Succesfully set rank for \"" + WordUtils.capitalizeFully(warp.name) + "\" to " + WordUtils.capitalizeFully(rank.toString()) + ".");
			
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] arg) {
		return new ArrayList<>();
	}
}
