package net.planckton.lib.position;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.event.PositionEvent;
import net.planckton.lib.event.PositionEvent.Action;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.rank.Rank;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

import com.google.common.collect.Iterables;

public class PositionManager extends Manager implements Listener {
	@Getter public static HashMap<String, Position> positionsByName;
	
	public static Collection<Position> getPositions() {
		return Collections.unmodifiableCollection(positionsByName.values());
	}
	
	@Override
	public void onEnable() {
		positionsByName = new HashMap<>();
		
		DatabaseLib.loadPositions();
		
		System.out.println("[PositionManager] Loaded " + positionsByName.size() + " positions.");
	}

	@Override
	public void onDisable() {
		positionsByName = null;
	}
	
	public static Position create(String name, Location ... spawnPoints) {
		return create(name, Rank.MEMBER, spawnPoints);
	}
	
	public static Position create(String name, Rank rank, Location ... spawnPoints) {
		if(hasPosition(name)) return null;
		
		Position pos = new Position(name, rank, spawnPoints);
		
		DatabaseLib.addPosition(pos);
		
		add(pos);
		
		Bukkit.getPluginManager().callEvent(new PositionEvent(pos, Action.ADD));
		
		return pos;
	}
	
	public static Position load(String locationName, Location location, Rank rank) {
		Position position = null;
		
		String positionName = locationName.replaceAll(".[0-9]{1,}$", "");
		
		if(hasPosition(positionName)) {
			position = getPosition(positionName);
			position.addSpawn(location);
		}
		else {
			position = new Position(positionName, rank, location);
			add(position);
		}
		
		return position;
	}
	
	public static boolean delete(@NonNull Position position) {
		Bukkit.getPluginManager().callEvent(new PositionEvent(position, Action.REMOVE));
		
		return DatabaseLib.delPosition(position);
	}
	
	public static boolean delete(String name) {
		Position position = positionsByName.remove(name);
		
		if(position == null) return false; 
		
		return delete(position);
	}
	
	public static int length() {
		return positionsByName.size();
	}
	
	//use this only if you just need to check if a 
	//warp exists and you do not need the warp
	public static boolean hasPosition(String name) {
		return positionsByName.containsKey(name);
	}
	
	//use this when you need to check if a warp exist,
	//and if so you want to use the warp. Just check
	//for null with the returned warp for existence
	public static Position getPosition(String name) {
		return positionsByName.get(name);
	}
	
	public static Position[] getPositions(int index, int amount) {
		//if out of bounds, throw an error
		if(index >= positionsByName.size()) {
			throw new IndexOutOfBoundsException(index + " is out of bound. size is " + positionsByName.size());
		}
		
		int indexedWarpsSize = amount;
		//calculate if the given amount and index fall within bounds
		//if not, limit the amount to the biggest amount possible
		if(index + amount > positionsByName.size()) {
			indexedWarpsSize = positionsByName.size() - index;
		}
		
		Position[] indexedWarps = new Position[indexedWarpsSize];
		
		for(int i = 0; i < indexedWarpsSize; i++) {
			indexedWarps[i] = Iterables.get(positionsByName.values(), index + i);
		}
		
		return indexedWarps;
	}
	
	//private methods
	//used internally to add warps to the hashmap
	private static void add(Position warp) {
		positionsByName.put(warp.name, warp);
	}

	public static void unloadPositionsFor(World world) {
		Iterator<Entry<String, Position>> it = positionsByName.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, Position> pair = it.next();
			
			if(pair.getValue().getPosition().getWorld().equals(world)) it.remove();
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onWorldLoad(WorldLoadEvent event) {
		System.out.println("[LythrionLib] New world loaded. Loading positons.");
		
		PositionManager.loadPositionsFor(event.getWorld());
	}
	
	public static void loadPositionsFor(World world) {
		DatabaseLib.loadPositions(world);
	}
}
