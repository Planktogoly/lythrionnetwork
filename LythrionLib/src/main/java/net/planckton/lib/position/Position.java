package net.planckton.lib.position;

import java.util.UUID;

import lombok.Getter;
import net.planckton.lib.DatabaseLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.rank.Rank;
import net.planckton.lib.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class Position {
	//DB UUID
	protected UUID id;
	//Index for warp manager
	protected int index;
	
	public String name;
	@Getter public Location[] locations;
	private Rank requiredRank;
	
	//protected so only the WarpManager can create a new instance
	protected Position(UUID id, String name, Location ... spawnPoints) {
		this(name, Rank.MEMBER, spawnPoints);
	}
	
	protected Position(String name, Rank requiredRank, Location ... spawnPoints) {
		this.name = name;
		this.requiredRank = requiredRank;
		this.locations = spawnPoints;
	}
	
	public void teleport(LythrionPlayer player) {
		teleport(player.getBukkitPlayer());
	}
	
	public void teleport(Player player) {
		if(locations.length == 0) {
			player.sendMessage(ChatColor.RED + "This location doesn't have a location D:, please warn a helper!");
			return;
		}
		else if(locations.length == 1) {
			player.teleport(locations[0]);
			return;
		}
		
		player.teleport(locations[Utils.random(locations.length)]);
	}

	public boolean createSpawn(Location location) {
		Location[] newSpawnPoints = new Location[locations.length + 1];
		
		for(int i = 0; i < locations.length; i++) {
			newSpawnPoints[i] = locations[i];
		}
		
		newSpawnPoints[newSpawnPoints.length - 1] = location;
		
		locations = newSpawnPoints;
		
		return DatabaseLib.addPosition(this, locations.length - 1);
	}
	
	protected void addSpawn(Location location) {
		Location[] newSpawnPoints = new Location[locations.length + 1];
		
		for(int i = 0; i < locations.length; i++) {
			newSpawnPoints[i] = locations[i];
		}
		
		newSpawnPoints[newSpawnPoints.length - 1] = location;
		
		locations = newSpawnPoints;
	}

	public void delSpawn(int index) {
		if(index >= locations.length) return;
		if(locations.length == 1) {
			DatabaseLib.delPosition(this, 0);
			locations = new Location[0];
			return;
		}
		
		Location[] oldSpawns = locations;
		locations = new Location[oldSpawns.length - 1];
		
		DatabaseLib.delPosition(this, index);
		
		for(int i = 0; i < oldSpawns.length; i++) {
			int id = i;
			if(id == index) continue;
			if(id > index) id--;
			
			locations[id] = oldSpawns[i];
			DatabaseLib.updatePosition(this, i, id);
		}
	}
	
	public Location[] getSpawns(int index, int pageItems) {
		int items = pageItems;
		
		if(index + pageItems >= locations.length) {
			items = locations.length - index;
		}
		
		Location[] locations = new Location[items];
		
		for(int i = 0; i < items; i ++) {
			locations[i] = locations[index + i];
		}
		
		return locations;
	}

	public Location getPosition() {
		if(locations.length == 1) {
			return locations[0];
		}
		
		return locations[Utils.random(locations.length)];
	}
	
	public Rank getRank() {
		if(requiredRank == null) return Rank.MEMBER;
		return requiredRank;
	}

	public void setRank(Rank rank) {
		this.requiredRank = rank;
	}

	public World getWorld() {
		return locations[0].getWorld();
	}
}
