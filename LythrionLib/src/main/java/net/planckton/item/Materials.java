package net.planckton.item;

import java.util.ArrayList;

import org.bukkit.Material;

public class Materials {
	private static final ArrayList<Material> BLOCKS_LIST = new ArrayList<Material>() {
		private static final long serialVersionUID = 6034872728643567842L;

		{
			for(Material mat : Material.values()) {
				if(!mat.isBlock()) continue;
				add(mat);
			}
		}
	};
	
	public static final Material[] BLOCKS_ARRAY = BLOCKS_LIST.toArray(new Material[BLOCKS_LIST.size()]);

	public static final ArrayList<String> BLOCKNAMES_LIST = new ArrayList<String>() {
		private static final long serialVersionUID = 6034872728938567842L;

		{
			for(Material mat : Material.values()) {
				if(!mat.isBlock()) continue;
				add(mat.name().toLowerCase());
			}
			
			add("stone_brick_stairs");
			add("stonebrick");
			add("stone_stairs");
			add("tallgrass");
			add("cobblestone_wall");
			add("spruce_strairs");
			add("wooden_slab");
			add("end_stone");
			add("stained_hardened_clay");
			add("stone_slab");
			add("double_stone_slab");
			add("skull");
		}
	};
	
	public static final String[] BLOCKNAMES_ARRAY = BLOCKNAMES_LIST.toArray(new String[BLOCKNAMES_LIST.size()]);

}
