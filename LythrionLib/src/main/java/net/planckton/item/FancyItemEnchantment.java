package net.planckton.item;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.bukkit.enchantments.Enchantment;

@AllArgsConstructor
public class FancyItemEnchantment {
	@Getter private Enchantment enchantment;
	@Getter private int level;
	@Getter private boolean ignoreLevelRestriction;
}
