package net.planckton.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.comphenix.attribute.NbtFactory;
import com.comphenix.attribute.NbtFactory.NbtCompound;
import com.google.gson.Gson;

import lombok.Getter;
import net.planckton.lib.utils.Utils;

public class FancyItem {
	public static FancyItem ITEM(Material material) {
		return ITEM(material, 1);
	}
	
	public static FancyItem ITEM(Material material, int amount) {
		return new FancyItem(material, amount);
	}
	
	public static FancyItem ITEM(Material material, int amount, int data) {
		return new FancyItem(material, amount, (byte) data);
	}
	
	public static FancyItem ITEM(ItemStack stack) {
		return new FancyItem(stack);
	}
	
	private ItemStack stack;
	@Getter private String name;
	private ArrayList<String> lores;
	@Getter private boolean unbreakable;
	private ArrayList<ItemFlag> flags;
	@Getter private int randomAddAmount = 0;
	
	private ArrayList<String> canPlaceOn;
	private ArrayList<String> canDestroy;
	private HashMap<String,Object> nbt;
	
	private Color color;
	private String skullOwner;
	
	private boolean useLeatherMeta = false;
	private boolean useSkullMeta = false;
	
	private static final Gson gson = GsonFactory.getNewGson(false);
	
	public static FancyItem fromJson(String json) {
		return gson.fromJson(json, FancyItem.class);
	}
	
	public static void main(String[] args) {
		FancyItem item = new FancyItem(Material.DIAMOND_BARDING).name(ChatColor.BLUE + "Kohi").lore(ChatColor.GRAY + "Full diamond prot 1, Sharp 2 diamond.").lore(ChatColor.GRAY + "Instant health 2 pots, enderpearls.").enchant(Enchantment.FIRE_ASPECT);
		System.out.println(item.toJson());
		
		//System.out.println(fromJson("{stack:{type:276},lores:[\"ERHMERGERD\"]}"));
	}
	
	public static FancyItem enchantedBook(Enchantment enchantments) {
		return enchantedBook(new FancyItemEnchantment(enchantments, 1, false));
	}
	
	public static FancyItem enchantedBook(Enchantment enchantments, int level) {
		return enchantedBook(new FancyItemEnchantment(enchantments, level, false));
	}
	
	public static FancyItem enchantedBook(Enchantment enchantments, int level, boolean ignoreLevelRestriction) {
		return enchantedBook(new FancyItemEnchantment(enchantments, level, ignoreLevelRestriction));
	}
	
	public static FancyItem enchantedBook(FancyItemEnchantment... enchantments) {
		ItemStack stack = new ItemStack(Material.ENCHANTED_BOOK);
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) stack.getItemMeta();
		
		for(FancyItemEnchantment ench : enchantments) {
			meta.addStoredEnchant(ench.getEnchantment(), ench.getLevel(), ench.isIgnoreLevelRestriction());
		}
		
		stack.setItemMeta(meta);
		
		return new FancyItem(stack);
	}
	
	public FancyItem(FancyItem item) {
		stack = item.stack.clone();
		name = item.name;
		if(item.lores != null) {
			lores = new ArrayList<String>();
			lores.addAll(item.lores);
		}
		unbreakable = item.unbreakable;
		if(item.flags != null) {
			flags = new ArrayList<ItemFlag>();
			flags.addAll(item.flags);
		}
		randomAddAmount = item.randomAddAmount;
		
		if(item.canPlaceOn != null) {
			canPlaceOn = new ArrayList<String>();
			canPlaceOn.addAll(item.canPlaceOn);
		}
		if(item.canDestroy != null) {
			canDestroy = new ArrayList<String>();
			canDestroy.addAll(item.canDestroy);
		}
		
		if(item.nbt != null) {
			nbt = new HashMap<String,Object>();
			nbt.putAll(item.nbt);
		}
	}
	
	public FancyItem(ItemStack stack) {
		this.stack = stack;
	}
	
	public FancyItem(Material material) {
		this(material, 1);
	}
	
	public FancyItem(Material material, int amount) {
		stack = new ItemStack(material, amount, (byte)0);
	}
	
	public FancyItem(Material material, int amount, byte data) {
		stack = new ItemStack(material, amount, data);
	}
	
	public String toJson() {
		return gson.toJson(this);
	}
	
	public FancyItem lore(String ...lores) {
		if(this.lores == null) this.lores = new ArrayList<String>();
		
		for(String lore : lores) {
			this.lores.add(lore);
		}
		
		return this;
	}
	
	public FancyItem lore(String lore) {
		if(lores == null) lores = new ArrayList<String>();
		
		lores.add(lore);
		
		return this;
	}
	
	public FancyItem name(String name) {
		this.name = name;
		
		return this;
	}
	
	public FancyItem enchant() {
		hideFlag(ItemFlag.HIDE_ENCHANTS);
		return enchantUnsafe(Enchantment.SILK_TOUCH, 32);
	}
	
	public FancyItem enchant(Enchantment enchantment) {
		return enchant(enchantment, 1);
	}
	
	public FancyItem enchant(Enchantment enchantment, int level) {
		stack.addEnchantment(enchantment, level);
		return this;
	}
	
	public FancyItem enchantUnsafe(Enchantment enchantment, int level) {
		stack.addUnsafeEnchantment(enchantment, level);
		return this;
	}
	
	public FancyItem hideFlag(ItemFlag flag) {
		if(flags == null) flags = new ArrayList<ItemFlag>();
		
		flags.add(flag);
		return this;
	}
	
	public FancyItem setColor(Color color) {
		if (!useLeatherMeta) {
			try {
				throw new Exception("Set UseLeatherMeta to true before using setColor()");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		this.color = color;
		
		return this;
	}
	
	public FancyItem setSkullOwner(String skullOwner) {
		if (!useSkullMeta) {
			try {
				throw new Exception("Set UseSkullMeta to true before using setSkullMeta()");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		this.skullOwner = skullOwner;
		
		return this;
	}
	
	public FancyItem setSkullMeta(boolean value) {
		this.useSkullMeta = value;	
		
		return this;
	}
	
	public FancyItem setUseLeatherMeta(boolean value) {
		this.useLeatherMeta = value;	
		
		return this;
	}
	
	public FancyItem unbreakable() {
		return unbreakable(true);
	}
	
	public FancyItem unbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		
		return this;
	}
	
	public FancyItem setRandomAddAmount(int randomAddAmount) {
		this.randomAddAmount = randomAddAmount;
		
		return this;
	}
	
	public FancyItem canPlaceOn(Material... materials) {
		if(canPlaceOn == null) canPlaceOn = new ArrayList<String>();
		
		for(Material material : materials) {
			canPlaceOn.add(material.name().replace(' ', '_').toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canDestroy(Material... materials) {
		if(canDestroy == null) canDestroy = new ArrayList<String>();
		
		for(Material material : materials) {
			canDestroy.add(material.name().replace(' ', '_').toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canPlaceOn(String... materials) {
		if(canPlaceOn == null) canPlaceOn = new ArrayList<String>();
		
		for(String material : materials) {
			canPlaceOn.add(material.toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canDestroy(String... materials) {
		if(canDestroy == null) canDestroy = new ArrayList<String>();
		
		for(String material : materials) {
			canDestroy.add(material.toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem addNBT(String key, Object data) {
		if(nbt == null) nbt = new HashMap<String,Object>();
		
		nbt.put(key, data);
		
		return this;
	}
	
	public FancyItem setAmount(int amount) {
		stack.setAmount(amount);
		
		return this;
	}
	
	public FancyItem setMaterial(Material material) {
		stack.setType(material);
		return this;
	}
	
	public ItemStack stack() {
		ItemStack stack = new ItemStack(this.stack);
		
		if (useLeatherMeta && !useSkullMeta) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) stack.getItemMeta();
			
			leatherArmorMeta.setColor(color);			
			stack.setItemMeta(leatherArmorMeta);
		}
		
		if (useSkullMeta && !useLeatherMeta) {
			SkullMeta skullMeta = (SkullMeta) stack.getItemMeta();
			
			skullMeta.setOwner(skullOwner);			
			stack.setItemMeta(skullMeta);
		}
		
		ItemMeta meta = stack.getItemMeta();
	
		if(name != null) meta.setDisplayName(name);
		if(unbreakable) meta.spigot().setUnbreakable(true);
		
		if(lores != null) {
			List<String> metaLores = meta.getLore();
			if(metaLores == null) metaLores = lores;
			else metaLores.addAll(lores);
			meta.setLore(metaLores);
		}
		
		if(flags != null) for(ItemFlag flag : flags) {
			meta.addItemFlags(flag);
		}
		
		stack.setItemMeta(meta);
		
		stack = NbtFactory.getCraftItemStack(stack);
		NbtCompound other = NbtFactory.fromItemTag(stack);
		if(canPlaceOn != null && canPlaceOn.size() > 0) other.putPath("CanPlaceOn", NbtFactory.createList(canPlaceOn));
		if(canDestroy != null && canDestroy.size() > 0) other.putPath("CanDestroy", NbtFactory.createList(canDestroy));
		if(nbt != null && nbt.size() > 0) {
			Iterator<Entry<String, Object>> it = nbt.entrySet().iterator();
		    while (it.hasNext()) {
		        Entry<String, Object> pair = it.next();
		        
		        other.putPath(pair.getKey(), pair.getValue());
		    }
		}
		
		if(randomAddAmount > 0) stack.setAmount(stack.getAmount() + Utils.random(randomAddAmount));
		
		return stack;
	}
	
	public FancyItem makeAdventurePlaceable() {
		return makeAdventurePlaceable(true);
	}
	
	public FancyItem makeAdventurePlaceable(boolean hide) {
		canPlaceOn(Materials.BLOCKNAMES_ARRAY);
		if(hide) hideFlag(ItemFlag.HIDE_PLACED_ON);
		
		return this;
	}
	
	
	//getters
	public Collection<String> getLores() {
		if(lores == null) return null;
		
		return Collections.unmodifiableCollection(lores);
	}

	public FancyItem hideAllFlags() {
		if(flags == null) flags = new ArrayList<ItemFlag>();
		
		for(ItemFlag flag : ItemFlag.values()) flags.add(flag);
		
		return this;
	}
	
	public Material getType() {
		return stack.getType();
	}

	public FancyItem setData(byte data) {
		stack.setDurability(data);
		return this;
	}

	public FancyItem clearLores() {
		if(lores == null) return this;
		
		lores.clear();
		
		return this;
	}
}
