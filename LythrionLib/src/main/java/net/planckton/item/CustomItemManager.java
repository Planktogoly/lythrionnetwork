package net.planckton.item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

public class CustomItemManager extends Manager implements Listener {
	
	public static final ItemStack HUB = new FancyItem(Material.WATCH)
			.name("" + ChatColor.RED + ChatColor.BOLD + "Hub")
			.lore(ChatColor.GRAY + "Click to go to the hub")
			.stack();		
	
	public static final ItemStack COSMETICS_CHEST = new FancyItem(Material.ENDER_CHEST)
			.name("" + ChatColor.GOLD + ChatColor.BOLD + "Cosmetics")
			.lore(ChatColor.GRAY + "Equip here your cosmetics!")
			.stack();
	
	public static final ItemStack PLAYER_SETTINGS = new FancyItem(Material.REDSTONE_COMPARATOR)
		.name("" + ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "Settings")
		.lore(ChatColor.GRAY + "Change miscellaneous settings")
		.stack();
	
	public static final ItemStack NAVIGATOR = new FancyItem(Material.COMPASS)
			.name("" + ChatColor.RED + ChatColor.BOLD + "Game selector")
			.lore(ChatColor.GRAY + "Click to join a game")
			.stack();
	
	public static final ItemStack LOBBIES = new FancyItem(Material.SLIME_BALL)
			.name("" + ChatColor.GREEN + ChatColor.BOLD + "Lobby selector")
			.lore(ChatColor.GRAY + "Click to select a lobby")
			.stack();
	
	@Override
	public void onEnable() {
	}

	@Override
	public void onDisable() {
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		ItemStack stack = player.getItemInHand();
		if(stack == null) return;
		if(stack.getType() == Material.AIR) return;
		ItemMeta meta = stack.getItemMeta();
		if(meta.getDisplayName() == null) return;
		
		if(stack.equals(PLAYER_SETTINGS)) {
			player.playSound(Sound.CLICK);
			player.balance.get().subtractCurrencyGeneral(1);
			event.setCancelled(true);
			
			//new PlayerSettingsScreen(player).open(player);
			return;
		}
		else if(stack.isSimilar(HUB)) {
			player.connectToHub();
			return;
		}
	}
}