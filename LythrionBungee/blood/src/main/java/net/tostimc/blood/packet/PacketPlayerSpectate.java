package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketPlayerSpectate extends PlayerPacket {

	private String player;	
	
	public PacketPlayerSpectate() {
		super(PacketType.PLAYER_SPECTATE);
	}
	
	public PacketPlayerSpectate(UUID uuid, String spectate) {
		super(PacketType.PLAYER_SPECTATE, uuid);
		
		this.player = spectate;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(player);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		player = in.readUTF();
	}
	
	public String getSpectatePlayer() {
		return player;
	}

}
