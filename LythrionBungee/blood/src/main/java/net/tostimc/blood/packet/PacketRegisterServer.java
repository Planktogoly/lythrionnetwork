package net.tostimc.blood.packet;

import lombok.Getter;
import net.tostimc.blood.ServerStatus;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketRegisterServer extends Packet {
	@Getter private String serverName;
	@Getter private String ip;
	@Getter private int port;
	@Getter private ServerStatus status;
	
	public PacketRegisterServer() {
		super(PacketType.REGISTER_SERVER);
	}
	
	public PacketRegisterServer(String serverName, String ip, int port, ServerStatus status) {
		super(PacketType.REGISTER_SERVER);
		
		this.serverName = serverName;
		this.ip = ip;
		this.port = port;
		this.status = status;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(serverName);
		out.writeUTF(ip);
		out.writeInt(port);
		out.writeUTF(status.name());
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		serverName = in.readUTF();
		ip = in.readUTF();
		port = in.readInt();
		status = ServerStatus.valueOf(in.readUTF());
	}
}
