package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PlayerPacket extends Packet {
	private UUID uuid;
	
	public PlayerPacket(PacketType type) {
		super(type);
	}
	
	public PlayerPacket(PacketType type, UUID uuid) {
		super(type);
		this.uuid = uuid;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(uuid.toString());
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		uuid = UUID.fromString(in.readUTF());
	}
	
	public UUID getUuid() {
		return uuid;
	}
}
