package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketPlayerJoinHub extends PlayerPacket {

	public PacketPlayerJoinHub() {
		super(PacketType.PLAYER_JOIN_HUB);
	}
	
	public PacketPlayerJoinHub(UUID uuid) {
		super(PacketType.PLAYER_JOIN_HUB, uuid);
	}

}
