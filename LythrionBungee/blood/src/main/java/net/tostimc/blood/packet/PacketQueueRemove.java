package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketQueueRemove extends PlayerPacket {

	public PacketQueueRemove() {
		super(PacketType.QUEUE_LEAVE);
	}
	
	public PacketQueueRemove(UUID uuid) {
		super(PacketType.QUEUE_LEAVE, uuid);
	}

}
