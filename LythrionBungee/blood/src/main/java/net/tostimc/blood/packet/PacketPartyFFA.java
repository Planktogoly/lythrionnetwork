package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketPartyFFA extends Packet {
	
	@Getter private String mode;
	@Getter private UUID uuid;
	
	public PacketPartyFFA() {
		super(PacketType.PARTY_FFA);
    }

    public PacketPartyFFA(UUID uuid, String mode) {
    	super(PacketType.PARTY_FFA);		
    	
    	this.uuid = uuid;
    	this.mode = mode;
    }
    
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(mode);
		out.writeUTF(uuid.toString());
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		mode = in.readUTF();
		uuid = UUID.fromString(in.readUTF());
	}

}
