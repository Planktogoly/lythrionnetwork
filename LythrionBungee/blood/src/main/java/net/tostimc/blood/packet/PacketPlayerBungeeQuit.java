package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketPlayerBungeeQuit extends PlayerPacket {
	public PacketPlayerBungeeQuit() {
		super(PacketType.PLAYER_BUNGEE_QUIT);
	}
	
	public PacketPlayerBungeeQuit(UUID uuid) {
		super(PacketType.PLAYER_BUNGEE_QUIT, uuid);
	}
}
