package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketPartyJoin extends PlayerPacket {

	public PacketPartyJoin() {
		super(PacketType.PARTY_JOIN);
	}
	
	public PacketPartyJoin(UUID uuid) {
		super(PacketType.PARTY_JOIN, uuid);		
	}

}
