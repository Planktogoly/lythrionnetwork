package net.tostimc.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public abstract class Packet {
	private static PacketType[] packetTypes = new PacketType[Byte.MAX_VALUE];
	
	static {
		PacketType.values();
	}
	
	public static enum PacketType {
		HANDSHAKE((byte)1, PacketHandshake.class),
		
		PLAYER_SERVER_JOIN((byte)10, PacketPlayerServerJoin.class),
		PLAYER_SERVER_QUIT((byte)11, PacketPlayerServerQuit.class),
		PLAYER_BUNGEE_JOIN((byte)12, PacketPlayerBungeeJoin.class),
		PLAYER_BUNGEE_QUIT((byte)13, PacketPlayerBungeeQuit.class),
		PLAYER_MESSAGE((byte)14, PacketPlayerMessage.class),
		PLAYER_COMMAND((byte)15, PacketPlayerCommand.class),
		PLAYER_TAB_COMPLETE((byte)16, PacketPlayerTabComplete.class),
		PLAYER_CONNECT((byte)17, PacketPlayerConnect.class),
		PLAYER_BALANCE_CONNECT((byte)18, PacketPlayerBalanceConnect.class),
		PLAYER_JOIN_HUB((byte) 19, PacketPlayerJoinHub.class),
		PLAYER_SPECTATE((byte) 20, PacketPlayerSpectate.class),
		
		QUEUE_JOIN((byte)25, PacketQueueJoin.class),
		QUEUE_LEAVE((byte)26, PacketQueueLeave.class),
		QUEUE_REMOVE((byte)27, PacketQueueRemove.class),
				
		REGISTER_COMMAND((byte)30, PacketRegisterCommand.class),
		REGISTER_SERVER((byte)31, PacketRegisterServer.class),
		
		INFO_REGISTER((byte)40, PacketInfoRegister.class),
		INFO_UPDATE((byte)41, PacketInfoUpdate.class),
		INFO_REQUEST((byte)42, PacketInfoRequest.class),
		INFO_REMOVE((byte)43, PacketInfoRemove.class),
		
		MESSAGE((byte)45, PacketMessage.class),
		
		SERVER_UPDATE((byte)50, PacketServerUpdate.class),
		
		PARTY_CREATE((byte)51, PacketPartyCreate.class),
		PARTY_DISBAND((byte)52, PacketPartyDisband.class),
		PARTY_FFA((byte)53, PacketPartyFFA.class),
		PARTY_ISINPARTY((byte)54, PacketIsInParty.class),
		PARTY_LEAVE((byte)55, PacketPartyLeave.class),
		PARTY_JOIN((byte)56, PacketPartyJoin.class),
		PARTY_SPLIT((byte) 57, PacketPartySplitFight.class);
		
		private byte id;
		private Class<? extends Packet> packetClass;
		
		private PacketType(byte id, Class<? extends Packet> packetClass) {
			this.id = id;
			this.packetClass = packetClass;
			
			packetTypes[id] = this;
		}
		
		public Class<? extends Packet> getPacketClass() {
			return packetClass;
		}
		
		public byte getId() {
			return id;
		}
	}
	
	public static Class<? extends Packet> getPacketClass(byte id) {
		PacketType type = packetTypes[id];
		if(type == null) return null;
		return type.packetClass;
	}
	
	private PacketType packetType;
	
	public Packet(PacketType type) {
		if(type.getPacketClass() != getClass()) {
			throw new RuntimeException("Packet " + getClass() + "was created with type of class " + type.getClass());
		}
		
		this.packetType = type;
	}
	
	public void fromBytes(byte[] bytes) {
		ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
		in.skipBytes(3);
		
		readBytes(in);
	}
	
	public byte[] toBytes() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		
		out.write(packetType.id);
		//add length afterwards
		out.writeShort(13);
		
		writeBytes(out);
		
		byte[] bytes = out.toByteArray();
		
		//add length
		short length = (short) bytes.length;
		bytes[2] = (byte)(length & 0xFF);
		bytes[1] = (byte)((length >> 8) & 0xFF);
		
		return bytes;
	}
	
	abstract void writeBytes(ByteArrayDataOutput out);
	abstract void readBytes(ByteArrayDataInput in);
	
	public PacketType getPacketType() {
		return packetType;
	}
	
	public int getPacketId() {
		return packetType.id;
	}
}
