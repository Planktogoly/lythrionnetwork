package net.tostimc.blood.util;

/**
 * @author Tim Biesenbeek
 */
@FunctionalInterface
public interface CatchRunnable<E extends Throwable> {

	void run() throws E;

	default CatchRunnable<E> andThen(CatchRunnable<E> after) {
		return () -> {
			run();
			after.run();
		};
	}
}
