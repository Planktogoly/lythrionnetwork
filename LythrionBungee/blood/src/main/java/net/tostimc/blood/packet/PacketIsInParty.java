package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketIsInParty extends Packet {

	@Getter private UUID uuid;
	@Getter private boolean value;
	@Getter private boolean isLeader;
	
	public PacketIsInParty() {
		super(PacketType.PARTY_ISINPARTY);
		
	}
	
	public PacketIsInParty(UUID uuid, boolean value, boolean isLeader) {
		super(PacketType.PARTY_ISINPARTY);
		
		this.uuid = uuid;
		this.value = value;	
		this.isLeader = isLeader;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeBoolean(value);
		out.writeUTF(uuid.toString());
		out.writeBoolean(isLeader);
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		value = in.readBoolean();
		uuid = UUID.fromString(in.readUTF());	
		isLeader = in.readBoolean();
	}

}
