package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketQueueJoin extends Packet {

	@Getter private int queueID;
	private UUID uuid;
	private String type;
	
	public PacketQueueJoin() {
		super(PacketType.QUEUE_JOIN);
	}
	
	public PacketQueueJoin(UUID uuid, int queueID, String type) {
		super(PacketType.QUEUE_JOIN);
		
		this.uuid = uuid;
		this.queueID = queueID;
		this.type = type;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(uuid.toString());
		out.writeInt(queueID);
		out.writeUTF(type);
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		uuid = UUID.fromString(in.readUTF());
		queueID = in.readInt();
		type = in.readUTF();
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public String getType() {
		return type;
		
	}

}
