package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketQueueLeave extends PlayerPacket {

	public PacketQueueLeave() {
		super(PacketType.QUEUE_LEAVE);
	}
	
	public PacketQueueLeave(UUID uuid) {
		super(PacketType.QUEUE_LEAVE, uuid);
	}

}
