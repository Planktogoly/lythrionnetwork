package net.tostimc.blood.packet;

import java.util.UUID;

import lombok.Getter;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketPlayerConnect extends PlayerPacket {
	@Getter private String server;
	@Getter private boolean force;

	public PacketPlayerConnect() {
		super(PacketType.PLAYER_CONNECT);
	}
	
	public PacketPlayerConnect(UUID id, String server) {
		this(id, server, false);
	}
	
	public PacketPlayerConnect(UUID id, String server, boolean force) {
		super(PacketType.PLAYER_CONNECT, id);
		
		this.server = server;
		this.force = force;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(server);
		out.writeBoolean(force);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		server = in.readUTF();
		force = in.readBoolean();
	}
}
