package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketPartyDisband extends PlayerPacket {
	
	public PacketPartyDisband() {
		super(PacketType.PARTY_DISBAND);
	}
	
	public PacketPartyDisband(UUID uuid) {
		super(PacketType.PARTY_DISBAND, uuid);		
	}

}
