package net.tostimc.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketInfoRemove extends Packet {
	private String server;
	private String key;

	public PacketInfoRemove() {
		super(PacketType.INFO_REMOVE);
	}
	
	public PacketInfoRemove(String server, String key) {
		super(PacketType.INFO_REMOVE);
		
		this.server = server;
		this.key = key;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(server);
		out.writeUTF(key);		
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		server = in.readUTF();
		key = in.readUTF();		
	}
	
	public String getServer() {
		return server;
	}
	
	public String getKey() {
		return key;
	}
}
