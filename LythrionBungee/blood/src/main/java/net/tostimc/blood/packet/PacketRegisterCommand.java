package net.tostimc.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketRegisterCommand extends Packet {
	private String command;
	
	public PacketRegisterCommand() {
		super(PacketType.REGISTER_COMMAND);
	}
	
	public PacketRegisterCommand(String command) {
		super(PacketType.REGISTER_COMMAND);
		this.command = command;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(command);
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		command = in.readUTF();
	}
	
	public String getCommand() {
		return command;
	}
}
