package net.tostimc.blood.packet;

import lombok.Getter;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketMessage extends Packet {
	@Getter private String from;
	@Getter private String to;
	@Getter private String channel;
	@Getter private byte[] data;
	
	public PacketMessage() {
		super(PacketType.MESSAGE);
	}
	
	public PacketMessage(String from, String to, String channel, byte[] data) {
		super(PacketType.MESSAGE);
		
		this.from = from;
		this.to = to;
		this.channel = channel;
		this.data = data;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(from);
		out.writeUTF(to);
		out.writeUTF(channel);
		
		out.writeShort(data.length);
		out.write(data);
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		from = in.readUTF();
		to = in.readUTF();
		channel = in.readUTF();
		
		short length = in.readShort();
		data = new byte[length];
		for(int i = 0; i < length; i++) data[i] = in.readByte();
	}
}
