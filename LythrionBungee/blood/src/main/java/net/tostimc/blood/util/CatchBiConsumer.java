package net.tostimc.blood.util;


/**
 * @author Tim Biesenbeek
 */
@FunctionalInterface
public interface CatchBiConsumer<T, T2, E extends Throwable> {

	void accept(T t, T2 t2) throws E;

	default CatchBiConsumer<T,T2, E> andThen(CatchBiConsumer<? super T, ? super T2, E> after) {
		return (T t, T2 t2) -> {
			accept(t, t2);
			after.accept(t, t2);
		};
	}
}

