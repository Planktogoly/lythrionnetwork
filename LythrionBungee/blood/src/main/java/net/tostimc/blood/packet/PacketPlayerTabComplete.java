package net.tostimc.blood.packet;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketPlayerTabComplete extends PlayerPacket {
	private static final Joiner joiner = Joiner.on(',');
	private static final Splitter splitter = Splitter.on(',');
	
	private String command;
	private List<String> suggestions;
	
	public PacketPlayerTabComplete() {
		super(PacketType.PLAYER_TAB_COMPLETE);
	}
	
	public PacketPlayerTabComplete(UUID id, String command) {
		super(PacketType.PLAYER_TAB_COMPLETE, id);
		
		this.command = command;
		this.suggestions = new ArrayList<String>();
	}
	
	public PacketPlayerTabComplete(UUID id, String command, List<String> suggestions) {
		super(PacketType.PLAYER_TAB_COMPLETE, id);
		
		this.command = command;
		this.suggestions = suggestions;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(command);
		out.writeUTF(joiner.join(suggestions));
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		command = in.readUTF();
		suggestions = splitter.splitToList(in.readUTF());
	}
	
	public String getCommand() {
		return command;
	}
	
	public List<String> getSuggestions() {
		return suggestions;
	}
	
	public void setSuggestions(List<String> suggestions) {
		this.suggestions = suggestions;
	}
}
