package net.tostimc.blood.packet;

import java.util.UUID;

public class PacketPartyCreate extends PlayerPacket {

	public PacketPartyCreate() {
		super(PacketType.PARTY_CREATE);
	}
	
	public PacketPartyCreate(UUID uuid) {
		super(PacketType.PARTY_CREATE, uuid);		
	}

}
