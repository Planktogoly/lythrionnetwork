package net.tostimc.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketHandshake extends Packet {
	public static enum InstanceType {
		SERVER((byte)0),BUNGEE((byte)1);
		
		public static InstanceType fromInt(int id) {
			for(InstanceType type : values()) {
				if(type.id != id) continue;
				
				return type;
			}
			
			return null;
		}
		
		private byte id;
		
		private InstanceType(byte id) {
			this.id = id;
		}
		
		public byte getId() {
			return id;
		}
	}
	
	private InstanceType instanceType;
	private String name;
	private int port;
	
	public PacketHandshake() {
		super(Packet.PacketType.HANDSHAKE);
	}
	
	public PacketHandshake(InstanceType type, String name, int port) {
		super(Packet.PacketType.HANDSHAKE);
		
		this.instanceType = type;
		this.name = name;
		this.port = port;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeInt(instanceType.getId());
		out.writeUTF(name);
		out.writeInt(port);
	}
	
	@Override
	void  readBytes(ByteArrayDataInput in) {
		instanceType = InstanceType.fromInt(in.readInt());
		name = in.readUTF();
		port = in.readInt();
	}
	
	//getters
	public InstanceType getInstanceType() {
		return instanceType;
	}
	
	public String getName() {
		return name;
	}
	
	public int getPort() {
		return port;
	}
}
