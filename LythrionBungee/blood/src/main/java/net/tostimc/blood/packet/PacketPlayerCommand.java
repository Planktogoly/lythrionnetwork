package net.tostimc.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketPlayerCommand extends PlayerPacket {
	private String command;

	public PacketPlayerCommand() {
		super(PacketType.PLAYER_COMMAND);
	}
	
	public PacketPlayerCommand(UUID id, String command) {
		super(PacketType.PLAYER_COMMAND, id);
		
		this.command = command;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(command);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		command = in.readUTF();
	}
	
	public String getCommand() {
		return command;
	}
}
