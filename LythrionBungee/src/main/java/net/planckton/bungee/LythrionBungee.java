package net.planckton.bungee;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.planckton.blood.ServerStatus;
import net.planckton.bungee.command.ConnectCommand;
import net.planckton.bungee.command.MOTDReloadCommand;
import net.planckton.bungee.command.ProxyReloadCommand;
import net.planckton.bungee.command.ServersCommand;
import net.planckton.bungee.db.Database;
import net.planckton.bungee.net.Bootstrapper;
import net.planckton.bungee.util.LythrionServerInfo;
import net.planckton.bungee.util.Rank;
import net.planckton.bungee.util.proxy.ProxyDetection;

public class LythrionBungee extends Plugin {
	@Getter private static LythrionBungee instance;
	@Getter private static String serverName;
	@Getter private static boolean devServer;
	@Getter private static Bootstrapper bootstrapper;
	
	public static HashMap<UUID, Rank> ranks = new HashMap<>();
	public static ArrayList<String> motds = new ArrayList<>();
	
	@Getter @Setter public static int totalOnlineCount;
	
	private static TreeMap<String, LythrionServerInfo> serverInfos = new TreeMap<String, LythrionServerInfo>();
	private static TreeMap<String, TreeSet<LythrionServerInfo>> availableServersPerType = new TreeMap<>();
	
	@Getter private static JsonObject lythrionConfig;
	
	public static void registerServer(ServerInfo serverInfo, ServerStatus status) {
		LythrionServerInfo info = serverInfos.get(serverInfo.getName());
		if(info == null) info = new LythrionServerInfo(serverInfo, status);
		else {
			info.setStatus(status);
			info.setServerInfo(serverInfo);
		}
		
		serverInfos.put(info.getName(), info);
		
		if(status == ServerStatus.ONLINE) addAvailableServer(info);
	}
	
	public static LythrionServerInfo getServer(String server) {
		return serverInfos.get(server);
	}
	
	public static void addAvailableServer(LythrionServerInfo info) {
		String type = info.getServerType();
		TreeSet<LythrionServerInfo> infos = availableServersPerType.get(type);
		if(infos == null) infos = new TreeSet<LythrionServerInfo>();
		
		infos.add(info);
		availableServersPerType.put(type, infos);
	}
	
	public static void removeAvailableServer(LythrionServerInfo info) {
		String type = info.getServerType();
		TreeSet<LythrionServerInfo> infos = availableServersPerType.get(type);
		if(infos == null) return;
		
		if(!infos.remove(info)) return;
		
		if(infos.size() > 0) {
			availableServersPerType.put(type, infos);
		}
		else {
			availableServersPerType.remove(type);
		}
	}
	
	public static Collection<LythrionServerInfo> getServers() {
		return serverInfos.values();
	}
	
	public static Set<LythrionServerInfo> getAvailableServersForType(String type) {
		return availableServersPerType.get(type);
	}
	
    public static void log(String message) {
    	System.out.println("[LythrionBungee] " + message);
    }
	
    @Override
    public void onEnable() {
    	instance = this;
    	serverName = new File("").getAbsoluteFile().getName();
    	devServer = serverName.startsWith("DEV");
    	
    	try {
			File configFile = new File("../config.json");
			lythrionConfig = new JsonParser().parse(new JsonReader(new FileReader(configFile))).getAsJsonObject();
		} catch (Exception e) {
			System.out.println("Could not load config D: RUNNNNN FORESTTT RUNNNNNNNN");
			e.printStackTrace();
			getProxy().stop("No config found");
			return;
		}

    	for(Entry<String, ServerInfo> entry : new LinkedList<>(getProxy().getServers().entrySet())) {
    		registerServer(entry.getValue(), ServerStatus.UNKOWN);
    	}
    	
    	boolean databaseConnected = false;
    	try {
    		databaseConnected = Database.openConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	JsonObject configHeart;
		if(LythrionBungee.isDevServer()) configHeart = LythrionBungee.getLythrionConfig().getAsJsonObject("heartdev");
		else configHeart = LythrionBungee.getLythrionConfig().getAsJsonObject("heart");
    	
    	bootstrapper = new Bootstrapper(configHeart.get("host").getAsString(), 25333);
    	bootstrapper.connect();
    	
    	if(databaseConnected) {
        	Database.loadMotds();
        	ProxyDetection.loadData();
    	}
    	
    	PluginManager pm = getProxy().getPluginManager();
    	
    	pm.registerCommand(this, new MOTDReloadCommand());
    	pm.registerCommand(this, new ConnectCommand());
    	pm.registerCommand(this, new ServersCommand());
    	pm.registerCommand(this, new ProxyReloadCommand());
        
    	pm.registerListener(this, new LythrionBungeeListener());
        
       // getProxy().getScheduler().schedule(this, new PlayerLogTask(), 0, 2, TimeUnit.MINUTES);
    }
    
    @Override
    public void onDisable() {
    	bootstrapper.stop();
    	Database.closeConnection();
    }
    
    public static Map<String, Set<LythrionServerInfo>> getAvailableServersPerType() {
		return ImmutableMap.copyOf(availableServersPerType);
	}
}