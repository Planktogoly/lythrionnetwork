package net.planckton.bungee.util.proxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Cleanup;
import lombok.NonNull;
import net.planckton.bungee.db.Database;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@SuppressWarnings("deprecation")
public class ProxyDetection {
	private static List<String> hosts = Collections.synchronizedList(new ArrayList<>());
	private static List<String> blockedASNs = Collections.synchronizedList(new ArrayList<>());
	
	public static boolean loadData() {
		try {
			@Cleanup Connection connectionWrite = Database.getConnectionPoolRead().getConnection();
			@Cleanup PreparedStatement statementHosts = connectionWrite.prepareStatement("SELECT host FROM proxy_host WHERE enabled=1");
			
			@Cleanup ResultSet rsHosts = statementHosts.executeQuery();
			
			hosts.clear();
			while(rsHosts.next()) {
				String host = rsHosts.getString("host");
				System.out.println("Loaded proxy check host: " + host);
				hosts.add(host);
			}
			System.out.println("Loaded " + hosts.size() + " proxy check hosts.");
			
			@Cleanup PreparedStatement statementAsns = connectionWrite.prepareStatement("SELECT asn FROM proxy_blockedasn WHERE enabled=1");
			@Cleanup ResultSet rsAsn = statementAsns.executeQuery();
			
			blockedASNs.clear();
			while(rsAsn.next()) {
				String blockedASN = rsAsn.getString("asn");
				System.out.println("Loaded blocked ASN: " + blockedASN);
				blockedASNs.add(blockedASN);
			}
			System.out.println("Loaded " + blockedASNs.size() + " blocked ASN's.");
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//Method used for testing locally, not actually used in TostiCord
	public static void main(String[] arg) {
		try {
			List<String> ips = new ArrayList<>();
			
			ips.add("217.146.13.38");
			ips.add("58.51.148.193");
			ips.add("109.132.101.255");
			ips.add("51.255.39.230");
			
			hosts.add("dnsbl.dronebl.org");
			hosts.add("dnsbl.proxybl.org");
			hosts.add("zombie.dnsbl.sorbs.net");
			hosts.add("http.dnsbl.sorbs.net");
			hosts.add("socks.dnsbl.sorbs.net");
			hosts.add("misc.dnsbl.sorbs.net");
			hosts.add("xbl.spamhaus.org");
			
			blockedASNs.add("AS42473");
			
			for(String s : ips) {
				ProxyCheckResult result = isProxy(s);
				System.out.println(s + ": " + result.isProxy() + "   " + result.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ProxyCheckResult isProxy(@NonNull String ip) {
		if(isProxyMashape(ip)) return new ProxyCheckResult(true, "mashape");
		
		String[] ipParts = ip.split("\\.");
		String reverseIP = new StringBuilder()
			.append(ipParts[3] + ".")
			.append(ipParts[2] + ".")
			.append(ipParts[1] + ".")
			.append(ipParts[0]).toString();
		
		for(String host : hosts) {
			if(isDnsbl(host, reverseIP)) return new ProxyCheckResult(true, host);
		}
		
		return new ProxyCheckResult(false, null);
	}
	
	protected static boolean isDnsbl(String host, String reverseIP) {
		try {
			HttpClient client = getNewHttpClient();
			HttpGet request = new HttpGet("https://dns-api.org/A/" + reverseIP + "." + host);
			
			HttpResponse response = client.execute(request);         
	        HttpEntity entity = response.getEntity();
	        
	        String result;
	        if (entity == null) return false;
	        
            InputStream instream = entity.getContent();
            result = convertStreamToString(instream);
			JsonObject json = new JsonParser().parse(result).getAsJsonArray().get(0).getAsJsonObject();
			
			if(json.getAsJsonPrimitive("error") != null) return false;
			
			boolean isProxy = json.getAsJsonPrimitive("value").getAsString().startsWith("127.0.0.");

			return isProxy;
		}
		catch(Exception e) {
			System.out.println("An error occured while checking for dnsbl for host: " + host);
			e.printStackTrace();
			return false;
		}
	}
	
	private static HttpClient getNewHttpClient() {
	    try {
	        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	        trustStore.load(null, null);

	        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

	        HttpParams params = new BasicHttpParams();
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        registry.register(new Scheme("https", sf, 443));

	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

	        return new DefaultHttpClient(ccm, params);
	    } catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
	
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	protected static boolean isProxyMashape(String ip) {
		try {
			URL url = new URL("https://iphub.p.mashape.com/api.php?ip=" + ip + "&showtype=4");

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("X-Mashape-Key", "75BohajlBamshConYyRjDfZj2zdRp1zEMfjjsnzaGBMzpjKrXh");
			connection.setRequestProperty("Accept", "application/json");

			JsonObject json = connectionToJSONObject(connection);
			if(json == null) return false;
			
			boolean isProxy = json.getAsJsonPrimitive("proxy").getAsInt() != 0;

			if(!isProxy) {
				for(String blockedASN : blockedASNs) {
					isProxy = json.get("asn").getAsString().startsWith(blockedASN);
					
					if(isProxy) break;
				}
			}
			
			connection.disconnect();

			return isProxy;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static JsonObject connectionToJSONObject(HttpURLConnection connection) {
		BufferedReader reader;
		try {
			connection.connect();
			
			if(connection.getResponseCode() == 500) return null;
			
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		} catch (IOException e) {
			System.out.println("Error while reading proxy detection connection: " + e.getMessage());
			return null;
		}
		StringBuilder builder = new StringBuilder();
		String aux = "";

		try {
			while ((aux = reader.readLine()) != null) {
			    builder.append(aux);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		String response = builder.toString();
		//System.out.println(response);
		
		JsonObject json = new JsonParser().parse(response).getAsJsonObject();
		
		return json;
	}
}
