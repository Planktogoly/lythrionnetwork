package net.planckton.bungee.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
public class ServerUpdate {
	@Getter @NonNull String from;
	@Getter @NonNull String to;
}
