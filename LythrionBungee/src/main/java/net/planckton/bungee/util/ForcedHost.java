package net.planckton.bungee.util;

import lombok.Getter;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;

public class ForcedHost {
	private String name;
	private boolean forcingMotd;
	@Getter private ServerInfo info;
	@Getter private ServerPing pingedInfo;
	@Getter private boolean updateOnPing;

	public ForcedHost(String name) {
		this.name = name;

		forcingMotd = false;

		updateInfo();
	}
	
	public String getMotd() {
		return pingedInfo.getDescriptionComponent().toLegacyText();
	}

	public void updateInfo() {
		info = ProxyServer.getInstance().getServerInfo(name);
		if (info != null)
			ping();
	}
	
	public boolean isForcedMotd() {
		return pingedInfo != null && forcingMotd;
	}

	private void ping() {
		info.ping(new ServerPinger());
	}

	private class ServerPinger implements Callback<ServerPing> {
		@Override
		public void done(ServerPing serverPing, Throwable throwable) {
			pingedInfo = serverPing;
			if(pingedInfo == null) return;
			
			boolean isHashtag = pingedInfo.getDescriptionComponent().toLegacyText().startsWith("$");
			
			forcingMotd = isHashtag;
			updateOnPing = isHashtag;
		}
	}
}
