package net.planckton.bungee.util.proxy;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ProxyCheckResult {
	@Getter private boolean proxy;
	@Getter private String key;
}
