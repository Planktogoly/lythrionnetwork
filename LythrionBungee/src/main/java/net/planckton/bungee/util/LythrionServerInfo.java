package net.planckton.bungee.util;

import java.io.File;
import java.io.FileReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.md_5.bungee.api.config.ServerInfo;
import net.planckton.blood.GameStatus;
import net.planckton.blood.ServerStatus;
import net.planckton.bungee.LythrionBungee;

public class LythrionServerInfo implements Comparable<LythrionServerInfo> {
	
	@Getter private JsonObject serverConfig;
	@Getter @Setter private @NonNull ServerInfo serverInfo;
	@Getter private @NonNull ServerStatus status;
	
	@Getter private String serverName;
	@Getter private String serverType;
	@Getter private int maxPlayers;
	
	@Getter private GameStatus gameStatus;
	@Getter private boolean isMinigame;
	
	public LythrionServerInfo(@NonNull ServerInfo serverInfo, @NonNull ServerStatus status) {
		this.serverInfo = serverInfo;
		this.status = status;
		
	    try {
         	File config = new File("/home/minecraft/" + serverInfo.getName() +  "/serverConfig.json");
	    	if (!config.exists()) {
	    		System.out.println("[LythrionBungee] WARNING ADD A SERVERCONFIG.YML TO YOUR SERVER FOLDER");
	    		return;
	    	}
	    
	    	serverConfig = new JsonParser().parse(new JsonReader(new FileReader(config))).getAsJsonObject();
	    	
	    	JsonObject server = serverConfig.getAsJsonObject("server");
	    	serverName = server.get("name").getAsString();
	    	serverType = server.get("type").getAsString().toLowerCase();
	    	maxPlayers = server.get("maxPlayers").getAsInt();
	    	
	    	JsonObject minigame = serverConfig.getAsJsonObject("minigame");
	    	isMinigame = minigame.get("isMinigame").getAsBoolean();
	    } catch (Exception e) {
    		System.out.println("[LythrionBungee] SOMETHING WENT WRONG WITH LOADING THE SERVER PROPERTIES");
	    	e.printStackTrace();
    		return;
	    }
	}
	
	public void setGameStatus(GameStatus gameStatus) {
		this.gameStatus = gameStatus;
		
		System.out.println("[LythrionBungee] Changed the gamestatus of the server " + serverName + " to " + gameStatus.name());
	}
	
	public void setStatus(ServerStatus status) {
		this.status = status;
		
		if(status == ServerStatus.ONLINE) LythrionBungee.addAvailableServer(this);
		else LythrionBungee.removeAvailableServer(this);
	}

	public String getName() {
		return serverInfo.getName();
	}

	@Override
	public int compareTo(LythrionServerInfo o) {
		return serverInfo.getName().compareTo(o.serverInfo.getName());
	}
}
