package net.planckton.bungee;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.planckton.blood.ServerStatus;
import net.planckton.blood.packet.PacketPlayerBungeeJoin;
import net.planckton.blood.packet.PacketPlayerBungeeQuit;
import net.planckton.bungee.db.Ban;
import net.planckton.bungee.db.Database;
import net.planckton.bungee.db.Database.AppealStatus;
import net.planckton.bungee.db.Database.WhitelistInfo;
import net.planckton.bungee.db.Offense;
import net.planckton.bungee.util.ForcedHost;
import net.planckton.bungee.util.LythrionServerInfo;
import net.planckton.bungee.util.Rank;
import net.planckton.bungee.util.proxy.ProxyCheckResult;
import net.planckton.bungee.util.proxy.ProxyDetection;

public class LythrionBungeeListener implements Listener {
	private static final TextComponent serverFullMessage = new TextComponent(
			"Server is full :(");
	private static final String serverErrorDatabase = ChatColor.GOLD + "Tosti" + ChatColor.BOLD + "MC\n" + ChatColor.RED + "Not connected to the database. Please contact staff.";
	private static final String noProxyString = ChatColor.GOLD
			+ "Tosti"
			+ ChatColor.BOLD
			+ "MC\n"
			+ ChatColor.RED
			+ "Proxy/VPN services are not allowed on TostiMC to prevent ban-evading. \nIf you believe this is an error, contact " + ChatColor.GOLD + "support@tostimc.net";
	private static final BaseComponent[] noProxyMessage = TextComponent
			.fromLegacyText(noProxyString);
	private static final String offlineOffenseString = ChatColor.GOLD + "Tosti"
			+ ChatColor.BOLD + "MC" + ChatColor.RESET + ChatColor.RED
			+ " Warning!\n" + ChatColor.GRAY
			+ " While you were offline you recieved an " + ChatColor.RED
			+ "offense" + ChatColor.GRAY + " for\n";
	private static final String offlineOffensesString = ChatColor.GOLD
			+ "Tosti" + ChatColor.BOLD + "MC" + ChatColor.RESET + ChatColor.RED
			+ " Warning!\n" + ChatColor.GRAY
			+ " While you were offline you recieved " + ChatColor.RED
			+ "offenses" + ChatColor.GRAY + " for\n";
	private static final String ipBanString = ChatColor.GOLD
			+ "Tosti"
			+ ChatColor.BOLD
			+ "MC\n"
			+ ChatColor.RED
			+ "Due to too many bans on the IP address you are currently on\nwe can no longer allow this IP to play on TostiMC.\nFor info contact support@tostimc.net\n\nIP ban caused by the following accounts: ";
	
	private static TextComponent reconnectMessage;
	
	{
		reconnectMessage = new TextComponent("Relogged from another location.");
		reconnectMessage.setColor(ChatColor.RED);
	}
	
	private static final Joiner JOINER = Joiner.on(", ");
	
	private static final HashMap<UUID, Long> lastLogin = new HashMap<UUID, Long>(); 
	
	public LythrionBungeeListener() {
		serverFullMessage.setColor(ChatColor.RED);
	}

	@EventHandler
	public void onPreLogin(PreLoginEvent event) {
		if(!Database.isConnected()) {
			event.setCancelled(true);
			event.setCancelReason(serverErrorDatabase);
			return;
		}
	}
	
	@EventHandler
	public void onLogin(final LoginEvent event) {
		if (Database.getConnectionPoolRead() == null
				|| Database.getConnectionPoolWrite() == null) {
			event.setCancelled(true);
			event.setCancelReason("Database down, Please contact a moderator.");
			return;
		}

		final PendingConnection connection = event.getConnection();
		final UUID id = connection.getUniqueId();
		
		try {
			Long lastLoginTime = lastLogin.remove(id);
			lastLogin.put(id, System.currentTimeMillis());
			
			if(lastLoginTime != null) {
				long timeBetweenLogin = System.currentTimeMillis() - lastLoginTime;
				if(timeBetweenLogin < 1_000) {
					event.setCancelled(true);
					event.setCancelReason(ChatColor.RED + "Please wait a while between logins");
					return;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error while checking login time spam.");
		}
		
		LythrionBungee.getInstance().getProxy();
		ProxiedPlayer otherPlayer = ProxyServer.getInstance().getPlayer(id);
		if(otherPlayer != null) {
			otherPlayer.disconnect(reconnectMessage);
			
			event.setCancelled(true);
			event.setCancelReason(ChatColor.RED + "Player was already logged in but has been kicked.\nYou can now relog.");
			return;
		}
		
		boolean isNew = !Database.playerExists(id, connection.getName());

		if (isNew) {
			Database.createPlayer(id, connection.getName());
			if(!checkForProxy(event)) return;
		} else {
			if(!checkForProxy(event)) return;

			if(activateOffenses(event)) return;

			if(checkForBan(event)) {
				Database.addRegisterToken(event.getConnection().getUniqueId(), event.getConnection().getAddress().getAddress().getHostAddress());
				
				return;
			}
		}
		
		if (isIpBanned(event)) return;
	}

	private boolean isIpBanned(LoginEvent event) {
		List<String> banned = Database.isIpBanned(event.getConnection().getAddress()
				.getAddress().getHostAddress());

		if (banned != null && banned.size() >= 2) {
			event.setCancelled(true);
			event.setCancelReason(ipBanString + JOINER.join(banned));
			
			return true;
		}

		return false;
	}
	
	private boolean activateOffenses(LoginEvent event) {
		List<Offense> activatedOffenses = Database.activateOffenses(event
				.getConnection().getUniqueId());

		if (activatedOffenses != null) {
			StringBuilder message = new StringBuilder(
					activatedOffenses.size() > 1 ? offlineOffensesString
							: offlineOffenseString);

			int maxBanLength = 0;
			int maxMuteLength = 0;

			boolean first = true;
			for (Offense offense : activatedOffenses) {
				int banLength = offense.getBanLength();
				int muteLength = offense.getMuteLength();

				if (banLength < 0) {
					maxBanLength = -1;
				} else if (banLength > maxBanLength)
					maxBanLength = banLength;

				if (muteLength < 0) {
					maxMuteLength = -1;
				} else if (muteLength > maxMuteLength)
					maxMuteLength = muteLength;

				if (!first) {
					message.append(ChatColor.GRAY + ", ");
				}
				first = false;

				message.append(ChatColor.RED + offense.getType().toLowerCase());
			}

			message.append(ChatColor.GRAY + " and were sanctioned to\na ");

			boolean hasBan = false;
			if (maxBanLength != 0) {
				if (maxBanLength < 0)
					message.append(ChatColor.RED + "Perm ban");
				else
					message.append(ChatColor.RED + timeToString(maxBanLength)
							+ " ban");

				hasBan = true;
			}

			if (maxMuteLength != 0 && maxBanLength >= 0) {
				if (hasBan)
					message.append(ChatColor.GRAY + " & ");

				if (maxMuteLength < 0)
					message.append(ChatColor.RED + "Perm mute");
				else
					message.append(ChatColor.RED + timeToString(maxMuteLength)
							+ " mute");
			}

			if (maxBanLength == 0) {
				message.append(ChatColor.GRAY + "\n\nRelog again to play.");
			}

			event.setCancelled(true);
			event.setCancelReason(message.toString());
			return true;
		}

		return false;
	}

	private String timeToString(int timeInMinutes) {
		int weeks = 0;
		int days = 0;
		int hours = 0;
		int mins = 0;

		int lengthLeft = timeInMinutes;

		if (lengthLeft > 0) {
			weeks = lengthLeft / (60 * 24 * 7);
			lengthLeft -= weeks * 60 * 24 * 7;
		}

		if (lengthLeft > 0) {
			days = lengthLeft / (60 * 24);
			lengthLeft -= days * 60 * 24;
		}

		if (lengthLeft > 0) {
			hours = lengthLeft / (60);
			lengthLeft -= hours * 60;
		}

		if (lengthLeft > 0) {
			mins = lengthLeft;
		}

		StringBuilder banString = new StringBuilder();

		if (weeks > 0)
			banString.append(weeks + " week" + (weeks > 1 ? "s" : ""));

		if (days > 0) {
			if (banString.length() > 0)
				banString.append(" ");
			banString.append(days + " day" + (days > 1 ? "s" : ""));
		}

		if (hours > 0) {
			if (banString.length() > 0)
				banString.append(" ");
			banString.append(hours + " hour" + (hours > 1 ? "s" : ""));
		}

		if (mins > 0) {
			if (banString.length() > 0)
				banString.append(" ");
			banString.append(mins + " min" + (mins > 1 ? "s" : ""));
		}

		return banString.toString();
	}

	private boolean checkForBan(LoginEvent event) {
		PendingConnection connection = event.getConnection();
		UUID id = connection.getUniqueId();
		
		// check ban
		Ban ban = Database.getBan(id);

		if (ban == null)
			return false;
		event.setCancelled(true);

		String banString = ChatColor.RED + "Banned" + ChatColor.GRAY + " from "
				+ ChatColor.GOLD + "Tosti" + ChatColor.BOLD + "MC\n";

		if (ban.getExpires() == null) {
			banString += ChatColor.RESET + "" + ChatColor.GRAY
					+ " Permanently banned due to " + ChatColor.RED
					+ ban.getOffense().toLowerCase();

			AppealStatus denied = Database.isApplealDenied(ban.getOffenseId());
			if (denied != null) {
				if (denied == AppealStatus.DENIED) {
					banString += "\n\n"
							+ ChatColor.GRAY
							+ "Your appeal has been " + ChatColor.RED + "denied\n"
							+ ChatColor.GRAY + "If you believe this is a bug contact " + ChatColor.GOLD + "support@tostimc.net\n"
							+ ChatColor.DARK_GRAY + "Mails regarding this ban will be ignored.";
				} else if (denied == AppealStatus.ACCEPTED) {
					banString += "\n\n"
							+ ChatColor.GRAY
							+ "Your appeal has been " + ChatColor.GREEN + "accepted\n"
							+ ChatColor.GRAY + "You can find your unban date at " + ChatColor.GOLD + "tostimc.net/offense/" + ban.getOffenseId();
				} else {
					banString += "\n\n" + ChatColor.GRAY
							+ "Your appeal is " + ChatColor.YELLOW + "pending" + ChatColor.GRAY + ".\nFore more info check "
							+ ChatColor.GOLD + "tostimc.net/offense/" + ban.getOffenseId();
				}
			} else {
				Database.insertAppeal(ban.getOffenseId(), connection
						.getAddress().getAddress().getHostAddress());
				banString += "\n\n" + ChatColor.GRAY + "You can now appeal at "
						+ ChatColor.GOLD + "tostimc.net/offense/" + ban.getOffenseId();
			}
		} else {
			DateFormat df = new SimpleDateFormat("dd-MM-/yy HH:mm");
			banString += ChatColor.RESET + "" + ChatColor.GRAY
					+ " Banned until " + ChatColor.RED
					+ df.format(ban.getExpires()) + "\n";
			banString += ChatColor.RESET + "" + ChatColor.GRAY + " Due to "
					+ ChatColor.RED + ban.getOffense().toLowerCase();
			banString += "\n\n" + ChatColor.GRAY + "For more info check "
					+ ChatColor.GOLD + "tostimc.net/offense/" + ban.getOffenseId();
		}

		event.setCancelReason(banString);
		return true;
	}

	private boolean checkForProxy(final LoginEvent event) {
		final PendingConnection connection = event.getConnection();
		final UUID id = connection.getUniqueId();

		final String ip = connection.getAddress().getAddress().getHostAddress();
		// check if whitelisted
		WhitelistInfo info = Database.isIpWhitelisted(id, ip);
		if (info != null) {
			if (!info.isAllowed()) {
				event.setCancelled(true);
				event.setCancelReason(noProxyString);
				return false;
			} else if (!info.isProxy()) {
				ProxyServer.getInstance().getScheduler()
						.runAsync(LythrionBungee.getInstance(), new Runnable() {

							@Override
							public void run() {
								ProxyCheckResult proxyCheck = ProxyDetection.isProxy(ip);
								if (!proxyCheck.isProxy()) return;

								Database.addWhitelist(id, ip, proxyCheck);
								connection.disconnect(noProxyMessage);
							}
						});
			}
			// Do an async check if not an proxy
		}
		// do first async proxy check
		else {
			ProxyServer.getInstance().getScheduler()
					.runAsync(LythrionBungee.getInstance(), new Runnable() {

						@Override
						public void run() {
							// if proxy kick and blacklist
							ProxyCheckResult proxyCheck = ProxyDetection.isProxy(ip);
							if (proxyCheck.isProxy()) {
								Database.addWhitelist(id, ip, proxyCheck);
								connection.disconnect(noProxyMessage);
								//proxyCheck
								return;
							}
							// if not whitelist and continue
							else {
								Database.addWhitelist(id, ip, proxyCheck);
							}
						}
					});
		}
		
		return true;
	}

	@EventHandler
	public void onPluginMessage(PluginMessageEvent event) {
		if (!(event.getSender() instanceof Server))
			return;

		if (!event.getTag().equals("BungeeCord"))
			return;

		ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
		String message = in.readUTF();

		if (message.equals("SETRANK")) {
			Rank rank = Rank.byName(in.readUTF());

			if (rank == null)
				return;
			ProxiedPlayer player = (ProxiedPlayer) event.getReceiver();

			LythrionBungee.ranks.put(player.getUniqueId(), rank);
		}
		else if (message.equals("BalanceConnect")) {
			String[] parts = in.readUTF().split(":");
			String serverType = parts[1];
			
			LythrionServerInfo server;
			if(parts.length > 2) {
				String random = parts[2];
				
				if(random.matches("[0-9]{0,}")) {
					server = findServer(serverType, Integer.parseInt(random));
				}
				else server = findServer(serverType, new Random(random.hashCode()));
			}
			else server = findServer(serverType);
			
			ProxiedPlayer player = (ProxiedPlayer) event.getReceiver();
			
			if(server == null) {
				player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Can't find any server of type " + serverType));
				return;
			}
			
			if(server.getStatus() != ServerStatus.ONLINE) {
				System.out.println("Wtf is wrong with this. " + server.getName() + " " + server.getServerType() + " " + server.getStatus());
			}
			
			player.connect(server.getServerInfo());
		}
	}
	
	@EventHandler
	public void onPostLogin(final PostLoginEvent event) {
		ProxiedPlayer player = event.getPlayer();
		UUID id = player.getUniqueId();

		Database.logLogin(id, player.getPendingConnection().getAddress()
				.getAddress().getHostAddress());
		
		LythrionBungee.getBootstrapper().getChannel()
				.writeAndFlush(new PacketPlayerBungeeJoin(id));

		LythrionBungee.ranks.put(id, Rank.fromString(Database.getRank(id)));
	}

	@EventHandler
	public void onDisconnect(PlayerDisconnectEvent event) {
		ProxiedPlayer player = event.getPlayer();
		UUID id = player.getUniqueId();

		Database.logLogout(id);

		LythrionBungee.getBootstrapper().getChannel()
			.writeAndFlush(new PacketPlayerBungeeQuit(id));
		
		LythrionBungee.ranks.remove(id);
		Database.disableNickName(id);
		
		lastLogin.remove(id);
	}
	
	private static final Random random = new Random();

	@EventHandler
	public void onProxyPing(ProxyPingEvent event) {
		if(!Database.isConnected()) {
			
		}
		else {
			ServerPing ping = event.getResponse();

			ForcedHost server = getForcedHost(event.getConnection());
			if (server != null && server.isForcedMotd()) {
				String motd = server.getMotd();
				ping.setDescriptionComponent(new TextComponent(motd.substring(1, motd.length())));
				return;
			}

			ping.setDescriptionComponent(new TextComponent(LythrionBungee.motds.get(random.nextInt(LythrionBungee.motds
					.size()))));
			ping.getPlayers().setOnline(LythrionBungee.getTotalOnlineCount());
			ping.getPlayers().setMax(500);
		}
	}

	private static HashMap<String, ForcedHost> forcedHosts = new HashMap<>();

	public static ForcedHost getForcedHost(PendingConnection con) {
		if (con.getVirtualHost() == null) {
			return null;
		}

		String forced = con.getListener().getForcedHosts().get(con.getVirtualHost().getHostString());
		if (forced == null)
			return null;
		ServerInfo info = ProxyServer.getInstance().getServerInfo(forced);

		ForcedHost host = forcedHosts.get(forced);
		if (host == null) {
			host = new ForcedHost(info.getName());
			forcedHosts.put(forced, host);
			return null;
		}

		return host;
	}
	
	@EventHandler
	public void onServerKick(ServerKickEvent event) {
		String message = ChatColor.stripColor(BaseComponent.toLegacyText(event.getKickReasonComponent()));
		if(message.startsWith("S:")) {
			String serverName = message.replace("S:", "");
			ServerInfo server = ProxyServer.getInstance().getServerInfo(serverName);
			
			if(server == null) {
				server = findServer("HUB").getServerInfo();
				
				
				if(server == null) {
					event.setKickReasonComponent(TextComponent.fromLegacyText(ChatColor.RED + "Something went wrong while switching from '" + event.getKickedFrom().getName() + "' to '" + serverName + "'. \nPlease contact staff."));
					return;
				}
				
				event.getPlayer().sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You could not be connected to " + serverName + "."));
				event.getPlayer().sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Connecting to " + server.getName() + "."));
			}
			
			event.setCancelled(true);
			event.setCancelServer(server);
		}
		else if(message.startsWith("C:")) {
			String[] parts = message.split(":");
			String serverType = parts[1];
			
			LythrionServerInfo server;
			if(parts.length > 2) {
				String random = parts[2];
				
				if(random.matches("[0-9]{0,}")) {
					server = findServer(serverType, Integer.parseInt(random));
				}
				else server = findServer(serverType, new Random(random.hashCode()));
			}
			else server = findServer(serverType);
			
			if(server == null) {
				server = findServer("HUB");
				
				if(server == null) {
					event.setKickReasonComponent(TextComponent.fromLegacyText(ChatColor.RED + "Something went wrong while switching from '" + event.getKickedFrom().getName() + "' to '" + serverType + "'. \nPlease contact staff."));
					return;
				}
				
				event.getPlayer().sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You could not be connected to " + serverType + "."));
				event.getPlayer().sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Connecting to " + server.getName() + "."));
			}
			
			event.setCancelled(true);
			event.setCancelServer(server.getServerInfo());
		}
	}
	
	private LythrionServerInfo findServer(String type) {
		return findServer(type, random);
	}
	
	private LythrionServerInfo findServer(String type, Random random) {
		Collection<LythrionServerInfo> servers = LythrionBungee.getAvailableServersForType(type);
		if(servers == null) return null;
		
		if(servers.size() == 1) return Iterables.getOnlyElement(servers);
		
		LythrionServerInfo info = Iterables.get(servers, random.nextInt(servers.size()));
		
		return info;
	}
	
	private LythrionServerInfo findServer(String type, int index) {
		Collection<LythrionServerInfo> servers = LythrionBungee.getAvailableServersForType(type);
		if(servers == null) return null;
		
		if(servers.size() == 1) return Iterables.getOnlyElement(servers);
		
		LythrionServerInfo info = Iterables.get(servers, index % servers.size());
		
		return info;
	}
}