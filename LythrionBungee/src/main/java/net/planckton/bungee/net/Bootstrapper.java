package net.planckton.bungee.net;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.File;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.planckton.blood.packet.PacketDecoder;
import net.planckton.blood.packet.PacketEncoder;
import net.planckton.blood.packet.PacketHandshake;
import net.planckton.bungee.LythrionBungee;

import com.google.common.collect.Iterables;

public class Bootstrapper implements Runnable {
	private String host;
	private int port;
	
	@Getter private Bootstrap bootstrap;

	@Getter private String serverName;
	@Getter private String serverType;

	@Getter private EventLoopGroup workerGroup;
	@Getter private Channel channel;
	
	private HeartConnectionHandler serverHandler;

	public Bootstrapper(String host, int port) {
		this.host = host;
		this.port = port;
		
		serverName = new File("").getAbsoluteFile().getName();
		serverType = serverName.replaceAll("[0-9]{1,}", "");

		workerGroup = new NioEventLoopGroup();

		bootstrap = configureBootstrap(new Bootstrap(), workerGroup);
		
		serverHandler = new HeartConnectionHandler(this);
	}
	
	public void connect() {
		System.out.println("[Bootstrapper] Attempting first connect.");
		
		ChannelFuture f;
		try {
			f = bootstrap.connect().sync();
		} catch (Exception e) {
			if(e.getMessage().startsWith("Connection refused")) {
				System.out.println("[Bootstrapper] First Connect failed: Connection refused. ");
			}
			else {
				System.out.println("[Bootstrapper] First Connect failed.");
				e.printStackTrace();
			}
			
			ProxyServer.getInstance().getScheduler().schedule(LythrionBungee.getInstance(), this, 5, TimeUnit.SECONDS);
			return;
		}
		
		channel = f.channel();
		
		sendInitPacket();
		
		System.out.println("[Bootstrapper] Connect was succesfull");
	}
	
	@SuppressWarnings("deprecation")
	private void sendInitPacket() {
		channel.writeAndFlush(
				new PacketHandshake(PacketHandshake.InstanceType.BUNGEE,
						serverName, "bungee", false, Iterables
						.get(ProxyServer.getInstance()
								.getConfig().getListeners(), 0)
						.getHost().getPort()));
	}

	@Override
	public void run() {
		connect();
	}

	public Bootstrap configureBootstrap(Bootstrap b, EventLoopGroup g) {
		b.group(g);
		b.channel(NioSocketChannel.class);
		b.option(ChannelOption.SO_KEEPALIVE, true);
		b.remoteAddress(host, port);
		b.handler(new ChannelInitializer<SocketChannel>() {
			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ch.pipeline().addLast(new PacketDecoder(), new PacketEncoder(),
						serverHandler);
			}
		});

		return b;
	}
	
	public void reconnect() {
		bootstrap = new Bootstrap();
		configureBootstrap(bootstrap, workerGroup);
		
		System.out.println("[Bootstrapper] Re-connecting...");
		bootstrap.connect().addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future)
					throws Exception {
				if (future.cause() != null) {
					if(future.cause().getMessage().startsWith("Connection refused")) System.out.println("[Bootstrapper] Failed to re-connect: Connection Refused");
					else System.out.println("[Bootstrapper] Failed to re-connect: " + future.cause());
					return;
				}
				
				channel = future.channel();
				
				sendInitPacket();
			}
		});
    }

	public void stop() {
		workerGroup.shutdownGracefully();
	}

	public boolean isConnected() {
		return channel.isOpen();
	}
}
