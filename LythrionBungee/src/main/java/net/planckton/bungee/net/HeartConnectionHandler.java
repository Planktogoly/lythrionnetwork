													package net.planckton.bungee.net;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoop;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Iterables;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.protocol.packet.TabCompleteResponse;
import net.planckton.blood.GameStatus;
import net.planckton.blood.ServerStatus;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketGameStatusUpdate;
import net.planckton.blood.packet.PacketInfoUpdate;
import net.planckton.blood.packet.PacketPlayerAdvancedMessage;
import net.planckton.blood.packet.PacketPlayerBalanceConnect;
import net.planckton.blood.packet.PacketPlayerBungeeJoin;
import net.planckton.blood.packet.PacketPlayerCommand;
import net.planckton.blood.packet.PacketPlayerConnect;
import net.planckton.blood.packet.PacketPlayerJoinMinigame;
import net.planckton.blood.packet.PacketPlayerMessage;
import net.planckton.blood.packet.PacketPlayerTabComplete;
import net.planckton.blood.packet.PacketRegisterCommand;
import net.planckton.blood.packet.PacketRegisterServer;
import net.planckton.blood.packet.PacketServerUpdate;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.util.LythrionServerInfo;

@Sharable
public class HeartConnectionHandler extends ChannelInboundHandlerAdapter implements Listener {
	private static final ProxyServer CORD = ProxyServer.getInstance();
	private HashSet<String> registeredCommands = new HashSet<>();
	private Bootstrapper bootstrapper;
	
	private static final Random random = new Random();
	
	public HeartConnectionHandler(Bootstrapper bootstrapper) {
		this.bootstrapper = bootstrapper;
		
		CORD.getPluginManager().registerListener(LythrionBungee.getInstance(), this);
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Packet packet = (Packet) msg;
		
		if(LythrionBungee.isDevServer()) System.out.println("[LythrionBungee] Recieved packet " + packet.getPacketId());
		
		LythrionServerInfo server;
		ProxiedPlayer player;
		
		switch (packet.getPacketType()) {
		case HANDSHAKE:
			System.out.println("[LythrionBungee] Succesfully identified ourself.");
			
			for(ProxiedPlayer players : CORD.getPlayers()) {
				ctx.writeAndFlush(new PacketPlayerBungeeJoin(players.getUniqueId()));
			}
			return;
		case PLAYER_ADVANCEDMESSAGE: 
			PacketPlayerAdvancedMessage	advancedMessage = (PacketPlayerAdvancedMessage) packet;
			
			player = CORD.getPlayer(advancedMessage.getUuid()); 
			
			if (player == null) {
				System.out.println("Player was null when message received");
				return;
			}
			
			player.sendMessage(ComponentSerializer.parse(advancedMessage.getAdvancedMessage()));
			return;
		case PLAYER_MESSAGE:
			PacketPlayerMessage message = (PacketPlayerMessage) packet;
			
			player = CORD.getPlayer(message.getUuid());
			
			if(player == null) {
				System.out.println("Null player message recieved");
				return;
			}
			
			player.sendMessage(TextComponent.fromLegacyText(message.getMessage()));
			return;
		case PLAYER_TAB_COMPLETE:
			PacketPlayerTabComplete tabComplete = (PacketPlayerTabComplete) packet;
			
			ProxiedPlayer tabPlayer = CORD.getPlayer(tabComplete.getUuid());
			
			if(tabPlayer == null) {
				System.out.println("Null player tab complete recieved");
				return;
			}
			
			tabPlayer.unsafe().sendPacket(new TabCompleteResponse(tabComplete.getSuggestions()));
			return;
		case PLAYER_CONNECT:
			PacketPlayerConnect playerConnect = (PacketPlayerConnect) packet;

			String serverName = playerConnect.getServer();
			
			server = findServer(serverName);
			
			player = CORD.getPlayer(playerConnect.getUuid());
			
			if(server == null) {
				player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Can't find any server of type " + serverName));
				return;
			}
			
			if(server.getStatus() != ServerStatus.ONLINE) {
				System.out.println("Wtf is wrong with this. " + server.getName() + " " + server.getServerType() + " " + server.getStatus());
			}
			
			player.connect(server.getServerInfo());
			return;
		case PLAYER_BALANCE_CONNECT:
			PacketPlayerBalanceConnect playerBalanceConnect = (PacketPlayerBalanceConnect) packet;
			
			String serverType = playerBalanceConnect.getServerType();
			String identifier = playerBalanceConnect.getIdentifier();
			
			if (identifier == null) server = findServer(serverType, new Random(random.hashCode()));
			
			if(identifier.matches("[0-9]{0,}")) server = findServer(serverType, Integer.parseInt(identifier));
			else server = findServer(serverType);
			
			player = CORD.getPlayer(playerBalanceConnect.getUuid());
			
			if(server == null) {
				player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Can't find any server of type " + serverType));
				return;
			}
			
			if(server.getStatus() != ServerStatus.ONLINE) {
				System.out.println("Wtf is wrong with this. " + server.getName() + " " + server.getServerType() + " " + server.getStatus());
			}
			
			player.connect(server.getServerInfo());
			return;
		case PLAYER_MINIGAME_CONNECT:
			PacketPlayerJoinMinigame playerMinigameConnect = (PacketPlayerJoinMinigame) packet;
			
			String minigame = playerMinigameConnect.getServer();
			player = CORD.getPlayer(playerMinigameConnect.getUuid());
			
			server = findMinigameServer(minigame);
			
			if (server == null) {
				player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "There are currently no available servers."));
				return;
			}
			
			player.connect(server.getServerInfo());
			return;
		case REGISTER_COMMAND:
			PacketRegisterCommand registerCommand = (PacketRegisterCommand) packet;
			
			System.out.println("[Brain] registered command " + registerCommand.getCommand());	
			registeredCommands.add(registerCommand.getCommand().toLowerCase());
			return;
		case REGISTER_SERVER:
			PacketRegisterServer registerServer = (PacketRegisterServer) packet;
			String newServerName = registerServer.getServerName();
			String ip = registerServer.getIp();
			int port = registerServer.getPort();
			
			System.out.println("[Brain] registered server " + newServerName + " at " + ip + ":" + port);	
			ServerInfo newInfo = CORD.constructServerInfo(newServerName, new InetSocketAddress(ip, port), "", false);
			
			LythrionBungee.registerServer(newInfo, registerServer.getStatus());
			CORD.getServers().put(newInfo.getName(), newInfo);
			return;
		case SERVER_UPDATE:
			PacketServerUpdate serverUpdate = (PacketServerUpdate) packet;
			
			LythrionServerInfo serverToUpdate = LythrionBungee.getServer(serverUpdate.getServer());
			if(serverToUpdate == null) {
				System.out.println("[Brain] ServerUpdate recieved but server " + serverUpdate.getServer() + " unkown");
				return;
			}
			
			serverToUpdate.setStatus(serverUpdate.getStatus());
			System.out.println("[Brain] Server status for " + serverToUpdate.getName() + " changed to: " + serverUpdate.getStatus());			
			return;
		case GAMESTATUS_UPDATE:
			PacketGameStatusUpdate statusUpdate = (PacketGameStatusUpdate) packet;
			
			LythrionServerInfo gameServerToUpdate = LythrionBungee.getServer(statusUpdate.getServer());
			if(gameServerToUpdate == null) {
				System.out.println("[Brain] gameServerUpdate recieved but server " + statusUpdate.getServer() + " unkown");
				return;
			}
			
			gameServerToUpdate.setGameStatus(statusUpdate.getStatus());
			return;
		case INFO_UPDATE:
			PacketInfoUpdate serverInfoUpdate = (PacketInfoUpdate) packet;
			
			String key = serverInfoUpdate.getKey();
			if(!key.equalsIgnoreCase("totalonline")) return;
			
			LythrionBungee.setTotalOnlineCount(Integer.parseInt(serverInfoUpdate.getInfo()));
			return;
		}
	}
	
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
    
    @Override
	public void channelInactive(final ChannelHandlerContext ctx) {
		System.out.println("Disconnected from: "
				+ ctx.channel().remoteAddress());
	}

	@Override
	public void channelUnregistered(final ChannelHandlerContext ctx) throws Exception {
    	System.out.println("[Brain] Disconnected :(");
    	
    	final EventLoop loop = ctx.channel().eventLoop();
	    	loop.schedule(new Runnable() {
		    	@Override
		    	public void run() {
		    	if(bootstrapper.isConnected()) return;
		    	bootstrapper.reconnect();
	    	}
    	}, 5, TimeUnit.SECONDS);
	}
    
	@EventHandler
	public void onCommand(ChatEvent event) {
		if(!event.isCommand()) return;
		 
		String message = event.getMessage();
		
		String[] parts = message.split(" ");
		parts[0] = parts[0].replaceFirst("/([a-zA-Z]{0,}:)?", "");
		
		if(!registeredCommands.contains(parts[0].toLowerCase())) return;
		event.setCancelled(true);
		
		bootstrapper.getChannel().writeAndFlush(new PacketPlayerCommand(((ProxiedPlayer)event.getSender()).getUniqueId(), message));
	 }
	
	@EventHandler
	public void onTabComplete(TabCompleteEvent event) {
		String message = event.getCursor();
		if(!message.startsWith("/")) return;
		
		String[] parts = message.split(" ");
		parts[0] = parts[0].replaceFirst("/([a-zA-Z]{0,}:)?", "");
		
		if(!registeredCommands.contains(parts[0].toLowerCase())) return;
		event.setCancelled(true);
		
		bootstrapper.getChannel().writeAndFlush(new PacketPlayerTabComplete(((ProxiedPlayer)event.getSender()).getUniqueId(), message));
	}
	
	private LythrionServerInfo findServer(String type) {
		return findServer(type, random);
	}
	
	private LythrionServerInfo findServer(String type, Random random) {
		Collection<LythrionServerInfo> servers = LythrionBungee.getAvailableServersForType(type.toLowerCase());
		if(servers == null) return null;
		
		if(servers.size() == 1)	return Iterables.getOnlyElement(servers);
		
		LythrionServerInfo info = Iterables.get(servers, random.nextInt(servers.size()));
		
		return info;
	}
	
	private LythrionServerInfo findServer(String type, int index) {
		Collection<LythrionServerInfo> servers = LythrionBungee.getAvailableServersForType(type.toLowerCase());
		if(servers == null) return null;
		
		if(servers.size() == 1) return Iterables.getOnlyElement(servers);
		
		LythrionServerInfo info = Iterables.get(servers, index % servers.size());
		
		return info;
	}
	
	private LythrionServerInfo findMinigameServer(String type) {
		ArrayList<LythrionServerInfo> servers = new ArrayList<>(LythrionBungee.getAvailableServersForType(type.toLowerCase()));
		if (servers.size() == 0) return null;
		
		Collections.sort(servers, new Comparator<LythrionServerInfo>() {

			@Override
			public int compare(LythrionServerInfo o1, LythrionServerInfo o2) {
				return o2.getServerInfo().getPlayers().size() - o1.getServerInfo().getPlayers().size();
			}
		});
		
		for (LythrionServerInfo server : servers) {
    		if (!server.isMinigame()) continue;
			
			if (!(server.getGameStatus() == GameStatus.LOBBY)) continue;
			if (!(server.getMaxPlayers() - server.getServerInfo().getPlayers().size() >= 1)) continue;
			return server;
		}
		
		return null;
	}
}
