package net.planckton.bungee.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.util.Rank;

public class ConnectCommand extends Command {
	private static final BaseComponent[] usageMessage = TextComponent.fromLegacyText(ChatColor.RED + "Usage: /connect <server> [group]");

	public ConnectCommand() {
		super("connect", null, "bconnect");
	}

	@Override
	public void execute(CommandSender cs, String[] arg) {
		if(!(cs instanceof ProxiedPlayer)) return;
		ProxiedPlayer player = (ProxiedPlayer) cs;
		
		Rank rank = LythrionBungee.ranks.get(player.getUniqueId());
		if(!rank.hasPower(Rank.OWNER)) return;
		
		if(arg.length == 0) {
			player.sendMessage(usageMessage);
			return;
		}
		
		StringBuilder builder = new StringBuilder();
		//add server
		builder.append("C:");
		builder.append(arg[0]);
		if(arg.length > 1) {
			builder.append(":");
			builder.append(arg[1]);
		}
		
		player.disconnect(TextComponent.fromLegacyText(builder.toString()));
	}
}
