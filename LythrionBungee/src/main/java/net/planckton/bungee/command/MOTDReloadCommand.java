package net.planckton.bungee.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.db.Database;
import net.planckton.bungee.util.Rank;

public class MOTDReloadCommand extends Command{

	public MOTDReloadCommand() {
		super("motdreload");
	}

	@Override
	public void execute(CommandSender cs, String[] arg) {
		if(cs instanceof ProxiedPlayer) {
			Rank rank = LythrionBungee.ranks.get(((ProxiedPlayer)cs).getUniqueId());
			if(!rank.hasPower(Rank.OWNER)) return;
		}
		
		TextComponent message = new TextComponent("Reloading MOTD's");
		message.setColor(ChatColor.GREEN);
		
		cs.sendMessage(message);
		
		Database.loadMotds();
	}

}
