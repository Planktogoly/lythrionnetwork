package net.planckton.bungee.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.util.Rank;
import net.planckton.bungee.util.proxy.ProxyDetection;

public class ProxyReloadCommand extends Command{

	public ProxyReloadCommand() {
		super("proxyreload");
	}

	@Override
	public void execute(CommandSender cs, String[] arg) {
		if(cs instanceof ProxiedPlayer) {
			Rank rank = LythrionBungee.ranks.get(((ProxiedPlayer)cs).getUniqueId());
			if(!rank.hasPower(Rank.OWNER)) return;
		}
		
		TextComponent message = new TextComponent("Reloading proxies");
		message.setColor(ChatColor.GREEN);
		
		cs.sendMessage(message);
		
		ProxyDetection.loadData();
	}

}
