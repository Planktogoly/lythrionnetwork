package net.planckton.bungee.command;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.base.Joiner;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.util.LythrionServerInfo;
import net.planckton.bungee.util.Rank;

public class ServersCommand extends Command {

	public ServersCommand() {
		super("servers", null, "bservers");
	}

	@Override
	public void execute(CommandSender cs, String[] arg) {
		if(cs instanceof ProxiedPlayer) {
			Rank rank = LythrionBungee.ranks.get(((ProxiedPlayer)cs).getUniqueId());
			if(!rank.hasPower(Rank.OWNER)) return;
		}
		
		cs.sendMessage(TextComponent.fromLegacyText(ChatColor.GOLD + "All servers:"));
		
		int counter = 0;
		for(LythrionServerInfo server : LythrionBungee.getServers()) {
			BaseComponent[] messageEntry = TextComponent.fromLegacyText(ChatColor.GRAY + "#" + ChatColor.GOLD + Integer.toString(++counter) + " " + server.getName() + ChatColor.GRAY + ". status: " + ChatColor.GOLD + server.getStatus());
			
			cs.sendMessage(messageEntry);
		}
		
		cs.sendMessage(TextComponent.fromLegacyText(""));
		cs.sendMessage(TextComponent.fromLegacyText(ChatColor.GOLD + "Available server types: "));
		
		Joiner joiner = Joiner.on(", ");
		
		counter = 0;
		for(Entry<String, Set<LythrionServerInfo>> serverType : LythrionBungee.getAvailableServersPerType().entrySet()) {
			ArrayList<String> names = new ArrayList<>(serverType.getValue().size());
			for(LythrionServerInfo info : serverType.getValue()) names.add(info.getName());
			
			BaseComponent[] messageEntry = TextComponent.fromLegacyText(ChatColor.GRAY + "#" + ChatColor.GOLD + Integer.toString(++counter) + " " + serverType.getKey() + ChatColor.GRAY + ". names: " + ChatColor.GOLD + joiner.join(names)); 
			
			cs.sendMessage(messageEntry);
		}
	}

}
