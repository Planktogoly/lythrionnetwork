package net.planckton.bungee.log;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.util.Rank;

public class PlayerLogRecord {
	public int ownersOnline;
	public int helpersOnline;
	public int buildersOnline;
	public int vipsOnline;
	public int membersOnline;
	
	public boolean fetch() {
		int newOwnersOnline = 0;
		int newHelpersOnline = 0;
		int newBuildersOnline = 0;
		int newVipsOnline = 0;
		int newMembersOnline = 0;
		
		for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			Rank rank = LythrionBungee.ranks.get(player.getUniqueId());
			
			switch(rank) {
			case BUILDER:
				newBuildersOnline++;
				break;
			case MOD:
				newHelpersOnline++;
				break;
			case MEMBER:
				newMembersOnline++;
				break;
			case OWNER:
				newOwnersOnline++;
				break;
			case VIP:
				newVipsOnline++;
				break;
			default:
				break;
			}
		}
		
		if(	ownersOnline == newOwnersOnline &&
			helpersOnline == newHelpersOnline &&
			buildersOnline == newBuildersOnline &&
			vipsOnline == newVipsOnline &&
			membersOnline == newMembersOnline) {			
			return false;
		}
		
		ownersOnline = newOwnersOnline;
		helpersOnline = newHelpersOnline;
		buildersOnline = newBuildersOnline;
		vipsOnline = newVipsOnline;
		membersOnline = newMembersOnline;
		
		return true;
	}
}
