package net.planckton.bungee.log;

import net.planckton.bungee.db.Database;

public class PlayerLogTask implements Runnable {
	private PlayerLogRecord record;
	
	public PlayerLogTask() {
		record = new PlayerLogRecord();
		Database.logPlayersOnline(record);
	}
	
	
	@Override
	public void run() {
		boolean changed = record.fetch();
		
		if(!changed) return;
		
		Database.logPlayersOnline(record);
	}

}
