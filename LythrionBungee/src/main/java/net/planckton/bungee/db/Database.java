package net.planckton.bungee.db;

import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.Future;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import lombok.AllArgsConstructor;
import lombok.Cleanup;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.bungee.LythrionBungee;
import net.planckton.bungee.log.PlayerLogRecord;
import net.planckton.bungee.util.Util;
import net.planckton.bungee.util.proxy.ProxyCheckResult;

import com.google.gson.JsonObject;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class Database {
	private static final boolean USE_DEV_DB_IF_POSSIBLE = true;
	
	private static LythrionBungee lytrhrionBungee;
	
	@Getter private static HikariDataSource connectionPoolRead;
	@Getter private static HikariDataSource connectionPoolWrite;
	@Getter private static DefaultEventExecutorGroup mysqlExecutorPool;
	
	public static boolean openConnection() throws Exception {
		lytrhrionBungee = LythrionBungee.getInstance();
		
		try {
			Runnable runnable = new Runnable() {
				
				@Override
				public void run() {
					synchronized (this) {
						mysqlExecutorPool = new DefaultEventExecutorGroup(4);
						
						notify();
					}
				}
			};
			
			lytrhrionBungee.getProxy().getScheduler().runAsync(lytrhrionBungee, runnable);
			
			synchronized (runnable) {
				runnable.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		
		openConnectionRead();
		openConnectionWrite();
		
		if(isConnected()) {
			System.out.println("[LythrionBungee] Succesfully connected to DB's.");
			return true;
		}
		else {
			System.out.println("[LythrionBungee] Could not connect to the DB's.");
			return false;
		}
	}
	
	private static HikariDataSource openConnectionTo(JsonObject config) {
		Future<HikariDataSource> future = mysqlExecutorPool.submit(new Callable<HikariDataSource>() {

			@Override
			public HikariDataSource call() throws Exception {
				String username = config.get("username").getAsString();
				String password = config.get("password").getAsString();
				String host = config.get("host").getAsString();
				String database = config.get("database").getAsString();
				
				// setup the connection pool
				HikariConfig config = new HikariConfig();
				config.setJdbcUrl("jdbc:mysql://" + host + "/" + database + "?useUnicode=true&characterEncoding=utf-8&useSSL=false"); // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
				config.setUsername(username); 
				config.setPassword(password);
				config.setMaximumPoolSize(10);
				
				try {
					return  new HikariDataSource(config);
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		});
		
		try {
			return future.get();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void openConnectionRead() throws Exception {
		JsonObject config;
		if(LythrionBungee.isDevServer() && USE_DEV_DB_IF_POSSIBLE) config = LythrionBungee.getLythrionConfig().getAsJsonObject("databasedev");
		else config = LythrionBungee.getLythrionConfig().getAsJsonObject("database");
		
		connectionPoolRead = openConnectionTo(config);
	}
	
	public static void openConnectionWrite() throws Exception {
		JsonObject config;
		if(LythrionBungee.isDevServer() && USE_DEV_DB_IF_POSSIBLE) config = LythrionBungee.getLythrionConfig().getAsJsonObject("databasedev");
		else config = LythrionBungee.getLythrionConfig().getAsJsonObject("database");
		
		connectionPoolWrite = openConnectionTo(config);
	}
	
	public static boolean isConnected() {
		if(connectionPoolRead == null) return false;
		if(connectionPoolWrite == null) return false;
		return true;
	}
	
	public static void closeConnection() {
		if(connectionPoolRead != null) connectionPoolRead.close();
		if(connectionPoolWrite != null) connectionPoolWrite.close();
		
		mysqlExecutorPool.shutdownGracefully();
		
		connectionPoolRead = null;
		connectionPoolWrite = null;
		
		mysqlExecutorPool = null;
	}
	
	public static Ban getBan(UUID id) {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup PreparedStatement statement = connectionRead.prepareStatement("SELECT o.id_offense, (sanction_start + INTERVAL o.ban_length MINUTE) AS expires, o.ban_length,ot.name FROM  `offense` o INNER JOIN offense_type ot ON ot.id_offense_type = o.id_offense_type WHERE id_player =? AND id_offense NOT IN (SELECT id_offense FROM offense_deactivated) HAVING expires > NOW( ) OR o.ban_length = -1 ORDER BY o.ban_length ASC , expires DESC LIMIT 0 , 1");
			statement.setString(1, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return null;
			
			int offenseId = rs.getInt("id_offense");
			Date expires = rs.getTimestamp("expires");
			
			if(rs.getInt("ban_length") < 0) expires = null;
			
			Ban ban = new Ban(offenseId, id, expires, rs.getString("name"));
			return ban;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getRank(UUID playerid) {
		String rank = "MEMBER";
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup Statement statement = connectionRead.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT rank FROM player_serverrank WHERE id_player='" + playerid + "'  ORDER BY id_serverrank DESC LIMIT 1");
			
			if (rs.next()) rank = rs.getString("rank");
		} catch (Exception e) {
			e.printStackTrace();
			return rank;
		}
		
		return rank;
	}
	
	public static String getNickName(UUID id) {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup Statement statement = connectionRead.createStatement();
			@Cleanup ResultSet rs = statement.executeQuery("SELECT name FROM player_nick WHERE id_player='" + id + "' AND active=1");
			
			if(!rs.next()) return null;
			
			return rs.getString("name");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean disableNickName(UUID id) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup Statement statement = connectionWrite.createStatement();
			statement.execute("UPDATE player_nick SET active=0 WHERE id_player='" + id + "'");
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean loadMotds() {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup PreparedStatement statement = connectionRead.prepareStatement("SELECT message FROM motd_serverlist WHERE active=1");
			@Cleanup ResultSet rs = statement.executeQuery();
			
			LythrionBungee.motds.clear();
			
			while(rs.next()) {
				LythrionBungee.motds.add(ChatColor.translateAlternateColorCodes('&', rs.getString("message").replace("%n", "\n")));
			}
			
			
			if(LythrionBungee.motds.size() == 0) {
				LythrionBungee.motds.add(ChatColor.translateAlternateColorCodes('&', "&6&lLythrion Network"));
			}			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean logPlayersOnline(PlayerLogRecord record) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement(""
					+ "INSERT INTO log_playersonline(owners_online,helpers_online,builders_online,vips_online,emeralds_online,diamonds_online,members_online) "
					+ "VALUES(?,?,?,?,?,?,?)");
			
			statement.setInt(1, record.ownersOnline);
			statement.setInt(2, record.helpersOnline);
			statement.setInt(3, record.buildersOnline);
			statement.setInt(4, record.vipsOnline);
			statement.setInt(7, record.membersOnline);
			
			statement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createPlayer(UUID id, String name) {
		try {
			System.out.println("[LythrionBungee] Creating player " + name + " with uuid: " + id);
			
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("INSERT INTO player(id_player) VALUES (?)");
			statement.setString(1, id.toString());
			
			statement.execute();
			
			@Cleanup PreparedStatement statementName = connectionWrite.prepareStatement("INSERT INTO player_name(id_player,name) VALUES (?,?)");
			statementName.setString(1, id.toString());
			statementName.setString(2, name);
			
			statementName.execute();
			
			@Cleanup PreparedStatement statementBalance = connectionWrite.prepareStatement("INSERT INTO player_balance(id_player) VALUES (?)");
			statementBalance.setString(1, id.toString());
			
			statementBalance.execute();
			
			@Cleanup PreparedStatement statementCrate = connectionWrite.prepareStatement("INSERT INTO player_crate(id_player) VALUES (?)");
			statementCrate.setString(1, id.toString());
			
			statementCrate.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean logLogin(UUID id, String ip) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("INSERT INTO log_login(id_player,hash_ip) VALUES (?,?)");
			
			statement.setString(1, id.toString());
			statement.setString(2, Util.hash256(ip + "_tmc"));
			
			statement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean logLogout(UUID id) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("UPDATE log_login SET logout_time=NOW(), online_time=TIME_TO_SEC(TIMEDIFF(logout_time,login_time)) WHERE id_player=? AND logout_time IS NULL ORDER BY id_log_login DESC LIMIT 1");
			
			statement.setString(1, id.toString());
			
			statement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@AllArgsConstructor
	public static class WhitelistInfo {
		@Getter UUID id;
		@Getter String ip;
		@Getter boolean allowed;
		@Getter boolean proxy;
	}
	
	public static boolean addWhitelist(UUID id, String ip, ProxyCheckResult result) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("INSERT INTO ip_whitelist(id_player,hash_ip,allowed,proxy,proxy_detection_host) VALUES(?,?,?,?,?)");
			
			statement.setString(1, id.toString());
			statement.setString(2, Util.hash256(ip + "_tmc"));
			statement.setBoolean(3, !result.isProxy());
			statement.setBoolean(4, result.isProxy());
			
			if(result.getKey() != null) {
				statement.setString(5, result.getKey());
			}
			else {
				statement.setNull(5, Types.VARCHAR);
			}
			
			return statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static WhitelistInfo isIpWhitelisted(UUID id, String ip) {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup PreparedStatement statement = connectionRead.prepareStatement("SELECT allowed,proxy FROM ip_whitelist WHERE id_player=? AND hash_ip=? ORDER by since DESC LIMIT 0,1");
			
			statement.setString(1, id.toString());
			statement.setString(2, Util.hash256(ip + "_tmc"));
			
			@Cleanup ResultSet rs = statement.executeQuery();
			if(rs == null) return null;
			if(!rs.next()) return null;
			
			return new WhitelistInfo(id, ip, rs.getBoolean("allowed"), rs.getBoolean("proxy"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean playerExists(UUID id, String name) {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup PreparedStatement statement = connectionRead.prepareStatement(""
					+ "SELECT pn.name "
					+ "FROM player AS p "
					+ "INNER JOIN player_name AS pn ON pn.id_player = p.id_player "
					+ "WHERE p.id_player = ? "
					+ "ORDER BY pn.id_player_name DESC "
					+ "LIMIT 1");
			
			statement.setString(1, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return false;
			String currentName = rs.getString("name");
			
			if(currentName.equals(name)) return true;
			
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statementName = connectionWrite.prepareStatement("INSERT player_name(id_player,name) VALUES(?,?)");
			statementName.setString(1, id.toString());
			statementName.setString(2, name);
			
			statementName.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static List<Offense> activateOffenses(UUID id) {
		try {
			@Cleanup Connection connectionRead = connectionPoolRead.getConnection();
			@Cleanup PreparedStatement statement = connectionRead.prepareStatement("SELECT o.id_offense,ot.name,o.ban_length,o.mute_length FROM offense o INNER JOIN offense_type ot ON ot.id_offense_type=o.id_offense_type WHERE id_player=? AND sanction_start IS NULL AND id_offense NOT IN (SELECT id_offense FROM offense_deactivated)");
			statement.setString(1, id.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			List<Offense> offenses = new ArrayList<>();
			
			while(rs.next()) {
				offenses.add(new Offense(rs.getInt("id_offense"), rs.getString("name"), rs.getInt("ban_length"), rs.getInt("mute_length")));
			}
			
			if(offenses.size() == 0) return null;
			
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statementUpdate = connectionWrite.prepareStatement("UPDATE offense SET sanction_start=NOW() WHERE id_player=? AND sanction_start IS NULL");
			statementUpdate.setString(1, id.toString());
			
			statementUpdate.executeUpdate();
			
			return offenses;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean addRegisterToken(UUID uuid, String ip) {
		try {
			@Cleanup Connection connection = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO register_token(id_player, hash_ip) VALUES (?,?)");
			
			statement.setString(1, uuid.toString());
			statement.setString(2, Util.hash256(ip + "_tmc"));
		
			statement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static List<String> isIpBanned(String ip) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("SELECT (SELECT pn.name FROM player_name pn WHERE pn.id_player=o.id_player ORDER BY id_player_name DESC LIMIT 1) AS name FROM offense o INNER JOIN offense_type ot ON o.id_offense_type = ot.id_offense_type WHERE o.ban_length = -1 AND id_offense NOT IN (SELECT id_offense FROM offense_deactivated) AND hash_ip=?");
			statement.setString(1, Util.hash256(ip + "_tmc"));
			
			ResultSet rs = statement.executeQuery();
			
			ArrayList<String> bannedNames = new ArrayList<>();
			while(rs.next()) bannedNames.add(rs.getString("name"));
			
			if(bannedNames.size() == 0) return null;
			
			return bannedNames;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean insertAppeal(int offenseId, String ip) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("INSERT INTO offense_login_token(id_offense,hash_ip) VALUES(?,?)");
			statement.setInt(1, offenseId);
			statement.setString(2, Util.hash256(ip + "_tmc"));
			
			statement.execute();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static AppealStatus isApplealDenied(int offenseId) {
		try {
			@Cleanup Connection connectionWrite = connectionPoolWrite.getConnection();
			@Cleanup PreparedStatement statement = connectionWrite.prepareStatement("SELECT result FROM offense_appeal LEFT JOIN offense_appeal_result ON offense_appeal.id_offense_appeal=offense_appeal_result.id_offense_appeal WHERE offense_appeal.id_offense=?");
			statement.setInt(1, offenseId);
			
			@Cleanup ResultSet rs = statement.executeQuery();
			if(!rs.next()) return null;
			String result = rs.getString("result");
			if(result == null) return AppealStatus.OPEN;
			
			return AppealStatus.valueOf(result);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static enum AppealStatus {
		ACCEPTED,DENIED,OPEN;
	}
}
