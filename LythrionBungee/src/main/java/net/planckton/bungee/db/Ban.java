package net.planckton.bungee.db;

import java.util.Date;
import java.util.UUID;

import lombok.Value;

@Value
public class Ban {
	int offenseId;
	UUID idPlayer;
	Date expires;
	String offense;
}
