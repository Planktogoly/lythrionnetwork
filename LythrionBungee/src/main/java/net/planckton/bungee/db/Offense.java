package net.planckton.bungee.db;

import lombok.Value;

@Value
public class Offense {
	int idOffense;
	String type;
	int banLength;
	int muteLength;
}
