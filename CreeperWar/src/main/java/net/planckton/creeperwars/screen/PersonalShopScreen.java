package net.planckton.creeperwars.screen;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.shop.ShopItem;
import net.planckton.creeperwars.shop.ShopManager;
import net.planckton.item.FancyItem;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class PersonalShopScreen extends Window {
	
	@Override
	protected void build() {		
		LinkedHashMap<String, LinkedList<ShopItem>> items = Manager.GetManager(ShopManager.class).getItems();		
		LinkedList<String> categories = new LinkedList<>(items.keySet());		
		
		for (int i = 0; i < 9; i++) {						
			setItem(i, new FancyItem(Material.STAINED_GLASS_PANE, 1, (byte) 7).name(" "));
		}
		
		for (int i = 0; i < 9; i++) {						
			setItem(i + 45, new FancyItem(Material.STAINED_GLASS_PANE, 1, (byte) 7).name(" "));
		}		
		
		for (int i = 0; i < categories.size(); i++) {
			int j = 1;
			for (ShopItem item : items.get(categories.get(i))) {
				
				addButton(i + (9 * j), new WindowButton(item.createItem()) {
					
					@Override
					public void onClick(LythrionPlayer player, InventoryClickEvent event) {
						CreeperPlayer creeperPlayer = (CreeperPlayer) player;
						Material ore = item.getOre();
						int amount = item.getAmountOre();
						
						PlayerInventory inv = player.getInventory();
						
						if (inv.containsAtLeast(new ItemStack(ore), amount)) {
						    if (item.getItem().getType() == Material.TNT) {
						    	if (inv.containsAtLeast(item.getItem().stack(), 5)) {
						    		player.sendMessage(ChatColor.RED + "You cannot have more than 5 TNT in your inventory!");
						    		player.playSound(Sound.VILLAGER_NO);
						    		return;
						    	}						    	
						    }
							
						    int left = amount;
						    while (left > 0) {
						    	ItemStack[] arrayOfItemStack = player.getInventory().getContents();
						    	for (int i = 0; i < arrayOfItemStack.length; i++) {
						    		ItemStack item = arrayOfItemStack[i];
						    		if ((item != null) && (item.getType() == ore)) {
								    	   if (item.getAmount() == 1) {
									    	   left--;
								    		   player.getInventory().clear(i);
								    		   break;
								           } else {
								        	   item.setAmount(item.getAmount() - 1);
									    	   left--;
									    	   break;
								           }
						    		}
						    	}
						    }
						    
						    player.playSound(Sound.ORB_PICKUP);
							ItemStack finalItem = new ItemStack(item.getItem().stack());
							finalItem.setAmount(item.getAmount());
							if (item.isUnbreakable()) {
								ItemMeta itemMeta = finalItem.getItemMeta();
								itemMeta.spigot().setUnbreakable(true);
								finalItem.setItemMeta(itemMeta);	
							}
							player.getInventory().addItem(finalItem);
							if (finalItem.getType().name().contains("PICKAXE")) creeperPlayer.getKeepItems().add(finalItem);
							return;
						}		
			
						
						player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "You cannot afford this item!");
						player.playSound(Sound.VILLAGER_NO);
						player.closeInventory();
					}
				});
				j++;
			}
		}
	}

	@Override
	protected String getName() {
		return "Personal Shop";
	}

	@Override
	protected int getRows() {
		return 6;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
