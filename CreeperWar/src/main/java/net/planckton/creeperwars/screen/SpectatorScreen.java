package net.planckton.creeperwars.screen;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class SpectatorScreen extends Window {		

	@Override
	protected void build() {
		ArrayList<LythrionPlayer> players = new ArrayList<>(PlayModeManager.getPlayersPerMode(PlayModePlayer.class));
		
		if (players.size() == 0) {
			setItem(0, new FancyItem(Material.BARRIER).name(ChatColor.RED + "No players that are playing"));
			return;
		}		
		
		for (int i = 0; i < players.size(); i++) {
			CreeperPlayer player = (CreeperPlayer) players.get(i);
			
			addButton(i, new WindowButton(new FancyItem(Material.SKULL_ITEM, 1, (byte)3)
					.name(player.getSelectedTeam().getColor() + player.getCurrentName())
					.lore(ChatColor.BLUE + "Click to spectate this player")) {
				
				final LythrionPlayer playingPlayer = player;
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {
					player.sendMessage(Warriors.getInstance().getPREFIX() + "Teleported to " + playingPlayer.getDisplayName());
					player.teleport(playingPlayer);				
				}
			});			
		}	
	}

	@Override
	protected String getName() {
		return "Players";
	}

	@Override
	protected int getRows() {
		int rows = (int) Math.max((Math.ceil((PlayModeManager.getPlayersPerMode(PlayModePlayer.class).size()) / 9.f)), 1);
		if(rows == 0) rows = 1;
		
		return rows;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}
}
