package net.planckton.creeperwars.screen;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.ability.BlizzardAbility;
import net.planckton.creeperwars.ability.BoostAbility;
import net.planckton.creeperwars.ability.FireBladeAbility;
import net.planckton.creeperwars.ability.FlareWindAbility;
import net.planckton.creeperwars.ability.LeaperAbility;
import net.planckton.creeperwars.kit.Kit;
import net.planckton.creeperwars.kit.KitManager;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.item.FancyItem;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

@AllArgsConstructor
public class KitSelectorScreen extends Window {

	@Getter private CreeperPlayer player;
	
	@Override
	protected void build() {		
		KitManager kitManager = Manager.GetManager(KitManager.class);
		ArrayList<Kit> kits = new ArrayList<>(kitManager.getKits());
		
		for (int i = 0; i < kits.size(); i++) {
			Kit kit = kits.get(i);
			
			FancyItem finalKit = new FancyItem(kit.getIcon());
			if (kit.equals(player.getSelectedKit())) {
				finalKit.enchant();
				finalKit.lore(ChatColor.BLUE + "Selected kit");
			} else {
				finalKit.lore(ChatColor.BLUE + "Click to select this kit");
			}
			
			int j = i + i;			
			addButton(j, new WindowButton(finalKit) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {
					CreeperPlayer creeperPlayer = (CreeperPlayer) player;
					if (kit.equals(creeperPlayer.getSelectedKit())) {
						player.sendMessage(Warriors.getInstance().getPREFIX() + "You already selected the kit " + kit.getColor() + kit.getName() + ChatColor.WHITE + ".");	
						return;
					}		

					kitManager.setKit((CreeperPlayer) player, kit);
					
					if (kit.getName().equalsIgnoreCase("leaper")) creeperPlayer.setAbility(new LeaperAbility(creeperPlayer));
					else if (kit.getName().equalsIgnoreCase("bomber")) creeperPlayer.setAbility(new FlareWindAbility(creeperPlayer));
					else if (kit.getName().equalsIgnoreCase("archer")) creeperPlayer.setAbility(new BlizzardAbility(creeperPlayer));
					else if (kit.getName().equalsIgnoreCase("warrior")) creeperPlayer.setAbility(new FireBladeAbility(creeperPlayer));
					else if (kit.getName().equalsIgnoreCase("miner")) creeperPlayer.setAbility(new BoostAbility(creeperPlayer));

					PlayModeLobby.getScoreKit().update(player);
					player.playSound(Sound.CLICK);
					player.sendMessage(Warriors.getInstance().getPREFIX() + "You have selected the kit " + kit.getColor() + kit.getName() + ChatColor.WHITE + ".");
					player.closeInventory();
				}
			});
		}
		
	}

	@Override
	protected String getName() {
		return "Select your kit";
	}

	@Override
	protected int getRows() {
		return 1;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
