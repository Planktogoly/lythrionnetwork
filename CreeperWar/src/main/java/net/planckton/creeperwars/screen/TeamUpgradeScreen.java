package net.planckton.creeperwars.screen;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.upgrade.CreeperUpgrade;
import net.planckton.creeperwars.upgrade.MineUpgrade;
import net.planckton.creeperwars.upgrade.TeamHealthUpgrade;
import net.planckton.creeperwars.upgrade.Upgrade;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class TeamUpgradeScreen extends Window {

	private Team team;
	
	public TeamUpgradeScreen(Team team) {
		this.team = team;
	}
	
	@Override
	protected void build() {
		ArrayList<Upgrade> upgrades = new ArrayList<>(team.getUpgrades());
		
		int j = 0;
		for (int i = 0; i < team.getUpgrades().size(); i++) {
			Upgrade upgrade = upgrades.get(i);
			
			j+=2;
			addButton(j, new WindowButton(upgrade.createItem()) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {
					CreeperPlayer gPlayer = (CreeperPlayer) player;
					
					if (upgrade instanceof CreeperUpgrade) {
						CreeperUpgrade gUpgrade = (CreeperUpgrade) upgrade;
						
						if (gUpgrade.getUpgradeNumber() != 4) {
							Material ore = gUpgrade.getPrices().get(gUpgrade.getUpgradeNumber()).getMaterial();
							int amount = gUpgrade.getPrices().get(gUpgrade.getUpgradeNumber()).getPrice();
							
							PlayerInventory inv = player.getInventory();
							
							if (inv.containsAtLeast(new ItemStack(ore), amount)) {
							    int left = amount;
							    while (left > 0) {
							    	ItemStack[] arrayOfItemStack = player.getInventory().getContents();
							    	for (int i = 0; i < arrayOfItemStack.length; i++) {
							    		ItemStack item = arrayOfItemStack[i];
							    		if ((item != null) && (item.getType() == ore)) {
									    	   if (item.getAmount() == 1) {
										    	   left--;
									    		   player.getInventory().clear(i);
									    		   break;
									           } else {
									        	   item.setAmount(item.getAmount() - 1);
										    	   left--;
										    	   break;
									           }
							    		}
							    	}
							    }
								
								gUpgrade.run(gPlayer);
								gPlayer.closeInventory();
								player.balance.get().rewardCrystals(10);
								player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 15, 1, true, false));	
								player.sendMessage(ChatColor.GRAY + "You got rewarded with haste II");
								return;
							} else {
								player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
								player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "You cannot afford this upgrade!");
								player.closeInventory();
								return;
							}
						} 
						
						player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
						player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "This upgrade has been fully upgraded!");
						player.closeInventory();
						return;
					} else if (upgrade instanceof TeamHealthUpgrade) {
						TeamHealthUpgrade tUpgrade = (TeamHealthUpgrade) upgrade;
						
						if (tUpgrade.getUpgradeNumber() != 4) {
							Material ore = tUpgrade.getPrices().get(tUpgrade.getUpgradeNumber()).getMaterial();
							int amount = tUpgrade.getPrices().get(tUpgrade.getUpgradeNumber()).getPrice();
							
							PlayerInventory inv = player.getInventory();
							
							if (inv.containsAtLeast(new ItemStack(ore), amount)) {
							    int left = amount;
							    while (left > 0) {
							    	ItemStack[] arrayOfItemStack = player.getInventory().getContents();
							    	for (int i = 0; i < arrayOfItemStack.length; i++) {
							    		ItemStack item = arrayOfItemStack[i];
							    		if ((item != null) && (item.getType() == ore)) {
									    	   if (item.getAmount() == 1) {
										    	   left--;
									    		   player.getInventory().clear(i);
									    		   break;
									           } else {
									        	   item.setAmount(item.getAmount() - 1);
										    	   left--;
										    	   break;
									           }
							    		}
							    	}
							    }
								
								tUpgrade.run(gPlayer);
								gPlayer.closeInventory();
								player.balance.get().rewardCrystals(10);
								player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 15, 1, true, false));	
								player.sendMessage(ChatColor.GRAY + "You got rewarded with haste II");
								return;
							} else {
								player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
								player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "You cannot afford this upgrade!");
								player.closeInventory();
								return;
							}
						}
						
						player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);	
						player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "This upgrade has been fully upgraded!");
						player.closeInventory();
						return;
					}  else if (upgrade instanceof MineUpgrade) {
						MineUpgrade tUpgrade = (MineUpgrade) upgrade;
						
						if (tUpgrade.getUpgradeNumber() != 4) {
							Material ore = tUpgrade.getPrices().get(tUpgrade.getUpgradeNumber()).getMaterial();
							int amount = tUpgrade.getPrices().get(tUpgrade.getUpgradeNumber()).getPrice();
							
							PlayerInventory inv = player.getInventory();
							
							if (inv.containsAtLeast(new ItemStack(ore), amount)) {
							    int left = amount;
							    while (left > 0) {
							    	ItemStack[] arrayOfItemStack = player.getInventory().getContents();
							    	for (int i = 0; i < arrayOfItemStack.length; i++) {
							    		ItemStack item = arrayOfItemStack[i];
							    		if ((item != null) && (item.getType() == ore)) {
									    	   if (item.getAmount() == 1) {
										    	   left--;
									    		   player.getInventory().clear(i);
									    		   break;
									           } else {
									        	   item.setAmount(item.getAmount() - 1);
										    	   left--;
										    	   break;
									           }
							    		}
							    	}
							    }
							    
								tUpgrade.run(gPlayer);
								gPlayer.closeInventory();								
								player.balance.get().rewardCrystals(10);
								player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 15, 1, true, false));	
								player.sendMessage(ChatColor.GRAY + "You got rewarded with haste II");
								return;
							} else {
								player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
								player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "You cannot afford this upgrade!");
								player.closeInventory();
								return;
							}
						}
						
						player.getBukkitPlayer().playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
						player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "This upgrade has been fully upgraded!");
						player.closeInventory();
						return;
					}					
				}
			});
		}		
	}

	@Override
	protected String getName() {
		return "Upgrade your team";
	}

	@Override
	protected int getRows() {
		return 1;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
