package net.planckton.creeperwars.screen;

import java.util.ArrayList;

import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;

import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.item.FancyItem;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

@AllArgsConstructor
public class TeamSelectorScreen extends Window {

	private CreeperPlayer player;
	
	@Override
	protected void build() {
		TeamManager teamManager = Manager.GetManager(TeamManager.class);
		ArrayList<Team> teams = new ArrayList<>(teamManager.getTeams());
		
		for (int i = 0; i < teams.size(); i++) {
		    Team team = teams.get(i);
			
			FancyItem finalTeam = new FancyItem(team.getIcon());
			if (team.equals(player.getSelectedTeam())) finalTeam.enchant();
			
			int j = i + i;			
			addButton(j + 1, new WindowButton(finalTeam) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {
					CreeperPlayer creeperPlayer = (CreeperPlayer) player;
					
					if (team.equals(creeperPlayer.getSelectedTeam())) {
						player.sendMessage(Warriors.getInstance().getPREFIX() + "You already joined team " + team.getColor() + team.getName() + ChatColor.WHITE + ".");	
						return;
					}		

					if (!(team.getMaxPlayers() == team.getPlayers().size())) {
						team.join(creeperPlayer);
						player.playSound(Sound.CLICK);
						player.getInventory().setItem(0, new FancyItem(team.getIcon()).stack());
						player.sendMessage(Warriors.getInstance().getPREFIX() + "You joined team " + team.getColor() + team.getName() + ChatColor.WHITE + ".");
						player.closeInventory();
						return;
					}
					
					player.closeInventory();
					player.sendMessage(Warriors.getInstance().getPREFIX() + "The team " + team.getColor() + team.getName() + ChatColor.WHITE + " is full!");
					return;
				}
			});
		}	
	}

	@Override
	protected String getName() {
		return "Select your team";
	}

	@Override
	protected int getRows() {
		return 1;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
