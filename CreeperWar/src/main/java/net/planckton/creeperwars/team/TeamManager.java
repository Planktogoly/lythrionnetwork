package net.planckton.creeperwars.team;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import org.bukkit.entity.Entity;

import lombok.Getter;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.holo.HoloManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.position.PositionManager;

public class TeamManager extends Manager {

	@Getter private LinkedHashSet<Team> teams = new LinkedHashSet<Team>();
	private Map<String, Team> teamByName = new HashMap<String, Team>();	
	
	@Getter private LinkedHashMap<String, Team> teamByPlayerName = new LinkedHashMap<>();
	
	public TeamManager() {
		super(PositionManager.class, GameManager.class, ChatManager.class, HoloManager.class);
	}
	
	@Override
	public void onEnable() {
		addTeam(new Team("Green", 5));
		addTeam(new Team("Red", 14));
		addTeam(new Team("Yellow", 4));
		addTeam(new Team("Blue", 11));
	}

	@Override
	public void onDisable() {
		for (Team team : teams) {
			for (Entity entity : team.getEntities()) {
				entity.remove();
			}
		}
	}
	
	public void addTeam(Team team) {
		teams.add(team);
		teamByName.put(team.getName(), team);
	}
	
	public Team getTeam(String name) {
		return teamByName.get(name);
	}
	
	public void setTeamPlayer(CreeperPlayer player, Team team) {
		teamByPlayerName.put(player.getName(), team);
	}

}
