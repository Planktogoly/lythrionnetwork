package net.planckton.creeperwars.team;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.creeper.CreeperBoss;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.upgrade.CreeperUpgrade;
import net.planckton.creeperwars.upgrade.MineUpgrade;
import net.planckton.creeperwars.upgrade.TeamHealthUpgrade;
import net.planckton.creeperwars.upgrade.Upgrade;
import net.planckton.item.FancyItem;
import net.planckton.lib.chat.ChatChannel;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.holo.Holo;
import net.planckton.lib.holo.HoloManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.position.Position;
import net.planckton.lib.position.PositionManager;

public class Team {
	
	@Getter private ChatColor color;
	@Getter private String name;
	private int iconItem;	
	@Getter private CreeperBoss creeper;
	
	@Getter @Setter private ArrayList<CreeperPlayer> startPlayers;
	@Getter private ArrayList<CreeperPlayer> players;	
	@Getter private int maxPlayers;
	
	@Getter private ChatChannel chat;
	
	@Getter private ArrayList<Upgrade> upgrades;
	@Getter private double health;
	
	@Getter private Holo hologram;
	private final HoloManager HOLOMANAGER = (HoloManager) Manager.GetManager(HoloManager.class);
	
    @Getter private ArrayList<Entity> entities = new ArrayList<>();
	
	public Team(String color, int itemByte) {
		this.startPlayers = new ArrayList<>();
		this.players = new ArrayList<>();
		this.upgrades = new ArrayList<>();
		
		this.name = color;
		this.color = ChatColor.valueOf(color.toUpperCase());
		this.iconItem = itemByte;
		
		chat = ChatManager.create(color + "Team", "Team", this.color + "[Team] "); 
		
		maxPlayers = Manager.GetManager(GameManager.class).getMaxPlayers() / 4;
		
		this.creeper = new CreeperBoss(this, name);
		
		upgrades.add(new CreeperUpgrade(this));
		upgrades.add(new TeamHealthUpgrade(this));
		upgrades.add(new MineUpgrade(this));

		Position pos = PositionManager.getPosition("guardianwar." + getName().toLowerCase() + ".villager");
	    Villager villager = pos.getWorld().spawn(pos.getPosition(), Villager.class);
	    
	    villager.setProfession(Profession.LIBRARIAN);
		villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 255));
		
		net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity)villager).getHandle();
        NBTTagCompound tag = nmsEntity.getNBTTag();
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        nmsEntity.c(tag);
        
        tag.setInt("NoAI", 1);
        tag.setInt("Silent", 1);
        
        nmsEntity.f(tag);
        
        entities.add(villager);
        
        hologram = new Holo(pos.getPosition().clone().add(0, 1.825, 0), getColor() + getName() + " shop");
        HOLOMANAGER.spawnHolo(hologram);
	}
	
	
	public void join(CreeperPlayer player) {	
		players.add(player);		
		
		if (player.getSelectedTeam() != null) player.getSelectedTeam().leave(player);
		
		player.setSelectedTeam(this);		
		if (GameManager.getPhase() == GamePhases.LOBBY) player.tab.setRankTabColor(color);
		else player.tab.setTabColor(color);
		
		Manager.GetManager(TeamManager.class).setTeamPlayer(player, this);
		
		if (GameManager.getPhase() == GamePhases.LOBBY) {
			for (CreeperPlayer teamPlayer : players) {
				teamPlayer.getInventory().setItem(0, new FancyItem(getIcon()).stack());
			}
		}
	}
	
	public void leave(CreeperPlayer player) {
		players.remove(player);
		PlayModePlayer.updatePlayers(this, creeper.getHealth() == 0 ? false : true);
		Manager.GetManager(TeamManager.class).getTeamByPlayerName().remove(player.getName());
		
		player.chat.setChatChannel(ChatManager.CHANNEL_GLOBAL);
		player.chat.removeAvailableChat(chat);
		player.chat.removeListeningChats(chat);
		
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) {
			if (getPlayers().size() == 0) {
				creeper.killCreeper(true, player);
				
				Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						if (GameManager.shouldWeEndGame()) GameManager.setState(GamePhases.POST_GAME);						
					}
				}, 10);
				return;
			}
		}
		
		player.tab.resetTabColor();
		player.setSelectedTeam(null);
		
		if (GameManager.getPhase() == GamePhases.LOBBY) {
			for (CreeperPlayer teamPlayer : players) {
				teamPlayer.getInventory().setItem(0, new FancyItem(getIcon()).stack());
			}
		}
	}
	
	public void sendMessage(String message) {
		for (CreeperPlayer player : players) {
			player.sendMessage(message);
		}
	}
	
	public void playSound(Sound sound) {
		for (CreeperPlayer player : players) {
			player.playSound(sound);
		}
	}
	
	public void sendTitle(String title, String subTitle, int fadeIn, int stay, int fadeOut) {
		for (CreeperPlayer player : players) {
			player.sendTitle(title, subTitle, fadeIn, stay, fadeOut);
		}
	}
	
	public void setHealth(double health) {
		this.health = health;
		
		for (CreeperPlayer player : players) {
			player.setHealthScale(true);
			player.setHealthScale(health);
			player.setMaxHealth(health);
		}
	}
	
	public void removeFences(int number, int radius) {
		Position position = PositionManager.getPosition("guardianwar.team." + getName().toLowerCase() + ".mine." + number);
				
		for (Location loc : position.getLocations()) {
		     for(double x = loc.getX() - radius; x <= loc.getX() + radius; x++){
		    	 
		    	 for(double y = loc.getY() - radius; y <= loc.getY() + radius; y++){
		    		 
		           for(double z = loc.getZ() - radius; z <= loc.getZ() + radius; z++){
		             Location finalLoc = new Location(loc.getWorld(), x, y, z);
		             
		             if (finalLoc.getBlock().getType() == Material.FENCE) {
		            	 finalLoc.getWorld().spigot().playEffect(finalLoc, Effect.SMOKE, 0, 0, 0.1f, 0.1f, 0.1f, 0.5f, 30, 35);
		            	 finalLoc.getBlock().setType(Material.AIR);		             
		             }
		           }	           
		         }	    	 
		     }
		}
	}
	
	public FancyItem getIcon() {
		FancyItem icon = new FancyItem(Material.WOOL, 1, (byte) iconItem).name(color.toString() + ChatColor.BOLD + name + ChatColor.DARK_GRAY + " (" + color + 
				players.size() + ChatColor.GRAY + "/" + color + "4" + ChatColor.DARK_GRAY + ")");
		icon.lore("");
		
		for (CreeperPlayer player : players) {
			icon.lore(ChatColor.GRAY + "- " + player.getSelectedTeam().getColor() + player.getName());
		}		
		return icon;
	}

}
