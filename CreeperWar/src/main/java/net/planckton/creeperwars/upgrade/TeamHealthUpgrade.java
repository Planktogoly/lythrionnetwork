package net.planckton.creeperwars.upgrade;

import java.util.LinkedHashMap;

import org.bukkit.Material;
import org.bukkit.Sound;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.item.FancyItem;

public class TeamHealthUpgrade extends Upgrade {

	private static FancyItem item = new FancyItem(Material.FERMENTED_SPIDER_EYE);
	
	public TeamHealthUpgrade(Team team) {
		super("Team health", createPrices(), team, item, "Upgrades every team members health!");
	}

	public static LinkedHashMap<Integer, UpgradeCost> createPrices() {
		LinkedHashMap<Integer, UpgradeCost> prices = new LinkedHashMap<Integer, UpgradeCost>();
		
		prices.put(1, new UpgradeCost(25, Material.GOLD_INGOT));
		prices.put(2, new UpgradeCost(25, Material.DIAMOND));
		prices.put(3, new UpgradeCost(25, Material.EMERALD));
		
		return prices;
	}	
	
	public void run(CreeperPlayer player) {
		run();
	}
	
	@Override
	public void run() {		
		if (getUpgradeNumber() == 1) {
			getTeam().setHealth(22);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "Team health" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 1" + ChatColor.WHITE + "!");
			getTeam().sendMessage("Your team mates have now 11 hearts!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(2);
			return;
		} else if (getUpgradeNumber() == 2) {
			getTeam().setHealth(24);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "Team health" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 2" + ChatColor.WHITE + "!");
			getTeam().sendMessage("Your team mates have now 12 hearts!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(3);
			return;
		} else if (getUpgradeNumber() == 3) {
			getTeam().setHealth(26);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "Team health" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 3" + ChatColor.WHITE + "!");
			getTeam().sendMessage("Your team mates have now 13 hearts!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(4);
			return;
		} 					
	}

}
