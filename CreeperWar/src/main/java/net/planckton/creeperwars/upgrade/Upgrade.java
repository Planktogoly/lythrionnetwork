package net.planckton.creeperwars.upgrade;

import java.util.LinkedHashMap;

import org.bukkit.Material;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.team.Team;
import net.planckton.item.FancyItem;

public abstract class Upgrade implements Runnable {
	
	@Getter private String name;
	@Getter @Setter private int upgradeNumber = 1;
	@Getter private LinkedHashMap<Integer, UpgradeCost> prices;
	@Getter private Team team;
	@Getter private FancyItem item;
	@Getter private String description;
	
	
	public Upgrade(String name, LinkedHashMap<Integer, UpgradeCost> prices, Team team, FancyItem item, String description) {
		this.name = name;
		this.prices = prices;
		this.team = team;		
		this.item = item;
		this.description = description;
	}
	
	public FancyItem createItem() {		
		FancyItem icon = new FancyItem(item);
		
		if (upgradeNumber != 4) {
			icon.name(ChatColor.YELLOW + getName() + " lvl. " + getUpgradeNumber());
			icon.lore("");
			icon.lore(ChatColor.GRAY + description);
			icon.lore("");
			if (prices.get(upgradeNumber).getMaterial() == Material.COAL) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + prices.get(upgradeNumber).getPrice() + " Coal");
			if (prices.get(upgradeNumber).getMaterial() == Material.IRON_INGOT) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + prices.get(upgradeNumber).getPrice() + " Iron");
			if (prices.get(upgradeNumber).getMaterial() == Material.GOLD_INGOT) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + prices.get(upgradeNumber).getPrice() + " Gold");
			if (prices.get(upgradeNumber).getMaterial() == Material.DIAMOND) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + prices.get(upgradeNumber).getPrice() + " Diamonds");		
			if (prices.get(upgradeNumber).getMaterial() == Material.EMERALD) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + prices.get(upgradeNumber).getPrice() + " Emeralds");
			
			icon.lore(ChatColor.BLUE + "Click to buy this item");
		} else {
			icon.name(ChatColor.YELLOW + getName() + " lvl. " + 3);
			icon.lore("");
			icon.lore(ChatColor.GRAY + description);
			icon.lore("");
			icon.lore(ChatColor.BLUE + "The upgrade has been fully upgraded.");
			icon.enchant();
		}
		icon.hideAllFlags();		
		return icon;
	}

}
