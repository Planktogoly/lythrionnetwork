package net.planckton.creeperwars.upgrade;

import java.util.LinkedHashMap;

import org.bukkit.Material;
import org.bukkit.Sound;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.item.FancyItem;

public class MineUpgrade extends Upgrade {
	
	private static FancyItem item = new FancyItem(Material.STONE_PICKAXE);

	public MineUpgrade(Team team) {
		super("Mine upgrade", createPrices(), team, item, "Upgrades your mine to gather different ores!");
	}
	
	public static LinkedHashMap<Integer, UpgradeCost> createPrices() {
		LinkedHashMap<Integer, UpgradeCost> prices = new LinkedHashMap<Integer, UpgradeCost>();
		
		prices.put(1, new UpgradeCost(25, Material.IRON_INGOT));
		prices.put(2, new UpgradeCost(25, Material.GOLD_INGOT));
		
		return prices;
	}	
	
	public void run(CreeperPlayer player) {	
		run();
	}

	@Override
	public void run() {
		if (getUpgradeNumber() == 1) {
			getTeam().removeFences(1, 5);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.YELLOW + "Mine" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 1" + ChatColor.WHITE + "!");
			getTeam().sendMessage("New part of the mine is open!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(2);
			return;
		} else if (getUpgradeNumber() == 2) {
			getTeam().removeFences(2, 5);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.YELLOW + "Mine" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 2" + ChatColor.WHITE + "!");
			getTeam().sendMessage("New part of the mine is open!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(4);
			return;
		}		
	}

}
