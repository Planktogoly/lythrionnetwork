package net.planckton.creeperwars.upgrade;

import org.bukkit.Material;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class UpgradeCost {

	@Getter private int price;
	@Getter private Material material;
}
