package net.planckton.creeperwars.upgrade;

import java.util.LinkedHashMap;

import org.bukkit.Material;
import org.bukkit.Sound;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.creeper.CreeperBoss;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.item.FancyItem;

public class CreeperUpgrade extends Upgrade {	
	
	private static FancyItem item = new FancyItem(Material.MONSTER_EGG, 1, (byte) 50);
	
	public CreeperUpgrade(Team team) {
		super("Creeper upgrade", createPrices(), team, item, "Upgrades your Creeper health!");
	}
	
	public static LinkedHashMap<Integer, UpgradeCost> createPrices() {
		LinkedHashMap<Integer, UpgradeCost> prices = new LinkedHashMap<Integer, UpgradeCost>();
		
		prices.put(1, new UpgradeCost(10, Material.IRON_INGOT));
		prices.put(2, new UpgradeCost(10, Material.DIAMOND));
		prices.put(3, new UpgradeCost(10, Material.EMERALD));
		
		return prices;
	}	
	
	public void run(CreeperPlayer player) {		
		run();
	}

	@Override
	public void run() {		
		CreeperBoss creeper = getTeam().getCreeper();
		if (getUpgradeNumber() == 1) {
			creeper.setHealth(creeper.getHealth() + 100);
			creeper.setMaxHealth(creeper.getMaxHealth() + 100);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.GREEN + "Creeper" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 1" + ChatColor.WHITE + "!");
			getTeam().sendMessage("He has now " + ChatColor.GRAY + (int) creeper.getHealth() + 
					ChatColor.WHITE +  "/" + ChatColor.GRAY + creeper.getMaxHealth() + ChatColor.WHITE + " health!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(2);
			return;
		} else if (getUpgradeNumber() == 2) {
			creeper.setHealth(creeper.getHealth() + 100);
			creeper.setMaxHealth(creeper.getMaxHealth() + 100);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.GREEN + "Creeper" + ChatColor.WHITE + "  upgraded to level" + ChatColor.RED + " 2" + ChatColor.WHITE + "!");
			getTeam().sendMessage("He has now " + ChatColor.GRAY + (int) creeper.getHealth() + 
					ChatColor.WHITE +  "/" + ChatColor.GRAY + creeper.getMaxHealth() + ChatColor.WHITE + " health!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(3);
			return;
		} else if (getUpgradeNumber() == 3) {
			creeper.setHealth(creeper.getHealth() + 100);
			creeper.setMaxHealth(creeper.getMaxHealth() + 100);
			getTeam().sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.GREEN + "Creeper" + ChatColor.WHITE + " upgraded to level" + ChatColor.RED + " 3" + ChatColor.WHITE + "!");
			getTeam().sendMessage("He has now " + ChatColor.GRAY + (int) creeper.getHealth() + 
					ChatColor.WHITE +  "/" + ChatColor.GRAY + creeper.getMaxHealth() + ChatColor.WHITE + " health!");
			getTeam().playSound(Sound.WITHER_SPAWN);
			setUpgradeNumber(4);
			return;
		} 
	}

}
