package net.planckton.creeperwars.game;

import org.bukkit.ChatColor;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum GamePhases {
	
	LOBBY(ChatColor.GREEN + "" + ChatColor.BOLD + "Lobby", new PhaseLobby()), 
	PREGAME(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "PreGame", new PhasePreGame()),
	GRACE(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Pre-game", new PhaseGrace()), 
	PVP(ChatColor.RED + "" + ChatColor.BOLD + "In-game", new PhasePVP()), 
	TRACKER(ChatColor.RED + "" + ChatColor.BOLD + "In-game", new PhaseTracker()),
	POST_GAME(ChatColor.YELLOW + "" + ChatColor.BOLD + "Restarting..", new PhasePost());
	
	@Getter private String phase;
	@Getter private GamePhase gamePhase;

}
