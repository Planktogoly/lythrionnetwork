package net.planckton.creeperwars.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import mkremins.fanciful.FancyMessage;
import net.planckton.blood.GameStatus;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.utils.ChatPrefix;

public class PhasePost extends GamePhase {

	private static int stopCountdown = 18;
	
	private final Warriors warriors = Warriors.getInstance();
	
	private Team winner;
	private boolean tie;
	
	@Override
	public void onPhaseStart() {
		LythrionLib.updateGameStatus(GameStatus.POSTGAME);
		tie = !GameManager.shouldWeEndGame();
		if (!tie) {
			for(LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
				CreeperPlayer gPlayer = (CreeperPlayer) player;
				winner = gPlayer.getSelectedTeam();
			}
			
			for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
				CreeperPlayer gPlayer = (CreeperPlayer) player;
				
				if (gPlayer.getSelectedTeam().getName().equalsIgnoreCase(winner.getName())) {
					player.sendTitle("&aYou won!", "&fTeam " + winner.getColor() + winner.getName() + " &fwon the game!", 10, 20 * 20, 10);
				} else {
					player.sendTitle("&cYou lose!", "&fTeam " + winner.getColor() + winner.getName() + " &fwon the game!", 10, 20 * 20, 10);	
				}
				player.sendMessage(Warriors.getInstance().getPREFIX() + "Team " + winner.getColor() + winner.getName() + ChatColor.WHITE + " won the game!");
			}
		} else {
			for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
				player.sendTitle("&eIt's a tie!", " ", 10, 20 * 20, 10);
			}
		}
		
		for (CreeperPlayer player : winner.getStartPlayers()) {
			Warriors.getInstance().addCrystals(player, player.balance.get().rewardCrystals(50));
		}
		
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			
			for (CreeperPlayer teamPlayer : team.getStartPlayers()) {
				teamPlayer.chat.setChatChannel(ChatManager.CHANNEL_GLOBAL);
				teamPlayer.chat.removeAvailableChat(team.getChat());
				teamPlayer.chat.removeListeningChats(team.getChat());
				
				if (team.getName().equalsIgnoreCase(winner.getName())) continue;
				
				Warriors.getInstance().addCrystals(teamPlayer, teamPlayer.balance.get().rewardCrystals(5));
			}
		}

		ArrayList<CreeperPlayer> players = new ArrayList<>(Warriors.getInstance().getCoinsEarned().keySet());			
		Collections.sort(players,new Comparator<CreeperPlayer>() {
		    @Override
		    public int compare(CreeperPlayer a, CreeperPlayer b) {
		        return warriors.getCoinsEarned().get(b) - warriors.getCoinsEarned().get(a);
		    }
		});	
		
		CreeperPlayer first = players.get(0);
		CreeperPlayer second = players.get(1);
		CreeperPlayer third = players.get(2);
		
		for (LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
			player.sendMessage(" ");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Game: " + ChatColor.GREEN + "CreeperWars                 ");
			player.sendMessage(" ");			
			player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "1st " + ChatColor.GRAY + first.getRank().getPrefix() + getTeamColor(first) + first.getName() + ChatColor.WHITE + " - " + ChatColor.AQUA + warriors.getCoinsEarned().get(first) + " crystals");
			player.sendMessage(ChatColor.YELLOW.toString() + ChatColor.BOLD + "2nd " + ChatColor.GRAY + second.getRank().getPrefix() + getTeamColor(second) + second.getName() + ChatColor.WHITE + " - " + ChatColor.AQUA + warriors.getCoinsEarned().get(second) + " crystals");
			player.sendMessage(ChatColor.RED .toString() + ChatColor.BOLD + "3rd " + ChatColor.GRAY + third.getRank().getPrefix() + getTeamColor(third) + third.getName() + ChatColor.WHITE + " - " + ChatColor.AQUA + warriors.getCoinsEarned().get(third) + " crystals");
			player.sendMessage(" ");
			player.sendMessage(" ");
		}
	}

	@Override
	public void run() {
		stopCountdown--;
		
		if (stopCountdown == 15) {
			for(LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
				player.sendMessage(new FancyMessage(ChatPrefix.LYTHRION_RED_ARROW + "Want to play this game again? Click ")
						.then(ChatColor.AQUA.toString() + "HERE")
						.command("/mjoin CreeperWar")
						.tooltip(ChatColor.WHITE + "Click to join")
						.then(ChatColor.WHITE + "!"));
			}
		}else if(stopCountdown == 0) {
			for(LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
				player.connectToHub();
			}			
			return;
		}
		else if(stopCountdown <= -2) {
			Bukkit.getServer().shutdown();			
			return;
		}
		
		if (tie) return;
		
		for(LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
			Firework firework = player.getWorld().spawn(player.getLocation(), Firework.class);
            FireworkMeta fireworkMeta = firework.getFireworkMeta();
            
            FireworkEffect effect = FireworkEffect
            		.builder()
            		.withFlicker()
            		.withTrail()
            		.with(Type.BALL)
            		.withColor(Color.WHITE, Color.RED)
            		.build();
            
            fireworkMeta.addEffect(effect);
            firework.setFireworkMeta(fireworkMeta);
            
            player.getBukkitPlayer().setPassenger(firework);
            
            Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), firework::detonate, 10);
		}
		
	}

	@Override
	public void onJoin(LythrionPlayer player) {
		player.kickPlayer("You cant spectate. The game has ended!");	
	}

	@Override
	public void onQuit(LythrionPlayer player) {		
	}
	
	public ChatColor getTeamColor(CreeperPlayer player) {
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			if (team.getStartPlayers().contains(player)) return team.getColor();
		}
		return ChatColor.WHITE;
	}

}
 