package net.planckton.creeperwars.game;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.blood.GameStatus;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeManager;

public class PhaseLobby extends GamePhase {

	private static int countDown = 60;
	private static int minPlayers = 10;
	
	@Getter @Setter private static BukkitTask task;
	
	@Override
	public void onPhaseStart() {		
		LythrionLib.updateGameStatus(GameStatus.LOBBY);
		
		task = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				int neededPlayers = minPlayers - PlayerManager.getOnlineLythrionPlayers().size();
				if (neededPlayers <= 0) return;		
				
				for (LythrionPlayer lythrionPlayer : PlayerManager.getOnlineLythrionPlayers()) {
					lythrionPlayer.sendActionBarMessage(ChatColor.WHITE + "We need " + ChatColor.RED.toString()	+ ChatColor.BOLD + neededPlayers + ChatColor.WHITE + " more players to start!");
				}
				
			}
		}, 0, 20);
	}

	@Override
	public void run() {
		Set<LythrionPlayer> lobbyPlayers = PlayModeManager.getPlayersPerMode(PlayModeLobby.class);
		if(lobbyPlayers.size() == 0) return;
		
		int lobbiedPlayers = lobbyPlayers.size();
		
		if (!(lobbiedPlayers >= minPlayers)) {			
			countDown = 30;
			PlayModeLobby.updateTimer(-1);
			
			if (task == null) {
				task = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						int neededPlayers = minPlayers - PlayerManager.getOnlineLythrionPlayers().size();
						if (neededPlayers <= 0) return;		
						
						for (LythrionPlayer lythrionPlayer : PlayerManager.getOnlineLythrionPlayers()) {
							lythrionPlayer.sendActionBarMessage(ChatColor.WHITE + "We need " + ChatColor.RED.toString()	+ ChatColor.BOLD + neededPlayers + ChatColor.WHITE + " more players to start!");
						}
						
					}
				}, 0, 20);
			}			
			return;
		}
		
		if (task != null) {
			task.cancel();
			task = null;
		}
		
		countDown--;		
		PlayModeLobby.updateTimer(countDown);
		if (lobbiedPlayers >= minPlayers) {
			if (countDown > 30) countDown = 30;
		}
		
		if (countDown == 1) {
			GameManager.setState(GamePhases.PREGAME);
			countDown = 30;
		}
	}

	@Override
	public void onJoin(LythrionPlayer player) {					
		Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + player.getRank().getColor() + player.getName() + ChatColor.WHITE + " joined the game!");
	}

	@Override
	public void onQuit(LythrionPlayer player) {		
		Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + player.getRank().getColor() + player.getName() + ChatColor.WHITE  + " left the game!");
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventory(InventoryClickEvent event) {	
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) return;		
			
		event.setCancelled(true); 
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickUpItem(PlayerPickupItemEvent event) {
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickUpItem(BlockBreakEvent event) {
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventoryDrag(InventoryDragEvent event) {
		if (!GameManager.getPhase().equals(GamePhases.LOBBY)) return;
		
		event.setCancelled(true);
	}
}
