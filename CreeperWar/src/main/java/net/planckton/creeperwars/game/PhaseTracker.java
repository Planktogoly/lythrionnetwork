package net.planckton.creeperwars.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModeDead;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.playmode.PlayModeSpectator;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeManager;

public class PhaseTracker extends GamePhase {
	
	int countdown = 600;
	
	@Override
	public void onPhaseStart() {
		for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
			CreeperPlayer gPlayer = (CreeperPlayer) player;
			gPlayer.getInventory().addItem(gPlayer.getSelectedKit().getCompass().stack());
			player.playSound(Sound.FIREWORK_BLAST);
			gPlayer.sendTitle("&e ", "&fTracker &ePhase &fhas begun!", 10 , 120, 10);
		}
		
		PlayModePlayer.updatePhase("Tracker");
	}

	@Override
	public void run() {
		countdown--;
		PlayModePlayer.updateTime(countdown);		
		
		if (countdown == 0) GameManager.setState(GamePhases.POST_GAME);
	}

	@Override
	public void onJoin(LythrionPlayer player) {
		player.setPlayMode(PlayModeSpectator.class);
	}

	@Override
	public void onQuit(LythrionPlayer player) {
		CreeperPlayer cPlayer = (CreeperPlayer) player;	
		if (cPlayer.isInPlayMode(PlayModePlayer.class)) {
			for (ItemStack stack : cPlayer.getInventory().getContents()) {
				if (cPlayer.getSelectedKit().getItems().contains(stack)) continue;
				if (stack == null) continue;
				if (stack.getType() == Material.ARROW) continue;
				if (stack.getType().name().contains("PICKAXE")) continue;
				if (stack != null && stack.getItemMeta() != null && stack.getItemMeta().getDisplayName() != null && stack.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLUE + "Throwable TNT")) continue;
				
				cPlayer.getWorld().dropItemNaturally(cPlayer.getLocation(), stack);
			}
		}
		
		if (cPlayer.isInPlayMode(PlayModePlayer.class) || cPlayer.isInPlayMode(PlayModeDead.class)) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + cPlayer.getDisplayName() + ChatColor.WHITE + " left the game!");
		if (cPlayer.getSelectedTeam() != null) cPlayer.getSelectedTeam().leave(cPlayer);		
	}

}
