package net.planckton.creeperwars.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.playmode.PlayModeSpectator;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;

public class PhaseGrace extends GamePhase {

	//420
	private int countdown = 80; 
	
	@Override
	public void onPhaseStart() {
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			int playerSize = team.getStartPlayers().size();
			
			for (CreeperPlayer player : team.getPlayers()) {
				if (playerSize == 1) {
					player.setHaste(true);
					player.setSpeed(true);
					player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
					player.sendMessage(Warriors.getInstance().getPREFIX() + "You have got a disadvantage, we got your back!");
				} else if (playerSize == 2) {
					player.setHaste(true);
					player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
					player.sendMessage(Warriors.getInstance().getPREFIX() + "You have got a disadvantage, we got your back!");
				}
				
				player.chat.addAvailableChats(team.getChat());
				player.chat.addListeningChats(team.getChat());
				player.chat.setChatChannel(team.getChat());
			}
		}
	}

	@Override
	public void run() {
		countdown--;
		PlayModePlayer.updateTime(countdown);
		
		if (countdown == 300) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "5" + ChatColor.WHITE + " minutes before the gates opens!");
		if (countdown == 60) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "1" + ChatColor.WHITE +  " minute before the gates opens!");
		if (countdown == 30) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "30" + ChatColor.WHITE +  " seconds before the gates opens!");
		if (countdown == 10) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "10" + ChatColor.WHITE +  " seconds before the gates opens!");
		
		if (countdown == 0) {
			Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + ChatColor.WHITE + "The gates has been blown up!");
			GameManager.setState(GamePhases.PVP);
			PlayModePlayer.updatePhase("PvP");
		}
	}

	@Override
	public void onJoin(LythrionPlayer player) {
		player.setPlayMode(PlayModeSpectator.class);
	}

	@Override
	public void onQuit(LythrionPlayer player) {
		CreeperPlayer creeperPlayer = (CreeperPlayer) player;	
		if (creeperPlayer.isInPlayMode(PlayModePlayer.class)) {
			for (ItemStack stack : creeperPlayer.getInventory().getContents()) {
				if (creeperPlayer.getSelectedKit().getItems().contains(stack)) continue;
				if (stack == null) continue;
				if (stack.getType() == Material.ARROW) continue;
				if (stack.getType().name().contains("PICKAXE")) continue;
				if (stack != null && stack.getItemMeta() != null && stack.getItemMeta().getDisplayName() != null && stack.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLUE + "Throwable TNT")) continue;
				
				creeperPlayer.getWorld().dropItemNaturally(creeperPlayer.getLocation(), stack);
			}
		}
		
		if (creeperPlayer.isInPlayMode(PlayModePlayer.class)) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + creeperPlayer.getDisplayName() + ChatColor.WHITE + " left the game!");
		if (creeperPlayer.getSelectedTeam() != null) creeperPlayer.getSelectedTeam().leave(creeperPlayer);		
	}

}
 