package net.planckton.creeperwars.game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.position.PositionManager;

public class GameManager extends Manager implements Listener, Runnable {
	
	@Getter private static GamePhases phase;	
	@Getter private int maxPlayers = 16;
	
	@Getter private BukkitTask task;	
	@Getter private World world;
	
	@Getter private ArrayList<Block> blocks = new ArrayList<>();	 
	@Getter private Scoreboard scoreboard;

	public GameManager() {
		super(PositionManager.class);
	}
	
	@Override
	public void onEnable() {
		setState(GamePhases.LOBBY);
		
		world = Bukkit.createWorld(new WorldCreator("mapCreeperWar"));
		world.setAutoSave(false);
		
		this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

		this.scoreboard.registerNewTeam("everyone");		

		Objective objective = this.scoreboard.registerNewObjective(ChatColor.RED + "❤", "health");
		objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
		
		task = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), this, 0, 20);		
	}

	@Override
	public void onDisable() {
		task.cancel();
		task = null;		
	}
	
	@Override
	public void run() {
		getPhase().getGamePhase().run();		
	}

	public static void setState(GamePhases newPhase) {
		if(phase != null) phase.getGamePhase().onPhaseEnd();
		
		phase = newPhase;
		newPhase.getGamePhase().onPhaseStart();		
		System.out.println("[Warriors] Updated phase.");
	}
	
	public static void join(LythrionPlayer player) {
		phase.getGamePhase().onJoin(player);
	}
	
	private static void leave(LythrionPlayer player) {
		phase.getGamePhase().onQuit(player);
	}		
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreakEvent(BlockBreakEvent event) {
		event.getBlock().getDrops().clear();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(LythrionPlayerJoinEvent event) {		
		join(event.getLythrionPlayer());
		
		event.getLythrionPlayer().chat.setChatChannel(ChatManager.CHANNEL_GLOBAL);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(LythrionPlayerQuitEvent event) {		
		event.getLythrionPlayer().cleanTitle();
		leave(event.getLythrionPlayer());
	}
	
	public void setTrackerPhase() {
		int creepersAlive = 0;
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			if (team.getCreeper().getHealth() != 0) creepersAlive++;
		}
		
		if (creepersAlive == 1) {
			setState(GamePhases.TRACKER);
			return;
		}
	}
	
	public static boolean shouldWeEndGame() {
		int teamsAlive = 0;
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			if (team.getPlayers().size() != 0) teamsAlive++;
		}
		
		if (teamsAlive == 1) return true;
		else return false;
	}
	
	public boolean hasBlock(Block block) {
		return blocks.contains(block);
	}
	
	public void addBlock(Block block) {
		blocks.add(block);
	}
	
	public void removeBlock(Block block) {
		blocks.remove(block);
	}
}
