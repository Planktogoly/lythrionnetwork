package net.planckton.creeperwars.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import net.md_5.bungee.api.ChatColor;
import net.planckton.blood.GameStatus;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModeDead;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.playmode.PlayModeSpectator;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeManager;

public class PhasePreGame extends GamePhase {

	private int countdown = 10;
	
	Random random = new Random();
	@SuppressWarnings("unchecked")
	@Override
	public void onPhaseStart() {
		if (PhaseLobby.getTask() != null) {
			PhaseLobby.getTask().cancel();
		}
		
		LythrionLib.updateGameStatus(GameStatus.INGAME);
		TeamManager teamManager = Manager.GetManager(TeamManager.class);
		
		for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModeLobby.class)) {
			CreeperPlayer creeperPlayer = (CreeperPlayer) player;
			
			ArrayList<Team> players = new ArrayList<>(teamManager.getTeams());			
			Collections.sort(players,new Comparator<Team>() {
			    @Override
			    public int compare(Team a, Team b) {
			        return a.getPlayers().size() - b.getPlayers().size();
			    }
			});	
			
			if (creeperPlayer.getSelectedTeam() == null) players.get(0).join(creeperPlayer);	
			
			player.setPlayMode(PlayModePlayer.class);
			PlayModePlayer.updateTime(420);
			Warriors.getInstance().getCoinsEarned().put((CreeperPlayer) player, 0);
		}
		
		for (Team team : teamManager.getTeams()) {
			if (team.getPlayers().size() == 0) team.getCreeper().killCreeper(false, null);
			
			team.setStartPlayers((ArrayList<CreeperPlayer>) team.getPlayers().clone());
			PlayModePlayer.updatePlayers(team, true);
		} 
	}

	@Override
	public void run() {		
		countdown--;
		ChatColor color = ChatColor.GREEN;
		if (countdown == 4) {
			color = ChatColor.GREEN;
		} else if (countdown == 3) {
			color = ChatColor.YELLOW;
		} else if (countdown == 2) {
			color = ChatColor.GOLD;
		}else if (countdown == 1) {
			color = ChatColor.RED;
		} 
		
		for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
			if (countdown != 0) player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
			player.sendTitle(color + "" + countdown, "", 0, 21, 0);
		}
		
        if (countdown == 0) {
    		for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
    			player.sendTitle("&eLet the games begin", "&aGood luck and have fun!");
    			player.playSound(Sound.ENDERDRAGON_GROWL);
    		}			
			GameManager.setState(GamePhases.GRACE);
			
	        Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
	    		for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
	    			player.sendMessage(ChatColor.GOLD.toString() + ChatColor.BOLD + "TIP: " + ChatColor.YELLOW + "Right click your Creeper to buy upgrades for your team!");
	    			player.playSound(Sound.CHICKEN_EGG_POP);
	    		}
	        }, 20 * 5);
			return;
		}
    }

	@Override
	public void onJoin(LythrionPlayer player) {		
		player.setPlayMode(PlayModeSpectator.class);
	}

	@Override
	public void onQuit(LythrionPlayer player) {
		CreeperPlayer cPlayer = (CreeperPlayer) player;	
		
		if (cPlayer.isInPlayMode(PlayModePlayer.class) || cPlayer.isInPlayMode(PlayModeDead.class)) Bukkit.broadcastMessage(Warriors.getInstance().getPREFIX() + cPlayer.getDisplayName() + ChatColor.WHITE + " left the game!");
		cPlayer.getSelectedTeam().leave(cPlayer);	
	}

}
