package net.planckton.creeperwars.game;

import lombok.RequiredArgsConstructor;
import net.planckton.lib.player.LythrionPlayer;

@RequiredArgsConstructor
public abstract class GamePhase {
	abstract public void onPhaseStart();
	public void onPhaseEnd() { };
	
	abstract public void run();
	
	abstract public void onJoin(LythrionPlayer player);
	abstract public void onQuit(LythrionPlayer player);
	//abstract public void onLogin(PlayerLoginEvent event, Rank rank);
	
	public void onDeath(LythrionPlayer player) { }
}
