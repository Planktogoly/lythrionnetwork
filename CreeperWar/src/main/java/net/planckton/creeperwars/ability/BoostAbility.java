package net.planckton.creeperwars.ability;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.PlayerManager;

public class BoostAbility extends Ability {

	@Getter private final static FancyItem ITEM = new FancyItem(Material.IRON_PICKAXE)
			.name(ChatColor.GRAY + "Miner Pickaxe")
            .unbreakable();
	
	private final Warriors WARRIRORS = Warriors.getInstance();
	
	private BukkitTask praticleTask;
	
	public BoostAbility(CreeperPlayer player) {
		super(25, 25, ITEM, true, "GoldDigger", player);
	}
	
	@Override
	public void run() {		
		for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 6, 6, 6)) {
			if (!(entity instanceof Player)) continue;
			
			CreeperPlayer gPlayer = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) entity);
			gPlayer.playSound(Sound.BLAZE_BREATH);
		}
		player.cleaPotionEffects();
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 10, 1, true, false));		
		
		praticleTask = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			if (!player.isInPlayMode(PlayModePlayer.class)) {
				praticleTask.cancel();
				player.cleaPotionEffects();
				return;
			}
			WARRIRORS.playEffect(player.getLocation(), Effect.SPELL, 0, 0, 0.2f, 0.2f, 0.2f, 0.0f, 100, 3);
		}, 0, 20);
		
		Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
			praticleTask.cancel();
			if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
		}, 20 * 11);
		
		setUseable(false);
		task = 	Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			int cooldown = getCooldown();
			cooldown--;
			player.setLevel(cooldown);
			setCooldown(cooldown);
			if (cooldown == 0) {
				setUseable(true);
				player.playSound(player.getLocation(), Sound.NOTE_PIANO, 3, 1);
				player.sendActionBarMessage(ChatColor.GREEN + "Boost is recharged!");
				setCooldown(getFinalCooldown());
				task.cancel();
			}
		}, 0, 20);
	}

}
