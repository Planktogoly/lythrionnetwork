package net.planckton.creeperwars.ability;

import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import lombok.Setter;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;
 
public abstract class Ability implements Runnable {
	
	@Getter @Setter private int finalCooldown;
	@Getter @Setter private int cooldown;
	@Getter @Setter private FancyItem item;
	@Getter @Setter private boolean useable;
	@Getter @Setter private String name;  
	
	protected CreeperPlayer player;
	@Getter	protected BukkitTask task;
	
	public Ability(int finalCooldown, int cooldown, FancyItem item, boolean useable, String name, CreeperPlayer player) {
		this.finalCooldown = finalCooldown;
		this.cooldown = cooldown;
		this.item = item;
		this.useable = useable;
		this.name = name;
		this.player = player;
	}
}
