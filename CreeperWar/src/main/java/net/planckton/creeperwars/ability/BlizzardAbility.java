package net.planckton.creeperwars.ability;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import lombok.Setter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.kit.ArcherKit;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.item.FancyItem;

public class BlizzardAbility extends Ability {
	
	@Getter @Setter private BukkitTask arrowTask;

	@Getter private final static FancyItem ITEM = new FancyItem(Material.BOW)
			.name(ChatColor.GREEN + "Archer bow")
			.lore(ChatColor.GRAY + "Right click to use Blizzard")
			.unbreakable();
	
	@Getter @Setter private boolean activated = false;
	@Getter @Setter private boolean fullydrawn = false;
	
	@Getter @Setter private BukkitTask removeTask;
	
	@Getter @Setter private CreeperPlayer target;
	
	public BlizzardAbility(CreeperPlayer player) {
		super(45, 45, ITEM, true, "Blizzard", player);
		
		runTask();
	}
	
	public void removeAbility() {
		if (arrowTask != null) arrowTask.cancel();
	}
	
	public void runTask(CreeperPlayer player) {
		this.target = player;
		run();
	}

	@Override
	public void run() {
		if (GameManager.getPhase() == GamePhases.GRACE) {
			player.sendMessage(ChatColor.RED + "You cannot use your ability in phase Grace.");
			return;
		}
		
		target.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * 5, 1, false, false));
		target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 5, 10, false, false));
		target.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 5, 128, false, false));
		target.playSound(Sound.GHAST_SCREAM);
		
		removeTask = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), new Runnable() {
			int i = 0;
			
			@Override
			public void run() {
				if (i == 20) removeTask.cancel();
				Warriors.getInstance().playEffect(target.getLocation(), Effect.SNOWBALL_BREAK, 0, 0, 0.9f, 0.9f, 0.9f, 0, 100, 10);
				Warriors.getInstance().playEffect(target.getLocation(), Effect.CLOUD, 0, 0, 0.8f, 0.9f, 0.8f, 0, 150, 8);
				Warriors.getInstance().playEffect(target.getLocation(), Effect.EXPLOSION, 0, 0, 0.8f, 0.9f, 0.8f, 0, 100, 8);
				i++;
			}
		}, 0, 5);

		setUseable(false);
		task = 	Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			int cooldown = getCooldown();
			cooldown--;
			player.setLevel(cooldown);
			setCooldown(cooldown);
			if (cooldown == 0) {
				setUseable(true);
				player.playSound(player.getLocation(), Sound.NOTE_PIANO, 3, 1);
				player.sendActionBarMessage(ChatColor.GREEN + "Blizzard is recharged!");
				setCooldown(getFinalCooldown());
				task.cancel();
			}
		}, 0, 20);
	}
	
	public void runTask() {
		arrowTask =  Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			   if (!player.isInPlayMode(PlayModePlayer.class)) return;
			   if (GameManager.getPhase() == GamePhases.GRACE || GameManager.getPhase() == GamePhases.PREGAME || GameManager.getPhase() == GamePhases.POST_GAME) return;
			   if (player.getInventory().containsAtLeast(((ArcherKit)player.getSelectedKit()).getArrow().stack(), 12)) return;
			   
			   player.playSound(Sound.ITEM_PICKUP);			
			   
			   if (player.getInventory().getItem(8) == null) player.getInventory().setItem(8, ((ArcherKit)player.getSelectedKit()).getArrow().setAmount(1).stack());			   
			   else if (player.getInventory().getItem(8).getType() == Material.ARROW) player.getInventory().addItem(((ArcherKit)player.getSelectedKit()).getArrow().setAmount(1).stack());
			   else player.getInventory().addItem(((ArcherKit)player.getSelectedKit()).getArrow().setAmount(1).stack());
		   }, 0, 20 * 7);
	}

}
