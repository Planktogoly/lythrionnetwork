package net.planckton.creeperwars.ability;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.kit.BomberKit;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.PlayerManager;

public class FlareWindAbility extends Ability {
	
	@Getter private BukkitTask task;
	@Getter private BukkitTask tntTask;

	@Getter private final static FancyItem ITEM = new FancyItem(Material.FEATHER)
			.name(ChatColor.WHITE.toString() + ChatColor.BOLD + "FlareWind")
			.lore(ChatColor.GRAY + "Right click to use FlareWind");
	
	private final Warriors WARRIRORS = Warriors.getInstance();

	public FlareWindAbility(CreeperPlayer player) {
		super(25, 25, ITEM, true, "FlareWind", player);
		
		runTask();
	}
	
	public void removeAbility() {
		if (task != null) task.cancel();
		if (tntTask != null) tntTask.cancel();
	}

	@Override
	public void run() {
		if (GameManager.getPhase() == GamePhases.GRACE) {
			player.sendMessage(ChatColor.RED + "You cannot use your ability in phase Grace.");
			return;
		}
		
		   double radiusSquared = 16.0D;
		   
		    List<Entity> entities = player.getBukkitPlayer().getNearbyEntities(4.0D, 4.0D, 4.0D);
		    for (Entity entity : entities) {
		      if (entity.getLocation().distanceSquared(player.getLocation()) <= radiusSquared) {
		        if ((entity instanceof Player)) {
		          Player damager = (Player)entity;
		          damager.teleport(damager.getLocation().add(0, 0.3, 0));
		          damager.setVelocity(damager.getLocation().getDirection().multiply(-1).multiply(2));
		          damager.damage(2.0D);
		          
		          WARRIRORS.playEffect(damager.getLocation(), Effect.CLOUD, 0, 0, 2.0F, 2.0F, 2.0F, 0.0F, 300, 5);
		        }
		      }
		    }
		    
			for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 8, 8, 8)) {
				if (!(entity instanceof Player)) continue;
				
				CreeperPlayer gPlayer = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) entity);
				gPlayer.playSound(Sound.BAT_LOOP);
			}

		    WARRIRORS.playEffect(player.getLocation(), Effect.SPELL, 0, 0, 1.0F, 1.0F, 1.0F, 0.0F, 100, 30);
		    WARRIRORS.playEffect(player.getLocation(), Effect.CRIT, 0, 0, 1.0F, 1.0F, 1.0F, 1.0F, 200, 30);
		    WARRIRORS.playEffect(player.getLocation(), Effect.MAGIC_CRIT, 0, 0, 1.0F, 1.0F, 1.0F, 1.0F, 300, 30);		
		    
			setUseable(false);
			task = 	Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
				int cooldown = getCooldown();
				cooldown--;
				player.setLevel(cooldown);
				setCooldown(cooldown);
				if (cooldown == 0) {
					setUseable(true);
					player.playSound(player.getLocation(), Sound.NOTE_PIANO, 3, 1);				
					player.sendActionBarMessage(ChatColor.GREEN + "FlareWind is recharged!");
					setCooldown(getFinalCooldown());
					task.cancel();
				}
			}, 0, 20);
	}
	
	public void runTask() {
		tntTask =  Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			   if (!player.isInPlayMode(PlayModePlayer.class)) return;
			   if (GameManager.getPhase() == GamePhases.GRACE || GameManager.getPhase() == GamePhases.PREGAME || GameManager.getPhase() == GamePhases.POST_GAME) return;
			   if (player.getInventory().containsAtLeast(((BomberKit)player.getSelectedKit()).getTnT().stack(), 3)) return;
			   
			   player.playSound(Sound.ITEM_PICKUP);			
			   
			   if (player.getInventory().getItem(8) == null) player.getInventory().setItem(8, ((BomberKit)player.getSelectedKit()).getTnT().setAmount(1).stack());			   
			   else if (player.getInventory().getItem(8).getType() == Material.TNT) player.getInventory().addItem(((BomberKit)player.getSelectedKit()).getTnT().setAmount(1).stack());
			   else player.getInventory().addItem(((BomberKit)player.getSelectedKit()).getTnT().setAmount(1).stack());
		   }, 0, 20 * 20);
	}

}
