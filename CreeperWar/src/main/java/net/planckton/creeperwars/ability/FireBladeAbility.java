package net.planckton.creeperwars.ability;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.PlayerManager;

public class FireBladeAbility extends Ability {

	@Getter private final static FancyItem ITEM = new FancyItem(Material.STONE_SWORD)
			.name(ChatColor.BLUE + "Warrior Sword")
			.unbreakable();
	
	private final Warriors WARRIRORS = Warriors.getInstance();
	
	public FireBladeAbility(CreeperPlayer player) {
		super(30, 30, ITEM, true, "FireBlade", player);
	}

	@Override
	public void run() {
		if (GameManager.getPhase() == GamePhases.GRACE) {
			player.sendMessage(ChatColor.RED + "You cannot use your ability in phase Grace.");
			return;
		}
		
		Location loc = player.getLocation();
	    double radiusSquared = 16.0D;
	   
	    List<Entity> entities = player.getBukkitPlayer().getNearbyEntities(4.0D, 4.0D, 4.0D);
	    for (Entity entity : entities) {
	      if (entity.getLocation().distanceSquared(player.getLocation()) <= radiusSquared) {
	        if ((entity instanceof Player)) {
	        	Player damager = (Player) entity;
	        	Location loc2 = damager.getLocation();
	        	
	        	damager.damage(3.0D);
	        	damager.setFireTicks(20 * 3);
	        	WARRIRORS.playEffect(loc2, Effect.LAVADRIP, 0, 0, 0.5F, 0.5F, 0.5F, 0.0F, 100, 20);
	        	WARRIRORS.playEffect(loc2, Effect.LAVA_POP, 0, 0, 0.5F, 0.5F, 0.5F, 0.0F, 100, 20);
	        }
	      }
	    }
	    
		for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 8, 8, 8)) {
			if (!(entity instanceof Player)) continue;
			
			CreeperPlayer gPlayer = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) entity);
			gPlayer.playSound(Sound.ZOMBIE_REMEDY);
		}
	    
	    WARRIRORS.playEffect(loc, Effect.FLAME, 0, 0, 2.0F, 2.0F, 2.0F, 0.0F, 100, 50);
	    WARRIRORS.playEffect(loc, Effect.COLOURED_DUST, 0, 14, 1.0F, 1.0F, 1.0F, 0.0F, 100, 20);
	    WARRIRORS.playEffect(loc, Effect.COLOURED_DUST, 0, 6, 1.0F, 1.0F, 1.0F, 0.0F, 100, 20);
		
		setUseable(false);
		task = 	Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			int cooldown = getCooldown();
			cooldown--;
			player.setLevel(cooldown);
			setCooldown(cooldown);
			if (cooldown == 0) {
				setUseable(true);
				player.playSound(player.getLocation(), Sound.NOTE_PIANO, 3, 1);
				player.sendActionBarMessage(ChatColor.GREEN +  "FireBlade is recharged!");
				setCooldown(getFinalCooldown());
				task.cancel();
			}
		}, 0, 20);
	}

}
