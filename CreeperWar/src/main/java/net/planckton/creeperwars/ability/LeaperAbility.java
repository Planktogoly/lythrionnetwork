package net.planckton.creeperwars.ability;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.PlayerManager;

public class LeaperAbility extends Ability {

	@Getter private final static FancyItem ITEM = new FancyItem(Material.IRON_AXE)
			.name(ChatColor.RED + "Leaper")
			.lore(ChatColor.GRAY + "Right click to leap forward")
			.unbreakable();
	
	public LeaperAbility(CreeperPlayer player) {
		super(10, 10, ITEM, true, "Leaper", player);		
	}

	public void run() {
		if (GameManager.getPhase() == GamePhases.GRACE) {
			player.sendMessage(ChatColor.RED + "You cannot use your ability in phase Grace.");
			return;
		}
		
		for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 12, 12, 12)) {
			if (!(entity instanceof Player)) continue;
			
			CreeperPlayer gPlayer = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) entity);
			gPlayer.playSound(Sound.BAT_TAKEOFF);
		}
		
		if (90 <= player.getBukkitPlayer().getEyeLocation().getYaw() || 40 >= player.getBukkitPlayer().getEyeLocation().getYaw()) {
			player.setVelocity(player.getBukkitPlayer().getEyeLocation().getDirection().normalize().multiply(1).setY(0.8));	
		} else {
			player.setVelocity(player.getBukkitPlayer().getEyeLocation().getDirection().normalize().multiply(2).setY(0.5));
		}
		
		setUseable(false);
		task = 	Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			int cooldown = getCooldown();
			cooldown--;
			player.setLevel(cooldown);
			setCooldown(cooldown);
			if (cooldown == 0) {
				setUseable(true);
				player.playSound(player.getLocation(), Sound.NOTE_PIANO, 3, 1);
				player.sendActionBarMessage(ChatColor.GREEN + "Leap is recharged!");
				setCooldown(getFinalCooldown());
				task.cancel();
			}
		}, 0, 20);
		
		player.getBukkitPlayer().setMetadata("falldamage", new FixedMetadataValue(Warriors.getInstance(), "false"));
		Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
			player.getBukkitPlayer().removeMetadata("falldamage", Warriors.getInstance());
		}, 20 * 2);		
	}

}
