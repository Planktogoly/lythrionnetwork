package net.planckton.creeperwars.player;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;
import lombok.Setter;
import net.planckton.creeperwars.ability.Ability;
import net.planckton.creeperwars.ability.BlizzardAbility;
import net.planckton.creeperwars.ability.FlareWindAbility;
import net.planckton.creeperwars.kit.Kit;
import net.planckton.creeperwars.team.Team;
import net.planckton.lib.player.LythrionPlayer;

public class CreeperPlayer extends LythrionPlayer { 
	
	@Getter @Setter private Kit selectedKit = null;
	@Getter @Setter private Team selectedTeam = null;
	@Getter private Ability Ability = null;
	
	@Getter @Setter private Long lastRightClick = null;
	
	@Getter @Setter private boolean haste = false;
	@Getter @Setter private boolean speed = false;
	
	@Getter @Setter private int countdown = 11;
	@Getter @Setter private BukkitTask deathTask;
	
	@Getter private ArrayList<ItemStack> keepItems = new ArrayList<>();
	
	@Getter private int crystalsEarned;
	
	@Override
	protected boolean load() {  
		return super.load();
	}
	
	@Override
	protected boolean loadAdditional() {
		return super.loadAdditional();
	}
	
	public void setAbility(Ability ability) {
		if (this.Ability instanceof BlizzardAbility) {
			BlizzardAbility abili = (BlizzardAbility) this.Ability;
			abili.removeAbility();
		} else if (this.Ability instanceof FlareWindAbility) {
			FlareWindAbility abili = (FlareWindAbility) this.Ability;
			abili.removeAbility();
		}
		
		this.Ability = ability;
	}	
}
