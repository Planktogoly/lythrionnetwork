package net.planckton.creeperwars.player;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GamePhase;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.lib.player.PlayerManager;

public class CreeperPlayerManager extends PlayerManager<CreeperPlayer>{

	public CreeperPlayerManager() {
		super(CreeperPlayer.class);
		
		enablePlayerCacheTime(-1);
	}
	
	@Override
	protected int getPerPlayerCacheTime(CreeperPlayer player) {
		if((player.isInPlayMode(PlayModeLobby.class) && !Warriors.getInstance().isInPhase(GamePhase.class)) || player.isInPlayMode(PlayModePlayer.class)) return super.getPerPlayerCacheTime(player);
		
		return 0;
	}

}
