package net.planckton.creeperwars.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Entity;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.EntityArrow;
import net.planckton.creeperwars.Warriors;

public class Utils {
	
	public Utils() {
	}
	
	@Getter private static final ArrayList<Material> materials = new ArrayList<>(); {
		materials.add(Material.SHEARS);
		materials.add(Material.STONE_PICKAXE);
		materials.add(Material.IRON_PICKAXE);
		materials.add(Material.DIAMOND_PICKAXE);
		materials.add(Material.FISHING_ROD);
		materials.add(Material.STONE_SWORD);
		materials.add(Material.IRON_SWORD);
		materials.add(Material.DIAMOND_SWORD);
		
		materials.add(Material.IRON_BOOTS);
		materials.add(Material.IRON_LEGGINGS);
		materials.add(Material.IRON_CHESTPLATE);
		materials.add(Material.IRON_HELMET);
		
		materials.add(Material.CHAINMAIL_BOOTS);
		materials.add(Material.CHAINMAIL_LEGGINGS);
		materials.add(Material.CHAINMAIL_CHESTPLATE);
		materials.add(Material.CHAINMAIL_HELMET);
		
		materials.add(Material.DIAMOND_BOOTS);
		materials.add(Material.DIAMOND_LEGGINGS);
		materials.add(Material.DIAMOND_CHESTPLATE);
		materials.add(Material.DIAMOND_HELMET);
	}	
	
	@Getter private final static ArrayList<Material> grass = new ArrayList<>(); {
		grass.add(Material.LONG_GRASS);
		grass.add(Material.YELLOW_FLOWER);
		grass.add(Material.RED_ROSE);
		grass.add(Material.CACTUS);
		grass.add(Material.BROWN_MUSHROOM);
		grass.add(Material.CROPS);
		grass.add(Material.DEAD_BUSH);
		grass.add(Material.DOUBLE_PLANT);
		grass.add(Material.RED_MUSHROOM);
		grass.add(Material.MELON_STEM);
		grass.add(Material.PUMPKIN_STEM);
	}
	
	@Getter private final static ArrayList<Material> armor = new ArrayList<>(); {
		armor.add(Material.LEATHER_BOOTS);
		armor.add(Material.LEATHER_LEGGINGS);
		armor.add(Material.LEATHER_CHESTPLATE);
		armor.add(Material.LEATHER_HELMET);
		
		armor.add(Material.IRON_BOOTS);
		armor.add(Material.IRON_LEGGINGS);
		armor.add(Material.IRON_CHESTPLATE);
		armor.add(Material.IRON_HELMET);
		
		armor.add(Material.GOLD_BOOTS);
		armor.add(Material.GOLD_LEGGINGS);
		armor.add(Material.GOLD_CHESTPLATE);
		armor.add(Material.GOLD_HELMET);
		
		armor.add(Material.CHAINMAIL_BOOTS);
		armor.add(Material.CHAINMAIL_LEGGINGS);
		armor.add(Material.CHAINMAIL_CHESTPLATE);
		armor.add(Material.CHAINMAIL_HELMET);
		
		armor.add(Material.DIAMOND_BOOTS);
		armor.add(Material.DIAMOND_LEGGINGS);
		armor.add(Material.DIAMOND_CHESTPLATE);
		armor.add(Material.DIAMOND_HELMET);
	}
	
	public static boolean isArmor(Material material) {
		for (Material mat : armor) {
			if (mat == material) return true;
		}
		return false;
	}
	
	public static boolean isSword(Material material) {
		if (material == Material.STONE_SWORD) return true;
		else if (material == Material.IRON_SWORD) return true;
		else if (material == Material.DIAMOND_SWORD) return true;
		return false;
	}
	
	public static boolean isBow(Material material) {
		return material == Material.BOW ? true : false;
	}
	
	public static boolean isValidBlock(int x, int y, int z) {
		return (x != -1) && (y != -1) && (z != -1);
    }
	
	@Setter private static Block block = null;
	
	public static Block getBlock(Entity entity) {
		Bukkit.getScheduler().runTask(Warriors.getInstance(), new Runnable() {
			
	        public void run() {
	          try {
	            EntityArrow entityArrow = ((CraftArrow)entity).getHandle();
	            
	            Field fieldX = EntityArrow.class.getDeclaredField("d");
	            
	            Field fieldY = EntityArrow.class.getDeclaredField("e");
	            
	            Field fieldZ = EntityArrow.class.getDeclaredField("f");
	            
	            fieldX.setAccessible(true);
	            fieldY.setAccessible(true);
	            fieldZ.setAccessible(true);
	            
	            int x = fieldX.getInt(entityArrow);
	            int y = fieldY.getInt(entityArrow);
	            int z = fieldZ.getInt(entityArrow);
	            Bukkit.broadcastMessage(x + " " +  " " + y + " " + z + " ");
	            if (Utils.isValidBlock(x, y, z)) block = entity.getWorld().getBlockAt(x, y, z);
	          }
	          catch (NoSuchFieldException e1) {
	        	  e1.printStackTrace();
	          } catch (SecurityException e1) {
	        	  e1.printStackTrace();
	          } catch (IllegalArgumentException e1) {
	            e1.printStackTrace();
	          } catch (IllegalAccessException e1) {
	            e1.printStackTrace();
	          }
	        }
	      });
		return block;
	}

}
