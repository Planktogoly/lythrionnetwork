package net.planckton.creeperwars.utils;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Entity;

import net.minecraft.server.v1_8_R3.EntityArrow;
import net.planckton.creeperwars.Warriors;
import net.planckton.lib.lvariable.Callback;

public class GRunnable implements Runnable {
	
	Callback<Block> call;
	Entity entity = null;
	
	public GRunnable(Callback<Block> c, Entity entity) {
		this.call = c;
		this.entity = entity;
		
		Bukkit.getScheduler().runTask(Warriors.getInstance(), this);
	}

	@Override
	public void run() {
        try {
            EntityArrow entityArrow = ((CraftArrow)entity).getHandle();
            
            Field fieldX = EntityArrow.class.getDeclaredField("d");
            
            Field fieldY = EntityArrow.class.getDeclaredField("e");
            
            Field fieldZ = EntityArrow.class.getDeclaredField("f");
            
            fieldX.setAccessible(true);
            fieldY.setAccessible(true);
            fieldZ.setAccessible(true);
            
            int x = fieldX.getInt(entityArrow);
            int y = fieldY.getInt(entityArrow);
            int z = fieldZ.getInt(entityArrow);
            if (Utils.isValidBlock(x, y, z)) call.call(entity.getWorld().getBlockAt(x, y, z));
          }
          catch (NoSuchFieldException e1) {
        	  e1.printStackTrace();
          } catch (SecurityException e1) {
        	  e1.printStackTrace();
          } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
          } catch (IllegalAccessException e1) {
            e1.printStackTrace();
          }
	}
}
