package net.planckton.creeperwars.utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OreRunnable implements Runnable {

	private Material material;
	private Block block;
	
	@Override
	public void run() {
		block.setType(material);	
	}

}
