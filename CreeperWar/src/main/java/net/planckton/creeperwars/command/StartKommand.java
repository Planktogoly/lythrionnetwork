package net.planckton.creeperwars.command;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.lib.kommand.Kommand;
import net.planckton.lib.kommand.OnKommand;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.rank.Rank;

public class StartKommand extends Kommand implements OnKommand {
	
	public StartKommand() {
		super("start");
	}
	
	@Override
	public void onCommand(LythrionPlayer player, String[] arg) {
		if (!player.hasRankPower(Rank.VIP)) return;
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) {
			player.sendMessage(ChatColor.RED + "Game has already started!");
			return;
		}
		if (PlayModeManager.getPlayersPerMode(PlayModeLobby.class).size() < 3) {
			player.sendMessage(ChatColor.RED + "You need atleast 3 players to start a game");
			return;
		}
		
		GameManager.setState(GamePhases.PREGAME);		
	}

}
