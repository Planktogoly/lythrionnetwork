package net.planckton.creeperwars.creeper;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.team.Team;
import net.planckton.lib.holo.Holo;
import net.planckton.lib.holo.HoloManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.position.Position;
import net.planckton.lib.position.PositionManager;

public class CreeperBoss implements Listener {
	
	@Getter private double health;
	@Getter @Setter private int maxHealth;
	
	@Getter private String id;
	
	@Getter private Location loc;
	
	@Getter private Team team;
	@Getter private Creeper creeper;
	@Getter private Holo healthBar;
	@Getter private Holo creeperStand;
	
	@Getter private boolean creeperDeath = false;
	
	private boolean informTeam = true;
	private boolean doAttack = true;
	
	private final HoloManager HOLOMANAGER = (HoloManager) Manager.GetManager(HoloManager.class);
	
	public CreeperBoss(Team team, String id) {
		this.health = 500;
		this.maxHealth = 500;
		this.team = team;
		this.id = id;
		
		Position creeperPos = PositionManager.getPosition("guardianwar.team." + team.getName().toLowerCase() + ".guardian");
		
		this.loc = creeperPos.getPosition();
		
		this.creeper = creeperPos.getWorld().spawn(creeperPos.getPosition(), Creeper.class);
		creeper.setPowered(true);
		
		creeper.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 255));
		
		net.minecraft.server.v1_8_R3.Entity creepernmsEntity = ((CraftEntity)creeper).getHandle();
        NBTTagCompound creeperTag = creepernmsEntity.getNBTTag();
        
        if (creeperTag == null) creeperTag = new NBTTagCompound();
        
        creepernmsEntity.c(creeperTag);
        
        creeperTag.setInt("NoAI", 1);
        creeperTag.setInt("Silent", 1);        
        creepernmsEntity.f(creeperTag);
        
        creeper.setMetadata("creeper", new FixedMetadataValue(Warriors.getInstance(), team.getName()));
        creeper.setMetadata("id", new FixedMetadataValue(Warriors.getInstance(), id));
        team.getEntities().add(creeper);
        
        creeperStand = new Holo(creeperPos.getPosition().clone().add(0, 2.1, 0), "" + team.getColor() + ChatColor.BOLD + team.getName() + ChatColor.RESET + " Creeper");
        HOLOMANAGER.spawnHolo(creeperStand);
        healthBar = new Holo(creeperPos.getPosition().clone().add(0, 1.8, 0), ChatColor.GREEN + "▉▉▉▉▉▉▉▉▉▉");
        HOLOMANAGER.spawnHolo(healthBar);
		
		Bukkit.getPluginManager().registerEvents(this, Warriors.getInstance());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage2(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Creeper)) return;
		if (!(event.getEntity() instanceof Player)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Creeper)) return;		
		if (event.getCause() == DamageCause.ENTITY_ATTACK) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntityType() == EntityType.CREEPER)) return;
		
		double damage = event.getFinalDamage();
		Entity entity = event.getEntity();
		
		if (!entity.getMetadata("id").get(0).asString().equalsIgnoreCase(id)) return;
		
		if (event.getDamager() instanceof Player) {
			CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getDamager());
			if (!player.isInPlayMode(PlayModePlayer.class)) return;
			
			if (entity.getMetadata("creeper").get(0).asString().equalsIgnoreCase(player.getSelectedTeam().getName())) {
				event.setCancelled(true);
				return;
			}
			player.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.WHITE + "Creepers health: " + ChatColor.GRAY + (int) health +  ChatColor.WHITE + "/" + ChatColor.GRAY + maxHealth);	
			
			damage(damage, (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getDamager()));
		} else if (event.getDamager() instanceof TNTPrimed) {
            if (event.getDamager().hasMetadata("player")) {
                CreeperPlayer p = (CreeperPlayer) PlayerManager.getLythrionPlayer(Bukkit.getPlayer(event.getDamager().getMetadata("player").get(0).asString()));
                if (p == null) {
                	event.setCancelled(true);
                	return;
                }
    			if (!p.isInPlayMode(PlayModePlayer.class)) return;
    			
    			if (entity.getMetadata("creeper").get(0).asString().equalsIgnoreCase(p.getSelectedTeam().getName())) {
    				event.setCancelled(true);
    				return;
    			}
    			p.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.WHITE + "Creepers health: " + ChatColor.GRAY + (int) health +  ChatColor.WHITE + "/" + ChatColor.GRAY + maxHealth);	
    			
    			damage(damage, p);
            }
		} else {
			event.setCancelled(true);
			return;
		}
		
		event.setDamage(0.0D);	
		
	    List<Entity> entities = event.getEntity().getNearbyEntities(15.0D, 15.0D, 15.0D);
	    for (Entity playSoundEntity : entities) {
	    	if (playSoundEntity instanceof Player) ((Player) playSoundEntity).playSound(playSoundEntity.getLocation(), Sound.CREEPER_HISS, 1, 1);	    	
	    }
	    
	    if (doAttack) {
	    	new CreeperAttack(loc, this);
	    	Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
	    		doAttack = true;	    	
		    }, 20 * ThreadLocalRandom.current().nextInt(8, 15));
	    	doAttack = false;
	    }
	    
	    if (informTeam) {
	    	team.sendTitle(" ", "&cYour creeper is under attack!", 10, 60, 10);
	    	Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
		    	informTeam = true;	    	
		    }, 20 * 20);
	    	informTeam = false;
	    }   
	}
	
	  public String barGraph(double x, double y) {
		  double xDivideY = x / y;
          int percent = (int) (10.0 * xDivideY);
          
	      StringBuilder message = new StringBuilder(12 + 10);
	      message.append(ChatColor.GREEN);
	      
	      if (percent > 0) message.append(stringRepeat("▉", (int) percent));
      
	      message.append(ChatColor.RED);
	      
	      if (percent < 10) message.append(stringRepeat("▉", 10 - (int) percent));
	      return message.toString();
	  }
	  
	  public String stringRepeat(String newString, int n) {
	    StringBuilder builder = new StringBuilder(n * newString.length());
	    
	    for (int x = 0; x < n; x++) {
	    	builder.append(newString);
	    }
	    
	    return builder.toString();
	  }
	
	public void setHealthBar() {
		healthBar.updateLines(barGraph(health, maxHealth));
	}
	
	public void setHealth(double health) {
		this.health = health;
		setHealthBar();
	}
	
	public void damage(double damage, CreeperPlayer player) {
		if (health <= damage) {			
			killCreeper(true, player);
			Warriors.getInstance().addCrystals(player, player.balance.get().rewardCrystals(20));
			return;
		}	
		
		health -= damage;		
		setHealthBar();
	}
	
	public void killCreeper(boolean notify, CreeperPlayer player) {
		if (creeperDeath) return;
		
		creeper.remove();
		HOLOMANAGER.despawnHolo(creeperStand);
		HOLOMANAGER.despawnHolo(healthBar);
			
		if (notify) {
			for (LythrionPlayer plplayer : PlayModeManager.getPlayersPerMode(PlayModePlayer.class)) {
				plplayer.sendMessage(ChatColor.WHITE + "The " + team.getColor() + team.getName() + ChatColor.WHITE + " creeper has been killed by " + player.getSelectedTeam().getColor() + player.getName() + ChatColor.WHITE + "!");
			}			
			team.sendMessage(Warriors.getInstance().getPREFIX() + ChatColor.RED + "Your creeper died!");
			
			Warriors.getInstance().playEffect(loc, Effect.LARGE_SMOKE, 0, 0, 0.5f, 0.5f, 0.5f, 0.0f, 100, 10);
			Warriors.getInstance().playEffect(loc, Effect.PARTICLE_SMOKE, 0, 0, 0.5f, 0.5f, 0.5f, 0.0f, 200, 10);
			loc.getWorld().strikeLightningEffect(loc);
		}
		PlayModePlayer.updatePlayers(getTeam(), false);
		health = 0;
		
		Manager.GetManager(GameManager.class).setTrackerPhase();
		creeperDeath= true;
	}
	
}
