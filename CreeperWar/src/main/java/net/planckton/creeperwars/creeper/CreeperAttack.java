package net.planckton.creeperwars.creeper;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitTask;

import net.planckton.creeperwars.Warriors;

public class CreeperAttack implements Runnable {

	private BukkitTask particleTask;
	private Location loc;
	
	private CreeperBoss creeper;
	
	public CreeperAttack(Location loc, CreeperBoss creeper) {
		this.loc = loc;
		this.creeper = creeper;
		
		particleTask = Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), this, 0, 10);
	}
	
	int i = 0;
	
	@Override
	public void run() {	
		if (creeper.isCreeperDeath()) particleTask.cancel();
		if (i == 5) {
		    TNTPrimed tnt = (TNTPrimed)loc.getWorld().spawn(loc, TNTPrimed.class);
		    tnt.setMetadata("id", new FixedMetadataValue(Warriors.getInstance(), creeper.getId()));
		    tnt.setFuseTicks(0);	
		    particleTask.cancel();
			return;
		}
		
		if (i == 0 || i == 3) {
		    List<Entity> entities = creeper.getCreeper().getNearbyEntities(15.0D, 15.0D, 15.0D);
		    for (Entity playSoundEntity : entities) {
		    	if (playSoundEntity instanceof Player) ((Player) playSoundEntity).playSound(playSoundEntity.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);	    	
		    }
		}
	    
		Warriors.getInstance().playEffect(loc, Effect.PARTICLE_SMOKE, 0, 0, 0.4f, 0.7f, 0.4f, 0, 400, 8);	
		Warriors.getInstance().playEffect(loc, Effect.FLAME, 0, 0, 0.6f, 1.0f, 0.6f, 0, 50, 9);	
		Warriors.getInstance().playEffect(loc, Effect.LAVA_POP, 0, 0, 0.5f, 1.0f, 0.5f, 0, 5, 8);
		i++;
    }

}
