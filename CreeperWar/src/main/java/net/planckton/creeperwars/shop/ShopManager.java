package net.planckton.creeperwars.shop;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import lombok.Getter;
import net.planckton.creeperwars.Database;
import net.planckton.lib.manager.Manager;

public class ShopManager extends Manager {

	@Getter private LinkedHashMap<String, LinkedList<ShopItem>> items = null;
	
	@Override
	public void onEnable() {
		items = Database.getShopItems();
	}

	@Override
	public void onDisable() {
				
	}

}
