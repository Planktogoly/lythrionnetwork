package net.planckton.creeperwars.shop;

import org.bukkit.Material;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.item.FancyItem;

@AllArgsConstructor
public class ShopItem {
	
	@Getter private String name;
	@Getter private FancyItem item;
	@Getter private Material ore;
	@Getter private int amountOre;
	@Getter private int amount;
	@Getter private boolean unbreakable;
	
	public FancyItem createItem() {
		FancyItem icon = new FancyItem(item).name(ChatColor.YELLOW + name + " x" + getAmount());
		icon.lore("");
		
		if (getOre() == Material.COAL) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + getAmountOre() + " Coal");
		if (getOre() == Material.IRON_INGOT) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + getAmountOre() + " Iron");
		if (getOre() == Material.GOLD_INGOT) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + getAmountOre() + " Gold");
		if (getOre() == Material.DIAMOND) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + getAmountOre() + " Diamonds");		
		if (getOre() == Material.EMERALD) icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + getAmountOre() + " Emeralds");

		icon.lore(ChatColor.BLUE + "Click to buy this item");
		icon.hideAllFlags();		
		return icon;
	}

}
