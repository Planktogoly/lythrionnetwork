package net.planckton.creeperwars.kit;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public abstract class Kit {
	
	@Getter private FancyItem compass = new FancyItem(Material.COMPASS).name(ChatColor.GREEN.toString() + ChatColor.BOLD + "Tracker").unbreakable();

	@Getter private final @NonNull String name;
	@Getter private final boolean defaultKit;
	@Getter private final @NonNull FancyItem icon;
	@Getter private final ChatColor color;
	@Getter private final int priceGeneral;
	@Getter private ArrayList<ItemStack> items = new ArrayList<>();
	
	public Kit(String name, boolean defaultKit, FancyItem icon, ChatColor color, int priceGeneral) {
		this.name = name;
		this.defaultKit = defaultKit;
		this.icon = icon;
		this.color = color;
		this.priceGeneral = priceGeneral;
		
		items.add(compass.stack());
	}
	
	abstract public void equip(CreeperPlayer player);
	
} 
