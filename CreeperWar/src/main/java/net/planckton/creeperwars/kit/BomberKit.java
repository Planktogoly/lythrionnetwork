package net.planckton.creeperwars.kit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public class BomberKit extends Kit implements Listener {

	private static final FancyItem ICON = new FancyItem(Material.TNT)
			.name("" + ChatColor.RED + ChatColor.BOLD + "Bomber Kit")
			.lore("")
			.lore(ChatColor.WHITE + "Ability:" + ChatColor.GREEN + " FlareWind")
			.lore(ChatColor.WHITE + "Description:" + ChatColor.GREEN + " Knocks your enemies away from you")
			.lore(ChatColor.RED + "Cooldown: 25 sec")
			.lore("")
			.lore(ChatColor.GRAY + "Will contain:")
			.lore(ChatColor.DARK_GRAY + "Full Leather except for the Golden Chestplate, Golden Pants")
			.lore("");
	
	private final ItemStack SWORD = new FancyItem(Material.WOOD_SWORD)
			.name(ChatColor.GRAY + "Default sword").unbreakable().stack();
	private final ItemStack PICKAXE = new FancyItem(Material.WOOD_PICKAXE)
			.name(ChatColor.GRAY + "Default pickaxe").unbreakable().stack();
	private final FancyItem TNT = new FancyItem(Material.TNT, 5).name(ChatColor.BLUE + "Throwable TNT");
	
	private final ItemStack LEATHER_BOOTS = new FancyItem(Material.LEATHER_BOOTS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack GOLD_LEGGINGS = new FancyItem(Material.GOLD_LEGGINGS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack GOLD_CHESTPLATE = new FancyItem(Material.GOLD_CHESTPLATE)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack LEATHER_HELMET = new FancyItem(Material.LEATHER_HELMET)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	
	public BomberKit() {
		super("Bomber", false, ICON, ChatColor.RED, 2000);
		
		Bukkit.getPluginManager().registerEvents(this, Warriors.getInstance());
	}

	@Override
	public void equip(CreeperPlayer player) {
		PlayerInventory inv = player.getInventory();
		
		inv.setItem(0, SWORD);
		inv.setItem(1, player.getAbility().getItem().stack());
		inv.setItem(2, PICKAXE);		
		
		if (!getItems().contains(SWORD)) getItems().add(SWORD); 
		if (!getItems().contains(player.getAbility().getItem().stack())) getItems().add(player.getAbility().getItem().stack()); 
		if (!getItems().contains(PICKAXE)) getItems().add(PICKAXE);
		
		inv.setBoots(LEATHER_BOOTS);
		inv.setLeggings(GOLD_LEGGINGS);
		inv.setChestplate(GOLD_CHESTPLATE);
		inv.setHelmet(LEATHER_HELMET);		
		
		if (!getItems().contains(LEATHER_BOOTS)) getItems().add(LEATHER_BOOTS);
		if (!getItems().contains(GOLD_LEGGINGS)) getItems().add(GOLD_LEGGINGS);
		if (!getItems().contains(GOLD_CHESTPLATE)) getItems().add(GOLD_CHESTPLATE);
		if (!getItems().contains(LEATHER_HELMET)) getItems().add(LEATHER_HELMET);
		
		if (GameManager.getPhase() == GamePhases.TRACKER) inv.addItem(getCompass().stack());
		
		for (ItemStack stack : player.getKeepItems()) {
			if (stack.getAmount() <= 0) stack.setAmount(1);
			inv.addItem(stack);
		}
		
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
	}
	
	public FancyItem getTnT() {
		return TNT;
	}
}
