package net.planckton.creeperwars.kit;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public class LeaperKit extends Kit implements Listener {
	
	private static final FancyItem ICON = new FancyItem(Material.FEATHER)
			.name("" + ChatColor.YELLOW + ChatColor.BOLD + "Leaper Kit")
			.lore("")
			.lore(ChatColor.WHITE + "Ability:" + ChatColor.GREEN + " Leap")
			.lore(ChatColor.WHITE + "Description:" + ChatColor.GREEN + " Leaps you forward")
			.lore(ChatColor.RED + "Cooldown: 10 sec")
			.lore("")
			.lore(ChatColor.GRAY + "Will contain:")
			.lore(ChatColor.DARK_GRAY + "Iron Axe, Full Gold")
			.lore("");
	
	private final ItemStack PICKAXE = new FancyItem(Material.WOOD_PICKAXE)
			.name(ChatColor.GRAY + "Default pickaxe").unbreakable().stack();
	private final ItemStack GOLD_BOOTS = new FancyItem(Material.GOLD_BOOTS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack GOLD_LEGGINGS = new FancyItem(Material.GOLD_LEGGINGS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack GOLD_CHESTPLATE = new FancyItem(Material.GOLD_CHESTPLATE)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack GOLD_HELMET = new FancyItem(Material.GOLD_HELMET)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	
	public LeaperKit() {
		super("Leaper", true, ICON, ChatColor.YELLOW, 1000);
	}

	@Override
	public void equip(CreeperPlayer player) {
		PlayerInventory inv = player.getInventory();
		
		inv.setItem(0, player.getAbility().getItem().stack());
		inv.setItem(1, PICKAXE);
		
		if (!getItems().contains(player.getAbility().getItem().stack())) getItems().add(player.getAbility().getItem().stack()); 
		if (!getItems().contains(PICKAXE)) getItems().add(PICKAXE);
		
		inv.setBoots(GOLD_BOOTS);
		inv.setLeggings(GOLD_LEGGINGS);
		inv.setChestplate(GOLD_CHESTPLATE);
		inv.setHelmet(GOLD_HELMET);		
		
		if (!getItems().contains(GOLD_BOOTS)) getItems().add(GOLD_BOOTS);
		if (!getItems().contains(GOLD_LEGGINGS)) getItems().add(GOLD_LEGGINGS);
		if (!getItems().contains(GOLD_CHESTPLATE)) getItems().add(GOLD_CHESTPLATE);
		if (!getItems().contains(GOLD_HELMET)) getItems().add(GOLD_HELMET);
		
		if (GameManager.getPhase() == GamePhases.TRACKER) inv.addItem(getCompass().stack());
		
		for (ItemStack stack : player.getKeepItems()) {
			if (stack.getAmount() <= 0) stack.setAmount(1);
			inv.addItem(stack);
		}
		
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
	}

}
