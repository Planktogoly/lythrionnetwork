package net.planckton.creeperwars.kit;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public class WarriorKit extends Kit implements Listener {
	
	private static final FancyItem ICON = new FancyItem(Material.GOLD_SWORD)
			.name("" + ChatColor.BLUE + ChatColor.BOLD + "Warrior Kit")
			.lore("")
			.lore(ChatColor.WHITE + "Ability:" + ChatColor.GREEN + " Fireblade")
			.lore(ChatColor.WHITE + "Description:" + ChatColor.GREEN + " Puts your enemies into fire")
			.lore(ChatColor.RED + "Cooldown: 30 sec")
			.lore("")
			.lore(ChatColor.GRAY + "Will contain:")
			.lore(ChatColor.DARK_GRAY + "Stone Sword, Full Chainmail")
			.lore("")
			.hideAllFlags();
	
	private final ItemStack PICKAXE = new FancyItem(Material.WOOD_PICKAXE)
			.name(ChatColor.GRAY + "Default pickaxe").unbreakable().stack();
	
	private final ItemStack CHAINMAIL_BOOTS = new FancyItem(Material.CHAINMAIL_BOOTS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack CHAINMAIL_LEGGINGS = new FancyItem(Material.CHAINMAIL_LEGGINGS)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack CHAINMAIL_CHESTPLATE = new FancyItem(Material.CHAINMAIL_CHESTPLATE)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	private final ItemStack CHAINMAIL_HELMET = new FancyItem(Material.CHAINMAIL_HELMET)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	
	public WarriorKit() {
		super("Warrior", false, ICON,  ChatColor.BLUE, 2000);
	}

	@Override
	public void equip(CreeperPlayer player) {
		PlayerInventory inv = player.getInventory();
		
		inv.setItem(0, player.getAbility().getItem().stack());
		inv.setItem(1, PICKAXE);		

		if (!getItems().contains(player.getAbility().getItem().stack())) getItems().add(player.getAbility().getItem().stack()); 
		if (!getItems().contains(PICKAXE)) getItems().add(PICKAXE);
		
		inv.setBoots(CHAINMAIL_BOOTS);
		inv.setLeggings(CHAINMAIL_LEGGINGS);
		inv.setChestplate(CHAINMAIL_CHESTPLATE);
		inv.setHelmet(CHAINMAIL_HELMET);		
		
		if (!getItems().contains(CHAINMAIL_BOOTS)) getItems().add(CHAINMAIL_BOOTS);
		if (!getItems().contains(CHAINMAIL_LEGGINGS)) getItems().add(CHAINMAIL_LEGGINGS);
		if (!getItems().contains(CHAINMAIL_CHESTPLATE)) getItems().add(CHAINMAIL_CHESTPLATE);
		if (!getItems().contains(CHAINMAIL_HELMET)) getItems().add(CHAINMAIL_HELMET);
		
		if (GameManager.getPhase() == GamePhases.TRACKER) inv.addItem(getCompass().stack());
		
		for (ItemStack stack : player.getKeepItems()) {
			if (stack.getAmount() <= 0) stack.setAmount(1);
			inv.addItem(stack);
		}
		
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
	}

}
