package net.planckton.creeperwars.kit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.utils.Utils;

public class KitManager extends Manager {
	
	@Getter private ArrayList<Kit> kits = new ArrayList<Kit>();
	private Map<String, Kit> kitsByName = new HashMap<String, Kit>();	
	
	@Override
	public void onEnable() {
		addKit(new LeaperKit());
		addKit(new ArcherKit());
		addKit(new WarriorKit());	
		addKit(new MinerKit());
		addKit(new BomberKit());
	}

	@Override
	public void onDisable() {
		Utils.nullify(this);		
	}
	
	public void addKit(Kit kit) {
		kits.add(kit);
		kitsByName.put(kit.getName(), kit);
	}
	
	public Kit getKit(String name) {
		return kitsByName.get(name);
	}
	
	public void setKit(CreeperPlayer player, Kit kit) {
		player.setSelectedKit(kit);
	}
}
