package net.planckton.creeperwars.kit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public class MinerKit extends Kit implements Listener {
	
	private static final FancyItem ICON = new FancyItem(Material.STONE_PICKAXE)
			.name("" + ChatColor.GRAY + ChatColor.BOLD + "Miner Kit")
			.lore("")
			.lore(ChatColor.WHITE + "Ability:" + ChatColor.GREEN + " GoldDigger")
			.lore(ChatColor.WHITE + "Description:" + ChatColor.GREEN + " Gives you Haste II for 7 seconds")
			.lore(ChatColor.RED + "Cooldown: 25 sec")
			.lore("")
			.lore(ChatColor.GRAY + "Will contain:")
			.lore(ChatColor.DARK_GRAY + "Iron Pickaxe, Full Leather except for the Golden Helmet")
			.lore("")
			.hideAllFlags();
	
	private final ItemStack SWORD = new FancyItem(Material.WOOD_SWORD)
			.name(ChatColor.GRAY + "Default sword").unbreakable().stack();
	private final ItemStack GOLD_HELMET = new FancyItem(Material.GOLD_HELMET)
			.name(ChatColor.GRAY + "Default armor").unbreakable().stack();
	
	private final ItemStack LEATHER_BOOTS = new FancyItem(Material.LEATHER_BOOTS)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.GRAY).unbreakable().stack();
	
	private final ItemStack LEATHER_LEGGINGS = new FancyItem(Material.LEATHER_LEGGINGS)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.GRAY).unbreakable().stack();
	
	private final ItemStack LEATHER_CHESTPLATE = new FancyItem(Material.LEATHER_CHESTPLATE)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.GRAY).unbreakable().stack();
	
	public MinerKit() {
		super("Miner", false, ICON, ChatColor.GRAY, 2500);
		
		Bukkit.getPluginManager().registerEvents(this, Warriors.getInstance());
	}

	@Override
	public void equip(CreeperPlayer player) {
		PlayerInventory inv = player.getInventory();
		
		inv.setItem(0, SWORD);
		inv.setItem(1, player.getAbility().getItem().stack());
		
		if (!getItems().contains(player.getAbility().getItem().stack())) getItems().add(player.getAbility().getItem().stack()); 
		if (!getItems().contains(SWORD)) getItems().add(SWORD); 
			
		inv.setBoots(LEATHER_BOOTS);		
		if (!getItems().contains(LEATHER_BOOTS)) getItems().add(LEATHER_BOOTS);
		
		inv.setLeggings(LEATHER_LEGGINGS);		
		if (!getItems().contains(LEATHER_LEGGINGS)) getItems().add(LEATHER_LEGGINGS);
		
		inv.setChestplate(LEATHER_CHESTPLATE);		
		if (!getItems().contains(LEATHER_CHESTPLATE)) getItems().add(LEATHER_CHESTPLATE);
		
		inv.setHelmet(GOLD_HELMET);			
		if (!getItems().contains(GOLD_HELMET)) getItems().add(GOLD_HELMET);
		
		if (GameManager.getPhase() == GamePhases.TRACKER) inv.addItem(getCompass().stack());
		
		for (ItemStack stack : player.getKeepItems()) {
			if (stack.getAmount() <= 0) stack.setAmount(1);
			inv.addItem(stack);
		}
		
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
	}

}
