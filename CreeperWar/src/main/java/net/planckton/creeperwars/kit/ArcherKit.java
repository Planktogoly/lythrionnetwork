package net.planckton.creeperwars.kit;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.item.FancyItem;

public class ArcherKit extends Kit implements Listener {
	
	private static final FancyItem ICON = new FancyItem(Material.BOW)
			.name("" + ChatColor.GREEN + ChatColor.BOLD + "Archer Kit")
			.lore("")
			.lore(ChatColor.WHITE + "Ability:" + ChatColor.GREEN + " Blizzard")
			.lore(ChatColor.WHITE + "Description:" + ChatColor.GREEN + " Blizzard will let your enemy freeze stuck")
			.lore(ChatColor.RED + "Cooldown: 45 sec")
			.lore("")
			.lore(ChatColor.GRAY + "Will contain:")
			.lore(ChatColor.DARK_GRAY + "Bow, Full Leather")
			.lore("");
	
	private final ItemStack PICKAXE = new FancyItem(Material.WOOD_PICKAXE)
			.name(ChatColor.GRAY + "Default pickaxe").unbreakable().stack();
	private final ItemStack SWORD = new FancyItem(Material.WOOD_SWORD)
			.name(ChatColor.GRAY + "Default sword").unbreakable().stack();
	
	private final FancyItem ARROW = new FancyItem(Material.ARROW, 12);
	
	private final ItemStack LEATHER_BOOTS = new FancyItem(Material.LEATHER_BOOTS)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.OLIVE).unbreakable().stack();	
	private final ItemStack LEATHER_LEGGINGS = new FancyItem(Material.LEATHER_LEGGINGS)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.OLIVE).unbreakable().stack();
	private final ItemStack LEATHER_CHESTPLATE = new FancyItem(Material.LEATHER_CHESTPLATE)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.OLIVE).unbreakable().stack();	
	private final ItemStack LEATHER_HELMET = new FancyItem(Material.LEATHER_HELMET)
			.name(ChatColor.GRAY + "Default armor").setUseLeatherMeta(true)
			.setColor(Color.OLIVE).unbreakable().stack();
	
	public ArcherKit() {
		super("Archer", false, ICON, ChatColor.GREEN, 2000);
	}

	@Override
	public void equip(CreeperPlayer player) {
		PlayerInventory inv = player.getInventory();
		
		inv.setItem(0, SWORD);
		inv.setItem(1, player.getAbility().getItem().stack());
		inv.setItem(2, PICKAXE);
	    
		if (!getItems().contains(player.getAbility().getItem().stack())) getItems().add(player.getAbility().getItem().stack()); 
		if (!getItems().contains(PICKAXE)) getItems().add(PICKAXE);
		if (!getItems().contains(SWORD)) getItems().add(SWORD); 
			
		inv.setBoots(LEATHER_BOOTS);		
		if (!getItems().contains(LEATHER_BOOTS)) getItems().add(LEATHER_BOOTS);
			
		inv.setLeggings(LEATHER_LEGGINGS);		
		if (!getItems().contains(LEATHER_LEGGINGS)) getItems().add(LEATHER_LEGGINGS);

		inv.setChestplate(LEATHER_CHESTPLATE);		
		if (!getItems().contains(LEATHER_CHESTPLATE)) getItems().add(LEATHER_CHESTPLATE);
		
		inv.setHelmet(LEATHER_HELMET);		
		if (!getItems().contains(LEATHER_HELMET)) getItems().add(LEATHER_HELMET);
		
		if (GameManager.getPhase() == GamePhases.TRACKER) inv.addItem(getCompass().stack());
		
		for (ItemStack stack : player.getKeepItems()) {
			if (stack.getAmount() <= 0) stack.setAmount(1);
			inv.addItem(stack);
		}
		
		if (player.isSpeed()) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
		if (player.isHaste()) player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, true, false));
	}
	
	public FancyItem getArrow() {
		return ARROW;
	}
}
