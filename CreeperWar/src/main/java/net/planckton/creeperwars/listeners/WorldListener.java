package net.planckton.creeperwars.listeners;

import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.PlayerManager;

public class WorldListener implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(PlayModePlayer.class)) return;		
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		if (event.getItem() == null) return;
		if (event.getItem().getType() != Material.FLINT_AND_STEEL) return;
		if (GameManager.getPhase() == GamePhases.GRACE) {
			event.setCancelled(true);
			player.sendActionBarMessage(ChatColor.RED + "You cannot use flint and steel in Grace phase");
			player.updateInventory();
			return;
		}
		
		ItemStack fns = player.getItemInHand();
		fns.setDurability((short) (fns.getDurability() + 16));
		
		if(fns.getDurability() > (short)63) {
			player.setItemInHand(null);
		}
	}
	
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event){
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (player == null) return;
		if (!player.isInPlayMode(PlayModePlayer.class)) return;		
		if(!(event.getCause() == TeleportCause.ENDER_PEARL)) return;
		
	    event.setCancelled(true);
	    player.getBukkitPlayer().setNoDamageTicks(1); 
	    player.teleport(event.getTo());
	}
	
	@EventHandler(priority = EventPriority.NORMAL) 
	public void onFoodlevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onWeatherChange(WeatherChangeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLeavesDecay(LeavesDecayEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBurn(BlockBurnEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityExplode(EntityExplodeEvent event) {
        if (!(event.getEntity() instanceof TNTPrimed)) return;	
		
		Iterator<Block> blocks = event.blockList().iterator();		
		while (blocks.hasNext()) {
			Block block = blocks.next();
			if (!((GameManager) Manager.GetManager(GameManager.class)).getBlocks().contains(block)) blocks.remove();				
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockSpread(BlockSpreadEvent event) {
		if (event.getSource().getType() == Material.FIRE) event.setCancelled(true);
		else if (event.getSource().getType() == Material.VINE) event.setCancelled(true); 
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCreatureSpawn(CreatureSpawnEvent event) {		
		if (!(GameManager.getPhase() == GamePhases.LOBBY)) return;
		if (event.getEntityType() == EntityType.VILLAGER) return;
		if (event.getEntityType() == EntityType.ARMOR_STAND) return;	
		if (!(event.getEntityType() == EntityType.CREEPER)) return;
		if (event.getLocation().getWorld().getName().equalsIgnoreCase("mapcreeperwar")) return;
		
		event.setCancelled(true); 
	}
}
