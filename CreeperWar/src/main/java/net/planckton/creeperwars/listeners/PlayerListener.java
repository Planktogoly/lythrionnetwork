package net.planckton.creeperwars.listeners;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import mkremins.fanciful.FancyMessage;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.ability.BlizzardAbility;
import net.planckton.creeperwars.kit.ArcherKit;
import net.planckton.creeperwars.kit.Kit;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.playmode.PlayModeDead;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.playmode.PlayModeSpectator;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.utils.ChatPrefix;

public class PlayerListener implements Listener {
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getPlayer());
		if(player.getPlayMode().getClass() != PlayModePlayer.class) return;
		
		if (player.getSelectedTeam().getCreeper().getHealth() == 0) {
			player.setPlayMode(PlayModeSpectator.class);
			player.getSelectedTeam().leave(player);	
			event.setRespawnLocation(player.getLocation());	
			player.sendTitle(ChatColor.RED + "Game Over!", "", 10, 80, 20);
			player.sendMessage(new FancyMessage(ChatPrefix.LYTHRION_RED_ARROW + "Want to play this game again? Click ")
					.then(ChatColor.AQUA.toString() + "HERE")
					.command("/mjoin CreeperWar")
					.tooltip(ChatColor.WHITE + "Click to join")
					.then(ChatColor.WHITE + "!"));
			return;
		}
		
		player.setPlayMode(PlayModeDead.class);
		event.setRespawnLocation(player.getLocation());			
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDeath(PlayerDeathEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getEntity());
		if (!player.isInPlayMode(PlayModePlayer.class)) return;
		
		Kit kit = player.getSelectedKit();
		Iterator<ItemStack> items = event.getDrops().iterator();
		
		while (items.hasNext()) {
			ItemStack item = items.next();
			if (kit.getItems().contains(item)) {
				items.remove();
				continue;
			}
			if (item.getType() == Material.ARROW) items.remove();
			if (item.getType().name().contains("PICKAXE")) items.remove();
			if (item != null && item.getItemMeta() != null && item.getItemMeta().getDisplayName() != null && item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLUE + "Throwable TNT")) {
				items.remove();
			    continue;
			}
		}		
		event.setDroppedExp(0);
		
		CreeperPlayer killer = null;
		String killerName = null;
			
		if (player.getKiller() != null) {
			killer = (CreeperPlayer) PlayerManager.getLythrionPlayer(player.getKiller());
			killerName = killer.getSelectedTeam().getColor() + killer.getCurrentName();
			
			if (killer.getSelectedKit() instanceof ArcherKit) {
				BlizzardAbility abili = (BlizzardAbility) killer.getAbility();
				if (abili.getRemoveTask() != null) abili.getRemoveTask().cancel();
			}
		}
		
		String deadName = player.getSelectedTeam().getColor() + player.getCurrentName();
		String deathMessage = " died";
		switch (player.getLastDamageCause().getCause()) {	
		case ENTITY_ATTACK:
			if (killer.equals(player)) {
            	Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " commited suicide");
            	break;
			}
			
			Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " was too weak to fight against " + killerName);		
			break;
		case PROJECTILE:
			if (killer.equals(player)) {
            	Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " commited suicide");
            	break;
			}
			
			Bukkit.broadcastMessage(killerName + "'s" + ChatColor.GRAY + " bow skill was too powerful against " + deadName);
			break;
		case FALL:
			Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " thought he could fly");
			break;
		case FIRE_TICK:
			Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " couldn't resist the fire");
			break;
		case ENTITY_EXPLOSION:
	        if(player.getLastDamageCause() instanceof EntityDamageByEntityEvent){
	            EntityDamageByEntityEvent edbe = (EntityDamageByEntityEvent) player.getLastDamageCause();
	            Entity damager = edbe.getDamager();
	            if (damager.hasMetadata("player")) {
	                CreeperPlayer p = (CreeperPlayer) PlayerManager.getLythrionPlayer(Bukkit.getPlayer(damager.getMetadata("player").get(0).asString()));
	                if (p == null) {
	                	Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " exploded into million pieces");
	                	break;
	                }
	                if (p.equals(player)) {
	                	Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " committed suicide");
	                	break;
	                }
	                
	                killer = p;
	                killerName = killer.getSelectedTeam().getColor() + killer.getCurrentName();
	                Bukkit.broadcastMessage(killerName + ChatColor.GRAY + " explode " + deadName + ChatColor.GRAY + " into million pieces");
	                break;
	            }
	        }
			
			Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " exploded into million pieces");
			break;
		case WITHER:
			if (killer.equals(player)) {
            	Bukkit.broadcastMessage(deadName + ChatColor.GRAY + " committed suicide");
            	break;
			}
			
			Bukkit.broadcastMessage(killerName + ChatColor.GRAY + " killed " + deadName + ChatColor.GRAY + " with Blizzard!");
			break;
		default:
			Bukkit.broadcastMessage(deadName + ChatColor.GRAY + deathMessage);
			break;
		}
		
		if (player.getSelectedTeam().getPlayers().size() == 1 && player.getSelectedTeam().getCreeper().isCreeperDeath()) {
			Bukkit.broadcastMessage(ChatColor.WHITE + "Team " + player.getSelectedTeam().getColor() + player.getSelectedTeam().getName() + ChatColor.WHITE + " has been defeated!");
		}
		
		Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				if(!event.getEntity().isDead()) return;
				if(!player.isOnline()) return;
				
				
				if (player.getAbility().getTask() !=  null) player.getAbility().getTask().cancel();
				player.respawn();
			}
		}, 20);
		
		if (killer != null && !killer.equals(player)) {
			Warriors.getInstance().addCrystals(killer, killer.balance.get().rewardCrystals(5));
		}
		
		event.setDeathMessage(null);
	}
}
