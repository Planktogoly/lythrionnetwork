package net.planckton.creeperwars.playmode;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.Getter;
import mkremins.fanciful.FancyMessage;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.ability.LeaperAbility;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.kit.KitManager;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.screen.KitSelectorScreen;
import net.planckton.creeperwars.screen.TeamSelectorScreen;
import net.planckton.item.CustomItemManager;
import net.planckton.item.FancyItem;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayMode;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.sidebar.AdvancedPersonalScore;
import net.planckton.lib.sidebar.AdvancedScore;
import net.planckton.lib.sidebar.AdvancedSidebar;
import net.planckton.lib.utils.PlayerCallable;

@SuppressWarnings("unused")
public class PlayModeLobby extends PlayMode implements Listener {
	
	@Getter private static AdvancedSidebar sidebar;
	private static AdvancedScore scoreTimeName;
	private static AdvancedScore scoreTime;
	private static AdvancedScore scorePlayersName;
	private static AdvancedScore scorePlayers;
	private static AdvancedScore scoreMapName;
	@Getter private static AdvancedScore scoreMap;
	private static AdvancedScore scoreKitName;
	@Getter private static AdvancedPersonalScore scoreKit;
	
	@Getter private final FancyItem NAMETAG = new FancyItem(Material.NAME_TAG)
			.name("" + ChatColor.BLUE + ChatColor.BOLD + "Teams")
			.lore(ChatColor.GRAY + "Right click to choose a team");
	
	@Getter private final FancyItem KIT = new FancyItem(Material.NETHER_STAR)
			.name("" + ChatColor.GREEN + ChatColor.BOLD + "Kit selector")
			.lore(ChatColor.GRAY + "Right click to select a kit");
	
	@Getter private final FancyItem BARRIER = new FancyItem(Material.BARRIER)
			.name("" + ChatColor.RED + "You did not select a team");
	
	@Getter private final FancyItem BOOK = new FancyItem(Material.WRITTEN_BOOK)
			.hideAllFlags()
			.name("" + ChatColor.YELLOW + ChatColor.BOLD + "Game Objective")
			.lore("" + ChatColor.GRAY + "Right click to see the game objective");
	
	@Override
	public void onSelect(LythrionPlayer player) {
		CreeperPlayer gPlayer = (CreeperPlayer) player;
		player.setGameMode(GameMode.ADVENTURE);
		
		player.setHealthScale(20);
		player.setMaxHealth(20);
		player.setHealth(20);
		player.setSaturation(20);
		player.setFoodLevel(20);
		player.setExp(0);
		player.setLevel(0);
		player.setFlySpeed(0.2f);
		player.setWalkSpeed(0.2f);
		player.cleaPotionEffects();		
		
		PlayerInventory inv = player.getInventory();
		inv.clear();
		
		inv.setHelmet(null);
		inv.setChestplate(null);
		inv.setLeggings(null);
		inv.setBoots(null);
		
		inv.setItem(0, BARRIER.stack());
		inv.setItem(1, NAMETAG.stack());	
		inv.setItem(2, BOOK.stack());
		inv.setItem(4, KIT.stack());	
		inv.setItem(8, CustomItemManager.HUB);
		
		gPlayer.setSelectedKit(Manager.GetManager(KitManager.class).getKit("Leaper"));
		gPlayer.setAbility(new LeaperAbility(gPlayer));
		
		getScoreKit().update(player);
		
		FancyMessage header = new 
				FancyMessage("" + ChatColor.RED + ChatColor.BOLD + " Lythrion Network ")
				.then("\nCreeperWar\n").color(ChatColor.GREEN);
		
		FancyMessage footer = new FancyMessage("" + ChatColor.GREEN + "\n  store.lythrion.net  ");
		
		player.setHeaderAndFooter(header, footer);
		updatePlayers();
		
		player.teleport(PositionManager.getPosition("guardianwar.spawn"));
		sidebar.display(player);
	}
	
	@EventHandler(priority = EventPriority.NORMAL) 
	public void onLythrionPlayerJoin(LythrionPlayerJoinEvent event) {
		LythrionPlayer player = event.getLythrionPlayer();
		if (!player.isInPlayMode(this)) return;
		
		FancyMessage header = new 
				FancyMessage("" + ChatColor.RED + ChatColor.BOLD + " Lythrion Network ")
				.then("\nCreeperWar\n").color(ChatColor.GREEN);
		
		FancyMessage footer = new FancyMessage("" + ChatColor.GREEN + "\n  store.lythrion.net  ");
		
		player.setHeaderAndFooter(header, footer);
		updatePlayers();
		
		player.getInventory().setHeldItemSlot(4);
		player.teleport(PositionManager.getPosition("guardianwar.spawn"));
		sidebar.display(player);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLytrionPlayerQuit(LythrionPlayerQuitEvent event) {
		CreeperPlayer player = (CreeperPlayer) event.getLythrionPlayer();
		if (!player.isInPlayMode(this)) return;
		
		if (player.getSelectedTeam() != null) player.getSelectedTeam().leave(player);
		updatePlayers();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		ItemStack stack = player.getItemInHand();
		if(stack == null) return;
		if(stack.getType() == Material.AIR) return;
		ItemMeta meta = stack.getItemMeta();
		if(meta.getDisplayName() == null) return;
		
		if(stack.equals(KIT.stack())) {
			event.setCancelled(true);
			
			new KitSelectorScreen(player).open(player);
			return;
		}
		else if (stack.equals(NAMETAG.stack())) {
			event.setCancelled(true);
			
			new TeamSelectorScreen(player).open(player);
			return;
		} else if (stack.equals(BOOK.stack())) {
			event.setCancelled(true);
			player.sendMessage(" ");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Game:" + ChatColor.GREEN + " CreeperWars");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.GOLD.toString() + ChatColor.BOLD + "Objective:");
			player.sendMessage(ChatColor.GRAY + "You need to defend your Creeper!");
			player.sendMessage(ChatColor.GRAY + "You will not respawn once your Creeper is dead.");
			player.sendMessage(ChatColor.GRAY + "There are also 3 phases,");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.YELLOW + "Phase Grace;" + ChatColor.GRAY + " Upgrade yourself and your team as quick");
			player.sendMessage(ChatColor.GRAY +"as possible by mining ores in the mines!");
			player.sendMessage(ChatColor.YELLOW + "Phase PvP;" + ChatColor.GRAY +" Collect emeralds in the middle mines!");
			player.sendMessage(ChatColor.GRAY +"Or eliminate the Creepers of your enemies! ");
			player.sendMessage(ChatColor.YELLOW + "Phase Tracker;" + ChatColor.GRAY + " Final phase, this phase will appear once");
			player.sendMessage(ChatColor.GRAY +"there is only one Creeper left. You will get a compass");
			player.sendMessage(ChatColor.GRAY +"to find and kill the last survivors!");
			player.sendMessage(" ");
			player.sendMessage(" ");
			player.playSound(Sound.ORB_PICKUP);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInventoryClick(InventoryClickEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInventoryDrag(InventoryDragEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		if(player.getPlayMode() != this) return;
		
		event.setCancelled(true);
	}
	
	
	public static void createScoreBoardLobby() {
		sidebar = new AdvancedSidebar("creeperwarbar", ChatColor.GREEN + "CreeperWar");
		
		sidebar.createWhiteSpace();
		scoreTimeName = sidebar.createScore("" + ChatColor.YELLOW + ChatColor.BOLD + "Time ");
		scoreTime = sidebar.createScore(ChatColor.WHITE + "waiting...");
		
		sidebar.createWhiteSpace();
		scorePlayersName = sidebar.createScore("" + ChatColor.GREEN + ChatColor.BOLD + "Players ");
		scorePlayers = sidebar.createScore(ChatColor.WHITE + "0/" + Manager.GetManager(GameManager.class).getMaxPlayers());
		
		sidebar.createWhiteSpace();
		scoreKitName = sidebar.createScore("" + ChatColor.BLUE + ChatColor.BOLD + "Kit ");
		scoreKit = sidebar.createPersonalScore("kit", new PlayerCallable<String>() {
			
			@Override
			public String call(LythrionPlayer player) {
				CreeperPlayer creeperPlayer = (CreeperPlayer) player;
				
				if (creeperPlayer.getSelectedKit() == null) return ChatColor.WHITE + "none";				
				return ChatColor.WHITE + creeperPlayer.getSelectedKit().getName();
			}
		});
		
		sidebar.createWhiteSpace();
		scoreMapName = sidebar.createScore("" + ChatColor.RED + ChatColor.BOLD + "Map ");
		scoreMap = sidebar.createScore(ChatColor.WHITE + "Random");
		
		sidebar.createWhiteSpace();
		sidebar.createScore(ChatColor.GRAY + "  play." + ChatColor.RED.toString() + ChatColor.BOLD + "lythrion" + ChatColor.GRAY + ".net");
	}
	
	public void updatePlayers() {
		int players = PlayModeManager.getPlayersPerMode(PlayModeLobby.class).size();
		
		scorePlayers.setValue("" + ChatColor.WHITE + players + "/" + Manager.GetManager(GameManager.class).getMaxPlayers());
	}
	
	private static int dots = 1;
	public static void updateTimer(int time) {
		if (time == -1) {
			if (dots == 3) {
				scoreTime.setValue(ChatColor.WHITE + "Waiting...");
				dots = 1;
				return;
			} else if (dots == 2) {
				scoreTime.setValue(ChatColor.WHITE + "Waiting..");
			} else if (dots == 1) {
				scoreTime.setValue(ChatColor.WHITE + "Waiting.");
			}
			dots++;
			return;
		}
		dots = 1;
		
		if (time <= 5) {
			for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModeLobby.class)) {
				player.playSound(Sound.NOTE_PLING);
				player.sendMessage(Warriors.getInstance().getPREFIX() + "The game starts in " + ChatColor.RED + time);
			}
		}
		
		if (time == 10) {			
			for (LythrionPlayer player : PlayModeManager.getPlayersPerMode(PlayModeLobby.class)) {
				player.playSound(Sound.NOTE_PLING);
				player.sendMessage(Warriors.getInstance().getPREFIX() + "The game starts in " + ChatColor.RED + time);
			}
		}
		scoreTime.setValue("" + ChatColor.WHITE + time +" seconds");
	}

}
