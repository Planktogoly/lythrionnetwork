package net.planckton.creeperwars.playmode;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayMode;

public class PlayModeDead extends PlayMode {
	
	@Override
	public void onSelect(LythrionPlayer player) {
		CreeperPlayer gPlayer = (CreeperPlayer) player;
		player.setGameMode(GameMode.CREATIVE);
		player.getBukkitPlayer().setFlying(true);
		player.clearInventory();
		player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 10, false, false), true);
		player.sendTitle("&cYou died", "", 0, 120, 0);
		
		for (LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {	
			if (p.isInPlayMode(PlayModePlayer.class)) {
				player.showPlayer(p);
				p.hidePlayer(player);
				continue;
			}
			
			if (p.isInPlayMode(PlayModeDead.class)) p.hidePlayer(player);
			if (p.isInPlayMode(PlayModeSpectator.class)) p.hidePlayer(player);		
		}
		
		gPlayer.setDeathTask(Bukkit.getScheduler().runTaskTimer(Warriors.getInstance(), () -> {
			int cooldown = gPlayer.getCountdown();
			cooldown--;
			gPlayer.setCountdown(cooldown);
			player.sendTitle("&cYou died", ChatColor.GRAY + "Spawning in " + cooldown, 0, 21, 0);
			
			if (cooldown == 0) {
				if (!player.isOnline()) return;				
				player.setPlayMode(PlayModePlayer.class);
				player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 3, false, false));
				gPlayer.setCountdown(11);
				gPlayer.getDeathTask().cancel();
			}
		}, 0, 20));	
	}
	
	@Override
	public void onDeSelect(LythrionPlayer player) {
		player.sendMessage(Warriors.getInstance().getPREFIX() + "Your Creeper brought you back to life!");
		player.sendTitle("&c ", ChatColor.YELLOW + "You got respawned!", 0, 20 * 1, 10);
	}
	
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getWhoClicked().getName());		
		if (!player.isInPlayMode(this.getClass())) return;	
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onVehicledestroy(VehicleDamageEvent event) {	
		if (!(event.getAttacker() instanceof Player)) return;
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getAttacker());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player)) return;
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getDamager());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
		
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInteraction (PlayerInteractEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	final void onEntityTarget(final EntityTargetEvent event) {
		
		if (event.isCancelled()) return;
		final Entity entity = event.getEntity();

		final Entity target = event.getTarget();
		if (!(target instanceof Player)) return;
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) target);
		if (player.isInPlayMode(PlayModePlayer.class)) return;
		
		if (entity instanceof ExperienceOrb){
			event.setCancelled(true);
			event.setTarget(null);
			return;
		}
	}
}
