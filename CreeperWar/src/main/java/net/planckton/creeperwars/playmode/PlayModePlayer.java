package net.planckton.creeperwars.playmode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.util.Vector;

import lombok.Getter;
import net.planckton.creeperwars.Warriors;
import net.planckton.creeperwars.ability.Ability;
import net.planckton.creeperwars.ability.BlizzardAbility;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhases;
import net.planckton.creeperwars.kit.ArcherKit;
import net.planckton.creeperwars.kit.MinerKit;
import net.planckton.creeperwars.kit.WarriorKit;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.screen.PersonalShopScreen;
import net.planckton.creeperwars.screen.TeamUpgradeScreen;
import net.planckton.creeperwars.team.Team;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.creeperwars.utils.GRunnable;
import net.planckton.creeperwars.utils.OreRunnable;
import net.planckton.creeperwars.utils.Utils;
import net.planckton.lib.lvariable.Callback;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.playmode.PlayModeVanilla;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.sidebar.AdvancedScore;
import net.planckton.lib.sidebar.AdvancedSidebar;

@SuppressWarnings("unused")
public class PlayModePlayer extends PlayModeVanilla {
	
	@Getter private static AdvancedSidebar sidebar;
	
	private static AdvancedScore scorePhaseName;
	private static AdvancedScore scorePhase;
	private static AdvancedScore scoreTimeName;
	private static AdvancedScore scoreTime;
	private static AdvancedScore scoreBlue;
	private static AdvancedScore scoreGreen;
	private static AdvancedScore scoreYellow;
	private static AdvancedScore scoreRed;
	
	private static HashMap<String, AdvancedScore> scores = new HashMap<>();

	private ArrayList<Material> ores = new ArrayList<>(); {
		ores.add(Material.COAL_ORE);
		ores.add(Material.IRON_ORE);
		ores.add(Material.GOLD_ORE);
		ores.add(Material.DIAMOND_ORE);
		ores.add(Material.EMERALD_ORE);
	}	
	
	@Override
	public void onSelect(LythrionPlayer player) {
		for (LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			if (p.isInPlayMode(PlayModePlayer.class)) {
				p.showPlayer(player);
			}
		}
		
		CreeperPlayer guardPlayer = (CreeperPlayer) player;
		
		guardPlayer.getInventory().clear();		
		guardPlayer.getSelectedKit().equip(guardPlayer);
		guardPlayer.updateInventory();
		guardPlayer.getAbility().setUseable(true);
		guardPlayer.getAbility().setCooldown(guardPlayer.getAbility().getFinalCooldown());
		guardPlayer.teleport(PositionManager.getPosition("guardianwar.team." + guardPlayer.getSelectedTeam().getName().toLowerCase() + ".spawn"));
		guardPlayer.setGameMode(GameMode.SURVIVAL);
		guardPlayer.setScoreBoard(Manager.GetManager(GameManager.class).getScoreboard());
		guardPlayer.tab.setTabColor(guardPlayer.getSelectedTeam().getColor());
		
		sidebar.display(guardPlayer);		
		
		for (LythrionPlayer lythPlayer : PlayModeManager.getPlayersPerMode(PlayModeSpectator.class)) {
			lythPlayer.showPlayer(player);
		}
		for (LythrionPlayer lythPlayer : PlayModeManager.getPlayersPerMode(PlayModeDead.class)) {
			lythPlayer.showPlayer(player);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickUp(PlayerPickupItemEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		
	    if (event.getItem().getItemStack().getType() == Material.TNT) {
	    	if (player.getInventory().containsAtLeast(event.getItem().getItemStack(), 5)) {
	    		event.setCancelled(true);
	    		return;
	    	}						    	
	    }
	    
		if (event.getItem().getItemStack().getItemMeta().getDisplayName() == null && event.getItem().getItemStack().getType().name().contains("PICKAXE")) {
			player.getKeepItems().add(event.getItem().getItemStack());
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;		
		
		ItemStack item = event.getItemDrop().getItemStack();
		
		if (item.getType() == Material.ARROW && player.getSelectedKit().getName().equalsIgnoreCase("Archer")) {
			event.setCancelled(true);
			return;
		}
		if (item.getItemMeta().getDisplayName() != null && item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLUE + "Throwable TNT")) {
			event.setCancelled(true);
			return;
		}
		if (item.getItemMeta().getDisplayName() == null && item.getType().name().contains("PICKAXE")) {
			player.getKeepItems().remove(item);
			return;
		}
		
		if (player.getSelectedKit().getItems().contains(item)) {
			event.setCancelled(true);
			return;
		}
	}	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent  event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;		
		
		if (GameManager.getPhase() == GamePhases.GRACE) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You cannot place blocks in phase Grace.");
			return;
		}
		
		Location blockPlaceLoc = event.getBlockPlaced().getLocation();
		
		for (Team team : Manager.GetManager(TeamManager.class).getTeams()) {
			if (team.getCreeper().isCreeperDeath()) continue;
			if (blockPlaceLoc.distance(team.getCreeper().getLoc()) <= 10.0) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED + "You cannot place blocks too close to an Creeper!");
				return;
			}
		}
		
		((GameManager)Manager.GetManager(GameManager.class)).addBlock(event.getBlock());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventoryClick(InventoryClickEvent event) {
		ItemStack cursor = event.getCursor();
		ItemStack clickedItem = event.getCurrentItem();
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		
		if (clickedItem == null) return;
    	if (cursor == null) return;
    	if (!(event.getClickedInventory().getType() == InventoryType.PLAYER)) return;
		if (!(cursor.getType() == Material.ENCHANTED_BOOK)) return;	
		
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) cursor.getItemMeta();
		
		try {
			for (Enchantment enchant : meta.getStoredEnchants().keySet()) {
				if (enchant.getName().equalsIgnoreCase(Enchantment.DAMAGE_ALL.getName())) {
					if (Utils.isSword(clickedItem.getType())) {
						event.setCancelled(true);
						ItemStack newItem = clickedItem;
						newItem.addEnchantment(enchant, 1);	
						player.getBukkitPlayer().setItemOnCursor(null);
						for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 6, 6, 6)) {
							if (!(entity instanceof Player)) continue;
							
							Player target = (Player) entity;
							target.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 1);
						}
						event.getInventory().setItem(event.getRawSlot(), newItem);
						return;
					} else {
						if (clickedItem.getType() != Material.AIR) player.sendMessage(ChatColor.RED + "You can only apply this enchantment to a sword.");
						return;
					}
				} else if (enchant.getName().equalsIgnoreCase(Enchantment.PROTECTION_ENVIRONMENTAL.getName())) {
					if (Utils.isArmor(clickedItem.getType())) {
						event.setCancelled(true);
						ItemStack newItem = clickedItem;
						newItem.addEnchantment(enchant, 1);			
						player.getBukkitPlayer().setItemOnCursor(null);
						for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 6, 6, 6)) {
							if (!(entity instanceof Player)) continue;
							
							Player target = (Player) entity;
							target.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 1);
						}
						event.getInventory().setItem(event.getRawSlot(), newItem);
						return;
					} else {
						if (clickedItem.getType() != Material.AIR) player.sendMessage(ChatColor.RED + "You can only apply this enchantment to an armor piece.");
						return;
					}
				} else if (enchant.getName().equalsIgnoreCase(Enchantment.ARROW_DAMAGE.getName())) {
					if (Utils.isBow(clickedItem.getType())) {
						event.setCancelled(true);
						ItemStack newItem = clickedItem;
						newItem.addEnchantment(enchant, 1);		
						player.getBukkitPlayer().setItemOnCursor(null);
						for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), 6, 6, 6)) {
							if (!(entity instanceof Player)) continue;
							
							Player target = (Player) entity;
							target.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 1);
						}
						event.getInventory().setItem(event.getRawSlot(), newItem);
						return;
					}else {
						if (clickedItem.getType() != Material.AIR) player.sendMessage(ChatColor.RED + "You can only apply this enchantment to a bow.");
						return;
					}
				}
			}
		} catch(ArrayIndexOutOfBoundsException e) {
			
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {		
		if (GameManager.getPhase() == GamePhases.LOBBY) return;
		if (GameManager.getPhase() == GamePhases.POST_GAME || GameManager.getPhase() == GamePhases.PREGAME) {
			event.setCancelled(true);
			return;
		}

		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;

		Block block = event.getBlock();

		if (Utils.getGrass().contains(block.getType())) return;
		
		int time = 20;
		if (ores.contains(block.getType())) {
			if (block.getType() == Material.IRON_ORE) {
				player.getInventory().addItem(new ItemStack(Material.IRON_INGOT));
				time = 15;
			}
			else if (block.getType() == Material.COAL_ORE) {
				player.getInventory().addItem(new ItemStack(Material.COAL));
				time = 10;
			}
			else if (block.getType() == Material.GOLD_ORE) {
				player.getInventory().addItem(new ItemStack(Material.GOLD_INGOT));
			}
			else if (block.getType() == Material.EMERALD_ORE) {
				player.getInventory().addItem(new ItemStack(Material.EMERALD));
				time = 25;
			}
			else if (block.getType() == Material.DIAMOND_ORE){
				player.getInventory().addItem(new ItemStack(Material.DIAMOND));
				time = 25;
			}
			
			player.playSound(Sound.ITEM_PICKUP);
			
			Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), new OreRunnable(block.getType(), block), 20 * time);
			block.setType(Material.COBBLESTONE);
			
			event.setCancelled(true);
			return;
		}
		
		GameManager manager = Manager.GetManager(GameManager.class);
		
		if (manager.hasBlock(block)) return;
		
		player.sendMessage(ChatColor.RED + "You cannot break this block.");
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
		if (GameManager.getPhase() == GamePhases.PREGAME || GameManager.getPhase() == GamePhases.LOBBY) return;
		if (GameManager.getPhase() == GamePhases.POST_GAME) {
			event.setCancelled(true);
			return;
		}
		
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		
		if (event.getRightClicked() instanceof Villager) {
			event.setCancelled(true);
			
			Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), () -> {
				new PersonalShopScreen().open(PlayerManager.getLythrionPlayer(event.getPlayer()));
			}, 1);
			return;
		} else if (event.getRightClicked() instanceof Creeper) {
			event.setCancelled(true);
			Entity entity = event.getRightClicked();
			
			if (!entity.getMetadata("creeper").get(0).asString().equalsIgnoreCase(player.getSelectedTeam().getName())) return;			
			
			Bukkit.getScheduler().runTaskLater(Warriors.getInstance(), ()-> {
				new TeamUpgradeScreen(player.getSelectedTeam()).open(player);
			}, 1);
			return;
		} 
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageEvent event) {
		if(!(event.getEntity() instanceof Player)) return;
		
		if (GameManager.getPhase() == GamePhases.POST_GAME) {
			event.setCancelled(true);
			return;
		}
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
		if (!player.isInPlayMode(this)) return;
		
		if (event.getCause() != DamageCause.FALL) return;
		if (GameManager.getPhase() == GamePhases.GRACE) {
			event.setCancelled(true);
			return;
		}
		
		if (!player.getBukkitPlayer().getMetadata("falldamage").isEmpty()) {
			event.setCancelled(true);
			player.getBukkitPlayer().removeMetadata("falldamage", Warriors.getInstance());
			return;
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamagEntity(EntityDamageByEntityEvent event) {
		if (GameManager.getPhase() == GamePhases.PREGAME || GameManager.getPhase() == GamePhases.LOBBY) return;
		if (GameManager.getPhase() == GamePhases.POST_GAME) {
			event.setCancelled(true);
			return;
		}
		
		
		if (event.getEntity() instanceof Villager) {
			event.setCancelled(true);
			return;
		}
		
		if (event.getEntity() instanceof Player) {
			if (event.getDamager() instanceof Player) {
				CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
				if (!player.isInPlayMode(this)) {
					event.setCancelled(true);
					return;
				}
				CreeperPlayer damager = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getDamager());
				if (!damager.isInPlayMode(this)) {
					event.setCancelled(true);	
					return;
				}
				
				if (player.getSelectedTeam().getName().equalsIgnoreCase(damager.getSelectedTeam().getName())) event.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMovement(PlayerMoveEvent event) {
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		
		if (!(GameManager.getPhase() == GamePhases.PREGAME)) {
			if (player.getBukkitPlayer().getItemInHand() != null && !(player.getBukkitPlayer().getItemInHand().getType() == Material.COMPASS)) return;
			
			CreeperPlayer nearestTarget = (CreeperPlayer) getNearest(player, 800.0);
			if (nearestTarget == null) {
				player.sendActionBarMessage(ChatColor.RED + "Searching for players...");
				return;
			}
			
			if (nearestTarget.getSelectedTeam().getName().equalsIgnoreCase(player.getSelectedTeam().getName())) {
				player.sendActionBarMessage(ChatColor.RED + "Searching for players...");
				return;
			}
			
			player.sendActionBarMessage(ChatColor.GRAY + "Player: " + nearestTarget.getSelectedTeam().getColor() + nearestTarget.getCurrentName() +
					ChatColor.GRAY + " Distance: " + ChatColor.WHITE + (int) player.getLocation().distance(nearestTarget.getLocation()) + " blocks");
			player.getBukkitPlayer().setCompassTarget(nearestTarget.getLocation());			
			return;
		}
		
		double fromX = event.getFrom().getX();
		double fromY = event.getFrom().getY();
		double fromZ = event.getFrom().getZ();
		double toX = event.getTo().getX();
		double toY = event.getTo().getY();
		double toZ = event.getTo().getZ();
		if (!(fromX == toX)) event.getTo().setX(fromX);
		if (!(fromY == toY)) event.getTo().setY(fromY);
		if (!(fromZ == toZ)) event.getTo().setZ(fromZ);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerShootBow(EntityShootBowEvent event) {
		if (!(event.getEntityType() == EntityType.PLAYER)) return;
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
		if (!player.isInPlayMode(PlayModePlayer.class)) return;
		
		event.getProjectile().setMetadata("team", new FixedMetadataValue(Warriors.getInstance(), player.getSelectedTeam().getName()));
		
		Ability ability = player.getAbility();		
		if (ability instanceof BlizzardAbility) {
			BlizzardAbility shotAbility = (BlizzardAbility) player.getAbility();
			if (!shotAbility.isActivated())	return;
			
			shotAbility.setActivated(false);
			event.getProjectile().setMetadata("knockback", new FixedMetadataValue(Warriors.getInstance(), player.getId()));
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onProjectileHit(ProjectileHitEvent event) {
		if (!(event.getEntity().getType() == EntityType.ARROW)) return;
				
		if (event.getEntity().getMetadata("knockback").isEmpty()) {
			event.getEntity().remove();
			return;
		}
		
		new GRunnable(new Callback<Block>() {
			
			@Override
			public void call(Block value) {
				if (value == null) {
					event.getEntity().remove();
					return;
				}		
								
				MetadataValue uuid = event.getEntity().getMetadata("knockback").get(0);
				event.getEntity().remove();
				
				CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((UUID) uuid.value());   
				player.sendMessage(Warriors.getInstance().getPREFIX() + "Blizzard missed!");
				
			}
		}, event.getEntity());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCreeperhit(EntityDamageByEntityEvent event) {
		if (!(event.getDamager().getType() == EntityType.PRIMED_TNT)) return;
		if (!(event.getEntity().getType() == EntityType.PLAYER)) return;
		
		Entity entity = event.getDamager();		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());
		if (entity.hasMetadata("player") && entity.getMetadata("player").get(0).asString().equalsIgnoreCase(player.getName())) return;		
		
		String id = player.getSelectedTeam().getCreeper().getId();
		if (entity.hasMetadata("id")) {
			if (id.equalsIgnoreCase(entity.getMetadata("id").get(0).asString())) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onrojectileHit(EntityDamageByEntityEvent event) {		
		if (!(event.getCause() == DamageCause.PROJECTILE)) return;
		if (!(event.getDamager().getType() == EntityType.ARROW)) return;
		if (!(event.getEntity().getType() == EntityType.PLAYER)) return;
		CreeperPlayer victim = (CreeperPlayer) PlayerManager.getLythrionPlayer((Player) event.getEntity());		
		if (!event.getDamager().getMetadata("team").isEmpty() && event.getDamager().getMetadata("team").get(0).asString().equalsIgnoreCase(victim.getSelectedTeam().getName())){
			event.setCancelled(true);
			return;
		}
		
		if (event.getDamager().getMetadata("knockback").isEmpty()) return;	
		
		MetadataValue uuid = event.getDamager().getMetadata("knockback").get(0);
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer((UUID) uuid.value());    
		
		BlizzardAbility ability = (BlizzardAbility) player.getAbility();
		ability.runTask(victim);
		event.setDamage(0);		
		player.sendMessage(Warriors.getInstance().getPREFIX() + "Blizzard has been applied!");
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (GameManager.getPhase() == GamePhases.PREGAME || GameManager.getPhase() == GamePhases.LOBBY) return;
		
		CreeperPlayer player = (CreeperPlayer) PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(PlayModePlayer.class)) return;
		ItemStack stack = player.getItemInHand();
		if(stack == null) return;
		if(stack.getType() == Material.AIR) return;
		
		Ability ability = player.getAbility();	
		if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (!(stack.getType() == Material.BOW)) return;
			
			if (ability instanceof BlizzardAbility) {
				if (!ability.isUseable()) {
					player.sendActionBarMessage(ChatColor.RED + "It's recharging...");
					return;
				}
				
				BlizzardAbility shotAbility = (BlizzardAbility) player.getAbility();
				
				if (shotAbility.isActivated()) {
					shotAbility.setActivated(false);
					player.sendMessage(Warriors.getInstance().getPREFIX() + "Deactivated Blizzard..");
					return;				
				}
				
				shotAbility.setActivated(true);
				player.sendMessage(Warriors.getInstance().getPREFIX() + "Activating Blizzard..");
				return;
			}
		}		
		
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;		
		
		if (player.getSelectedKit() instanceof WarriorKit) {
			if (stack.getType().name().contains("SWORD")) {
				if (!ability.isUseable()) {
					player.sendActionBarMessage(ChatColor.RED + "It's recharging...");
					return;
				}
				ability.run();
			}
		} else if (player.getSelectedKit() instanceof MinerKit) {
			if (stack.getType().name().contains("PICKAXE")) {
				if (!ability.isUseable()) {
					player.sendActionBarMessage(ChatColor.RED + "It's recharging...");
					return;
				}
				ability.run();
			}
		} else if (player.getSelectedKit() instanceof ArcherKit) {
			
		} else if (ability.getItem().stack().isSimilar(stack)) {		
			if (!ability.isUseable()) {
				player.sendActionBarMessage(ChatColor.RED + "It's recharging...");
				return;
			}
			ability.run();
		}
		
		if (GameManager.getPhase() == GamePhases.GRACE) {
			if (stack.getType() == Material.TNT) player.sendActionBarMessage(ChatColor.RED + "You cannot throw TNT in Grace phase!");
			return;
		}
		if (stack.getType() == Material.TNT) {		
		      event.setCancelled(true);
		      if (stack.getAmount() == 1) {
		        player.getInventory().remove(stack);
		      } else {
		    	  stack.setAmount(stack.getAmount() - 1);
		      }
		      TNTPrimed tnt = (TNTPrimed)player.getWorld().spawn(player.getBukkitPlayer().getEyeLocation(), TNTPrimed.class);
		      tnt.setMetadata("player", new FixedMetadataValue(Warriors.getInstance(), player.getName()));
		      tnt.setMetadata("id", new FixedMetadataValue(Warriors.getInstance(), player.getSelectedTeam().getCreeper().getId()));
		      tnt.setMetadata("creeper", new FixedMetadataValue(Warriors.getInstance(), player.getSelectedTeam().getName()));
		      Vector playerDirection = player.getBukkitPlayer().getEyeLocation().getDirection();
		      Vector smallerVector = playerDirection.multiply(1.0D);
		      tnt.setVelocity(smallerVector);
		}
	}
	
	public static void createScoreBoardInGame() {
		TeamManager manager = Manager.GetManager(TeamManager.class);
		
		sidebar = new AdvancedSidebar("cprwargame", ChatColor.GREEN + "CreeperWar");
		
		sidebar.createWhiteSpace();
		scorePhaseName = sidebar.createScore("" + ChatColor.YELLOW + ChatColor.BOLD + "Phase ");
		scorePhase = sidebar.createScore(ChatColor.WHITE + "Grace");
		
		sidebar.createWhiteSpace();
		scoreTimeName = sidebar.createScore("" + ChatColor.AQUA + ChatColor.BOLD + "Time ");
		scoreTime = sidebar.createScore(ChatColor.WHITE + "2:30");
		
		sidebar.createWhiteSpace();		
		Team blue = manager.getTeam("Blue");
		scoreBlue = sidebar.createScore(blue.getColor().toString() + ChatColor.BOLD + blue.getName() + ChatColor.WHITE + "(" + blue.getPlayers().size() + " players)");
		scores.put("Blue", scoreBlue);
		
		Team yellow = manager.getTeam("Yellow");
		scoreYellow = sidebar.createScore(yellow.getColor().toString() + ChatColor.BOLD + ChatColor.GRAY + " (" + yellow.getPlayers().size() + " players)");
		scores.put("Yellow", scoreYellow);
		
		Team red = manager.getTeam("Red");
		scoreRed = sidebar.createScore(red.getColor().toString() + ChatColor.BOLD + red.getName() + ChatColor.GRAY + " (" + red.getPlayers().size() + " players)");
		scores.put("Red", scoreRed);
		
		Team green = manager.getTeam("Green");
		scoreGreen = sidebar.createScore(green.getColor().toString() + ChatColor.BOLD + green.getName() + ChatColor.GRAY + " (" + green.getPlayers().size() + " players)");
		scores.put("Green", scoreGreen);
		
		sidebar.createWhiteSpace();
		sidebar.createScore(ChatColor.GRAY + "  play." + ChatColor.RED.toString() + ChatColor.BOLD + "lythrion" + ChatColor.GRAY + ".net");
	}
	
	public static void updatePhase(String phase) {
		scorePhase.setValue(ChatColor.WHITE + phase);
	}
	
	public static void updatePlayers(Team team, boolean creeper){		
		if (team.getPlayers().size() == 0) {
			scores.get(team.getName()).setValue(team.getColor().toString() + ChatColor.STRIKETHROUGH + team.getName() + ChatColor.RED + " DEAD");
			return;
		}	
		
		String player = "players";
		if (team.getPlayers().size() == 1) player = "player";
		
		if (creeper) {
			scores.get(team.getName()).setValue(team.getColor().toString()  + team.getName() + ChatColor.GRAY + " (" + team.getPlayers().size() + " " + player + ")");	
			return;
		}
		
		scores.get(team.getName()).setValue(team.getColor().toString() + ChatColor.STRIKETHROUGH + team.getName() + ChatColor.GRAY + " (" + team.getPlayers().size() + " " + player + ")");		
	}
	
	public static void updateTime(int time) {
		scoreTime.setValue(timeToString(time));
	}
	
	private static String timeToString(int time) {
		int mins = time / 60;
		int secs = time % 60;
		
		return ChatColor.WHITE + (mins >= 10 ? "" + mins : "0" + mins) + ":" + (secs >= 10 ? "" + secs : "0" + secs);
	}
	
	private LythrionPlayer getNearest(LythrionPlayer player, Double range) {
        double distance = Double.POSITIVE_INFINITY; 
        LythrionPlayer target = null;
        for (Entity entity : player.getBukkitPlayer().getNearbyEntities(range, range, range)) {
        	if (!(entity instanceof Player)) continue;
        	
        	if(entity == player) continue; 
        	double distanceto = player.getLocation().distance(entity.getLocation());
        	if (distanceto > distance) continue;
        	distance = distanceto;
        	LythrionPlayer lTarget =PlayerManager.getLythrionPlayer((Player) entity);
        	if (!lTarget.isInPlayMode(PlayModePlayer.class)) continue;
        	target = lTarget;
        }        
        
        return target;
    }
}
