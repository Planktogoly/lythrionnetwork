package net.planckton.creeperwars.playmode;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mkremins.fanciful.FancyMessage;
import net.planckton.creeperwars.screen.SpectatorScreen;
import net.planckton.item.CustomItemManager;
import net.planckton.item.FancyItem;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.player.prefix.PlayerPrefix;
import net.planckton.lib.playmode.PlayMode;
import net.planckton.lib.position.PositionManager;

public class PlayModeSpectator extends PlayMode {
	
	private static final ItemStack SPECTATOR_ITEM = new FancyItem(Material.BOOK).name(ChatColor.RED.toString() + ChatColor.BOLD + "Spectator")
			.lore(ChatColor.GRAY + "Right click to spectate in-game players").stack();
	
	@Override
	public void onSelect(LythrionPlayer player) {
		player.setGameMode(GameMode.CREATIVE);
		player.getBukkitPlayer().setFlying(true);
		player.clearInventory();
		player.getInventory().setItem(0, SPECTATOR_ITEM);
		player.getInventory().setItem(8, CustomItemManager.HUB);
		player.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 10, false, false), true);
		
		for (LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {	
			if (p.isInPlayMode(PlayModePlayer.class)) {
				player.showPlayer(p);
				p.hidePlayer(player);
				continue;
			} else if (p.isInPlayMode(PlayModeSpectator.class)) {
				p.hidePlayer(player);
				player.hidePlayer(p);
				continue;
			}  else if (p.isInPlayMode(PlayModeDead.class)) {
				p.hidePlayer(player);
				player.hidePlayer(p);
				continue;
			}
		}
		
		
		player.tab.setTabColor(ChatColor.GRAY);
		
		FancyMessage header = new 
				FancyMessage("" + ChatColor.RED + ChatColor.BOLD + " Lythrion Network ")
				.then("\nCreeperWar\n").color(ChatColor.GREEN);
		
		FancyMessage footer = new FancyMessage("" + ChatColor.GREEN + "\n  store.lythrion.net  ");
		
		player.setHeaderAndFooter(header, footer);

		PlayModePlayer.getSidebar().display(player);
		
		player.prefixes.addPrefixLocal(new PlayerPrefix(ChatColor.DARK_GRAY + "SPEC", ChatColor.GRAY + "Spectator"));
	}
	
	@Override
	public void onDeSelect(LythrionPlayer player) {				
		for (LythrionPlayer p : PlayerManager.getOnlineLythrionPlayers()) {
			if (p.isInPlayMode(PlayModePlayer.class)) {
				p.showPlayer(player);
			}
		}
		
		player.cleanTitle();
	}
	
	@EventHandler(priority = EventPriority.NORMAL) 
	public void onLythrionPlayerJoin(LythrionPlayerJoinEvent event) {
		LythrionPlayer player = event.getLythrionPlayer();
		if (!player.isInPlayMode(this)) return;
		
		player.teleport(PositionManager.getPosition("guardianwar.deathspawn"));
		
		FancyMessage header = new 
				FancyMessage("" + ChatColor.RED + ChatColor.BOLD + " Lythrion Network ")
				.then("\nCreeperWar\n").color(ChatColor.GREEN);
		
		FancyMessage footer = new FancyMessage("" + ChatColor.GREEN + "\n  store.lythrion.net  ");
		
		player.setHeaderAndFooter(header, footer);

		PlayModePlayer.getSidebar().display(player);
	}
	
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getWhoClicked().getName());		
		if (!player.isInPlayMode(this.getClass())) return;	
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onVehicledestroy(VehicleDamageEvent event) {	
		if (!(event.getAttacker() instanceof Player)) return;
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getAttacker());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player)) return;
		
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getDamager());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!player.isInPlayMode(this)) return;
	
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getItem().equals(SPECTATOR_ITEM)) {
					player.playSound(Sound.CLICK);
					new SpectatorScreen().open(player);
					return;
		    }
		}	
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInteraction (PlayerInteractEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player)event.getPlayer());
		if (!(player.getPlayMode() == this)) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	final void onEntityTarget(final EntityTargetEvent event) {
		
		if (event.isCancelled()) return;
		final Entity entity = event.getEntity();

		final Entity target = event.getTarget();
		if (!(target instanceof Player)) return;
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) target);
		if (player.isInPlayMode(PlayModePlayer.class)) return;
		
		if (entity instanceof ExperienceOrb){
			event.setCancelled(true);
			event.setTarget(null);
			return;
		}
	}

}
