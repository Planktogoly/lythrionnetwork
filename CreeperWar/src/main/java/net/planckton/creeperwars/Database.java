package net.planckton.creeperwars;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import lombok.Cleanup;
import net.md_5.bungee.api.ChatColor;
import net.planckton.creeperwars.shop.ShopItem;
import net.planckton.creeperwars.utils.Utils;
import net.planckton.item.FancyItem;
import net.planckton.lib.DatabaseLib;

public class Database {

	public static LinkedHashMap<String, LinkedList<ShopItem>> getShopItems() {
		try {
			LinkedHashMap<String, LinkedList<ShopItem>> items = new LinkedHashMap<>();

			@Cleanup Connection connection = DatabaseLib.getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT * FROM guardian_shop");

			@Cleanup ResultSet rs = statement.executeQuery();
			LinkedList<ShopItem> list = null;

			while (rs.next()) {
				if (!items.containsKey(rs.getString("category"))) list = new LinkedList<>();
				else list = items.get(rs.getString("category"));

				try {
					String material = rs.getString("item");
					if (material.equalsIgnoreCase("SHARPNESS_1")) {
						ItemStack stack = new ItemStack(Material.ENCHANTED_BOOK);
						EnchantmentStorageMeta meta = (EnchantmentStorageMeta) stack.getItemMeta();
						meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
						stack.setItemMeta(meta);
						FancyItem icon = new FancyItem(stack);
						icon.lore(" ", ChatColor.GRAY + "You can apply this enchantment with", 
								ChatColor.GRAY + "placing the enchanting book on" , ChatColor.GRAY + "your tool/armor in your inventory!");
						ShopItem item = new ShopItem(rs.getString("name"), icon, Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), false);
						list.add(item);
						items.put(rs.getString("category"), list);
						System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
						continue;
					} else if (material.equalsIgnoreCase("POWER_1")) {
						ItemStack stack = new ItemStack(Material.ENCHANTED_BOOK);
						EnchantmentStorageMeta meta = (EnchantmentStorageMeta) stack.getItemMeta();
						meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
						stack.setItemMeta(meta);
						FancyItem icon = new FancyItem(stack);
						icon.lore(" ", ChatColor.GRAY + "You can apply this enchantment with", 
								ChatColor.GRAY + "placing the enchanting book on" , ChatColor.GRAY + "your tool/armor in your inventory!");
						ShopItem item = new ShopItem(rs.getString("name"), icon, Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), false);
						list.add(item);
						items.put(rs.getString("category"), list);
						System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
						continue;
					} else if (material.equalsIgnoreCase("PROTECTION_1")) {
						ItemStack stack = new ItemStack(Material.ENCHANTED_BOOK);
						EnchantmentStorageMeta meta = (EnchantmentStorageMeta) stack.getItemMeta();
						meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
						stack.setItemMeta(meta);
						FancyItem icon = new FancyItem(stack);
						icon.lore(" ", ChatColor.GRAY + "You can apply this enchantment with", 
								ChatColor.GRAY + "placing the enchanting book on" , ChatColor.GRAY + "your tool/armor in your inventory!");
						ShopItem item = new ShopItem(rs.getString("name"), icon, Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), false);
						list.add(item);
						items.put(rs.getString("category"), list);
						System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
						continue;
					} else if (material.equalsIgnoreCase("WOOL")) {
						FancyItem item = new FancyItem(Material.valueOf(rs.getString("item"))).setData((byte) 14);
						
						ShopItem shopItem = new ShopItem(rs.getString("name"), item, Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), false);
						list.add(shopItem);
						items.put(rs.getString("category"), list);	
						System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
						continue;
					} else if (material.equalsIgnoreCase("STAINED_CLAY")) {
						FancyItem item = new FancyItem(Material.valueOf(rs.getString("item"))).setData((byte) 14);
						
						ShopItem shopItem = new ShopItem(rs.getString("name"), item, Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), false);
						list.add(shopItem);
						items.put(rs.getString("category"), list);	
						System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
						continue;
					}

					boolean unbreakable = false;
					if (isUnbreakable(Material.valueOf(material))) unbreakable = true;

					ShopItem item = new ShopItem(rs.getString("name"), new FancyItem(Material.valueOf(rs.getString("item"))), Material.valueOf(rs.getString("material")), rs.getInt("price"), rs.getInt("amount"), unbreakable);
					list.add(item);
					items.put(rs.getString("category"), list);	
					System.out.println("[CreeperWar] Added a new item to the shop " + rs.getString("name") + " in the category " + rs.getString("category"));
				} catch (IllegalArgumentException e) {
					System.out.println("[CreeperWar] " + rs.getString("material") + " is not a bukkit material");
				}
			}
			return items;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static boolean isUnbreakable(Material material) {
		for (Material mat : Utils.getMaterials()) {
			if (mat == material) return true;
		}
		return false;
	}

}
