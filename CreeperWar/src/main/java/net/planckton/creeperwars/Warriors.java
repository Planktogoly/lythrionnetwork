package net.planckton.creeperwars;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.planckton.creeperwars.command.StartKommand;
import net.planckton.creeperwars.game.GameManager;
import net.planckton.creeperwars.game.GamePhase;
import net.planckton.creeperwars.kit.KitManager;
import net.planckton.creeperwars.listeners.PlayerListener;
import net.planckton.creeperwars.listeners.WorldListener;
import net.planckton.creeperwars.player.CreeperPlayer;
import net.planckton.creeperwars.player.CreeperPlayerManager;
import net.planckton.creeperwars.playmode.PlayModeDead;
import net.planckton.creeperwars.playmode.PlayModeLobby;
import net.planckton.creeperwars.playmode.PlayModePlayer;
import net.planckton.creeperwars.playmode.PlayModeSpectator;
import net.planckton.creeperwars.shop.ShopManager;
import net.planckton.creeperwars.team.TeamManager;
import net.planckton.creeperwars.utils.Utils;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.commands.CommandManager;
import net.planckton.lib.event.DefaultPlayModeRequestEvent;
import net.planckton.lib.event.ManagersEnabledEvent;
import net.planckton.lib.event.RequestPlayerManagerEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.utils.UniChars;

public class Warriors extends JavaPlugin implements Listener {
	
	@Getter private final String PREFIX = ChatColor.GREEN + "CreeperWar " + ChatColor.GRAY + UniChars.ARROWS_RIGHT + " " + ChatColor.RESET;
	
	private static Warriors instance;
	
	@Getter private CreeperPlayerManager playerManager = new CreeperPlayerManager();
	
	@Getter private HashMap<CreeperPlayer, Integer> coinsEarned = new HashMap<>();
	
	public static Warriors getInstance() {
		return instance;
	}
	
	public Warriors() {
		instance = this;
	}
	
	public void onEnable() {			
		for(File file : new File("../world/").listFiles()) {
			if(!file.isDirectory()) continue;
			if(!file.getName().startsWith("map")) continue;
			
			if(LythrionLib.isDevServer()) System.out.println(file.getName());
			
			try {
				System.out.println("Copying " + file.getName() + "...");

				FileUtils.copyDirectory(new File("../world/" + file.getName()), new File(file.getName()));
				
				System.out.println("Copied " + file.getName() + " succesfully.");
			} catch (IOException e) {
				System.out.println("Failed copying " + file.getName() + ".");
				e.printStackTrace();
			}
		}
		
		for (World world : Bukkit.getWorlds()) {
			world.setFullTime(0);
			world.setStorm(false);
			world.setThundering(false);
		}
		
		Manager.registerManager(new GameManager());		
		Manager.registerManager(new KitManager());
		Manager.registerManager(new TeamManager());
		Manager.registerManager(new ShopManager());
		
		PlayModeManager.registerPlayMode(new PlayModeLobby());
		PlayModeManager.registerPlayMode(new PlayModePlayer());
		PlayModeManager.registerPlayMode(new PlayModeDead());
		PlayModeManager.registerPlayMode(new PlayModeSpectator());
		
		CommandManager.registerCommand(instance, new StartKommand());
		
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new WorldListener(), this); 
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this); 
		
		new Utils();
	}
	
	public void onDisable() {
		for(World world : Bukkit.getWorlds()) {
			Bukkit.unloadWorld(world, false);
		}
		
		int deleted = 0;
		int attemptDelete = 0;
		for(File file : new File("").getAbsoluteFile().listFiles()) {
			if(file.isDirectory() && file.getName().startsWith("map")) {
				System.out.println("Attempt delete " + file.getName() + "...");
				attemptDelete++;
				
				if(FileUtils.deleteQuietly(file.getAbsoluteFile())) {
					System.out.println("Deleted " + file.getName() + ".");
					deleted++;
				}
				else System.out.println("Could not delete " + file.getName() + ".");
			}
		}
		
		System.out.println("Deleted " + deleted + "/" + attemptDelete + " maps.");
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onManagersEnabled(ManagersEnabledEvent event) {
		PlayModeLobby.createScoreBoardLobby();
		PlayModePlayer.createScoreBoardInGame();
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onRequestPlayerManager(RequestPlayerManagerEvent event) {
		event.setManager(playerManager);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onLythrionPlayerJoin(DefaultPlayModeRequestEvent event) {
		event.setPlayMode(PlayModeLobby.class);
	}
	
	@SafeVarargs
	public final boolean isInPhase(Class<? extends GamePhase>... phases) {
		for(Class<? extends GamePhase> phase : phases) {
			if(GameManager.getPhase().getGamePhase().getClass() == phase) return true;
		}
		
		return false;
	}
	
	public void playEffect(Location location, Effect effect, int id, int data, float offsetX, float offsetY, float offsetZ, float speed, int particleCount, int radius) {
		for (LythrionPlayer player : PlayerManager.getOnlineLythrionPlayers()) {
			player.spigot().playEffect(location, effect, id, data, offsetX, offsetY, offsetZ, speed, particleCount, radius);
		}
	}
	
	public void addCrystals(CreeperPlayer name, int amount) {
		if (!coinsEarned.containsKey(name)) {
			coinsEarned.put(name, amount);
			return;
		}
		
		int crystals = coinsEarned.get(name);
		
		coinsEarned.put(name, crystals += amount);
	}

}
