package net.planckton.brain.party;

import java.util.ArrayList;
import java.util.HashMap;

import net.planckton.brain.player.Player;

public class PartyManager {
	
	private HashMap<Player, Party> partyByLeader = new HashMap<>();
	private HashMap<Player, Party> partyByPlayer = new HashMap<>();
    private ArrayList<Party> parties = new ArrayList<>();
	
	public void addParty(Party party) {
		parties.add(party);
		partyByLeader.put(party.getLeader(), party);
		partyByPlayer.put(party.getLeader(), party);
	}
	
	public Party getPartyByPlayer(Player player) {
		return partyByPlayer.get(player);
	}
	
	public Party getPartyByLeader(Player player) {
		return partyByLeader.get(player);
	}
	
	public void removeParty(Player player) {
		partyByPlayer.remove(player);
	}
	
	public void removeFullParty(Player leader) {
		partyByLeader.remove(leader);
	}

	public void removePartyObject(Party party) {
		parties.remove(party);		
	}
	
	public void addLeaderToParty(Player leader, Party party) {
		partyByLeader.put(leader, party);		
	}
	
	public void addPlayerToParty(Player player, Party party) {
		partyByPlayer.put(player, party);		
	}
	
}
