package net.planckton.brain.party;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import net.planckton.blood.packet.PacketPartyCreate;
import net.planckton.blood.packet.PacketPartyDisband;
import net.planckton.blood.packet.PacketPartyJoin;
import net.planckton.blood.packet.PacketPartyLeave;
import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.Prefix;

public class Party {
	
	@Setter @Getter private Player leader;
	@Getter private ArrayList<Player> players = new ArrayList<>();
	
	@Getter private ArrayList<Player> invites = new ArrayList<>();
	
	@Getter private int maximumPlayers = 4;
	
	@Getter private boolean open = false;
	
	public Party(Player player, Player invite) {
		this.leader = player;		
		
		if (player.getRank().hasPower(Rank.DION)) maximumPlayers = 8;
		if (player.getRank().hasPower(Rank.LYTHRION)) maximumPlayers = 12;	
		if (player.getRank().hasPower(Rank.MOD)) maximumPlayers = -1;
	    
	    invitePlayer(invite);
	    players.add(player);
		
		player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyCreate(player.getId()));	
		
		Brain.getBrainServer().getPartyManager().addParty(this);	
	}
	
	public boolean isLeader(Player player) {
		return leader == player;
	}
	
	public boolean isInParty(Player player) {
		for (Player member : players) {
			if (player.equals(member)) return true;
		}
		return false;
	}

	public void warpPlayers() {
		ServerAdapter server = leader.getServerAdapter();
		
		for (Player member : players) {
			if (member.getServerAdapter().equals(server)) continue;
			
			member.sendMessage(Prefix.PARTY_ARROWS + "The leader warped you to his server.");
			member.connect(server);
		}
	}
	
	public void acceptPlayer(Player player) {
		PartyManager manager = Brain.getBrainServer().getPartyManager();	
		if (!(maximumPlayers == -1)) {
			if (maximumPlayers == getPlayers().size()) {
				player.sendMessage(Prefix.LYTHRION_RED_ARROW + "The party is full!");
				return;
			}			
		}
		
		if (open) {
			player.sendMessage(Prefix.PARTY_ARROWS + "You joined " + leader.getDisplayName() + ChatColor.WHITE + "'s party!");
			leader.sendMessage(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has joined the party.");
			broadcast(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has joined the party.");
			
			manager.addPlayerToParty(player, this);
			
			players.add(player);
			invites.remove(player);
			player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyJoin(player.getId()));
			return;
		}
		
		if (!invites.contains(player)) {
			player.sendMessage(Prefix.PARTY_ARROWS + "The party is not open. Ask the leader for an invite.");
			return;
		}
		
		player.sendMessage(Prefix.PARTY_ARROWS + "You joined " + leader.getDisplayName() + ChatColor.WHITE + "'s party!");
		leader.sendMessage(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has joined the party.");
		broadcast(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has joined the party.");
		
		manager.addPlayerToParty(player, this);
		
		players.add(player);
		invites.remove(player);
		
		player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyJoin(player.getId()));
	}
	
	public void invitePlayer(Player player) {
		if (!(maximumPlayers == -1)) {
			if (maximumPlayers == getPlayers().size()) {
				leader.sendMessage(Prefix.LYTHRION_RED_ARROW + "The party is full! Please consider donating at " + ChatColor.GOLD + "store.tostimc.net");
				return;
			}			
		}
		
		if (players.contains(player)) {
			leader.sendMessage(Prefix.PARTY_ARROWS + "Player is already in your party");
			return;
		}
		
		if (invites.contains(player)) {
			leader.sendMessage(Prefix.PARTY_ARROWS + player.getDisplayName() + " is already in your party");
			return;
		}
		
		player.sendMessage(Prefix.PARTY_ARROWS + leader.getDisplayName() + ChatColor.WHITE + " invited you to his party. Use" + ChatColor.GOLD + " /party join " + leader.getCurrentName() + ChatColor.WHITE + " to join his party.");
		invites.add(player);
	    leader.sendMessage(Prefix.PARTY_ARROWS + "You invited " + player.getDisplayName() + ChatColor.WHITE + " to your party.");
	    broadcast(Prefix.PARTY_ARROWS + leader.getDisplayName() + ChatColor.WHITE + " invited " + player.getDisplayName() + ChatColor.WHITE + " to the party.");
		return;
	}

	public void kickPlayer(Player kickedPlayer) {
		PartyManager manager = Brain.getBrainServer().getPartyManager();		
		
		players.remove(kickedPlayer);		
		manager.removeParty(kickedPlayer);
		
		kickedPlayer.sendMessage(Prefix.PARTY_ARROWS + "You got kicked out of " + leader.getDisplayName() + ChatColor.WHITE + "'s party.");
		leader.sendMessage(Prefix.PARTY_ARROWS + "You kicked " + kickedPlayer.getDisplayName() + ChatColor.WHITE + " from the party.");
		
		kickedPlayer.getServerAdapter().getChannel().writeAndFlush(new PacketPartyLeave(kickedPlayer.getId()));
	}

	public void leave(Player player) {
		if (leader.equals(player)) {
			leader.sendMessage(Prefix.PARTY_ARROWS + "You are the leader of the party. Use " + ChatColor.GOLD + "/party disband" + ChatColor.WHITE + ".");
			return;
		}
		
		PartyManager manager = Brain.getBrainServer().getPartyManager();		
		
		players.remove(player);		
		manager.removeParty(player);
		
		player.sendMessage(Prefix.PARTY_ARROWS + "You left " + leader.getDisplayName() + ChatColor.WHITE + "'s party.");
		leader.sendMessage(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has left the party.");		
		broadcast(Prefix.PARTY_ARROWS + player.getDisplayName() + ChatColor.WHITE + " has left the party.");
		
		player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyLeave(player.getId()));
	}
	
	public void broadcast(String message) {
		for (Player player : players) {
			if (player.equals(leader)) continue;
			
			player.sendMessage(message);
		}
	}

	public void promotePlayer(Player promotePlayer) {
		broadcast(Prefix.PARTY_ARROWS + promotePlayer.getDisplayName() + ChatColor.WHITE + " got promoted to leader!");	
		leader.sendMessage(Prefix.PARTY_ARROWS + "You promoted " + promotePlayer.getDisplayName() + ChatColor.WHITE + " to leader.");
		
		PartyManager manager = Brain.getBrainServer().getPartyManager();
		manager.removeFullParty(leader);
		
		leader = promotePlayer;
		
		manager.addLeaderToParty(leader, this);
	}

	public void disband() {
		broadcast(Prefix.PARTY_ARROWS + leader.getDisplayName() + ChatColor.WHITE + " has disbanded the party!");
		leader.sendMessage(Prefix.PARTY_ARROWS + "You disbanded the party.");
		
		PartyManager manager = Brain.getBrainServer().getPartyManager();		
		for (Player player : players) {
			manager.removeParty(player);
			player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyLeave(player.getId()));
		}
		
		manager.removeFullParty(leader);
		manager.removePartyObject(this);				
		
		leader.getServerAdapter().getChannel().writeAndFlush(new PacketPartyDisband(leader.getId()));
		
		invites = null;
		players = null;
		leader = null;		
	}

	public void open() {
		if (open) {
			open = false;
			broadcast(Prefix.PARTY_ARROWS + "The party is now closed for everyone.");
			leader.sendMessage(Prefix.PARTY_ARROWS + "The party is now closed for everyone.");
			return;
		}
		
		broadcast(Prefix.PARTY_ARROWS + "The party is now open for everyone.");
		leader.sendMessage(Prefix.PARTY_ARROWS + "The party is now open for everyone.");				
		
		open = true;
	}

	public void newLeader() {
		if (players.size() == 1) {
			disband();
			return;
		}
		PartyManager manager = Brain.getBrainServer().getPartyManager();
		manager.removeFullParty(leader);
		
		players.remove(leader);
		
		leader = players.iterator().next();
		manager.addLeaderToParty(leader, this);
		
		broadcast(Prefix.PARTY_ARROWS + leader.getDisplayName() + " is now the leader of your party.");
		leader.sendMessage(Prefix.PARTY_ARROWS + "You are now the leader of the party.");		
	}

	public void teleportPlayers() {
		ServerAdapter server = leader.getServerAdapter();
		
		for (Player member : players) {
			if (member.getServerAdapter().equals(server)) continue;
			
			member.connect(server);
		}		
	}
	
}
