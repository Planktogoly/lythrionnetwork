package net.planckton.brain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.concurrent.Callable;

import com.google.gson.JsonObject;
import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.Future;
import lombok.Cleanup;
import lombok.Getter;
import net.planckton.brain.player.GameProfile;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.HashUtil;

public class Database {
	private static final boolean USE_DEV_DB_IF_POSSIBLE = true;
	
	private static BoneCP connection;
	private static DefaultEventExecutorGroup mysqlExecutorPool;
	
	@Getter private static String database;
	
	public static boolean openConnection() throws Exception {
		Brain.info("Creating mysql executor group.");
		mysqlExecutorPool = new DefaultEventExecutorGroup(4); // 4 threads
		
		JsonObject config;
		if(Brain.isDev() && USE_DEV_DB_IF_POSSIBLE) {
			Brain.info("Picking development DB config");
			config = Brain.getConfig().getAsJsonObject("databasedev");
		}
		else {
			Brain.info("Picking production DB config");
			config = Brain.getConfig().getAsJsonObject("database");
		}
		
		String username = config.get("username").getAsString();
		String password = config.get("password").getAsString();
		String host = config.get("host").getAsString();
		database = config.get("database").getAsString();
		
		// setup the connection pool
		Brain.info("Creating BoneCP config");
		BoneCPConfig boneCPConfig = new BoneCPConfig();
		boneCPConfig.setJdbcUrl("jdbc:mysql://" + host + "/" + database + "?useUnicode=true&characterEncoding=utf-8"); // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
		boneCPConfig.setUsername(username); 
		boneCPConfig.setPassword(password);
		boneCPConfig.setMinConnectionsPerPartition(5);
		boneCPConfig.setMaxConnectionsPerPartition(10);
		boneCPConfig.setPartitionCount(1);

		boneCPConfig.setIdleConnectionTestPeriodInMinutes(10);
		boneCPConfig.setConnectionTestStatement("/* ping */ SELECT 1");
		
		try {
			Brain.info("Creating BoneCP");
			connection = new BoneCP(boneCPConfig);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		Brain.info("[tostiplugin] Succesfully connected to DB.");
		return true;
	}
	
	public static void closeConnection() {
		
		try {
			if(connection != null) connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if(mysqlExecutorPool != null) mysqlExecutorPool.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isConnected() throws RuntimeException {
		if(connection == null) return false;
		
		try {
			return !connection.getDbIsDown().get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Connection getConnection() throws SQLException {
		return connection.getConnection();
	}
	
	public static Future<Rank> getRank(final UUID playerid) {
		return mysqlExecutorPool.submit(new Callable<Rank>() {

			public Rank call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT rank FROM player_serverrank WHERE id_player=? ORDER BY id_serverrank DESC LIMIT 1");
					statement.setString(1, playerid.toString());
					
					@Cleanup ResultSet rs = statement.executeQuery();
					
					if (!rs.next()) return Rank.MEMBER;
					
					return Rank.fromString(rs.getString("rank"));
				} catch (SQLException e) {
					e.printStackTrace();
					return Rank.MEMBER;
				}
			}
		});
	}
	
	public static Future<Integer> getGroupIDPlayer(UUID uuid) {
		return mysqlExecutorPool.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statementGroups = connection.prepareStatement("SELECT group_id FROM player_group WHERE player_id=?");
					statementGroups.setString(1, uuid.toString());
					
					@Cleanup ResultSet rsGroupID = statementGroups.executeQuery();

					if (!rsGroupID.next()) return 0;
						
					return rsGroupID.getInt("group_id");	
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return 0;
			}
			
		});		
	}
	
	public static Future<Boolean> setRank(final UUID playerId, final Rank rank) {
		return mysqlExecutorPool.submit(new Callable<Boolean>() {
			
			@Override
			public Boolean call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO player_serverrank(id_player,rank) VALUES(?,?)");
					statement.setString(1, playerId.toString());
					statement.setString(2, rank.toString());
					
					statement.execute();
					
					return true;
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		});
	}
	
	public static Future<UUID> getUUIDbyName(final String playerName) {
		return mysqlExecutorPool.submit(new Callable<UUID>() {

			@Override
			public UUID call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT id_player FROM player_name WHERE name=? ORDER BY id_player_name DESC LIMIT 1");
					statement.setString(1, playerName);
					
					@Cleanup ResultSet rs = statement.executeQuery();
					
					if(!rs.next()) return null;

					return UUID.fromString(rs.getString("id_player"));
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		});
	}
	
	public static Future<String> getNameByUUID(final UUID id) {
		return mysqlExecutorPool.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT name FROM player_name WHERE id_player=? ORDER BY id_player_name DESC LIMIT 1");
					statement.setString(1, id.toString());
					
					@Cleanup ResultSet rs = statement.executeQuery();
					
					if(!rs.next()) return null;
					
					return rs.getString("name");
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		});
	}
	
	public static Future<GameProfile> getNickName(final UUID id) {
		return mysqlExecutorPool.submit(new Callable<GameProfile>() {

			@Override
			public GameProfile call() throws Exception {
				try {
					@Cleanup Connection connection = getConnection();
					@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT uuid,name FROM player_nick WHERE id_player=? AND active=1");
					statement.setString(1, id.toString());
					
					@Cleanup ResultSet rs = statement.executeQuery();
					
					if(!rs.next()) return null;
					
					return new GameProfile(UUID.fromString(rs.getString("uuid")), rs.getString("name"));
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		});
	}

	public static boolean isAuth(String username, String password) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT 1 AS success FROM player_logins INNER JOIN player ON player.id_player=player_logins.id_player WHERE latest_name=? AND password=?");
			
			statement.setString(1, username);
			statement.setString(2, HashUtil.hash256(password + "_tmc"));
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return false;

			return rs.getBoolean("success");
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean isAuthPreHashed(String username, String password) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT 1 AS success FROM player_logins INNER JOIN player ON player.id_player=player_logins.id_player WHERE latest_name=? AND password=?");
			
			statement.setString(1, username);
			statement.setString(2, password);
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) return false;

			return rs.getBoolean("success");
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean startSpectatorMode(Player player) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("INSERT INTO spectatormode(id_player) VALUES (?)");
			statement.setString(1, player.getId().toString());
			
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean stopSpectatorMode(Player player) {
		try {
			@Cleanup Connection connection = getConnection();
			String statementString = "DELETE FROM spectatormode WHERE id_player='" + player.getId() + "'";
			
			@Cleanup Statement statement = connection.createStatement();
			statement.execute(statementString);
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean hasPermission(UUID playerId, String permission) {
		try {
			@Cleanup Connection connection = getConnection();
			@Cleanup PreparedStatement statement = connection.prepareStatement("SELECT permission FROM player_permission WHERE id_player =? UNION SELECT permission FROM group_permission WHERE group_id IN ( SELECT group_id FROM player_group WHERE player_id =? UNION SELECT group_child FROM group_child WHERE group_id = ( SELECT group_id FROM player_group WHERE player_id =? LIMIT 1 ))");
			statement.setString(1, playerId.toString());
			statement.setString(2, playerId.toString());
			statement.setString(3, playerId.toString());
			
			@Cleanup ResultSet rs = statement.executeQuery();
			
			while(rs.next()) {
				String perm = rs.getString("permission");
				
				if(permission.matches(perm)) return true;
			}
			
			return false;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
