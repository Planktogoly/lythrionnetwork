package net.planckton.brain.command.executors;

import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;

public class HelpOpCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if (arg.length == 0) {
			player.sendMessage(ChatColor.RED + "usage: /helpop <question>");
			return;
		}
		
		if (player.hasHelpop() && !Brain.getBrainServer().getHelpopManager().getHelpopsByPlayerUUID().get(player.getId()).isHandled()) {
			player.sendMessage(ChatColor.RED + "Please be patient, you can only send one question at a time.");
			return;
		}
		
		String message = "";
		for(int i = 0; i < arg.length; i++) {
			message += arg[i];
			
			if(i < arg.length) message += " ";
		}
		
		if (Brain.getBrainServer().getStaff().size() == 0) {
			player.sendMessage(ChatColor.RED + "There is no staff online!");
			return;
		}
		
		Brain.getBrainServer().getHelpopManager().createHelpOp(player, message);
		return;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return EMPTY_LIST;
	}

}
