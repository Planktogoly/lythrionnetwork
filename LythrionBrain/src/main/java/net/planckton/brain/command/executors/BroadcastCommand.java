package net.planckton.brain.command.executors;

import java.util.Collection;
import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.network.BrainServer;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;

public class BroadcastCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if(!player.hasRank(Rank.SRMOD)) return;
		
		if(arg.length == 0) {
			player.sendMessage(ChatColor.RED + "Usage: /" + command + " [global or local] <message>");
			return;
		}
		
		String message = ChatColor.YELLOW + " " + ChatColor.BOLD;
		int messageStart = 1;
		Collection<Player> players;
		if(arg[0].equalsIgnoreCase("local")) {
			message += "LOCAL";
			
			players = player.getServerAdapter().getPlayers();
		}
		else {
			message += "GLOBAL";
			
			if(!arg[0].equalsIgnoreCase("global")) messageStart = 0;
			players = Brain.getBrainServer().getPlayers();
		}
		
		message += ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + " || " + player.getRank().getColor() + player.getName() + ChatColor.GRAY + " " + BrainServer.ARROWS_RIGHT + ChatColor.WHITE + " ";
		
		
		for(int i = messageStart; i < arg.length; i++) {
			message += arg[i];
			
			if(i >= arg.length - 1) continue;
			message += " ";
		}
		
		message = ChatColor.translateAlternateColorCodes('&', message);
		
		for(Player reciever : players) {
			try {
				reciever.sendMessage(message);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return null;
	}
}
