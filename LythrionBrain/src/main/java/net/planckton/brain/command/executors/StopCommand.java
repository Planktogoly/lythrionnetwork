package net.planckton.brain.command.executors;

import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;

public class StopCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if(!player.hasRank(Rank.OWNER)) {
			return;
		}
		
		Brain.stop();
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return null;
	}
}
