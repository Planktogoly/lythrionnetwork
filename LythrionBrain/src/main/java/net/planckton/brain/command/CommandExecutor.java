package net.planckton.brain.command;

import java.util.ArrayList;
import java.util.List;

public interface CommandExecutor {
	public static List<String> EMPTY_LIST = new ArrayList<String>();
	
	public void execute(CommandSender sender, String command, String arg[]);
	public List<String> onTabComplete(CommandSender sender, String arg[]);
}