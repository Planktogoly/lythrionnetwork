package net.planckton.brain.command;

import net.planckton.brain.Brain;

public class ConsoleSender implements CommandSender {
	public static final ConsoleSender sender = new ConsoleSender();
	
	public static ConsoleSender getSender() {
		return sender;
	}
	
	private ConsoleSender() {
		
	}
	
	@Override
	public void sendMessage(String message) {
		Brain.info(message);
	}
	
	@Override
	public String getName() {
		return "Lythrion Console";
	}
}