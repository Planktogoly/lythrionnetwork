package net.planckton.brain.command.executors;

import java.util.List;
import java.util.UUID;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.helpop.HelpOp;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.Prefix;

public class StaffCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if (!player.getRank().hasPower(Rank.JRMOD)) return;
		
		if (arg.length <= 0) {
			player.sendMessage(ChatColor.RED + "Usage: /staff help <id>");
			return;
		}
		
		if (arg[0].equalsIgnoreCase("help")) {
			UUID uuid = null;
			try {
				uuid = UUID.fromString(arg[1]);
			} catch (Exception e) {
				player.sendMessage(Prefix.LYTHRION_RED_ARROW + "This is not an ID.");
				return;
			}
			HelpOp helpop = Brain.getBrainServer().getHelpopManager().getHelpops().get(uuid);
			
			if (helpop == null) {
				player.sendMessage(Prefix.LYTHRION_RED_ARROW + "There is no question with this ID");
				return;
			}
			
			if (helpop.isHandled()) {
				player.sendMessage(Prefix.LYTHRION_RED_ARROW + "This player is already getting helped by " + helpop.getHelper().getDisplayName());
				return;
			}
			
			Player questioner = Brain.getPlayer(helpop.getQuestioner());
			if (questioner == null) {
				player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Player is not online anymore!");
				return;
			}
			
			player.sendMessage(Prefix.LYTHRION_RED_ARROW + ChatColor.WHITE + "You are now helping " + questioner.getDisplayName());
			questioner.sendMessage(player.getDisplayName() + ChatColor.WHITE + " is going to help you with your question!");			
			helpop.setHandled(true);
			helpop.setHelper(player);
			return;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return EMPTY_LIST;
	}

}
