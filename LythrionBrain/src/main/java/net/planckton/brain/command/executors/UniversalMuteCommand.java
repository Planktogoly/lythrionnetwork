package net.planckton.brain.command.executors;

import java.util.List;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.planckton.blood.packet.PacketMessage;
import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.network.BrainServer;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.Prefix;

public class UniversalMuteCommand implements CommandExecutor {
	private BrainServer heartServer = Brain.getBrainServer();
	private boolean universalMuteEnabled;
	
	@Override
	public void execute(CommandSender sender, String command, String[] args) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if(!player.hasRank(Rank.SRMOD)) {
			return;
		}
		
		universalMuteEnabled = !universalMuteEnabled;
		
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("globalmute");
		out.writeBoolean(universalMuteEnabled);
		
		byte[] bytes = out.toByteArray();
		
		for(ServerAdapter server : heartServer.getServerAdapters()) {
			server.getChannel().writeAndFlush(new PacketMessage("HEART", server.getName(), "chat", bytes));
		}
		
		if(universalMuteEnabled) {
			heartServer.broadcastMessage(Prefix.LYTHRION_RED_ARROW + ChatColor.GRAY + "UniversalMute has been " + ChatColor.RED + "enabled.");
		}
		else {
			heartServer.broadcastMessage(Prefix.LYTHRION_RED_ARROW + ChatColor.GRAY + "UniversalMute has been " + ChatColor.GREEN + "disabled.");
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return EMPTY_LIST;
	}
}