package net.planckton.brain.command.executors;

import java.util.List;
import java.util.concurrent.Callable;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;

public class GtpCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] args) {
		if(!(sender instanceof Player)) return;
		final Player senderPlayer = (Player)sender;
		
		if (!senderPlayer.hasRank(Rank.JRMOD)) return;
		
		if(args.length < 1) {
			senderPlayer.sendMessage(ChatColor.RED + "Usage: /gtp <name>");
			return;
		}
		
		final Player targetPlayer = Brain.getPlayer(args[0]);
		if(targetPlayer == null) {
			senderPlayer.sendMessage(ChatColor.RED + "Player was not found");
			return;
		}
		
		if(targetPlayer == senderPlayer) {
			senderPlayer.sendMessage(ChatColor.RED + "This is you ey");
			return;
		}
		
		senderPlayer.sendMessage(ChatColor.GREEN + "Teleporting...");
		
		ServerAdapter targetAdapter = targetPlayer.getServerAdapter();
		if(targetAdapter != senderPlayer.getServerAdapter()) {
			senderPlayer.sendMessage(ChatColor.GREEN + "Connecting to " + targetAdapter.getName() + "...");
			senderPlayer.connect(targetAdapter);
			
			Brain.getExecutorPool().submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					Thread.sleep(500);
					
					senderPlayer.executeServerCommand("/tp " + targetPlayer.getName()); 
					
					return null;
				}
			});
		}
		else senderPlayer.executeServerCommand("/tp " + targetPlayer.getName()); 
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return null;
	}
}