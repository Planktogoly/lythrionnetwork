package net.planckton.brain.command.executors;

import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;

public class MessageCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if(arg.length <= 1) {
			player.sendMessage(ChatColor.RED + "Usage: /" + command + " <player> <message>");
			return;
		}
		
		Player toPlayer = Brain.getPlayer(arg[0]);
		
		if(toPlayer == null) {
			toPlayer = Brain.getNickedPlayer(arg[0]);
			
			if(toPlayer == null) {
				player.sendMessage(ChatColor.RED + "This player is not online");
				return;
			}
		}
		else if(toPlayer.isNicked() && toPlayer.getName().equalsIgnoreCase(arg[0])) {
			player.sendMessage(ChatColor.RED + "This player is not online");
			return;
		}
		
		if (toPlayer.equals(player)) {
			player.sendMessage(ChatColor.RED + "You cannot message yourself.");
			return;
		}
		
		if (!toPlayer.hasRank(Rank.JRMOD)) {
			if(IgnoreCommand.hasIgnored(player, toPlayer)) {
				player.sendMessage(ChatColor.RED + "You have ignored this player. Use /ignore remove <name>.");
				return;
			}
			
			if(IgnoreCommand.hasIgnored(toPlayer, player)) {
				player.sendMessage(ChatColor.RED + "You cannot message this player.");
				return;
			}
		}
		
		String message = "";
		for(int i = 1; i < arg.length; i++) {
			message += arg[i];
			
			if(i < arg.length - 1) message += " ";
		}
		
		toPlayer.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "MSG "+ player.getDisplayName() + ChatColor.DARK_GRAY + " -> " + toPlayer.getDisplayName() + ChatColor.DARK_GRAY + " " + ChatColor.WHITE + message);
		player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD  + "MSG "+ player.getDisplayName() + ChatColor.DARK_GRAY + " -> " + toPlayer.getDisplayName() + ChatColor.DARK_GRAY + " " + ChatColor.WHITE + message);
	
		toPlayer.setReplyPlayer(player.getName());
		player.setReplyPlayer(toPlayer.getName());
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return null;
	}

}
