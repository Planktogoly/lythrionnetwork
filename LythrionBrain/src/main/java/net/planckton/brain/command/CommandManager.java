package net.planckton.brain.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;

import lombok.NonNull;
import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;

public class CommandManager {
	private Map<String, CommandExecutor> executors;
	
	public CommandManager() {
		executors = new ConcurrentHashMap<>();
	}
	
	public void registerCommand(CommandExecutor executor, String... commands) {
		for(String command : commands) executors.put(command.toLowerCase(), executor);
	}

	public void onCommand(@NonNull CommandSender sender, @NonNull String command) {		
		String[] args = command.split(" ");
		if(args[0].startsWith("/")) args[0] = args[0].substring(1, args[0].length());
		
		CommandExecutor executor = executors.get(args[0].toLowerCase());
		if(executor == null) {
			Brain.info("Unkown command: " + args[0]);
			return;
		}
		
		Brain.info(sender.getName() + " command: " + command);
		try {
			executor.execute(sender, args[0], Arrays.copyOfRange(args, 1, args.length));
		} catch(Exception e) {
			Brain.info("An error occured while preforming command:\"" + command + "\" by " + sender.getName());
			e.printStackTrace();
		}
	}
	
	public List<String> onTabComplete(Player player, String command) {		
		String[] args = command.split(" ");
		if (!command.endsWith(" ") && args.length == 1) return null;
		if(args[0].startsWith("/")) args[0] = args[0].substring(1, args[0].length());
		
		CommandExecutor executor = executors.get(args[0].toLowerCase());
		if(executor == null) {
			Brain.info("Unknown tab command: " + args[0]);
			return null;
		}
		
		Brain.debug(player.getName() + " tab: " + command);
		List<String> suggestions = executor.onTabComplete(player, Arrays.copyOfRange(args, 1, args.length));
		
		if(suggestions == null) {
			suggestions = new ArrayList<String>();
			if (args.length - 1 == 0) {
				for(Player p : Brain.getPlayers()) {
					suggestions.add(p.getName());
				}
				return suggestions;
			}
			
			SortedMap<String, Player> players = Brain.getPlayers(args[args.length - 1]);
			if(players == null)	return null;

			for(Player p : players.values()) { 
				suggestions.add(p.getName());
			}
			
			Iterator<Entry<String, Player>> it = Brain.getBrainServer().getPlayerManager().getNickedPlayers().entrySet().iterator();
		    while (it.hasNext()) {
		        Entry<String, Player> pair = it.next();
		        
		        if(!pair.getKey().toLowerCase().startsWith(args[args.length - 1].toLowerCase())) continue;
				
				suggestions.add(pair.getValue().getNickName());
		    }
		}
		
		return suggestions;
	}

	public Set<String> getRegisteredCommands() {
		return executors.keySet();
	}
}