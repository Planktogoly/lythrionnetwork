package net.planckton.brain.command.executors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;
import net.planckton.brain.util.Prefix;

public class ListCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] args) {
		if (!(sender instanceof Player)) return;
		Player senderPlayer = (Player)sender;
		
		sender.sendMessage("");
		sender.sendMessage(Prefix.LYTHRION_RED_ARROW + "Players online" + ChatColor.WHITE + ": " + ChatColor.RED + Brain.getBrainServer().getPlayers().size());
		sender.sendMessage(Prefix.LYTHRION_RED_ARROW + "Players in " + ChatColor.GRAY + senderPlayer.getServerAdapter().getName() + ChatColor.WHITE + ": " 
		+ ChatColor.RED + senderPlayer.getServerAdapter().getPlayers().size());
		
		ArrayList<Player> players = new ArrayList<>(Brain.getBrainServer().getStaff());
		Collections.sort(players, new Comparator<Player>() {

			@Override
			public int compare(Player a, Player b) {					
				return a.getRank().getRankId() - b.getRank().getRankId();
			}
		});		
				
		String message = Prefix.LYTHRION_RED_ARROW + "Online staff" + ChatColor.WHITE + ": ";
		
		for (Player staff : players) {
			message += staff.getRank().getColor() + staff.getName() + ChatColor.GRAY + ", ";
		}		
		sender.sendMessage(message);
		sender.sendMessage(" ");
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return EMPTY_LIST;
	}
}