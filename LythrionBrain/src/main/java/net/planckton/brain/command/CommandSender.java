package net.planckton.brain.command;

public interface CommandSender {
	public String getName();
	public void sendMessage(String message);
}