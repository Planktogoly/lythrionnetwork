package net.planckton.brain.command.executors;

import java.util.LinkedList;
import java.util.List;

import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.party.Party;
import net.planckton.brain.party.PartyManager;
import net.planckton.brain.player.Player;
import net.planckton.brain.util.Prefix;

public class PartyCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] args) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		PartyManager partyManager = Brain.getBrainServer().getPartyManager();
		
		if (args.length < 1) {
			player.sendMessage(ChatColor.GOLD + "Party commands:");
			player.sendMessage("/party join -" + ChatColor.GRAY + " Accept a party invite.");
			player.sendMessage("/party invite -" + ChatColor.GRAY + " Invite someone to the party");
			player.sendMessage("/party leave -" + ChatColor.GRAY + " Leave the party");
			player.sendMessage("/party kick -" + ChatColor.GRAY + " Kick someone from the party");
			player.sendMessage("/party disband -" + ChatColor.GRAY + " Disband the party");
			player.sendMessage("/party list -" + ChatColor.GRAY + " Shows who is in your party");
			player.sendMessage("/party promote - " + ChatColor.GRAY + "Promote someone to party leader");
			player.sendMessage("/party warp -" + ChatColor.GRAY + " Warps all members to your server");
			player.sendMessage("/party open -" + ChatColor.GRAY + " Makes your party open for everyone");
			return;
		}		

		if (args[0].equalsIgnoreCase("invite")) {
		   if (args.length < 2) {
			   player.sendMessage(Prefix.PARTY_ARROWS + "/party invite <player> - Invite someone to the party");
			   return;
		   }
		   
		   Party party = partyManager.getPartyByPlayer(player);
		   Player invitedPlayer = Brain.getBrainServer().getPlayer(args[1]);
		   
		   
		   if (party == null) {
			   if (invitedPlayer == null) {
				   player.sendMessage(Prefix.PARTY_ARROWS + "Player is not online.");
				   return;
			   }
			   
			   if (player.equals(invitedPlayer)) {
				   player.sendMessage(Prefix.PARTY_ARROWS + "You can not invite yourself.");
				   return;
			   }
			   
			   new Party(player, invitedPlayer);
			   return;
		   }

		   if (invitedPlayer == null) {
			   player.sendMessage(Prefix.PARTY_ARROWS + "Player is not online.");
		   }
		   
		   if (player.equals(invitedPlayer)) {
			   player.sendMessage(Prefix.PARTY_ARROWS + "You can not invite yourself.");
			   return;
		   }
		   
		   party.invitePlayer(invitedPlayer);
		   return;
		}
		else if (args[0].equalsIgnoreCase("join")) {
			if (args.length < 2) {
				player.sendMessage(Prefix.PARTY_ARROWS + "/party join <player> - Accept a party invite.");
				return;
			}
			
			if (player.isInParty()) {
				player.sendMessage(Prefix.PARTY_ARROWS + ChatColor.RED + "You are already in the party! Use /party leave first.");
				return;
			}
			
			Player leaderParty = Brain.getPlayer(args[1]);
			if (leaderParty == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "Player is not online.");
		    }
			
			Party party = partyManager.getPartyByLeader(leaderParty);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "This player does not have a party.");
				return;
			}	
			
			party.acceptPlayer(player);
			return;
		}
		else if (args[0].equalsIgnoreCase("list")) {
			Party party = partyManager.getPartyByPlayer(player);			   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You do not have a party. Create one with the command /party invite <player>.");
				return;
		    }

			player.sendMessage(Prefix.PARTY_ARROWS + "Party list:");
			
			int i = 2;
			player.sendMessage(ChatColor.GRAY + "" + 1 + ". " + party.getLeader().getDisplayName() + ChatColor.GRAY + " (Leader)");
			
			for (Player member : party.getPlayers()) {
				if (party.isLeader(member)) continue;
				
				player.sendMessage(ChatColor.GRAY + "" + i + ". " + member.getDisplayName());
				i++;
			}
			return;
		}
		else if (args[0].equalsIgnoreCase("disband")) {
			Party party = partyManager.getPartyByLeader(player);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not the party leader or you dont have a party.");
				return;
			}	
			
			party.disband(); 
		}
		else if (args[0].equalsIgnoreCase("kick")) {
			if (args.length < 2) {
				player.sendMessage(Prefix.PARTY_ARROWS + "/party kick <player> - kicks the specific player from the party");
				return;
			}
			
			Party party = partyManager.getPartyByLeader(player);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not the party leader or you do not have a party.");
				return;
			}	
			
			Player kickedPlayer  = Brain.getPlayer(args[1]);
			if (kickedPlayer == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "Player is not online.");
				return;
		    }
			
			if (!party.isInParty(kickedPlayer)) {
				player.sendMessage(Prefix.PARTY_ARROWS + ChatColor.RED + "This player is not in the party.");
				return;
			}
			
			party.kickPlayer(kickedPlayer);
			return;	
		}
		else if (args[0].equalsIgnoreCase("leave")) {
			Party party = partyManager.getPartyByPlayer(player);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not in a party.");
				return;
			}	
			 
			party.leave(player);
		}
		else if (args[0].equalsIgnoreCase("promote")) {
			Party party = partyManager.getPartyByLeader(player);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not the party leader or you dont have a party.");
				return;
			}	
			
			Player promotePlayer  = Brain.getPlayer(args[1]);
			if (promotePlayer == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "Player is not online.");
				return;
		    }
			
			if (!party.isInParty(promotePlayer)) {
				player.sendMessage(Prefix.PARTY_ARROWS + ChatColor.RED + "This player is not in the party.");
				return;
			}
			
			party.promotePlayer(promotePlayer);
			return;
		}
		else if (args[0].equalsIgnoreCase("warp")) {
			   Party party = partyManager.getPartyByLeader(player);			   
			   if (party == null) {
				   player.sendMessage(Prefix.PARTY_ARROWS + "You do not have a party. Create one with the command /party invite <player>.");
				   return;
			   }
			   
			   party.warpPlayers();
			   return;
		}
		else if (args[0].equalsIgnoreCase("open")) {
			Party party = partyManager.getPartyByLeader(player);		   
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not the party leader or you dont have a party.");
				return;
			}
			
			party.open(); 
			return;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		if(arg.length > 0) return null;
		
		List<String> suggestions = new LinkedList<String>();
		suggestions.add("join");
		suggestions.add("invite");
		suggestions.add("leave");
		suggestions.add("kick");
		suggestions.add("disband");
		suggestions.add("list");
		suggestions.add("promote");
		suggestions.add("warp");
		suggestions.add("open");
		
		if(suggestions.size() == 0) return EMPTY_LIST;
		else return suggestions;
	}

}
