package net.planckton.brain.command.executors;

import java.util.List;

import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;

public class UsageCommand implements CommandExecutor {

	@Override
	public void execute(CommandSender sender, String command, String[] arg) {
		if(!(sender instanceof Player)) return;
		Player player = (Player) sender;
		
		if(!player.hasRank(Rank.OWNER)) {
			return;
		}
		
		showRamUsage(sender);
		sender.sendMessage(ChatColor.RED + "Performing garbage collection...");
		System.gc();
		showRamUsage(sender);
	}
	
	private void showRamUsage(CommandSender sender) {
		Runtime runtime = Runtime.getRuntime();

	    long maxMemory = runtime.totalMemory();
	    long freeMemory = runtime.freeMemory();
	    int mb = 1024 * 1024; 
		
		sender.sendMessage(ChatColor.GOLD + "RAM Usage: " + ChatColor.GRAY + (int)((maxMemory - freeMemory) / mb) + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + (int)(maxMemory / mb) + ChatColor.GOLD + "Mb");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		return null;
	}
}
