package net.planckton.brain.command.executors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import com.google.common.base.Joiner;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.planckton.brain.Brain;
import net.planckton.brain.Database;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandExecutor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.HashMapList;
import net.planckton.brain.util.Prefix;

public class IgnoreCommand implements CommandExecutor {
	private static final Joiner namesJoiner = Joiner.on(',');
	private static HashMapList<UUID, UUID> ignoredPlayers = new HashMapList<>();
	
	public static boolean hasIgnored(Player player1, Player player2) {
		return ignoredPlayers.containsValue(player1.getId(), player2.getId());
	}
	
	@Override
	public void execute(CommandSender sender, String command, final String[] args) {
		if(!(sender instanceof Player)) return;
		final Player player = (Player) sender;
		
		if(args.length < 1) {
			player.sendMessage(ChatColor.RED + "Usage: /ignore <player>");
			player.sendMessage(ChatColor.RED + "Usage: /ignore list");
			player.sendMessage(ChatColor.RED + "Usage: /ignore remove <player>");
			return;
		}
		
		if(args[0].equalsIgnoreCase("list")) {
			HashSet<UUID> players = ignoredPlayers.get(player.getId());
			if(players == null) {
				player.sendMessage(ChatColor.RED + "You have no ignored players.");
				return;
			}
			
			final List<UUID> resolveUuidNames = Collections.synchronizedList(new ArrayList<UUID>());
			
			final List<String> resolvedNames = new LinkedList<String>();
			for(UUID uuid : players) {
				Player ignoredPlayer = Brain.getPlayer(uuid);
				
				if(ignoredPlayer == null) {
					resolveUuidNames.add(uuid);
					continue;
				}
				
				resolvedNames.add(ignoredPlayer.getName());
			}
			
			if(resolveUuidNames.size() > 0) {
				final LinkedList<Future<String>> futures = new LinkedList<>();
				
				for(UUID uuid : resolveUuidNames) {
					Future<String> future = Database.getNameByUUID(uuid);
					futures.add(future);
					
					future.addListener(new GenericFutureListener<Future<String>>() {

						@Override
						public void operationComplete(Future<String> future) throws Exception {
							if(!future.isSuccess()) return;
							
							resolvedNames.add(future.get());
						}
					});
				}
				
				Brain.getExecutorPool().submit(new Callable<Void>() {

					@Override
					public Void call() throws Exception {
						for(Future<String> future : futures) future.sync();
						
						sendListMessage(player, resolvedNames);
						return null;
					}
				});
				return;
			}
			
			sendListMessage(player, resolvedNames);
			return;
		}
		
		if(args[0].equalsIgnoreCase("remove")) {
			if(args.length < 2) {
				player.sendMessage(ChatColor.RED + "Usage: /ignore remove <player>");
				return;
			}
			
			Player removePlayer = Brain.getPlayer(args[1]);
			
			if(removePlayer == null) {
				removePlayer = Brain.getNickedPlayer(args[0]);
				
				if(removePlayer == null) {
					Database.getUUIDbyName(args[1]).addListener(new GenericFutureListener<Future<UUID>>() {

						@Override
						public void operationComplete(Future<UUID> future) throws Exception {
							if(!future.isSuccess() || future.get() == null) {
								player.sendMessage(Prefix.LYTHRION_RED_ARROW + ChatColor.RED + "Could not find player with the name: " + args[1]);
								return;
							}
							
							ignoredPlayers.remove(player.getId(), future.get());
							player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Succesfully un-ignored player " + args[1]);
						}
					});
					return;
				}
			}
			
			ignoredPlayers.remove(player.getId(), removePlayer.getId());
			player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Succesfully un-ignored player " + removePlayer.getName());
			return;
		}
		
		Player ignorePlayer = Brain.getPlayer(args[0]);
		
		if(ignorePlayer == null) {
			ignorePlayer = Brain.getNickedPlayer(args[0]);
			
			if(ignorePlayer == null) {
				Database.getUUIDbyName(args[0]).addListener(new GenericFutureListener<Future<UUID>>() {

					@Override
					public void operationComplete(Future<UUID> future) throws Exception {
						if(!future.isSuccess() || future.get() == null) {
							player.sendMessage(Prefix.LYTHRION_RED_ARROW + ChatColor.RED + "Could not find player with the name: " + args[0]);
							return;
						}
						
						UUID uuid = future.get();
						Database.getRank(uuid).addListener(new GenericFutureListener<Future<? super Rank>>() {

							@Override
							public void operationComplete(Future<? super Rank> future) throws Exception {
								Rank rank = (Rank) future.get();
								if (rank.hasPower(Rank.JRMOD)) {
									player.sendMessage(Prefix.LYTHRION_RED_ARROW + "You can't ignore mods and higher.");
									return;
								}
								
								ignoredPlayers.add(player.getId(), uuid);
								player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Succesfully ignored player " + args[0]);
							}							
						});
					}
				});
				return;
			}
		}
		
		if (ignorePlayer.hasRank(Rank.JRMOD)) {
			player.sendMessage(Prefix.LYTHRION_RED_ARROW + "You can't ignore mods and higher.");
			return;
		}
 		
		ignoredPlayers.add(player.getId(), ignorePlayer.getId());
		player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Succesfully ignored player " + ignorePlayer.getName());
		return;
	}
	
	private static void sendListMessage(Player player, List<String> names) {
		player.sendMessage(ChatColor.RED + "Ignored players: " + namesJoiner.join(names));
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String[] arg) {
		if(arg.length > 0) {
			if (arg[0].equalsIgnoreCase("list")) return EMPTY_LIST;
			return null;
		}
		
		List<String> suggestions = new LinkedList<String>();
		suggestions.add("list");
	    suggestions.add("remove");
		
		if(suggestions.size() == 0) return EMPTY_LIST;
		else return suggestions;
	}
}