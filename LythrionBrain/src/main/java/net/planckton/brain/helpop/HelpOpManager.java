package net.planckton.brain.helpop;

import java.util.HashMap;
import java.util.UUID;

import lombok.Getter;
import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.chat.ClickEvent;
import net.planckton.brain.chat.ComponentBuilder;
import net.planckton.brain.chat.HoverEvent;
import net.planckton.brain.chat.HoverEvent.Action;
import net.planckton.brain.chat.TextComponent;
import net.planckton.brain.chat.serializer.ComponentSerializer;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.Rank;
import net.planckton.brain.util.Prefix;

public class HelpOpManager {
	
	@Getter private HashMap<UUID, HelpOp> helpops = new HashMap<>();
	@Getter private HashMap<UUID, HelpOp> helpopsByPlayerUUID = new HashMap<>();

	public void createHelpOp(Player questioner, String question) {
		HelpOp helpop = new HelpOp(questioner, questioner.getId(), question);
		helpop.setId(UUID.randomUUID());
		
		questioner.sendMessage(Prefix.LYTHRION_RED_ARROW + "Your question has been sent! Please wait patently.");		
		
		TextComponent message = new TextComponent(ChatColor.YELLOW.toString() + ChatColor.BOLD + "\u258E " + ChatColor.WHITE + "Click");
		TextComponent clickMessage = new TextComponent(ChatColor.RED + " HERE ");
		clickMessage.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("Click to help this player").create()));
		clickMessage.setClickEvent(new ClickEvent(net.planckton.brain.chat.ClickEvent.Action.RUN_COMMAND, "/staff help " + helpop.getId()));
		message.addExtra(clickMessage);
		message.addExtra(ChatColor.WHITE + "to help this player");		
		
		for (Player player : Brain.getPlayers()) {
			if (player.getRank().hasPower(Rank.JRMOD)) {
				player.sendMessage(" ");
				player.sendMessage(ChatColor.YELLOW.toString() + ChatColor.BOLD + "\u258E Q " + ChatColor.GRAY + questioner.getName() + ChatColor.DARK_GRAY + ": " 
						+ ChatColor.WHITE + question);
				player.sendAdvancedMessage(ComponentSerializer.toString(message));
				player.sendMessage(" ");
			}
		}
		
		helpopsByPlayerUUID.put(questioner.getId(), helpop);
		helpops.put(helpop.getId(), helpop);		
	}	
}
