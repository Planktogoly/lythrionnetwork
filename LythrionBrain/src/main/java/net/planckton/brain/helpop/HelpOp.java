package net.planckton.brain.helpop;

import java.util.Timer;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;
import net.planckton.brain.util.Prefix;

public class HelpOp extends java.util.TimerTask {
	
	@Getter private String questioner;
	@Getter private UUID questionerUUID;
	@Getter private String question;
	
	@Getter @Setter private Player helper;
	@Getter @Setter private boolean handled;
	
	@Getter @Setter private UUID id;
	@Getter private Timer timer;
	
	public HelpOp(Player questioner, UUID questionerUUID, String question) {
		this.question = question;
		this.questionerUUID = questionerUUID;
		this.questioner = questioner.getName();
		
		timer = new Timer();
		timer.schedule(this, 1000 * 180);
	}

	@Override
	public void run() {
		Player player = Brain.getPlayer(questioner);
		if (player != null) {
			if (!handled) player.sendMessage(Prefix.LYTHRION_RED_ARROW + "Your question expired! There may be staff AFK, sorry for the inconvenience.");
		}		
		
		Brain.getBrainServer().getHelpopManager().getHelpopsByPlayerUUID().remove(questionerUUID);	
		Brain.getBrainServer().getHelpopManager().getHelpops().remove(id);			
		System.out.println("Removed helpop from " + questioner + " Handled " + handled);
	}

}
