package net.planckton.brain.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class HashMapList<K,V> implements MapSet<K, V> {
	private @NonNull Map<K, HashSet<V>> map;
	
	public HashMapList() {
		this(new HashMap<>());
	}
	
	public void add(K key, V value) {
		HashSet<V> list = map.get(key);
		if(list == null) list = new HashSet<V>();
		
		list.add(value);
		map.put(key, list);
	}
	
	public HashSet<V> get(Object key) {
		return map.get(key);
	}
	
	public void remove(K key, V value) {
		HashSet<V> list = map.get(key);
		if(list == null) return;
		
		list.remove(value);
		map.put(key, list);
	}
	
	public boolean containsValue(K key, V value) {
		HashSet<V> list = map.get(key);
		if(list == null) return false;
		
		return list.contains(value);
	}
}
