package net.planckton.brain.util;

import net.planckton.brain.chat.ChatColor;

public class Prefix {
	public static final String LYHTRION = "Lythrion" + ChatColor.RESET;
	public static final String LYHRION_RED = ChatColor.RED.toString() + ChatColor.BOLD + "Lythrion" + ChatColor.RESET;
	public static final String LYTHRION_RED_ARROW = ChatColor.RED.toString() + ChatColor.BOLD + "Lythrion" + ChatColor.GRAY + " " + UniChars.ARROWS_RIGHT + " " + ChatColor.RESET;
	public static final String PARTY_ARROWS = ChatColor.GOLD + "Lythrion" + ChatColor.BOLD + "Party" + ChatColor.GRAY + " " + UniChars.ARROWS_RIGHT + " " + ChatColor.RESET;
}
