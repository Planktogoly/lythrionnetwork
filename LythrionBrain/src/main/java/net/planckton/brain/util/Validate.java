package net.planckton.brain.util;

public class Validate {
	public static void notNull(Object... objects) {
		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			if(object != null) continue;
			
			throw new RuntimeException("Argument #" + i + " cannot be null");
		}
	}
}
