package net.planckton.brain.player;

import java.util.UUID;

public class GameProfile {
	private final UUID uuid;
	private final String name;
	
	public GameProfile(UUID uuid, String name) {
		this.uuid = uuid;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public UUID getUuid() {
		return uuid;
	}
}
