package net.planckton.brain.player;

import java.util.UUID;

import com.google.gson.JsonObject;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.planckton.blood.packet.PacketPlayerBalanceConnect;
import net.planckton.blood.packet.PacketPlayerCommand;
import net.planckton.blood.packet.PacketPlayerConnect;
import net.planckton.blood.packet.PacketPlayerMessage;
import net.planckton.blood.packet.PacketPlayerAdvancedMessage;
import net.planckton.brain.Brain;
import net.planckton.brain.Database;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.command.CommandSender;
import net.planckton.brain.network.BungeeAdapter;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.util.Validate;
import net.planckton.brain.web.JsonSerializable;

public class Player implements CommandSender, JsonSerializable {
	@Getter private UUID id;
	@Getter private String name;
	@Getter @Setter private String nickName;
	
	@Getter @Setter private @NonNull BungeeAdapter bungeeAdapter;
	@Getter private ServerAdapter serverAdapter;
	
	@Getter private Rank rank;	
	@Getter private int group_id;
	
	@Getter @Setter private @NonNull String replyPlayer;
	
	@Getter @Setter private ServerAdapter hub;
	
	protected Player(UUID id, BungeeAdapter bungeeAdapter) {
		this.id = id;
		this.bungeeAdapter = bungeeAdapter;
		
		Database.getNameByUUID(id).addListener(new FutureListener<String>() { 
			@Override
			public void operationComplete(Future<String> future) throws Exception {
				setName(future.get());
			}
		});
		
		final Player thisPlayer = this;
		Database.getRank(id).addListener(new FutureListener<Rank>() {
			@Override
			public void operationComplete(Future<Rank> future) throws Exception {
				rank = future.get();
				
				Brain.getPlayerManager().addPlayerByRank(thisPlayer);
			}
		});
		
		Database.getGroupIDPlayer(id).addListener(new FutureListener<Integer>() {

			@Override
			public void operationComplete(Future<Integer> future) throws Exception {
				group_id = future.get();	
				
				Brain.getPlayerManager().addPlayersByGroupID(thisPlayer);
			}
		});
	}
	
	public void destroy() {
		if(bungeeAdapter != null) bungeeAdapter.removePlayer(id);
		if(serverAdapter != null) serverAdapter.removePlayer(id);
		
		id = null;
		name = null;
		bungeeAdapter = null;
		serverAdapter = null;
		rank = null;
		replyPlayer = null;
	}
	
	@Override
	public void sendMessage(@NonNull String message) {	
		if(bungeeAdapter == null) {
			Brain.info("[ERROR] Player does not have a bungee connection");
			Brain.info("[ERROR] Player: " + this);
			Brain.info(Thread.currentThread().getStackTrace().toString());
			return;
		}
		
		bungeeAdapter.getChannel().writeAndFlush(new PacketPlayerMessage(id, message));
	}

	public void sendAdvancedMessage(@NonNull String message) {	
		if(bungeeAdapter == null) {
			Brain.info("[ERROR] Player does not have a bungee connection");
			Brain.info("[ERROR] Player: " + this);
			Brain.info(Thread.currentThread().getStackTrace().toString());
			return;
		}
		
		bungeeAdapter.getChannel().writeAndFlush(new PacketPlayerAdvancedMessage(id, message));
	}
	
	@Override
	public String toString() {
		return "Player:[id=" + id + 
				",name=" + name + 
				",bungee=" + (bungeeAdapter != null ? bungeeAdapter.getName() : "null") + 
				",server=" + (serverAdapter != null ? serverAdapter.getName() : "null") + "]";
	}
	
	//protected methods
	protected void setName(@NonNull String name) {
		Validate.notNull(name);
		
		this.name = name;
		Brain.getPlayerManager().addPlayerByName(this);
	}
	
	/**
	 * Connects the player to the supplied server. If the connection
	 * cannot be made the server will stay where he was. To force use
	 * {@link #connect(ServerAdapter, boolean) connect(server, force)} instead.
	 * 
	 * @param server {@link ServerAdapter} to connect to
	 * @see {@link #connect(ServerAdapter, boolean) connect(server, force)} 
	 */
	public void connectToHub() {
		connect(hub, true);
	}
	
	/**
	 * Connects the player to the supplied server type with balancing.
	 * 
	 * @param servertype {@link ServerAdapter} to connect to
	 * @param identifier which server they need to connect to
	 */
	public void balanceConnectToServerType(String serverType, int identifier){
		getBungeeAdapter().getChannel().writeAndFlush(new PacketPlayerBalanceConnect(id, serverType, Integer.toString(identifier)));
	}
	
	/**
	 * Connects the player to the supplied server. If the connection
	 * cannot be made the server will stay where he was. To force use
	 * {@link #connect(ServerAdapter, boolean) connect(server, force)} instead.
	 * 
	 * @param server {@link ServerAdapter} to connect to
	 * @see {@link #connect(ServerAdapter, boolean) connect(server, force)} 
	 */
	public void connect(ServerAdapter server) {
		connect(server.getName());
	}
	
	/**
	 * Connects the player to the supplied server. If force is true
	 * and the connection cannot be made the player will connect
	 * to the default hub or get kicked.
	 * 
	 * @param server {@link ServerAdapter} to connect to
	 * @param force Whether to kick force the connection
	 */
	public void connect(ServerAdapter server, boolean force) {
		connect(server.getName(), force);
	}
	
	/**
	 * Connects the player to the supplied server. If the connection
	 * cannot be made the server will stay where he was. To force use
	 * {@link #connect(String, boolean) connect(server, force)} instead.
	 * 
	 * @param server Server to connect to
	 * @see {@link #connect(String, boolean) connect(server, force)} 
	 */
	public void connect(String server) {
		connect(server, false);
	}
	
	/**
	 * Connects the player to the supplied server. If force is true
	 * and the connection cannot be made the player will connect
	 * to the default hub or get kicked.
	 * 
	 * @param server Server to connect to
	 * @param force Whether to kick force the connection
	 */
	public void connect(String server, boolean force) {
		bungeeAdapter.getChannel().writeAndFlush(new PacketPlayerConnect(id, server, force));
	}
	
	/**
	 * Executes a command on behalf of the player on the current
	 * Server the player is logged on.
	 * 
	 * @param command The command to execute
	 */
	public void executeServerCommand(String command) {
		serverAdapter.getChannel().writeAndFlush(new PacketPlayerCommand(id, command));
	}
	
	//getters
	public String getDisplayName(boolean includeNickName) {
		return nickName != null && includeNickName ? (ChatColor.GRAY + nickName) : (rank.getColor() + name);
	}
	
	public String getDisplayName() {
		return getDisplayName(true);
	}
	
	public String getCurrentName(boolean includeNickName) {
		return nickName != null && includeNickName ? nickName : name;
	}
	
	public String getCurrentName() {
		return getCurrentName(true);
	}
	
	public void setServerAdapter(@NonNull ServerAdapter serverAdapter) {
		if(this.serverAdapter != null) this.serverAdapter.getPlayers().remove(id);
		
		if (serverAdapter.getServerType().equalsIgnoreCase("hub")) hub = serverAdapter;
		
		this.serverAdapter = serverAdapter;
	}

	public boolean isOnline() {
		return bungeeAdapter != null;
	}

	public boolean hasRank(@NonNull Rank rank) {
		return this.rank.hasPower(rank);
	}

	public boolean isNicked() {
		return nickName != null;
	}
	
	public boolean isInParty() {
		return Brain.getBrainServer().getPartyManager().getPartyByPlayer(this) != null;
	}
	
	public boolean hasHelpop() {
		return Brain.getBrainServer().getHelpopManager().getHelpopsByPlayerUUID().containsKey(id);
	}

	@Override
	public JsonObject toJson() {
		JsonObject object = new JsonObject();
		object.addProperty("name", getName());
		object.addProperty("server", getServerAdapter().getName());
		object.addProperty("bungee", getBungeeAdapter().getName());
		
		return object;
	}
}
