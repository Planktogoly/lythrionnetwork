package net.planckton.brain.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import lombok.Getter;
import net.planckton.brain.Brain;
import net.planckton.brain.network.BungeeAdapter;
import net.planckton.brain.network.BrainMessageCallback;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.util.HashMapList;
import net.planckton.brain.util.Validate;

public class PlayerManager implements BrainMessageCallback {
	private Map<UUID, Player> players = new ConcurrentHashMap<>();
	private Map<String, Player> playersByName = new ConcurrentHashMap<>();
	private TreeMap<String, Player> playersByNameSorted = new TreeMap<String, Player>();
	private HashMapList<Rank, Player> playersByRank = new HashMapList<>(new ConcurrentHashMap<>());
	private HashMapList<Integer, Player> playersByGroupid = new HashMapList<>(new ConcurrentHashMap<>());
	private Map<UUID, ServerAdapter> playerPreRegisteredServerAdapters = new ConcurrentHashMap<>();	
	@Getter private Map<Rank, ArrayList<Player>> staffByRank = new ConcurrentHashMap<>();
	@Getter private ArrayList<Player> staff = new ArrayList<>();
	@Getter private Map<String, Player> nickedPlayers = new ConcurrentHashMap<>();
	
	public PlayerManager() {
		Brain.getBrainServer().registerHeartMessageCallback(this);
	}
	
	public Player onPlayerBungeeJoin(BungeeAdapter adapter, UUID id) {
		Validate.notNull(adapter, id);
		
		Player player = players.get(id);
		
		if(player == null) {
			player = new Player(id, adapter);
			
			ServerAdapter server = playerPreRegisteredServerAdapters.remove(id);
			if(server != null) {
				player.setServerAdapter(server);
				
				server.addPlayer(player);
			}
		}
		else player.setBungeeAdapter(adapter);
		
		players.put(id, player);
		return player;
	}

	public void onPlayerBungeeQuit(UUID id) {
		Validate.notNull(id);
		
		Player player = players.get(id);
		
		if(player == null) return;
		
		players.remove(id);
		playersByRank.remove(player.getRank(), player);
		playersByName.remove(player.getName().toLowerCase());
		playersByNameSorted.remove(player.getName().toLowerCase());
		playersByGroupid.remove(player.getGroup_id(), player);
		if (staffByRank.containsKey(player.getRank())) staffByRank.remove(player.getRank());
		if (staff.contains(player)) staff.remove(player);
		if(player.isNicked()) nickedPlayers.remove(player.getNickName());
		
		player.destroy();
	}
	
	@Override
	public void onBrainMessage(String channel, byte[] data) {
		if(!channel.equals("nickname")) return;
		ByteArrayDataInput in = ByteStreams.newDataInput(data);
		
		String action = in.readUTF();
		if(action.equals("update")) {
			UUID playerId = UUID.fromString(in.readUTF());
			String nickedName = in.readUTF();
			
			Player player = getPlayer(playerId);
			if(player == null) return;
			
			player.setNickName(nickedName);
			nickedPlayers.put(nickedName.toLowerCase(), player);
		}
		else if(action.equals("remove")) {
			UUID playerId = UUID.fromString(in.readUTF());
			
			Player player = getPlayer(playerId);
			if(player == null) return;
			
			nickedPlayers.remove(player.getNickName().toLowerCase());
			player.setNickName(null);
		}
	}
	
	//getters
	public Player getPlayer(UUID id) {
		Validate.notNull(id);
		
		return players.get(id);
	}
	
	public Collection<Player> getPlayers() {
		return players.values();
	}

	public Player getPlayer(String name) {
		Validate.notNull(name);
		
		return playersByName.get(name.toLowerCase());
	}
	
	public SortedMap<String, Player> getPlayers(String name) {
		Validate.notNull(name);
		return playersByNameSorted.subMap(name.toLowerCase(), name.toLowerCase() + Character.MAX_VALUE);
	}
	
	public HashSet<Player> getPlayersByRank(Rank rank) {
		return playersByRank.get(rank);
	}
	
	public HashSet<Player> getPlayersByGroupID(Integer groupID) {
		return playersByGroupid.get(groupID);
	}
	
	public Player getNickedPlayer(String nickname) {
		return nickedPlayers.get(nickname.toLowerCase());
	}
	
	//protected methods
	protected void addPlayerByName(Player player) {
		Validate.notNull(player);
		
		playersByName.put(player.getName().toLowerCase(), player);
		playersByNameSorted.put(player.getName().toLowerCase(), player);
	}

	public void preRegisterServer(UUID id, ServerAdapter serverAdapter) {
		playerPreRegisteredServerAdapters.put(id, serverAdapter);
	}

	public void addPlayerByRank(Player player) {
		playersByRank.add(player.getRank(), player);
		
		ArrayList<Player> staffList = staffByRank.get(player.getRank());
		if (staffList == null) staffList = new ArrayList<>();
		staffList.add(player);
		
		if (player.getRank().hasPower(Rank.JRMOD)) {
			staffByRank.put(player.getRank(), staffList);
			staff.add(player);
		}
	}
	
	public void addPlayersByGroupID(Player player) {
		playersByGroupid.add(player.getGroup_id(), player);
	}
}
