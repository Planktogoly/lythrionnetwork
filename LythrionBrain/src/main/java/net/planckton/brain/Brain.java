package net.planckton.brain;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.UUID;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import io.netty.util.concurrent.DefaultEventExecutorGroup;
import lombok.Getter;
import net.planckton.brain.command.CommandManager;
import net.planckton.brain.command.ConsoleSender;
import net.planckton.brain.command.executors.BroadcastCommand;
import net.planckton.brain.command.executors.DebugCommand;
import net.planckton.brain.command.executors.GtpCommand;
import net.planckton.brain.command.executors.HelpOpCommand;
import net.planckton.brain.command.executors.IgnoreCommand;
import net.planckton.brain.command.executors.ListCommand;
import net.planckton.brain.command.executors.MessageCommand;
import net.planckton.brain.command.executors.PartyCommand;
import net.planckton.brain.command.executors.ReplyCommand;
import net.planckton.brain.command.executors.RestartCommand;
import net.planckton.brain.command.executors.StaffCommand;
import net.planckton.brain.command.executors.StopCommand;
import net.planckton.brain.command.executors.UniversalMuteCommand;
import net.planckton.brain.command.executors.UsageCommand;
import net.planckton.brain.network.BrainServer;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.PlayerManager;
import net.planckton.brain.player.Rank;
import net.planckton.brain.twitch.TwitchBot;

public class Brain {
	@Getter private static String name;
	@Getter private static boolean dev;
	
	@Getter private static JsonObject config;
	
	@Getter private static DefaultEventExecutorGroup executorPool;
	
	@Getter private static CommandManager commandManager;
	@Getter private static ConsoleSender consoleSender;
	
	@Getter private static BrainServer brainServer;
	
	@Getter private static TwitchBot bot;
	
	@Getter private static boolean debug;
	
	public static void main(String[] args) throws Exception {
		System.out.println("Starting...");
		
		name = new File("").getAbsoluteFile().getName();
		info("Server name: " + name);
		dev = name.startsWith("DEV");
		info("Dev server: " + dev);
		debug = dev;
		
		try {
			File configFile = new File("../config.json");
			config = new JsonParser().parse(new JsonReader(new FileReader(configFile))).getAsJsonObject();
			info("Config loaded succesfully.");
		} catch (Exception e) {
			info("Could not load config D: RUNNNNN FORESTTT RUNNNNNNNN");
			e.printStackTrace();
			System.exit(0);
			return;
		}
		
		//services
		executorPool = new DefaultEventExecutorGroup(4);
		
		//bot = new TwitchBot();
		
		try {
			Database.openConnection();
		} catch(Exception e) {
			info("Something went wrong while conneting to the db");
			e.printStackTrace();
			//System.exit(0);
			//return;
		}
		
		if(!Database.isConnected()) {
			info("Not connected to the db, shutting down.");
			//System.exit(0);
			//return;
		}
		
		brainServer = new BrainServer();
		try {
			brainServer.start(25333);
		} catch (Exception e) {
			info("Something went wrong while starting the devserver");
			e.printStackTrace();
			//System.exit(0);
			//return;
		}
		
		commandManager = new CommandManager();
		commandManager.registerCommand(new ListCommand(), "list");
		commandManager.registerCommand(new MessageCommand(), "message","msg","pm","tell", "t");
		commandManager.registerCommand(new ReplyCommand(), "reply","r");
		commandManager.registerCommand(new BroadcastCommand(), "broadcast","say");
		commandManager.registerCommand(new IgnoreCommand(), "ignore");
		commandManager.registerCommand(new UniversalMuteCommand(), "universalmute");
		commandManager.registerCommand(new GtpCommand(), "gtp", "globaltp");
		commandManager.registerCommand(new PartyCommand(), "party", "p");
		commandManager.registerCommand(new HelpOpCommand(), "helpop");
		commandManager.registerCommand(new StaffCommand(), "staff");
		
		commandManager.registerCommand(new RestartCommand(), "hrestart");
		commandManager.registerCommand(new StopCommand(), "hstop");
		commandManager.registerCommand(new UsageCommand(), "husage");
		commandManager.registerCommand(new DebugCommand(), "hdebug");
		
		consoleSender = ConsoleSender.getSender();		
		info("Succesfully started heart.");
		
		while (true) { Thread.sleep(Integer.MAX_VALUE); }
	}
	
	public static void stop() {
		info("Stoping...");
		
		destroy();
	}
	
	public static void restart() {
		info("Restarting...");
		
		destroy();
	}
	
	public static void destroy() {
		info("Stopping...");
		
		try {
			debug("Stopping heart server");
			if(brainServer != null) brainServer.stop();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			debug("Stopping twitchbot");
			if(bot != null) bot.disconnect();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			debug("Stopping database");
			Database.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			debug("Stopping executorpool");
			if(executorPool != null) executorPool.shutdownGracefully().await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		info("Stopped.");
		System.exit(0);
	}
	
	//statics forwards
	public static Player getPlayer(String name) {
		return brainServer.getPlayer(name);
	}
	
	//logging
	public static void info(String msg) {
		System.out.println("[" + name + "] " + msg);
	}
	
	public static void debug(String msg) {
		if(!debug) return;
		
		System.out.println("[DEBUG] [" + name + "] " + msg);
	}
	
	public static void error(String msg, Throwable cause) {
		System.out.println("[" + name + "] " + msg);
		
		cause.printStackTrace();
	}
	
	//getters
	public static Collection<Player> getPlayers() {
		return brainServer.getPlayers();
	}
	
	public static SortedMap<String, Player> getPlayers(String string) {
		return brainServer.getPlayers(string);
	}

	public static HashSet<Player> getPlayersByRank(Rank rank) {
		return brainServer.getPlayersByRank(rank);
	}
	
	public static HashSet<Player> getPlayersByGroupID(Integer groupId) {
		return brainServer.getPlayerManager().getPlayersByGroupID(groupId);
		
	}
	
	public static Player getPlayer(UUID id) {
		return brainServer.getPlayer(id);
	}

	public static Player getNickedPlayer(String nickedName) {
		return brainServer.getNickedPlayer(nickedName);
	}

	public static PlayerManager getPlayerManager() {
		return brainServer.getPlayerManager();
	}
	
	public static int getOnlinePlayerCount() {
		return brainServer.getPlayers().size();
	}

	public static void toggleDebug() {
		debug = !debug;
	}
}
