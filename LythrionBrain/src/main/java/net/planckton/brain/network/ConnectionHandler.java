package net.planckton.brain.network;

import java.util.UUID;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketHandshake;
import net.planckton.blood.packet.PacketHandshake.InstanceType;
import net.planckton.blood.packet.PacketMessage;
import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;

public class ConnectionHandler extends ChannelHandlerAdapter {
	private BrainServer server;
	private ConnectionAdapter adapter;
	
	public ConnectionHandler(BrainServer server) {
		this.server = server;
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Packet packet = (Packet) msg;
		
		switch(packet.getPacketType()) {
		case HANDSHAKE:
			PacketHandshake handshake = (PacketHandshake) packet;
			InstanceType type = handshake.getInstanceType();
			String name = handshake.getName();
			int port = handshake.getPort();
			
			Brain.info("Recieved handshake from: " + name + " of type: " + type);
			
			Channel channel = ctx.channel();
			if(type == InstanceType.SERVER) adapter = server.addServer(channel, name, handshake.getType(), handshake.isMinigame(), port);
			else adapter = server.addBungee(channel, name, port);
			
			channel.writeAndFlush(packet);
			return;
		case MESSAGE:
			PacketMessage packetMessage = (PacketMessage) packet;
			String toServer = packetMessage.getTo();
			  
			if(toServer.startsWith("all")) {
				boolean allServer = toServer.endsWith("server");
				boolean allBungee = toServer.endsWith("bungee");
				
				if(allServer || allBungee) {
					if(allServer) for(ServerAdapter adapterMessage : Brain.getBrainServer().getServerAdapters()) {
						if(adapterMessage.getChannel() == ctx.channel()) continue;
						adapterMessage.getChannel().writeAndFlush(packetMessage);
					}
					
					if(allBungee) for(BungeeAdapter adapterMessage : Brain.getBrainServer().getBungeeAdapters()) {
						if(adapterMessage.getChannel() == ctx.channel()) continue;
						adapterMessage.getChannel().writeAndFlush(packetMessage);
					}
				}
				else {
					String allWhat = toServer.substring(3);
					
					for(ServerAdapter adapterMessage : Brain.getBrainServer().getServerAdapters()) {
						if(!adapterMessage.getName().startsWith(allWhat)) continue;
						if(adapterMessage.getChannel() == ctx.channel()) continue;
						
						adapterMessage.getChannel().writeAndFlush(packetMessage);
					}
				}
			}
			else if(toServer.startsWith("%")) {
				UUID playerId = UUID.fromString(toServer.substring(1));
				Player player = Brain.getPlayer(playerId);
				if(player == null) return;
				
				ServerAdapter server = player.getServerAdapter();
				if(server == null) return;
				
				server.getChannel().writeAndFlush(packetMessage);
			}
			else if(toServer.equals("heart")) {
				Brain.getBrainServer().callHeartMessage(packetMessage.getChannel(), packetMessage.getData());
			}
			else {
				ConnectionAdapter adapterMessage = Brain.getBrainServer().getServerAdapter(packetMessage.getTo());
				if(adapterMessage == null) adapterMessage = Brain.getBrainServer().getBungeeAdapter(packetMessage.getTo());
				
				adapterMessage.getChannel().writeAndFlush(packetMessage);
			}
			
			return;
		}
		
		ctx.fireChannelRead(packet);
	}
	
	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		Brain.info("A connection disconnected");
		
		if(adapter == null) return;
		adapter.onDisconnect();
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		Brain.error("Error in handler", cause);
		
		ctx.close();
	}
}
