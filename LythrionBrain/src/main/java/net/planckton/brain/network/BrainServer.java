package net.planckton.brain.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.SortedMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.Getter;
import net.planckton.blood.packet.PacketDecoder;
import net.planckton.blood.packet.PacketEncoder;
import net.planckton.blood.packet.PacketRegisterCommand;
import net.planckton.blood.packet.PacketRegisterServer;
import net.planckton.blood.packet.PacketServerUpdate;
import net.planckton.brain.Brain;
import net.planckton.brain.chat.ChatColor;
import net.planckton.brain.helpop.HelpOpManager;
import net.planckton.brain.party.PartyManager;
import net.planckton.brain.player.Player;
import net.planckton.brain.player.PlayerManager;
import net.planckton.brain.player.Rank;

public class BrainServer { 
	public static final String ARROWS_RIGHT = "\u27A4 ";
	public static final String CHAT_PREFIX = ChatColor.GOLD + "Lyra" + ChatColor.RESET + " " + ChatColor.WHITE + "\u00BB ";
	public static final String CHAT_PREFIX_RED = ChatColor.RED + "Lyra" + ChatColor.RESET + " " + ChatColor.WHITE + "\u00BB ";
	public static final String CHAT_PREFIX_GREEN = ChatColor.GREEN + "Lyra" + ChatColor.RESET + " " + ChatColor.WHITE + "\u00BB ";
	public static final String CHAT_PREFIX_BLUE = ChatColor.BLUE + "Lyra" + ChatColor.RESET + " " + ChatColor.WHITE + "\u00BB ";
	
	@Getter private ServerBootstrap bootstrap;
	@Getter private EventLoopGroup bossGroup;
	@Getter private EventLoopGroup workerGroup;
	@Getter private Channel channel;
	
	@Getter private ChannelGroup openChannels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	@Getter private ChannelGroup serverChannels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	@Getter private ChannelGroup bungeeChannels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	
	private Map<String, ServerAdapter> serverAdapters = new ConcurrentHashMap<>();
	private Map<String, BungeeAdapter> bungeeAdapters = new ConcurrentHashMap<>();
	
	@Getter private PlayerManager playerManager;
	@Getter private PartyManager partyManager;
	@Getter private HelpOpManager helpopManager;
	
	@Getter private ArrayList<BrainMessageCallback> brainMessageCallbacks = new ArrayList<>();
	
	public void start(int port) throws Exception {
		playerManager = new PlayerManager();
		partyManager = new PartyManager();
		helpopManager = new HelpOpManager();
		
		bossGroup = new NioEventLoopGroup();
		workerGroup = new NioEventLoopGroup();
		
		final BrainServer server = this;
		
		bootstrap = new ServerBootstrap();
		bootstrap
				.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(new ChannelInitializer<SocketChannel>() {

					@Override
					protected void initChannel(SocketChannel ch)
							throws Exception {
						Brain.info("New connection from: " + ch.localAddress().getAddress().getHostAddress());
						
						ch.pipeline().addLast(new PacketDecoder(), new PacketEncoder(), new ConnectionHandler(server));
						
						openChannels.add(ch);
					}
				})
				.option(ChannelOption.SO_BACKLOG, 128)
				.childOption(ChannelOption.SO_KEEPALIVE, true);
		
		channel = bootstrap.bind(port).sync().channel();
		Brain.info("Succesfully bound server");
		
		Brain.info("Succesfully started server");
	}

	@SuppressWarnings("deprecation")
	public void stop() {
		try {
			Brain.debug("Closing channel");
			if(channel != null) channel.close().sync();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			Brain.debug("Closing ChannelGroups");
			openChannels.close().sync();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			Brain.debug("Closing workerGroup");
			if (workerGroup != null) workerGroup.shutdownNow();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Brain.debug("Closing bossGroup");
			if (bossGroup != null) bossGroup.shutdownNow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ServerAdapter addServer(Channel channel, String name, String type, boolean isminigame, int port) {
		serverChannels.add(channel);
		
		ServerAdapter adapter = serverAdapters.get(name);
		if(adapter == null) {
			InetSocketAddress socketAddress = (InetSocketAddress) channel.remoteAddress();
		    InetAddress inetaddress = socketAddress.getAddress();
		    String ipAddress = inetaddress.getHostAddress(); // IP address of client
			if(ipAddress.equals("127.0.0.1")) ipAddress = "94.23.40.218";
		    
			adapter = new ServerAdapter(this, channel, name, type, isminigame, ipAddress, port);
			synchronized (serverAdapters) {
				serverAdapters.put(name, adapter);
			}
		}
		else adapter.handleReconnect(channel);
		
		for(BungeeAdapter bungee : bungeeAdapters.values()) {
			Brain.info("REGISTERING SERVER " + adapter.getName() + " at " + adapter.getIp() + ":" + adapter.getPort());
			bungee.getChannel().writeAndFlush(new PacketRegisterServer(adapter.getName(), adapter.getIp(), adapter.getPort(), adapter.getServerStatus()));
		}
		
		channel.pipeline().addLast("ServerAdapter", adapter);
		
		return adapter;
	}
	
	public ConnectionAdapter addBungee(final Channel channel, String name, int port) {
		bungeeChannels.add(channel);
		
		BungeeAdapter adapter = bungeeAdapters.get(name);
		if(adapter == null) {
			InetSocketAddress socketAddress = (InetSocketAddress) channel.remoteAddress();
		    InetAddress inetaddress = socketAddress.getAddress();
		    String ipAddress = inetaddress.getHostAddress(); // IP address of client
			
			adapter = new BungeeAdapter(this, channel, name, ipAddress, port);
			bungeeAdapters.put(name, adapter);
		}
		else adapter.handleReconnect(channel);
		
		channel.pipeline().addLast("BungeeAdapter", adapter);

		for(String command : Brain.getCommandManager().getRegisteredCommands()) {
			Brain.debug("REGISTERING COMMAND " + command);
			channel.write(new PacketRegisterCommand(command));
		}
		
		synchronized (serverAdapters) {
			for(ServerAdapter server : serverAdapters.values()) {
				Brain.debug("REGISTERING SERVER " + server.getName() + " at " + server.getIp() + ":" + server.getPort());
				channel.write(new PacketRegisterServer(server.getName(), server.getIp(), server.getPort(), server.getServerStatus()));
				channel.write(new PacketServerUpdate(server.getName(), server.getServerStatus()));
			}
		}
		
		channel.flush();
		
		return adapter;
	}
	
	public void broadcastMessage(String message) {
		for(Player player : playerManager.getPlayers()) {
			player.sendMessage(message);
		}
	}
	
	public void registerHeartMessageCallback(BrainMessageCallback callback) {
		brainMessageCallbacks.add(callback);
	}
	
	public void callHeartMessage(String channel, byte[] data) {
		for(BrainMessageCallback callback : brainMessageCallbacks) {
			try {
				callback.onBrainMessage(channel, data);
			} catch(Exception e) {
				Brain.info("Could not call brain message for channel: " + channel + ". For listener " + callback.getClass().toString());
				e.printStackTrace();
				return;
			}
		}
	}
	
	//static getters
	public Player getPlayer(UUID id) {
		return playerManager.getPlayer(id);
	}
	
	public Player getPlayer(String name) {
		return playerManager.getPlayer(name);
	}
	
	public SortedMap<String, Player> getPlayers(String name) {
		return playerManager.getPlayers(name);
	}

	//getters
	public Collection<ServerAdapter> getServerAdapters() {
		return serverAdapters.values();
	}
	
	public ServerAdapter getServerAdapter(String name) {
		return serverAdapters.get(name);
	}
	
	public Collection<BungeeAdapter> getBungeeAdapters() {
		return bungeeAdapters.values();
	}
	
	public BungeeAdapter getBungeeAdapter(String name) {
		return bungeeAdapters.get(name);
	}
	
	public Collection<Player> getPlayers() {
		return playerManager.getPlayers();
	}
	
	public Collection<Player> getStaffByRank(Rank rank) {
		return playerManager.getStaffByRank().get(rank);
	}
	
	public Collection<Player> getStaff() {
		return playerManager.getStaff();
	}

	public HashSet<Player> getPlayersByRank(Rank rank) {
		return playerManager.getPlayersByRank(rank);
	}

	public Player getNickedPlayer(String nickname) {
		return playerManager.getNickedPlayer(nickname);
	}
}
