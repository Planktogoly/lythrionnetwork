package net.planckton.brain.network;

import java.util.List;
import java.util.UUID;

import com.google.gson.JsonObject;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketPlayerBungeeJoin;
import net.planckton.blood.packet.PacketPlayerBungeeQuit;
import net.planckton.blood.packet.PacketPlayerCommand;
import net.planckton.blood.packet.PacketPlayerTabComplete;
import net.planckton.brain.Brain;
import net.planckton.brain.party.Party;
import net.planckton.brain.player.Player;
import net.planckton.brain.web.JsonSerializable;

@Sharable
public class BungeeAdapter extends ConnectionAdapter implements JsonSerializable {
	
	public BungeeAdapter(BrainServer server, Channel channel, String name, String ip, int port) {
		super(server, channel, name, ip, port);
	}
	
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Packet packet = (Packet) msg;

		Brain.debug("[PACKET] " + packet.getPacketType() + " from " + getName());
		
		Player player;
		
		switch(packet.getPacketType()) {
		case PLAYER_BUNGEE_JOIN:
			PacketPlayerBungeeJoin serverJoin = (PacketPlayerBungeeJoin) packet;
			UUID id = serverJoin.getUuid();
			
			player = Brain.getBrainServer().getPlayerManager().onPlayerBungeeJoin(this, id); 
			
			addPlayer(player);
			
			Brain.debug("Player join: " + id);
			return;
		case PLAYER_BUNGEE_QUIT:
			PacketPlayerBungeeQuit serverQuit = (PacketPlayerBungeeQuit) packet;	
			player = Brain.getBrainServer().getPlayer(serverQuit.getUuid());
			
			Party party = Brain.getBrainServer().getPartyManager().getPartyByPlayer(player);
			if (!(party == null)) {
				if (party.isLeader(player)) party.newLeader(); 
				else party.leave(player);				
			}
			
			Brain.getBrainServer().getPlayerManager().onPlayerBungeeQuit(serverQuit.getUuid());
			removePlayer(serverQuit.getUuid());
			
			Brain.debug("Player quit: " + serverQuit.getUuid());
			return;
		case PLAYER_COMMAND:
			PacketPlayerCommand command = (PacketPlayerCommand) packet;
			
			Player commandPlayer = Brain.getPlayer(command.getUuid());
			Brain.getCommandManager().onCommand(commandPlayer, command.getCommand());
			return;
		case PLAYER_TAB_COMPLETE:
			PacketPlayerTabComplete tabComplete = (PacketPlayerTabComplete) packet;
			
			Player tabPlayer = Brain.getPlayer(tabComplete.getUuid());
			List<String> suggestions = Brain.getCommandManager().onTabComplete(tabPlayer, tabComplete.getCommand());
			if(suggestions == null) return;
			
			tabComplete.setSuggestions(suggestions);
			getChannel().writeAndFlush(tabComplete);
			return;
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}


	@Override
	public JsonObject toJson() {
		JsonObject adapterObject = new JsonObject();
		adapterObject.addProperty("name", getName());
		adapterObject.addProperty("active", getChannel().isActive());
		adapterObject.addProperty("online", getPlayers().size());
		
		return adapterObject;
	}
}
