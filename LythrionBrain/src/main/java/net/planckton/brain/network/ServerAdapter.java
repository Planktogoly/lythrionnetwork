package net.planckton.brain.network;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.JsonObject;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.NonNull;
import net.planckton.blood.GameStatus;
import net.planckton.blood.ServerStatus;
import net.planckton.blood.packet.Packet;
import net.planckton.blood.packet.PacketGameStatusUpdate;
import net.planckton.blood.packet.PacketInfoRegister;
import net.planckton.blood.packet.PacketInfoRemove;
import net.planckton.blood.packet.PacketInfoRequest;
import net.planckton.blood.packet.PacketInfoUpdate;
import net.planckton.blood.packet.PacketIsInParty;
import net.planckton.blood.packet.PacketPartyDisband;
import net.planckton.blood.packet.PacketPartyLeave;
import net.planckton.blood.packet.PacketPlayerCommand;
import net.planckton.blood.packet.PacketPlayerConnect;
import net.planckton.blood.packet.PacketPlayerJoinMinigame;
import net.planckton.blood.packet.PacketPlayerServerJoin;
import net.planckton.blood.packet.PacketPlayerServerQuit;
import net.planckton.blood.packet.PacketServerUpdate;
import net.planckton.brain.Brain;
import net.planckton.brain.party.Party;
import net.planckton.brain.party.PartyManager;
import net.planckton.brain.player.Player;
import net.planckton.brain.util.Prefix;
import net.planckton.brain.web.JsonSerializable;

@Sharable
public class ServerAdapter extends ConnectionAdapter implements JsonSerializable {
	private static final Map<String,Set<ServerAdapter>> infoListeners = new ConcurrentHashMap<>();
	
	private static void registerInfoListener(String server, String key, ServerAdapter adapter) {
		String listKey = server + ":" + key;
		Set<ServerAdapter> listeners = infoListeners.get(listKey);
		if(listeners == null) listeners = new HashSet<ServerAdapter>();
		
		listeners.add(adapter);
		infoListeners.put(listKey, listeners);
	}
	
	private final HashMap<String, String> infos = new HashMap<String, String>();

	@Getter private String serverName;
	@Getter private ServerStatus serverStatus = ServerStatus.ONLINE;
	@Getter private String serverType;
	
	@Getter private GameStatus gameStatus = GameStatus.NO_MINIGAME;
	@Getter private boolean isMinigame;
	
	public ServerAdapter(BrainServer server, Channel channel, String name, String type, boolean isminigame, String ip, int port) {
		super(server, channel, name, ip, port);		

		serverName = name;
		serverType = type;
		isMinigame = isminigame;
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Packet packet = (Packet) msg;
		
		System.out.println("[Brain] Recieved packet " + packet.getPacketId());
		
		PartyManager partyManager = Brain.getBrainServer().getPartyManager();
		Player player;
		Party party;
		
		switch(packet.getPacketType()) {
		case PLAYER_SERVER_JOIN:
			PacketPlayerServerJoin serverJoin = (PacketPlayerServerJoin) packet;
			
			final UUID id = serverJoin.getUuid();
			final ServerAdapter adapter = this;
			
			player = Brain.getPlayer(id);
			
			if(player == null) {
				Brain.getPlayerManager().preRegisterServer(id, this);
				return;
			}
			
			new Thread() {
				@Override
				public void run() {
	
					Player player = Brain.getPlayer(id);
					
					int tries = 0;
					while(player == null) {
						try {
							sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						player = Brain.getPlayer(id);
						tries++;
						
						if(tries > 10) {
							throw new RuntimeException("Server " + adapter.getName() + " send player with uuid " + id + " but player is not registered.");
						}
					}
					
					addPlayer(player);
					setInfo("pcount", getPlayers().size() + "");
					player.setServerAdapter(adapter);
					
					if (serverType.equalsIgnoreCase("DM") || serverType.equalsIgnoreCase("FFA")) {
						Party party = Brain.getBrainServer().getPartyManager().getPartyByLeader(player);
						if (!(party == null)) {
							party.teleportPlayers();
						}
					}
				}
			}.start();			
			return;
		case PLAYER_SERVER_QUIT:
			PacketPlayerServerQuit serverQuit = (PacketPlayerServerQuit) packet;
			
			removePlayer(serverQuit.getUuid());
			setInfo("pcount", getPlayers().size() + "");
			return;
		case PLAYER_CONNECT:
			PacketPlayerConnect connectPacket = (PacketPlayerConnect) packet;
			
			player = Brain.getPlayer(connectPacket.getUuid());
			
			player.getBungeeAdapter().getChannel().writeAndFlush(connectPacket);			
			return;
		case PLAYER_MINIGAME_CONNECT:
			PacketPlayerJoinMinigame joinMinigame = (PacketPlayerJoinMinigame) packet;
			
			player = Brain.getPlayer(joinMinigame.getUuid());
			
			player.getBungeeAdapter().getChannel().writeAndFlush(joinMinigame);
			return;
		case PLAYER_COMMAND: 
			PacketPlayerCommand command = (PacketPlayerCommand) packet;
			
			Player commandPlayer = Brain.getPlayer(command.getUuid());
			Brain.getCommandManager().onCommand(commandPlayer, command.getCommand());
			return;
		case INFO_UPDATE:
			PacketInfoUpdate infoUpdate = (PacketInfoUpdate) packet;
			
			//store info
			setInfo(infoUpdate.getKey(), infoUpdate.getInfo());
			return;
		case INFO_REGISTER:
			PacketInfoRegister infoRegister = (PacketInfoRegister) packet;
			
			//register ourselves to this key
			registerInfoListener(infoRegister.getServer(), infoRegister.getKey(), this);
			
			//check if we have this key value stored
			ServerAdapter serverRegister = Brain.getBrainServer().getServerAdapter(infoRegister.getServer());
			if(serverRegister == null) return;
			String info = serverRegister.getInfo(infoRegister.getKey());
			if(info == null) return;
			
			getChannel().write(new PacketInfoUpdate(serverRegister.getName(), infoRegister.getKey(), info));
			return;			
		case INFO_REMOVE:
			PacketInfoRemove infoRemove = (PacketInfoRemove) packet;
			
			infos.remove(infoRemove.getKey());
			infoListeners.remove(infoRemove.getServer() + ":" + infoRemove.getKey());
			
			return;			
		case INFO_REQUEST:
			PacketInfoRequest infoRequest = (PacketInfoRequest) packet;
			
			ServerAdapter serverRequest = Brain.getBrainServer().getServerAdapter(infoRequest.getServer());
			if(serverRequest == null) return;
			
			if(infoRequest.getKey().equals("pcount")) {
				getChannel().writeAndFlush(new PacketInfoUpdate(infoRequest.getServer(), infoRequest.getKey(), serverRequest.getPlayers().size() + ""));
				return;
			}
			
			serverRequest.getChannel().writeAndFlush(infoRequest);
			return;
		case SERVER_UPDATE:
			PacketServerUpdate packetUpdate = (PacketServerUpdate) packet;
			
			handleStatus(packetUpdate.getStatus());
			return;
		case GAMESTATUS_UPDATE:
			PacketGameStatusUpdate packetGameStatusUpdate = (PacketGameStatusUpdate) packet;
			
			setGameStatus(packetGameStatusUpdate.getStatus());
			return;
		case PARTY_DISBAND:
			PacketPartyDisband packetDisband = (PacketPartyDisband) packet;
			
			player = Brain.getPlayer(packetDisband.getUuid());
			if (player == null) return;
			
			party = partyManager.getPartyByLeader(player);
			if (party == null) {
				player.getServerAdapter().getChannel().writeAndFlush(new PacketPartyDisband(player.getId()));
				return;
			}
			
			partyManager.getPartyByLeader(player).disband();
			return;	
		case PARTY_ISINPARTY:
			PacketIsInParty packetIsInParty = (PacketIsInParty) packet;
			
			player = Brain.getPlayer(packetIsInParty.getUuid());
			if (player == null) return;
			
			boolean value = false;
			boolean isleader = false;
			party = partyManager.getPartyByPlayer(player);
			if (party != null) value = true;
			else return;
			
			if (party.getLeader().equals(player)) isleader = true;
			
			getChannel().writeAndFlush(new PacketIsInParty(player.getId(), value, isleader));
			return;
		case PARTY_LEAVE:
			PacketPartyLeave packetPartyLeave = (PacketPartyLeave) packet;
			
			player = Brain.getPlayer(packetPartyLeave.getUuid());
			if (player == null) return;

			party = partyManager.getPartyByPlayer(player);
			if (party == null) {
				player.sendMessage(Prefix.PARTY_ARROWS + "You are not in a party.");
				return;
			}
			
			party.leave(player);
			return;
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
	
	@Override
	protected void onReconnect() {
		
	}
	
	@Override
	protected void onDisconnect() {
		if(serverStatus != ServerStatus.RESTARTING) setStatus(ServerStatus.OFFLINE);
	}
	
	public String getInfo(@NonNull String key) {
		return infos.get(key.toLowerCase());
	}
	
	private void setInfo(@NonNull String key, @NonNull String value) {
		infos.put(key.toLowerCase(), value);
		
		//send info to other listening servers
		Set<ServerAdapter> listeners = infoListeners.get(getName() + ":" + key);
		if(listeners == null) return;
		
		PacketInfoUpdate packet = new PacketInfoUpdate(getName(), key, value);
		
		for(ServerAdapter reciever : listeners) {
			reciever.getChannel().writeAndFlush(packet);
		}
	}
	
	private void setGameStatus(@NonNull GameStatus gameStatus) {
		this.gameStatus = gameStatus;
		
		PacketGameStatusUpdate packet = new PacketGameStatusUpdate(getName(), gameStatus);
		for(BungeeAdapter adapter : Brain.getBrainServer().getBungeeAdapters()) {
			try {
				adapter.getChannel().writeAndFlush(packet);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void setStatus(@NonNull ServerStatus serverStatus) {
		this.serverStatus = serverStatus;
		
		PacketServerUpdate packet = new PacketServerUpdate(getName(), serverStatus);
		for(BungeeAdapter adapter : Brain.getBrainServer().getBungeeAdapters()) {
			try {
				adapter.getChannel().writeAndFlush(packet);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void handleStatus(@NonNull ServerStatus serverStatus) {
		setStatus(serverStatus);
		
		if(serverName == null) return;
		
		switch(serverStatus) {
		case RESTARTING:
		case OFFLINE:
		case MAINTENANCE:
		case ONLINE:
		case PENDING_RESTART:
		case UNKOWN:
			break;
		}
	}
	
	public void forceStatus(@NonNull ServerStatus serverStatus) {
		setStatus(serverStatus);
		
		getChannel().write(new PacketServerUpdate(getName(), serverStatus));
		
		handleStatus(serverStatus);
	}

	@Override
	public JsonObject toJson() {
		JsonObject adapterObject = new JsonObject();
		adapterObject.addProperty("name", getName());
		adapterObject.addProperty("active", getChannel().isActive());
		adapterObject.addProperty("online", getPlayers().size());
		
		return adapterObject;
	}
}
