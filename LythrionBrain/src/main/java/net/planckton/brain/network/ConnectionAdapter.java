package net.planckton.brain.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.planckton.blood.packet.PacketInfoUpdate;
import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;

public abstract class ConnectionAdapter extends ChannelHandlerAdapter {
	@Getter private BrainServer server;
	
	@Getter private Channel channel;
	@Getter private String name;
	@Getter private String ip;
	@Getter private int port;
	
	private Map<UUID, Player> players = new ConcurrentHashMap<>();
	
	public ConnectionAdapter(BrainServer server, Channel channel, String name, String ip, int port) {
		this.server = server;
		this.channel = channel;
		this.name = name;
		this.ip = ip;
		this.port = port;
		
		Brain.info("New connection from " + name);
	}
	
	protected final void handleReconnect(Channel channel) {
		this.channel = channel;
		
		InetSocketAddress socketAddress = (InetSocketAddress) channel.remoteAddress();
	    InetAddress inetaddress = socketAddress.getAddress();
	    String ipAddress = inetaddress.getHostAddress(); // IP address of client
	    if(ipAddress.equals("127.0.0.1")) ipAddress = "94.23.40.218";
	    ip = ipAddress;
		
		onReconnect();
		Brain.info("Re-opened connection from " + name);
	}
	
	protected void onReconnect() {
		
	}
	
	protected void onDisconnect() {
		
	}
	
	@Override
	public String toString() {
		return "Connection:[name=" + name +",active=" + channel.isActive() + ",players=" + players.size() + "]";
	}
	
	public void addPlayer(Player player) {
		players.put(player.getId(), player);
		
		updateCount();
	}
	
	public void removePlayer(UUID id) {
		players.remove(id);
		
		updateCount();
	}
	
	private void updateCount() {		
		PacketInfoUpdate totalCountUpdatePacketJoined = new PacketInfoUpdate("HEART", "totalonline", Integer.toString(getServer().getPlayers().size()));
		for(Channel adapter : new ArrayList<>(getServer().getBungeeChannels())) {
			try {
				adapter.writeAndFlush(totalCountUpdatePacketJoined);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@AllArgsConstructor
	private static class Server {
		@Getter private String name;
		@Getter private int online;
	}
	
	//getters
	public Collection<Player> getPlayers() {
		return players.values();
	}
}
