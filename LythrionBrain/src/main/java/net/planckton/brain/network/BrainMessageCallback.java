package net.planckton.brain.network;

public interface BrainMessageCallback {
	public void onBrainMessage(String channel, byte[] data);
}
