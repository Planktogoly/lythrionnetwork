package net.planckton.brain.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.planckton.brain.Brain;

public class StatusOnline extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		JsonObject object = new JsonObject();
		
		object.addProperty("online", Brain.getOnlinePlayerCount());
		
		return object;
	}
}
