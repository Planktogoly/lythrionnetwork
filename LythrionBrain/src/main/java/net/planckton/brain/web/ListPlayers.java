package net.planckton.brain.web;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;

public class ListPlayers extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		JsonArray players = new JsonArray();
		
		for(Player player : Brain.getPlayers()) {
			players.add(player.toJson());
		}
		
		return players;
	}
}
