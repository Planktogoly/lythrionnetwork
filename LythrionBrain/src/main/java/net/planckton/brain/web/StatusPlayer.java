package net.planckton.brain.web;

import com.google.gson.JsonObject;

import net.planckton.brain.Brain;
import net.planckton.brain.player.Player;

public class StatusPlayer extends HeartServerResource {
	
	@Override
	public JsonObject present() {
		String name = (String) getRequest().getAttributes().get("name");
		
		Player player = Brain.getPlayer(name);
		if(player == null) {
			throw new HeartServerResourceError("Player not found");
		}
		
		return player.toJson();
	}
}
