package net.planckton.brain.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class HeartServerResource extends ServerResource {
	private static final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); 
	
	@RequiredArgsConstructor
	public static final class HeartServerResourceError extends UncheckedExecutionException {
		private static final long serialVersionUID = 1L;
		
		@Getter private final String errorMessage;
		
	}
	
	@Get("json")
	public String onGet() {
		JsonObject meta = new JsonObject();
		JsonElement data = null;
		
		meta.addProperty("time", dt.format(new Date()));
		
		try {
			data = present();
			
			meta.addProperty("status", "success");
		} catch(HeartServerResourceError e) {
			e.printStackTrace();
			
			meta.addProperty("status", "error");
			meta.addProperty("message", e.getErrorMessage());
		} catch(Exception e) {
			e.printStackTrace();
			
			meta.addProperty("status", "error");
		}
		
		if(!meta.has("message")) meta.add("message", null);
		
		
		JsonObject response = new JsonObject();
		response.add("meta", meta);
		response.add("data", data);
		
		return response.toString();
	}
	
	protected abstract JsonElement present();
}
