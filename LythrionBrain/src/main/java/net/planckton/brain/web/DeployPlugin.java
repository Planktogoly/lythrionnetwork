package net.planckton.brain.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import net.planckton.brain.Database;

public class DeployPlugin extends ServerResource {
	private static Status STATUS_ACCESS_DENIED = new Status(403, "Access denied");
	
	@Post
	public Representation accept(Representation entity) throws Exception {
		long start = System.currentTimeMillis();
		
		Representation result = null;
	    if (entity != null) {
	    	if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {
	    		String username = (String) getRequest().getAttributes().get("username");
	    		String password = (String) getRequest().getAttributes().get("password");
	    		String type = (String) getRequest().getAttributes().get("type");
	    		
	    		boolean allowed = Database.isAuthPreHashed(username, password);
	    		System.out.println("[PushPlugin] Auth from " + username + (allowed ? " succeded" : " failed"));
	    		if(!allowed) {
	    			System.out.println("Acces denied wrong username or password");
	    			setStatus(STATUS_ACCESS_DENIED);
	    			return result;
	    		}
	    		
	    		boolean hasPermission  = Database.hasPermission(Database.getUUIDbyName(username).get(), "developer.plugin.push." + type);
	    		System.out.println("[PushPlugin] " + username + " has " + (allowed ? "" : " no ") + " permission for " + type);
	    		if(!hasPermission) {
	    			System.out.println("No permission for " + type);
	    			setStatus(STATUS_ACCESS_DENIED);
	    			return result;
	    		}
	    		
	    		// 1/ Create a factory for disk-based file items
	            DiskFileItemFactory factory = new DiskFileItemFactory();
	            factory.setSizeThreshold(1000240);

	            // 2/ Create a new file upload handler based on the Restlet
	            // FileUpload extension that will parse Restlet requests and
	            // generates FileItems.
	            RestletFileUpload upload = new RestletFileUpload(factory);

	            // 3/ Request is parsed by the handler which generates a
	            // list of FileItems
	            FileItemIterator fileIterator = upload.getItemIterator(entity);

	            // Process only the uploaded item called "fileToUpload"
	            // and return back
	            boolean found = false;
	            while (fileIterator.hasNext() && !found) {
	                FileItemStream fi = fileIterator.next();
	                if (fi.getFieldName().equals("fileUpload")) {
	                    found = true;
	                    // consume the stream immediately, otherwise the stream
	                    // will be closed.
	                    String filename = fi.getName(); 
	                    System.out.println("[PushPlugin] " + username + " pushing: " + filename);
	                    
	                    File targetFile = new File("../../plugins/" + type + "/" + filename);
	                    
	                    if(!targetFile.exists()) {
	                    	File parentDir = targetFile.getParentFile();
	                    	if(!parentDir.exists()) {
	                    		System.out.println("[PushPlugin] Creating new dir in " + type + ": " + targetFile.getName());
	                    		parentDir.mkdirs();
	                    	}
	                    	
	                    	System.out.println("[PushPlugin] Created new file in " + type + ": " + targetFile.getName());
	                    	targetFile.createNewFile();
	                    }
	                    
	                    InputStream in = fi.openStream();
	                    FileOutputStream out = new FileOutputStream(targetFile);
	                    IOUtils.copy(in, out);
	                    
	                    in.close();
	                    out.close();
	                    
	                    System.out.println("[PushPlugin] Written to " + targetFile.getAbsoluteFile().getAbsolutePath());
	                
	                    result = new StringRepresentation("OK", MediaType.TEXT_PLAIN);
	                }
	            }
	        } else {
	            // POST request with no entity.
	            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
	        }
	    }
	    
	    System.out.println("[PushPlugin] request took " + (System.currentTimeMillis() - start));
	    
	    return result;
	}
}
