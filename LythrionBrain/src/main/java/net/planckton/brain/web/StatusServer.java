package net.planckton.brain.web;

import net.planckton.brain.Brain;
import net.planckton.brain.network.ServerAdapter;

import com.google.gson.JsonElement;

public class StatusServer extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		String name = (String) getRequest().getAttributes().get("name");
		
		ServerAdapter server = Brain.getBrainServer().getServerAdapter(name);
		if(server == null) {
			throw new HeartServerResourceError("Server not found");
		}
		
		return server.toJson();
	}
}
