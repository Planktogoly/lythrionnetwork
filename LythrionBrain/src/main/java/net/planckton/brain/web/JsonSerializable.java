package net.planckton.brain.web;

import com.google.gson.JsonObject;

public interface JsonSerializable {
	public JsonObject toJson();
}
