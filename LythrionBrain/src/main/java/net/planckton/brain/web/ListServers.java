package net.planckton.brain.web;

import net.planckton.brain.Brain;
import net.planckton.brain.network.ServerAdapter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class ListServers extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		JsonArray servers = new JsonArray();
		
		for(ServerAdapter adapter : Brain.getBrainServer().getServerAdapters()) {
			servers.add(adapter.toJson());
		}
		
		return servers;
	}
}
