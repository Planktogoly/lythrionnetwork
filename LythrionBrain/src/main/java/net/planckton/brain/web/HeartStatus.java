package net.planckton.brain.web;

import com.google.gson.JsonObject;

import net.planckton.brain.Brain;
import net.planckton.brain.Database;
import net.planckton.brain.network.BrainServer;

public class HeartStatus extends HeartServerResource {
	
	@Override
	public JsonObject present() {
		JsonObject object = new JsonObject();
		
		JsonObject objectDatabase = new JsonObject();
		objectDatabase.addProperty("name", Database.getDatabase());
		objectDatabase.addProperty("connected", Database.isConnected());
		
		JsonObject objectHeartServer = new JsonObject();
		BrainServer heartServer = Brain.getBrainServer();
		objectHeartServer.addProperty("open", heartServer.getChannel().isOpen());
		objectHeartServer.addProperty("active", heartServer.getChannel().isActive());
		
		object.add("database", objectDatabase);
		object.add("heartserver", objectHeartServer);
		
		return object;
	}
}
