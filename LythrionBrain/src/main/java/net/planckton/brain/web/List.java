package net.planckton.brain.web;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.planckton.brain.Brain;
import net.planckton.brain.network.BungeeAdapter;
import net.planckton.brain.network.ServerAdapter;
import net.planckton.brain.player.Player;

public class List extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		JsonObject object = new JsonObject();
	
		JsonArray bungees = new JsonArray();
		for(BungeeAdapter adapter : Brain.getBrainServer().getBungeeAdapters()) {
			bungees.add(adapter.toJson());
		}
		
		JsonArray servers = new JsonArray();
		for(ServerAdapter adapter : Brain.getBrainServer().getServerAdapters()) {
			servers.add(adapter.toJson());
		}
		
		JsonArray players = new JsonArray();
		for(Player player : Brain.getPlayers()) {
			players.add(player.toJson());
		}
		
		object.add("bungees", bungees);
		object.add("servers", servers);
		object.add("players", players);
		
		return object;
	}
}
