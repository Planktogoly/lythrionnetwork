package net.planckton.brain.web;

import net.planckton.brain.Brain;
import net.planckton.brain.network.BungeeAdapter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class ListBungees extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		JsonArray bungees = new JsonArray();
		
		for(BungeeAdapter adapter : Brain.getBrainServer().getBungeeAdapters()) {
			bungees.add(adapter.toJson());
		}
		
		return bungees;
	}
}
