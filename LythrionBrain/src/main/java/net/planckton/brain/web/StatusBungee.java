package net.planckton.brain.web;

import net.planckton.brain.Brain;
import net.planckton.brain.network.BungeeAdapter;

import com.google.gson.JsonElement;

public class StatusBungee extends HeartServerResource {
	
	@Override
	public JsonElement present() {
		String name = (String) getRequest().getAttributes().get("name");
		
		BungeeAdapter bungee = Brain.getBrainServer().getBungeeAdapter(name);
		if(bungee == null) {
			throw new HeartServerResourceError("Bungee not found");
		}
		
		return bungee.toJson();
	}
}
