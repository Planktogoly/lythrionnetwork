package net.planckton.brain.twitch;

import java.io.IOException;

import lombok.Getter;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.PircBot;

public class ChannelConnection extends PircBot {
	private TwitchManager manager;
	@Getter private String channel;
	
	public ChannelConnection(TwitchManager manager, String channel) {
		this.manager = manager;
		this.channel = channel;
		
		setName("tostinetwork");
		setVerbose(true);
	}
	
	public void connect() throws NickAlreadyInUseException, IOException, IrcException {
		connect("irc.twitch.tv", 6667, "oauth:h3xen5e5swzeqjm7jc1o511ba22f7c");
		joinChannel(channel);
		sendMessage("Tostinetwork reporting for duty");
	}
	
	@Override
	protected void onMode(String channel, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
		System.out.println(channel + "  " + sourceNick + "   " + mode);
	}
	
	@Override
	protected void onMessage(String channel, String sender, String login, String hostname, String message) {
		System.out.println("MESSAGE IN: " + sender + ": " + message);
		if(!message.startsWith("!")) return;
		
		manager.onCommand(this, sender, message);
		
	}

	public void sendMessage(String message) {
		sendMessage(channel, message);
	}
}
