package net.planckton.brain.twitch;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jibble.pircbot.IrcException;

public class TwitchManager {
	private Map<String, ChannelConnection> bots = new ConcurrentHashMap<>();
	private Map<String, TwitchCommand> commands = new ConcurrentHashMap<String, TwitchCommand>();
	
	public TwitchManager() {
		registerCommand(new TwitchCommand("test") {
			
			@Override
			public void onCommand(ChannelConnection connection, String user, String[] args) {
				connection.sendMessage(user + " used the test command");
			}
		});
		
		ChannelConnection connection = new ChannelConnection(this, "#mentally");
		bots.put(connection.getChannel(), connection);
		
		try {
			connection.connect();
		} catch (IOException | IrcException e) {
			e.printStackTrace();
		}
	}
	
	public void registerCommand(TwitchCommand command) {
		commands.put(command.getCommand(), command);
	}

	public void onCommand(ChannelConnection connection, String sender, String message) {
		String[] parts = message.split(" ");
		String command = parts[0].replaceFirst("!", "");
		parts = Arrays.copyOfRange(parts, 1, parts.length);
		
		TwitchCommand commandExec = commands.get(command);
		if(commandExec == null) return;
		
		commandExec.onCommand(connection, sender, parts);
	}
}
