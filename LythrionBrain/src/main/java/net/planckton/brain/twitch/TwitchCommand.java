package net.planckton.brain.twitch;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class TwitchCommand {
	@Getter @NonNull public String command;
	
	abstract public void onCommand(ChannelConnection connection, String user, String[] args);
}
