package net.planckton.blood;

public enum GameStatus {
	
	NO_MINIGAME, LOBBY, INGAME, POSTGAME;

}
