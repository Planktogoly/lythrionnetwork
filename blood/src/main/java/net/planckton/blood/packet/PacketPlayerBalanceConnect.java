package net.planckton.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketPlayerBalanceConnect extends  PlayerPacket {
	@Getter private String serverType;
	@Getter private String identifier;
	@Getter private boolean force;

	public PacketPlayerBalanceConnect() {
		super(PacketType.PLAYER_BALANCE_CONNECT);
	}
	
	public PacketPlayerBalanceConnect(UUID id, String serverType, String identifier) {
		this(id, serverType, false, identifier);
	}
	
	public PacketPlayerBalanceConnect(UUID id, String serverType, boolean force, String identifier) {
		super(PacketType.PLAYER_BALANCE_CONNECT, id);
		
		this.serverType = serverType;
		this.force = force;
		this.identifier = identifier;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(serverType);
		out.writeBoolean(force);
		out.writeUTF(identifier);;
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		serverType = in.readUTF();
		force = in.readBoolean();
		identifier = in.readUTF();
	}
}
