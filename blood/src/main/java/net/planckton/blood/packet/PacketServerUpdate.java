package net.planckton.blood.packet;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.blood.ServerStatus;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketServerUpdate extends Packet {
	@Getter private String server;
	@Getter private ServerStatus status;
	
	public PacketServerUpdate() {
		super(PacketType.SERVER_UPDATE);
	}
	
	public PacketServerUpdate(@NonNull String server, @NonNull ServerStatus status) {
		super(PacketType.SERVER_UPDATE);
		
		this.server = server;
		this.status = status;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(server);
		out.writeUTF(status.name());
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		server = in.readUTF();
		status = ServerStatus.valueOf(in.readUTF());
	}
}
