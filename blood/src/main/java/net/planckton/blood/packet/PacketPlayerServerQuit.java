package net.planckton.blood.packet;

import java.util.UUID;

public class PacketPlayerServerQuit extends PlayerPacket {
	public PacketPlayerServerQuit() {
		super(PacketType.PLAYER_SERVER_QUIT);
	}
	
	public PacketPlayerServerQuit(UUID uuid) {
		super(PacketType.PLAYER_SERVER_QUIT, uuid);
	}
}
