package net.planckton.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;
import lombok.NonNull;
import net.planckton.blood.GameStatus;

public class PacketGameStatusUpdate extends Packet {
	
	@Getter private String server;
	@Getter private GameStatus status;

	public PacketGameStatusUpdate() {
		super(PacketType.GAMESTATUS_UPDATE);
	}
	
	public PacketGameStatusUpdate(@NonNull String server, @NonNull GameStatus status) {
		super(PacketType.GAMESTATUS_UPDATE);
		
		this.server = server;
		this.status = status;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(server);
		out.writeUTF(status.name());		
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		server = in.readUTF();
		status = GameStatus.valueOf(in.readUTF());		
	}

}
