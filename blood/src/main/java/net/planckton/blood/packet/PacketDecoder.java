package net.planckton.blood.packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if(in.readableBytes() < 3) {
			return;
		}
		
		short length = in.getShort(in.readerIndex() + 1);
		
		if(in.readableBytes() < length) {
			return;
		}
		
		byte packetId = in.getByte(in.readerIndex());
		Class<? extends Packet> packetClass = Packet.getPacketClass(packetId);
		
		if(packetClass == null) {
			System.out.println("Unknown packet: " + packetId);
			return;
		}
		
		Packet packet = packetClass.newInstance();
		byte[] bytes = new byte[length];
		in.readBytes(bytes);
		packet.fromBytes(bytes);
		out.add(packet);		
	}
}
