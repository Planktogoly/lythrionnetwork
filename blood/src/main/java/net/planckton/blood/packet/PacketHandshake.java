package net.planckton.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketHandshake extends Packet {
	public static enum InstanceType {
		SERVER((byte)0),BUNGEE((byte)1);
		
		public static InstanceType fromInt(int id) {
			for(InstanceType type : values()) {
				if(type.id != id) continue;
				
				return type;
			}
			
			return null;
		}
		
		private byte id;
		
		private InstanceType(byte id) {
			this.id = id;
		}
		
		public byte getId() {
			return id;
		}
	}
	
	@Getter private InstanceType instanceType;
	@Getter private boolean isMinigame;
	@Getter private String name;
	@Getter private String type;
	@Getter private int port;
	
	public PacketHandshake() {
		super(Packet.PacketType.HANDSHAKE);
	}
	
	public PacketHandshake(InstanceType type, String name, String serverType, boolean isMinigame, int port) {
		super(Packet.PacketType.HANDSHAKE);
		
		this.instanceType = type;
		this.isMinigame = isMinigame;
		this.name = name;
		this.type = serverType;
		this.port = port;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeInt(instanceType.getId());
		out.writeBoolean(isMinigame);
		out.writeUTF(name);
		out.writeUTF(type);
		out.writeInt(port);
	}
	
	@Override
	void  readBytes(ByteArrayDataInput in) {
		instanceType = InstanceType.fromInt(in.readInt());
		isMinigame = in.readBoolean();
		name = in.readUTF();
		type = in.readUTF();
		port = in.readInt();
	}
}
