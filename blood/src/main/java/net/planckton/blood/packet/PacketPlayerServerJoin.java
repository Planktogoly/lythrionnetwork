package net.planckton.blood.packet;

import java.util.UUID;

public class PacketPlayerServerJoin extends PlayerPacket {
	public PacketPlayerServerJoin() {
		super(PacketType.PLAYER_SERVER_JOIN);
	}
	
	public PacketPlayerServerJoin(UUID uuid) {
		super(PacketType.PLAYER_SERVER_JOIN, uuid);
	}
}
