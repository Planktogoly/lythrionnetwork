package net.planckton.blood.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketInfoUpdate extends Packet {
	private String server;
	private String key;
	private String info;
	
	public PacketInfoUpdate() {
		super(PacketType.INFO_UPDATE);
	}
	
	public PacketInfoUpdate(String server, String key, String info) {
		super(PacketType.INFO_UPDATE);
		
		this.server = server;
		this.key = key;
		this.info = info;
	}

	@Override
	void writeBytes(ByteArrayDataOutput out) {
		out.writeUTF(server);
		out.writeUTF(key);
		out.writeUTF(info);
	}

	@Override
	void readBytes(ByteArrayDataInput in) {
		server = in.readUTF();
		key = in.readUTF();
		info = in.readUTF();
	}
	
	public String getServer() {
		return server;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getInfo() {
		return info;
	}
}
