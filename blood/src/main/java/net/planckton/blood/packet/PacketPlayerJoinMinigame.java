package net.planckton.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketPlayerJoinMinigame extends PlayerPacket {

	@Getter private String server;
	@Getter private boolean force;
	
	public PacketPlayerJoinMinigame() {
		super(PacketType.PLAYER_MINIGAME_CONNECT);
	}
	
	public PacketPlayerJoinMinigame(UUID uuid) {
		super(PacketType.PLAYER_MINIGAME_CONNECT, uuid);
	}
	
	public PacketPlayerJoinMinigame(UUID uuid, String server, boolean force) {
		super(PacketType.PLAYER_MINIGAME_CONNECT, uuid);
		
		this.server = server;
		this.force = force;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(server);
		out.writeBoolean(force);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		server = in.readUTF();
		force = in.readBoolean();
	}

}
