package net.planckton.blood.packet;

import java.util.UUID;

public class PacketPlayerBungeeJoin extends PlayerPacket {
	public PacketPlayerBungeeJoin() {
		super(PacketType.PLAYER_BUNGEE_JOIN);
	}
	
	public PacketPlayerBungeeJoin(UUID uuid) {
		super(PacketType.PLAYER_BUNGEE_JOIN, uuid);
	}
}
