package net.planckton.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import lombok.Getter;

public class PacketPlayerAdvancedMessage extends PlayerPacket {
	
	@Getter private String advancedMessage;
	
	public PacketPlayerAdvancedMessage() {
		super(PacketType.PLAYER_ADVANCEDMESSAGE);
	}
	
	public PacketPlayerAdvancedMessage(UUID uuid, String message) {
		super(PacketType.PLAYER_ADVANCEDMESSAGE, uuid);
		
		this.advancedMessage = message;
	} 
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(advancedMessage);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) { 
		super.readBytes(in);
		
		advancedMessage = in.readUTF();
	}
}
