package net.planckton.blood.packet;

import java.util.UUID;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

public class PacketPlayerMessage extends PlayerPacket {
	private String message;
	
	public PacketPlayerMessage() {
		super(PacketType.PLAYER_MESSAGE);
	}
	
	public PacketPlayerMessage(UUID uuid, String message) {
		super(PacketType.PLAYER_MESSAGE, uuid);
		
		this.message = message;
	}
	
	@Override
	void writeBytes(ByteArrayDataOutput out) {
		super.writeBytes(out);
		
		out.writeUTF(message);
	}
	
	@Override
	void readBytes(ByteArrayDataInput in) {
		super.readBytes(in);
		
		message = in.readUTF();
	}
	
	public String getMessage() {
		return message;
	}
}
