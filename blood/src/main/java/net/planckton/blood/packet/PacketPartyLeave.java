package net.planckton.blood.packet;

import java.util.UUID;

public class PacketPartyLeave extends PlayerPacket {

	public PacketPartyLeave() {
		super(PacketType.PARTY_LEAVE);
	}
	
	public PacketPartyLeave(UUID uuid) {
		super(PacketType.PARTY_LEAVE, uuid);		
	}

}
