package net.planckton.blood;

public enum ServerStatus {
	UNKOWN, ONLINE, RESTARTING, OFFLINE, PENDING_RESTART, MAINTENANCE;
}
