package net.planckton.blood.util;


/**
 * @author Tim Biesenbeek
 */
@FunctionalInterface
public interface CatchConsumer<T, E extends Throwable> {

	void accept(T t) throws E;

	default CatchConsumer<T, E> andThen(CatchConsumer<? super T, E> after) {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}
}

