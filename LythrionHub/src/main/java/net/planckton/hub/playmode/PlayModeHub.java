package net.planckton.hub.playmode;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import net.md_5.bungee.api.ChatColor;
import net.planckton.hub.windows.GameSelectorWindow;
import net.planckton.hub.windows.LobbyWindow;
import net.planckton.hub.windows.PlayerProfileWindow;
import net.planckton.item.CustomItemManager;
import net.planckton.lib.event.LythrionPlayerQuitEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;
import net.planckton.lib.playmode.PlayMode;

public class PlayModeHub extends PlayMode implements Listener {
	
    ArrayList<LythrionPlayer> cooldown = new ArrayList<>();
    
	private final ArrayList<String> list = new ArrayList<>(); {
		list.add(ChatColor.GRAY + "Click to see your player profile");
	}
	
	@Override
	public void onSelect(LythrionPlayer player) {
		player.clearInventory();
		player.setGameMode(GameMode.ADVENTURE);
		player.setFoodLevel(20);
		player.setHealth(20.0D);
		player.setExp(0.0F);
		player.setLevel(0);
		player.setFireTicks(0);
		
		PlayerInventory inv = player.getInventory();
		
		inv.setHelmet(null);
		inv.setChestplate(null);
		inv.setLeggings(null);
		inv.setBoots(null);
		
		inv.setItem(0, CustomItemManager.NAVIGATOR);
		inv.setItem(1, getPlayerHead(player));
		inv.setItem(4, CustomItemManager.COSMETICS_CHEST);
		inv.setItem(7, CustomItemManager.PLAYER_SETTINGS);
		inv.setItem(8, CustomItemManager.LOBBIES);	
		
		player.updateInventory();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLythrionPlayerQuit(LythrionPlayerQuitEvent event) {
		cooldown.remove(event.getLythrionPlayer());
	}
	
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
        if (!player.isInPlayMode(this)) return;
                 
        if (player.getGameMode() == GameMode.CREATIVE) return;        
           
        if (!cooldown.contains(player)) {
        	player.getBukkitPlayer().setAllowFlight(true);
        } else {
        	player.getBukkitPlayer().setAllowFlight(false);
        }
           
        if (player.getBukkitPlayer().isOnGround()) {
        	cooldown.remove(player);
       }
    }
    
    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
        if (!player.isInPlayMode(this)) return;   	
           
        if (!cooldown.contains(player)) {
        	event.setCancelled(true);
            cooldown.add(player);
            player.playSound(Sound.BAT_TAKEOFF);
            player.setVelocity(player.getLocation().getDirection().multiply(1.6D).setY(0.9D));       
            player.getBukkitPlayer().setAllowFlight(false);
        }
    }	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		if (!player.isInPlayMode(this)) return;
		event.setCancelled(true); 
		
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		ItemStack stack = player.getItemInHand();
		if(stack == null) return;
		if(stack.getType() == Material.AIR) return;
		ItemMeta meta = stack.getItemMeta();
		if(meta.getDisplayName() == null) return;
		
		if (stack.isSimilar(CustomItemManager.NAVIGATOR)) {
			player.playSound(Sound.NOTE_BASS_GUITAR);
			new GameSelectorWindow(player).open(player);
			return;
		} else if (stack.isSimilar(CustomItemManager.LOBBIES)) {
			new LobbyWindow().open(player);
			return;
		}  else if (stack.isSimilar(CustomItemManager.COSMETICS_CHEST)) {
			return;
		} else if (stack.getItemMeta().getDisplayName().contains("Profile")) {
			new PlayerProfileWindow(player).open(player);
			player.playSound(Sound.SUCCESSFUL_HIT);
			return;
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInventoryClick(InventoryDragEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		if (!player.isInPlayMode(this)) return;
		
		event.setCancelled(true);
		player.updateInventory();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInventoryClick(InventoryClickEvent event) {
		LythrionPlayer player = PlayerManager.getLythrionPlayer((Player) event.getWhoClicked());
		if (!player.isInPlayMode(this)) return;
		
		event.setCancelled(true);
		player.updateInventory();
	}
	
	private ItemStack getPlayerHead(LythrionPlayer player) {
		ItemStack stats = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta statmeta = (SkullMeta) stats.getItemMeta();
		statmeta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Player Profile");
		statmeta.setLore(list);
		statmeta.setOwner(player.getName());
		stats.setItemMeta(statmeta);
		
		return stats;
	}
}
