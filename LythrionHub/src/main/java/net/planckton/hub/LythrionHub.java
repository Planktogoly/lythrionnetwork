package net.planckton.hub;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.planckton.hub.crate.CrateManager;
import net.planckton.hub.listener.WorldListener;
import net.planckton.hub.playmode.PlayModeHub;
import net.planckton.lib.event.DefaultPlayModeRequestEvent;
import net.planckton.lib.event.ManagersEnabledEvent;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.playmode.PlayModeManager;
import net.planckton.lib.sidebar.AdvancedPersonalScore;
import net.planckton.lib.sidebar.AdvancedScore;
import net.planckton.lib.sidebar.AdvancedSidebar;
import net.planckton.lib.utils.PlayerCallable;

public class LythrionHub extends JavaPlugin implements Listener {
	
	@Getter private static LythrionHub instance;
	
	@Getter private static AdvancedSidebar sidebar;
	
	@Getter private static AdvancedScore nameInfo;
	@Getter private static AdvancedPersonalScore scoreName;
	@Getter private static AdvancedScore crystalsInfo;
	@Getter private static AdvancedPersonalScore scoreCrystals;
	@Getter private static AdvancedScore rankInfo;
	@Getter private static AdvancedPersonalScore scoreRank;
	
	@Override
	public void onEnable() {
		instance = this;
		
		Manager.registerManager(new CrateManager());
		
		PlayModeManager.registerPlayMode(new PlayModeHub());
		
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new WorldListener(), this);
		
		for (World world : Bukkit.getWorlds()) {
			world.setTime(0);
		}
	}
	
	@Override
	public void onDisable() {
		instance = null;
	}
	
	@EventHandler
	public void onManagersEnabled(ManagersEnabledEvent event) {
		sidebar = new AdvancedSidebar("hub", ChatColor.RED + "Lythrion Network");
		
		sidebar.createWhiteSpace();
		
		nameInfo = sidebar.createScore("" + ChatColor.BLUE + ChatColor.BOLD + "Name");
		scoreName = sidebar.createPersonalScore("name", new PlayerCallable<String>() {
			
			public String call(LythrionPlayer player) {
				return ChatColor.WHITE + player.getName();
			}
		});
		
		sidebar.createWhiteSpace();
		
		crystalsInfo = sidebar.createScore("" + ChatColor.AQUA + ChatColor.BOLD + "Crystals");
		scoreCrystals = sidebar.createPersonalScore("crystals", new PlayerCallable<String>() {
			
			public String call(LythrionPlayer player) {
				return "" + ChatColor.WHITE + player.balance.get().getCurrencyGeneral();
			}
		});
		
		sidebar.createWhiteSpace();
		
		rankInfo = sidebar.createScore("" + ChatColor.YELLOW + ChatColor.BOLD + "Rank");
		scoreRank = sidebar.createPersonalScore("rank", new PlayerCallable<String>() {
			
			public String call(LythrionPlayer player) {
				return player.getRank().getColor() + WordUtils.capitalize(player.getRank().name().toLowerCase());
			}
		});
		
		sidebar.createWhiteSpace();
		sidebar.createScore(ChatColor.GRAY + "  play." + ChatColor.RED.toString() + ChatColor.BOLD + "lythrion" + ChatColor.GRAY + ".net");
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onDefaultPlayModeRequest(DefaultPlayModeRequestEvent event) {
		event.setPlayMode(PlayModeHub.class);
	}
}
