package net.planckton.hub.crate.animation;

import org.bukkit.World;

import lombok.Getter;
import lombok.Setter;
import net.planckton.lib.player.LythrionPlayer;

public abstract class CrateAnimation {
	
	@Getter @Setter protected World world;

	public abstract void startAnimation(LythrionPlayer player);
	public abstract void stopAnimation(LythrionPlayer player);

}
