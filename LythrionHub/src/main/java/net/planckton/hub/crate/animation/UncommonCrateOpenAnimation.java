package net.planckton.hub.crate.animation;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.comphenix.packetwrapper.WrapperPlayServerBlockAction;
import com.comphenix.protocol.wrappers.BlockPosition;

import lombok.Getter;
import net.planckton.hub.LythrionHub;
import net.planckton.lib.holo.Holo;
import net.planckton.lib.holo.HoloManager;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;

public class UncommonCrateOpenAnimation implements Runnable {
	
	@Getter private ArrayList<Holo> holos = new ArrayList<>();
	@Getter private ArrayList<Item> items = new ArrayList<>();
	
	private int i;
	private Location chestLocation;	
	private LythrionPlayer player;	
	private World world;
	
	public UncommonCrateOpenAnimation(Integer i, Location chestLocation, LythrionPlayer player) {
		this.i = i;
		this.chestLocation = chestLocation;
		this.player = player;
		this.world = this.player.getWorld();
	}
	
	@Override
	public void run() {				
		if (i == 1) {					
			UncommonCrateAnimation.setPacketTaskChest1(Bukkit.getScheduler().runTaskTimer(LythrionHub.getInstance(), new Runnable() {
				public void run() {
					WrapperPlayServerBlockAction action = new WrapperPlayServerBlockAction();
					action.setBlockType(Material.ENDER_CHEST);
					action.setLocation(new BlockPosition(chestLocation.getBlockX(), chestLocation.getBlockY(), chestLocation.getBlockZ())); 
					action.setByte1(1);
					action.setByte2(1);
					
					for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						action.sendPacket(onlinePlayer);
					}
				}
			}, 0, 3)); 
		} else if (i == 2) {
			UncommonCrateAnimation.setPacketTaskChest2(Bukkit.getScheduler().runTaskTimer(LythrionHub.getInstance(), new Runnable() {
				public void run() {
					WrapperPlayServerBlockAction action = new WrapperPlayServerBlockAction();
					action.setBlockType(Material.ENDER_CHEST);
					action.setLocation(new BlockPosition(chestLocation.getBlockX(), chestLocation.getBlockY(), chestLocation.getBlockZ())); 
					action.setByte1(1);
					action.setByte2(1);
					
					for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						action.sendPacket(onlinePlayer);
					}
				}
			}, 0, 3));
		} else if (i == 3) {
			UncommonCrateAnimation.setPacketTaskChest3(Bukkit.getScheduler().runTaskTimer(LythrionHub.getInstance(), new Runnable() {
				public void run() {
					WrapperPlayServerBlockAction action = new WrapperPlayServerBlockAction();
					action.setBlockType(Material.ENDER_CHEST);
					action.setLocation(new BlockPosition(chestLocation.getBlockX(), chestLocation.getBlockY(), chestLocation.getBlockZ())); 
					action.setByte1(1);
					action.setByte2(1);
					
					for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						action.sendPacket(onlinePlayer);
					}
				}
			}, 0, 3));
		}
		
		Bukkit.getScheduler().runTaskLater(LythrionHub.getInstance(), () -> {
			Holo holo = new Holo(chestLocation.clone().add(0, 1.5, 0), ChatColor.RED + "Something you won");
			((HoloManager) Manager.GetManager(HoloManager.class)).spawnHolo(holo);
			
			Item item = world.dropItem(chestLocation.clone().add(0, 0.5, 0), new ItemStack(Material.DIAMOND_BARDING));
			item.setMetadata("noPickUP", new FixedMetadataValue(LythrionHub.getInstance(), true));
			item.setVelocity(new Vector(0, 0, 0));
	        
			items.add(item);
			holos.add(holo);					
			world.spigot().playEffect(chestLocation.clone().add(0, 0.75, 0), Effect.PORTAL, 0, 0, 0.2f, 0.2f, 0.2f, 0.0f, 100, 25);
			player.playSound(Sound.CHEST_OPEN);
		}, 5);		
	}
	
	public void removeAnimation() {
		for (Holo holo : holos) {
			 ((HoloManager) Manager.GetManager(HoloManager.class)).despawnHolo(holo);
		}
			
		for (Item item : items) {
			item.remove();
		}
	}


}
