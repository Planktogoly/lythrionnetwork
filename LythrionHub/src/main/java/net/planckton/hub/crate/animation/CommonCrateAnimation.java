package net.planckton.hub.crate.animation;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.Chest;
import org.bukkit.scheduler.BukkitTask;

import lombok.Setter;
import net.planckton.hub.LythrionHub;
import net.planckton.hub.crate.CrateManager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.position.PositionManager;

public class CommonCrateAnimation extends CrateAnimation {

	private final Chest CHEST = new Chest(BlockFace.EAST);
	
	private BukkitTask task;	
	
	@Setter private static BukkitTask packetTaskChest1;
	@Setter private static BukkitTask packetTaskChest2;
	@Setter private static BukkitTask packetTaskChest3;
	
	private ArrayList<CommonCrateOpenAnimation> animations = new ArrayList<>();
	
	private Location locationChest = null; 
	
	@Override
	public void startAnimation(LythrionPlayer player) {
		this.world = player.getWorld();
		
		player.teleport(PositionManager.getPosition("crate.spawn")); 
		
		Bukkit.getScheduler().runTaskLater(LythrionHub.getInstance(), new Runnable() {
			public void run() {
				task = Bukkit.getScheduler().runTaskTimer(LythrionHub.getInstance(), new Runnable() {
					
					int i = 1; 
					
					public void run() {
						if (i == 4) {
							task.cancel(); 
							return;
						}
						
						if (i == 1) locationChest = PositionManager.getPosition("crate.chest." + 2).getPosition();
						if (i == 2) locationChest = PositionManager.getPosition("crate.chest." + 3).getPosition();
						if (i == 3) locationChest = PositionManager.getPosition("crate.chest." + 1).getPosition();			
						
						Block block = world.getBlockAt(locationChest);
						block.setType(Material.CHEST);
						BlockState blockState = block.getState();
						blockState.setData(CHEST);
						blockState.update();
						
						player.playSound(Sound.CHICKEN_EGG_POP);
						world.spigot().playEffect(locationChest.clone().add(0, 0.75, 0), Effect.SPELL, 0, 0, 0.3f, 0.5f, 0.3f, 0.0f, 150, 25);
						
						CommonCrateOpenAnimation animation = new CommonCrateOpenAnimation(i, locationChest, player);
						animations.add(animation);
						Bukkit.getScheduler().runTaskLater(LythrionHub.getInstance(), animation, 20 * (3 + i));
						i++;
					}
				}, 0, 20 * 1); 
				
				Bukkit.getScheduler().runTaskLater(LythrionHub.getInstance(), new Runnable() {
					public void run() {
						stopAnimation(player);
					}
				}, 20 * 16);
			}
		}, 20);
	}

	@Override
	public void stopAnimation(LythrionPlayer player) {		
		Location chestOne = PositionManager.getPosition("crate.chest.1").getPosition();
		Location chestTwo = PositionManager.getPosition("crate.chest.2").getPosition();
		Location chestThree = PositionManager.getPosition("crate.chest.3").getPosition();
		
		world.getBlockAt(chestOne).setType(Material.AIR);
		world.getBlockAt(chestTwo).setType(Material.AIR);
		world.getBlockAt(chestThree).setType(Material.AIR);
	
		world.spigot().playEffect(chestOne, Effect.EXPLOSION_HUGE, 0, 0, 0.2f, 0.2f, 0.2f, 0.0f, 2, 25);
		world.spigot().playEffect(chestTwo, Effect.EXPLOSION_HUGE, 0, 0, 0.2f, 0.2f, 0.2f, 0.0f, 2, 25);
		world.spigot().playEffect(chestThree, Effect.EXPLOSION_HUGE, 0, 0, 0.2f, 0.2f, 0.2f, 0.0f, 2, 25);
		
		packetTaskChest1.cancel();
		packetTaskChest2.cancel();
		packetTaskChest3.cancel();
		
		for (CommonCrateOpenAnimation animation : animations) {
			animation.removeAnimation();
		}
		
		CrateManager.setInUse(false);		
		player.playSound(Sound.EXPLODE);
	}

}
