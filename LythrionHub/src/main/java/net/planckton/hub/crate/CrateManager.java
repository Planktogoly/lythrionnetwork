package net.planckton.hub.crate;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import lombok.Getter;
import lombok.Setter;
import net.planckton.hub.windows.CrateShopWindow;
import net.planckton.lib.manager.Manager;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.PlayerManager;

public class CrateManager extends Manager implements Listener {

	@Getter @Setter private static boolean inUse = false;
	
	@Override
	public void onEnable() {
	}

	@Override
	public void onDisable() {

	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		Material blockType = event.getClickedBlock().getType();		
		if(blockType != Material.SIGN_POST && blockType != Material.WALL_SIGN) return;
	
		LythrionPlayer player = PlayerManager.getLythrionPlayer(event.getPlayer());
		new CrateShopWindow().open(player);
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerPickup(PlayerPickupItemEvent event) {        
        if (event.getItem().getMetadata("noPickUP").isEmpty()) return;
        
        event.setCancelled(true);
    }
	
}
