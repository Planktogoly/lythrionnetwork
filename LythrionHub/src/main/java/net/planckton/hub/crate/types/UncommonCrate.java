package net.planckton.hub.crate.types;

import net.planckton.hub.crate.CrateManager;
import net.planckton.hub.crate.animation.UncommonCrateAnimation;
import net.planckton.lib.player.LythrionPlayer;

public class UncommonCrate extends Crate {

	public UncommonCrate() {
		super(new UncommonCrateAnimation());

	}

	@Override
	public void openCrate(LythrionPlayer player) {
		CrateManager.setInUse(true);
		getCrateAnimation().startAnimation(player);
	}

}
