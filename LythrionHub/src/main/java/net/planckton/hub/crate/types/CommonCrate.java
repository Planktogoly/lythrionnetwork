package net.planckton.hub.crate.types;

import net.planckton.hub.crate.CrateManager;
import net.planckton.hub.crate.animation.CommonCrateAnimation;
import net.planckton.lib.player.LythrionPlayer;

public class CommonCrate extends Crate {

	public CommonCrate() {
		super(new CommonCrateAnimation());
	}

	@Override
	public void openCrate(LythrionPlayer player) {
		CrateManager.setInUse(true);
		getCrateAnimation().startAnimation(player);
	}

}
