package net.planckton.hub.crate.types;

import lombok.Getter;
import net.planckton.hub.crate.animation.CrateAnimation;
import net.planckton.lib.player.LythrionPlayer;

public abstract class Crate {

	@Getter private CrateAnimation crateAnimation;
	
	public Crate(CrateAnimation crateAnimation) {
		this.crateAnimation = crateAnimation;
	}
	
	
	public abstract void openCrate(LythrionPlayer player);
	
}
