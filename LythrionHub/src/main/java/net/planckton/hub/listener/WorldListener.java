package net.planckton.hub.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import mkremins.fanciful.FancyMessage;
import net.planckton.hub.LythrionHub;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.chat.ChatManager;
import net.planckton.lib.event.LythrionPlayerChangeRankEvent;
import net.planckton.lib.event.LythrionPlayerJoinEvent;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.player.data.Balance.PlayerBalanceChangeEvent;
import net.planckton.lib.position.PositionManager;

public class WorldListener implements Listener {
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLytrhionRankChange(LythrionPlayerChangeRankEvent event) {
		LythrionHub.getScoreRank().update(event.getLythrionPlayer());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerBalanceChange(PlayerBalanceChangeEvent event) {
		LythrionHub.getScoreCrystals().update(event.getPlayer());
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) return;		
		if (!(event.getCause() == DamageCause.VOID)) return;
		
		((Player) event.getEntity()).teleport(PositionManager.getPosition("hub.spawn").getPosition());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().teleport(PositionManager.getPosition("hub.spawn").getPosition());
		event.getPlayer().getInventory().setHeldItemSlot(0);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onLythrionPlayerJoin(LythrionPlayerJoinEvent event) {
		LythrionPlayer player = event.getLythrionPlayer();

		LythrionHub.getSidebar().display(player);
		player.chat.setChatChannel(ChatManager.CHANNEL_GLOBAL);
		player.chat.listenToChat(ChatManager.CHANNEL_GLOBAL);
		
		FancyMessage header = new 
				FancyMessage("" + ChatColor.RED + ChatColor.BOLD + " Lythrion Network ")
				.then("\n" + LythrionLib.getServerName()  + "\n").color(ChatColor.GREEN);
		
		FancyMessage footer = new FancyMessage("" + ChatColor.GREEN + "\n  store.lythrion.net  ");
		
		player.setHeaderAndFooter(header, footer);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onLeavesDecay(LeavesDecayEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockIgnite(BlockIgniteEvent event) {
		if(event.getCause() == IgniteCause.FLINT_AND_STEEL) return;
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onWorldUnload(WorldUnloadEvent event) {
		PositionManager.unloadPositionsFor(event.getWorld());
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockSpread(BlockSpreadEvent event) {
		if (event.getSource().getType() == Material.FIRE) event.setCancelled(true);
		else if (event.getSource().getType() == Material.VINE) event.setCancelled(true); 
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBurn(BlockBurnEvent event) {
		event.setCancelled(true);
	}
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityExplode(EntityExplodeEvent event) {
		event.blockList().clear();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockFade(BlockFadeEvent event) {
		if(event.getBlock().getType() != Material.GRASS) event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onWeatherChange(WeatherChangeEvent event) {
		if (event.toWeatherState()) event.setCancelled(true); 
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockFormEvent(BlockFormEvent event) {
		if(event.getNewState().getType() == Material.ICE) event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPhysics(BlockPhysicsEvent event) {
		if(event.getBlock().getType() == Material.LADDER) event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPhysics(LeavesDecayEvent event) {
		event.setCancelled(true);
	}
	
}
