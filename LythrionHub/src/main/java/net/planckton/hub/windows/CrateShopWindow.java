package net.planckton.hub.windows;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class CrateShopWindow extends Window {
	
	private final FancyItem COMMON = new FancyItem(Material.CHEST)
			.name(ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "Common " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "Receive the most basic cosmetics")
			.lore(ChatColor.GRAY + "on our network! Maybe if you are lucky")
			.lore(ChatColor.GRAY + "you will receive a rare item!", " ")
			.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + 2500 + ChatColor.AQUA + " Crystals", 
					ChatColor.GRAY + "or purchase them at" + ChatColor.RED + " store.lythrion.net", " ",
					ChatColor.BLUE + "Click to buy a Common crate");
	
	private final FancyItem UNCOMMON = new FancyItem(Material.ENDER_CHEST)
			.name(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Uncommon " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "The gods have created a better chest")
			.lore(ChatColor.GRAY + "than there was before. A chest that was generated")
			.lore(ChatColor.GRAY + "by co-operating with the other gods!", " ")			
			.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + 3000 + ChatColor.AQUA + " Crystals",
					ChatColor.GRAY + "or purchase them at" + ChatColor.RED + " store.lythrion.net", " ",
					ChatColor.BLUE + "Click to buy an Uncommon crate");
	
	private final FancyItem LEGENDARY = new FancyItem(Material.ENDER_PORTAL_FRAME)
			.name(ChatColor.GOLD.toString() + ChatColor.BOLD + "Legendary " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "Zeus's treasure is inside of this chest.")
			.lore(ChatColor.GRAY + "Try to find it, once you find it you")
			.lore(ChatColor.GRAY + "will receive an amazing reward.", " ")		
			.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + 4000 + ChatColor.AQUA + " Crystals",
					ChatColor.GRAY + "or purchase them at" + ChatColor.RED + " store.lythrion.net", " ",
					ChatColor.BLUE + "Click to buy a Legendary crate");
	
	private final FancyItem TOWNY = new FancyItem(Material.WORKBENCH)
			.name(ChatColor.AQUA.toString() + ChatColor.BOLD + "Towny " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "This chest is to get special and")
			.lore(ChatColor.GRAY + "rare items in our game mode Towny.", " ")		
			.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + 6000 + ChatColor.AQUA + " Crystals", 
					ChatColor.GRAY + "or purchase them at" + ChatColor.RED + " store.lythrion.net", " ",
					ChatColor.BLUE + "Click to buy a Towny crate");
	
	private final FancyItem CRATES = new FancyItem(Material.MAGMA_CREAM)
			.name(ChatColor.BLUE.toString() + ChatColor.BOLD + "Your crates")
			.lore(ChatColor.GRAY + "Click to see all your crates!");

	@Override
	protected void build() {
		addButton(2, new WindowButton(COMMON) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				if (player.balance.get().getCurrencyGeneral() < 2500) {
					player.sendMessage(ChatColor.RED + "You cannot buy a Common crate!");
					player.playSound(Sound.VILLAGER_NO);
					return;
				}
				
				new ConfirmBuyWindow(() -> {
					player.balance.get().subtractCurrencyGeneral(2500);
					player.crate.addCommonCrate();
					player.sendTitle(ChatColor.GREEN + "You bought a", ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "Common " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate");
					player.playSound(Sound.VILLAGER_YES);
					player.closeInventory();
				}, () -> open(player), new FancyItem(COMMON).clearLores(), 2500).open(player);
				return;
			}
		});
		
		addButton(3, new WindowButton(UNCOMMON) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				if (player.balance.get().getCurrencyGeneral() < 3000) {
					player.sendMessage(ChatColor.RED + "You cannot buy an Uncommon crate!");
					player.playSound(Sound.VILLAGER_NO);
					return;
				}
				
				new ConfirmBuyWindow(() -> {
					player.balance.get().subtractCurrencyGeneral(3000);
					player.crate.addUnCommonCrate();
					player.sendTitle(ChatColor.GREEN + "You bought an", ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Uncommon " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate");
					player.playSound(Sound.VILLAGER_YES);
					player.closeInventory();
				}, () -> open(player), new FancyItem(UNCOMMON).clearLores(), 3000).open(player);
				return;
			}
		});
		
		addButton(5, new WindowButton(LEGENDARY) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				if (player.balance.get().getCurrencyGeneral() < 4000) {
					player.sendMessage(ChatColor.RED + "You cannot buy a Legendary crate!");
					player.playSound(Sound.VILLAGER_NO);
					return;
				}
				
				new ConfirmBuyWindow(() -> {
					player.balance.get().subtractCurrencyGeneral(4000);
					player.crate.addLegendaryCrate();
					player.sendTitle(ChatColor.GREEN + "You bought a", ChatColor.GOLD.toString() + ChatColor.BOLD + "Legendary " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate");
     				player.playSound(Sound.VILLAGER_YES);
					player.closeInventory();
				}, () -> open(player), new FancyItem(LEGENDARY).clearLores(), 4000).open(player);
				return;
			}
		});
		
		addButton(6, new WindowButton(TOWNY) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				if (player.balance.get().getCurrencyGeneral() < 6000) {
					player.sendMessage(ChatColor.RED + "You cannot buy a Towny crate!");
					player.playSound(Sound.VILLAGER_NO);
					return;
				}
				
				new ConfirmBuyWindow(() -> {
					player.balance.get().subtractCurrencyGeneral(6000);
					player.crate.addTownyCrate();
					player.sendTitle(ChatColor.GREEN + "You bought a", ChatColor.AQUA.toString() + ChatColor.BOLD + "Towny " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate");
					player.playSound(Sound.VILLAGER_YES);
					player.closeInventory();
				}, () -> open(player), new FancyItem(TOWNY).clearLores(), 6000).open(player);
				return;
			}
		});
		
		addButton(13, new WindowButton(CRATES) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.closeInventory();
				new CrateWindow(player).open(player);				
			}
		});
	}

	@Override
	protected String getName() {
		return "Dr. Crate's shop";
	}

	@Override
	protected int getRows() {
		return 2;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
