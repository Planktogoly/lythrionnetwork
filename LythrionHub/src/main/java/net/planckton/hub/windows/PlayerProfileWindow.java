package net.planckton.hub.windows;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.CustomItemManager;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;
import net.planckton.lib.windows.PlayerSettingWindow;

public class PlayerProfileWindow extends Window {

	private LythrionPlayer player;
	
	public PlayerProfileWindow(LythrionPlayer player) {
		this.player = player;
	}
	
	@Override
	protected void build() {
		
		addButton(13, new WindowButton(new FancyItem(Material.EXP_BOTTLE)
				.name(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Boosters")
				.lore(ChatColor.GRAY + "Booster: " + ChatColor.WHITE + "CreeperWar x2 1h 11m")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.closeInventory();
				
				//new PlayerSettingWindow().open(player);				
			}
		});
		
		setItem(29, new FancyItem(Material.PAPER)
				.name(ChatColor.RED.toString() + ChatColor.BOLD + "Information")
				.lore(ChatColor.GRAY + "First join: " + ChatColor.WHITE + player.general.getFullTime())
				.lore(ChatColor.GRAY + "Total hours: " + ChatColor.WHITE + "11h 34m")
				.lore(" ")
				.lore(ChatColor.GRAY + "Level: " + ChatColor.WHITE + "11")
				.lore(ChatColor.GRAY + "Experience till next level: " + ChatColor.WHITE + "1457")
				.lore(ChatColor.GRAY + "Total experience: " + ChatColor.WHITE + "1457"));
		
		addButton(31, new WindowButton(new FancyItem(CustomItemManager.PLAYER_SETTINGS)) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.closeInventory();
				
				new PlayerSettingWindow().open(player);				
			}
		});
		
		addButton(33, new WindowButton(new FancyItem(Material.BOOK)
				.name(ChatColor.GREEN.toString() + ChatColor.BOLD + "Stats")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.closeInventory();
				
				//new PlayerSettingWindow().open(player);				
			}
		});
		
		setItem(49, new FancyItem(Material.PRISMARINE_SHARD)
				.name(ChatColor.AQUA + "Crystals: " + ChatColor.WHITE + player.balance.get().getCurrencyGeneral())
				.lore(ChatColor.GRAY + "Total Crystals: " + ChatColor.WHITE + player.balance.get().getCurrencyGeneralTotal()));
	}

	@Override
	protected String getName() {
		return "Player profile";
	}

	@Override
	protected int getRows() {
		return 6;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
