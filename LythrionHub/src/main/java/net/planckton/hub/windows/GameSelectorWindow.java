package net.planckton.hub.windows;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.position.PositionManager;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class GameSelectorWindow extends Window {
	
	public GameSelectorWindow(LythrionPlayer player) {	}
	
	@Override
	protected void build() {		
		addButton(13, new WindowButton(new FancyItem(Material.DIAMOND_SWORD)
				.name(ChatColor.RED + "Coming Soon")
				.lore(ChatColor.GRAY + "Click to play a game of xxxxx")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				//player.teleport(PositionManager.getPosition("hub.parkour"));				
			}
		});
		
		addButton(21, new WindowButton(new FancyItem(Material.IRON_BARDING)
				.name(ChatColor.RED + "Castle Panic")
				.lore(ChatColor.GRAY + "Click to play a game of Castle Panic")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.connectToMinigame("castlepanic");				
			}
		});
		
		addButton(22, new WindowButton(new FancyItem(Material.STAINED_GLASS, 1, (byte) 1)
				.name(ChatColor.BLUE + "Rising")
				.lore(ChatColor.GRAY + "Click to play a game of Rising")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.connectToMinigame("rising");					
			}
		});
		
		addButton(23, new WindowButton(new FancyItem(Material.SKULL_ITEM, 1, (byte) 4)
				.name(ChatColor.RED.toString() + ChatColor.BOLD + "Game: " + ChatColor.GREEN + "CreeperWars")
				.lore(" ")
				.lore(ChatColor.GRAY + "Each team has 4 players and one Creeper.")
				.lore(ChatColor.GRAY + "Defend your Creeper and get good stuff in")
				.lore(ChatColor.GRAY + "the mines to upgrade yourself and your team")
				.lore(ChatColor.GRAY + "to kill the Creepers and your enemies!")
				.lore(" ")
				.lore(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Booster: " + ChatColor.WHITE + "Planckton's x2 1h 11m")
				.lore(" ")
				.lore(ChatColor.BLUE + "Click to play this game!")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.connectToMinigame("creeperwar");		
			}
		});
	
		addButton(39, new WindowButton(new FancyItem(Material.WOOD_STEP)
				.name(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Parkour")
				.lore(ChatColor.GRAY + "Teleport to the parkour")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.teleport(PositionManager.getPosition("hub.parkour"));	
				player.playSound(Sound.ENDERMAN_TELEPORT);
			}
		});
		addButton(40, new WindowButton(new FancyItem(Material.CHEST)
				.name(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Jeff")
				.lore(ChatColor.GRAY + "Click to teleport to Jeff!")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.teleport(PositionManager.getPosition("hub.crates"));	
				player.playSound(Sound.ENDERMAN_TELEPORT);
			}
		});		
		addButton(41, new WindowButton(new FancyItem(Material.SLIME_BALL)
				.name(ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Lobby spawn")
				.lore(ChatColor.GRAY + "Teleport to the lobby spawn")) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				player.teleport(PositionManager.getPosition("hub.spawn"));	
				player.playSound(Sound.ENDERMAN_TELEPORT);
			}
		});
	}

	@Override
	protected String getName() {
		return "Game Menu";
	}

	@Override
	protected int getRows() {
		return 5;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}
}
