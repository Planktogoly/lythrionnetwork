package net.planckton.hub.windows;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.md_5.bungee.api.ChatColor;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class ConfirmBuyWindow extends Window {

	private Runnable yesRunnable;
	private Runnable noRunnable;
	private FancyItem icon;
	private int price;
	
	
	public ConfirmBuyWindow(Runnable yesRunnable, Runnable noRunnable, FancyItem icon, int price) {
		this.yesRunnable = yesRunnable;
		this.noRunnable = noRunnable;
		this.icon = icon;
		this.price = price;
	}

	private final FancyItem YESBUTTON = new FancyItem(Material.STAINED_CLAY, 1, (byte) 5)
			.name(ChatColor.GREEN.toString() + ChatColor.BOLD + "BUY");
	private final FancyItem NOBUTTON = new FancyItem(Material.STAINED_CLAY, 1, (byte) 14)
			.name(ChatColor.RED.toString() + ChatColor.BOLD + "CANCEL");
	
	
	@Override
	protected void build() {
		addButton(11, new WindowButton(NOBUTTON) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				noRunnable.run();
			}
		});
		
		setItem(13, icon.lore(ChatColor.GRAY + "Cost: " + ChatColor.WHITE + price + ChatColor.AQUA + " Crystals"));
		
		addButton(15, new WindowButton(YESBUTTON) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {
				yesRunnable.run();				
			}
		});
		
	}

	@Override
	protected String getName() {
		return "Are you sure?";
	}

	@Override
	protected int getRows() {
		return 3;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
