package net.planckton.hub.windows;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.hub.crate.CrateManager;
import net.planckton.hub.crate.types.CommonCrate;
import net.planckton.hub.crate.types.UncommonCrate;
import net.planckton.item.FancyItem;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class CrateWindow extends Window {

	private LythrionPlayer player;	
	
	public CrateWindow(LythrionPlayer player) {
		this.player = player;
	}
	
	
	private FancyItem information = new FancyItem(Material.SIGN)
			.name(ChatColor.GREEN.toString() + ChatColor.BOLD + "Information");
	
	private final FancyItem BACKBUTTON = new FancyItem(Material.BARRIER)
			.name(ChatColor.RED.toString() + ChatColor.BOLD + "Go back");
	
	private final FancyItem GIFT = new FancyItem(Material.CAKE)
			.name(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Gifts");
	
	private final FancyItem COMMON = new FancyItem(Material.CHEST)
			.name(ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "Common " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "Receive the most basic cosmetics")
			.lore(ChatColor.GRAY + "on our network! Maybe if you are lucky")
			.lore(ChatColor.GRAY + "you will receive a rare item!", " ")
			.lore(ChatColor.BLUE + "Click to open this Common Crate!");
	
	private final FancyItem UNCOMMON = new FancyItem(Material.ENDER_CHEST)
			.name(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Uncommon " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "The gods have created a better chest")
			.lore(ChatColor.GRAY + "than there was before. A chest that was generated")
			.lore(ChatColor.GRAY + "by co-operating with the other gods!", " ")			
			.lore(ChatColor.BLUE + "Click to open this Uncommon Crate!");
	
	private final FancyItem LEGENDARY = new FancyItem(Material.ENDER_PORTAL_FRAME)
			.name(ChatColor.GOLD.toString() + ChatColor.BOLD + "Legendary " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "Zeus's treasure is inside of this chest.")
			.lore(ChatColor.GRAY + "Try to find it, once you find it you")
			.lore(ChatColor.GRAY + "will receive an amazing reward.", " ")	
			.lore(ChatColor.BLUE + "Click to open this Legendary Crate!");
	
	private final FancyItem TOWNY = new FancyItem(Material.WORKBENCH)
			.name(ChatColor.AQUA.toString() + ChatColor.BOLD + "Towny " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Crate")
			.lore(" ", ChatColor.GRAY + "This chest is to get special and")
			.lore(ChatColor.GRAY + "rare items in our game mode Towny.", " ")	
			.lore(ChatColor.BLUE + "Click to open this Towny Crate!");
	
	@Override
	protected void build() {	
		for (int i = 45; i <= 53; i++) {
			if (i == 47) i = 52;			
			setItem(i, new FancyItem(Material.STAINED_GLASS_PANE, 1, (byte) 14).name(" "));
		}
		
		addButton(49, new WindowButton(BACKBUTTON) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {	
				player.closeInventory();
				new CrateShopWindow().open(player);
			}
		});
		
		addButton(48, new WindowButton(GIFT) {
			
			@Override
			public void onClick(LythrionPlayer player, InventoryClickEvent event) {				
			}
		});
		
		setItem(50, information.lore(ChatColor.GRAY + "Towny Crates: " + ChatColor.WHITE + player.crate.getTownycrate(),
				ChatColor.GRAY + "Legendary Crates: " + ChatColor.WHITE + player.crate.getLegendaryCrate(),
				ChatColor.GRAY + "Uncommon Crates: " + ChatColor.WHITE + player.crate.getUncommonCrate(),
				ChatColor.GRAY + "Common Crates: " + ChatColor.WHITE + player.crate.getCommonCrate()));
		
		for (int i = 0; i < player.crate.getTownycrate(); i++) {
			if (i > 44) return;
		
			addButton(i, new WindowButton(TOWNY) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {					
				}
			});
		}
		
		int y = player.crate.getTownycrate();
		
		for (int i = y; i < player.crate.getLegendaryCrate() + y; i++) {
			if (i > 44) return;
			
			addButton(i, new WindowButton(LEGENDARY) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {					
				}
			});
		}
		
		y += player.crate.getLegendaryCrate();
		
		for (int i = y; i < player.crate.getUncommonCrate() + y; i++) {
			if (i > 44) return;
			
			addButton(i, new WindowButton(UNCOMMON) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {			
					if (CrateManager.isInUse()) return;
					
					player.crate.useUncommonCrate();
					new UncommonCrate().openCrate(player);
				}
			});
		}
		
		y += player.crate.getUncommonCrate();

		for (int i = y; i < player.crate.getCommonCrate() + y; i++) {
			if (i > 44) return;
			
			addButton(i, new WindowButton(COMMON) {
				
				@Override
				public void onClick(LythrionPlayer player, InventoryClickEvent event) {
					if (CrateManager.isInUse()) return;
					
					player.crate.useCommonCrate();
					new CommonCrate().openCrate(player);
				}
			});
		}
	}

	@Override
	protected String getName() {
		return "Your Crates";
	}

	@Override
	protected int getRows() {
		return 6;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
