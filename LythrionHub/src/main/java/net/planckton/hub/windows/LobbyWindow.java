package net.planckton.hub.windows;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.planckton.item.FancyItem;
import net.planckton.lib.LythrionLib;
import net.planckton.lib.player.LythrionPlayer;
import net.planckton.lib.window.Window;
import net.planckton.lib.window.WindowButton;

public class LobbyWindow extends Window {

	@Override
	protected void build() {
		int y = 1;
		for (int i = 10; i < 17; i++) {
			String lobbyName = "Lobby-" + y;
			
			if (lobbyName.equalsIgnoreCase(LythrionLib.getServerName())) {
				addButton(i, new WindowButton(new FancyItem(Material.EMPTY_MAP)
						.name(ChatColor.DARK_AQUA + lobbyName)) {
					
					@Override
					public void onClick(LythrionPlayer player, InventoryClickEvent event) {
											
					}
				});
			} else {
				addButton(i, new WindowButton(new FancyItem(Material.PAPER)
						.name(ChatColor.AQUA + lobbyName)) {
					
					@Override
					public void onClick(LythrionPlayer player, InventoryClickEvent event) {
											
					}
				});
			}
			y++;
		}
	}

	@Override
	protected String getName() {
		return "Lobby menu";
	}

	@Override
	protected int getRows() {
		return 3;
	}

	@Override
	protected boolean isNoButtons() {
		return false;
	}

}
